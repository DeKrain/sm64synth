use crate::{
	structs::*,
	ultra::*,
	data::*,
	load::*,
	heap::*,
	synthesis::*,
	external::*,
};

use std::ptr::NonNull;

static mut sAudioCmdCursor: u8 = 0;
static mut sAudioCmdLastRefreshPos: u8 = 0;

/// Called from threads:
/// - thread4_sound
#[no_mangle] 
pub const extern "C" fn create_next_audio_frame_task() -> *mut SPTask {
	std::ptr::null_mut()
}

#[no_mangle]
#[export_name = "create_next_audio_buffer"]
pub unsafe extern "C" fn create_next_audio_buffer_c(samples: NonNull<[i16; 2]>, num_samples: usize) {
	create_next_audio_buffer(NonNull::slice_from_raw_parts(samples, num_samples))
}

pub unsafe fn create_next_audio_buffer(samples: NonNull<[[i16; 2]]>) {
	gAudioFrameCount += 1;
	gCurrAudioFrameDmaCount = 0;
	decrease_sample_dma_ttls();

	let mut msg = std::mem::MaybeUninit::<OSMesg>::uninit();
	loop {
		if osRecvMesg(OSMesgQueuePreset.assume_init_ref(), msg.as_mut_ptr(), Blocking::NoBlock).is_ok() {
			println!("Received reset preset");
			gAudioResetPresetIdToLoad = msg.assume_init_read() as usize as u8;
			gAudioResetStatus.set(AudioResetStatus::StepStopPlayers);
		}

		if gAudioResetStatus.get() != AudioResetStatus::Complete {
			//audio_reset_session();
			//gAudioResetStatus.set(AudioResetStatus::Complete);
			if let ResetState::Ready = audio_shut_down_and_reset_step() {
				if gAudioResetStatus.get() == AudioResetStatus::Complete {
					let _ = osSendMesg(OSMesgQueuePresetLoaded.assume_init_ref(), gAudioResetPresetIdToLoad as usize as OSMesg, Blocking::NoBlock);
				}
				continue;
			}
		}

		break;
	}

	if osRecvMesg(OSMesgQueueCmdSync.assume_init_ref(), msg.as_mut_ptr(), Blocking::NoBlock).is_ok() {
		eu_execute_audio_cmds(msg.assume_init_read() as usize as u16);
	}

	let mut writtenCmds = 0;
	synthesis_execute(NonNull::new(gAudioCmdBuffers[0]).unwrap(), &mut writtenCmds, samples.as_ptr().cast(), samples.len());
	debug_assert!(writtenCmds <= gMaxAudioCmds as usize);
	crate::ultra64_abi::mixer::process_audio_cmd(std::slice::from_raw_parts(gAudioCmdBuffers[0], writtenCmds));
	gAudioRandom = gAudioRandom.wrapping_add(gAudioFrameCount).wrapping_mul(gAudioFrameCount);
	gAudioRandom = gAudioRandom.wrapping_add((writtenCmds / 8) as u32);
}

pub const EU_CMD_PRELOAD_SEQ: u8 = 0x81; // Unused
pub const EU_CMD_LOAD_SEQ: u8 = 0x82;
pub const EU_CMD_FADE_OUT_PLAYER: u8 = 0x83;
pub const EU_CMD_SET_SOUND_MODE: u8 = 0xf0; // Unused
pub const EU_CMD_MUTE_PLAYERS: u8 = 0xf1;
pub const EU_CMD_UNMUTE_PLAYERS: u8 = 0xf2;

pub const PLAYER_CMD_SET_FADE_VOLUME_SCALE: u8 = 0x41; // Unused
pub const PLAYER_CMD_SET_VARIATION: u8 = 0x46;
pub const PLAYER_CMD_SET_TEMPO: u8 = 0x47; // Unused
pub const PLAYER_CMD_SET_TRANSPOSITION: u8 = 0x48; // Unused

pub const CHANNEL_CMD_SET_VOLUME_SCALE: u8 = 0x01;
pub const CHANNEL_CMD_SET_VOLUME: u8 = 0x02;
pub const CHANNEL_CMD_SET_PAN: u8 = 0x03;
pub const CHANNEL_CMD_SET_FREQ_SCALE: u8 = 0x04;
pub const CHANNEL_CMD_SET_REVERB_VOLUME: u8 = 0x05;
pub const CHANNEL_CMD_SET_PORT: u8 = 0x06; // Unused
// ---
pub const CHANNEL_CMD_STOP_SOMETHING: u8 = 0x08; // Unused

unsafe fn eu_global_audio_cmd(cmd: &EuAudioCmd) {
	match cmd.u.s.op {
		EU_CMD_PRELOAD_SEQ => preload_sequence(cmd.u.s.arg2, PreloadMask::Banks | PreloadMask::Sequence),
		EU_CMD_LOAD_SEQ | 0x88 => {
			let player = cmd.u.s.player as usize;
			let seqId = cmd.u.s.arg2;
			load_sequence(player, seqId, cmd.u.s.arg3 != 0);
			seq_player_fade_in(player, cmd.u2.as_u32 as u16);
		}
		EU_CMD_FADE_OUT_PLAYER => {
			let player = cmd.u.s.player as usize;
			if gSequencePlayers[player].flags.contains(SequencePlayerFlags::Enabled) {
				if cmd.u2.as_i32 == 0 {
					gSequencePlayers[player].disable();
				} else {
					seq_player_fade_to_zero_volume(player, cmd.u2.as_i32);
				}
			}
		}
		EU_CMD_SET_SOUND_MODE => gSoundMode = SoundMode::from_u8(cmd.u2.as_u8.v),
		EU_CMD_MUTE_PLAYERS => for player in &mut gSequencePlayers {
			player.flags |= SequencePlayerFlags::Muted | SequencePlayerFlags::RecalculateVolume;
		}
		EU_CMD_UNMUTE_PLAYERS => for player in &mut gSequencePlayers {
			player.flags -= SequencePlayerFlags::Muted;
			player.flags |= SequencePlayerFlags::RecalculateVolume;
		}
		_ => {
			// Probably goes here?
			eprintln!("Error: undefined port command: 0x{:02X}", cmd.u.s.op);
		}
	}
}

fn seq_player_fade_to_zero_volume(player: usize, mut fadeOutTime: i32) {
	if fadeOutTime == 0 {
		fadeOutTime = 1;
	}
	let player = unsafe { &mut gSequencePlayers[player] };
	player.fadeVelocity = -(player.fadeVolume / fadeOutTime as f32);
	player.state = SequencePlayerStateEU::FadeOut;
	player.fadeRemainingFrames = fadeOutTime as u16;
}

fn seq_player_fade_in(player: usize, fadeInTime: u16) {
	if fadeInTime == 0 {
		return;
	}

	let player = unsafe { &mut gSequencePlayers[player] };
	player.state = SequencePlayerStateEU::FadingIn;
	player.fadeTimerUnkEu = fadeInTime;
	player.fadeRemainingFrames = fadeInTime;
	player.fadeVolume = 0.0;
	player.fadeVelocity = 0.0;
}

unsafe fn port_eu_init_queues() {
	sAudioCmdCursor = 0;
	sAudioCmdLastRefreshPos = 0;
	macro_rules! queue {
		($q:expr) => ($q.as_ptr().cast_mut());
	}
	osCreateMesgQueue(queue!(OSMesgQueueFrame), std::slice::from_mut(&mut OSMesg0));
	osCreateMesgQueue(queue!(OSMesgQueueCmdSync), &mut OSMesg1);
	osCreateMesgQueue(queue!(OSMesgQueuePreset), std::slice::from_mut(&mut OSMesg2));
	osCreateMesgQueue(queue!(OSMesgQueuePresetLoaded), std::slice::from_mut(&mut OSMesg3));
}

pub const fn build_global_cmd(cmd: u8) -> u32 {
	//debug_assert!(cmd & 0x80 != 0);
	(cmd as u32) << 24
}

pub const fn build_player_cmd(cmd: u8, player: u8) -> u32 {
	//debug_assert!(cmd & 0xC0 == 0x40);
	((cmd as u32) << 24) | ((player as u32) << 16)
}

pub const fn build_player_cmd_seq(cmd: u8, player: u8, seq: u8) -> u32 {
	//debug_assert!(cmd & 0xC0 == 0x40);
	((cmd as u32) << 24) | ((player as u32) << 16) | ((seq as u32) << 8)
}

pub const fn build_channel_cmd(cmd: u8, player: u8, channel: u8) -> u32 {
	//debug_assert!(cmd & 0xC0 == 0);
	((cmd as u32) << 24) | ((player as u32) << 16) | ((channel as u32) << 8)
}

pub unsafe fn send_audio_cmd_u32(bits: u32, arg: u32) {
	let cmd = &mut euAudioCmd[sAudioCmdCursor as usize];
	cmd.u.first = bits as i32;
	cmd.u2.as_u32 = arg;
	sAudioCmdCursor = sAudioCmdCursor.wrapping_add(1);
	if sAudioCmdCursor == sAudioCmdLastRefreshPos {
		panic!("Audio cmd buffer overflow!");
	}
}

#[inline(always)]
pub unsafe fn send_audio_cmd_f32(bits: u32, arg: f32) {
	send_audio_cmd_u32(bits, std::mem::transmute(arg));
}

pub unsafe fn send_audio_cmd_byte(bits: u32, arg: i8) {
	let tmp = (arg as i32) << 24;
	send_audio_cmd_u32(bits, tmp as u32);
}

pub unsafe fn eu_flush_audio_commands() {
	let _ = osSendMesg(OSMesgQueueCmdSync.assume_init_ref(), (((sAudioCmdLastRefreshPos as usize) << 8) | (sAudioCmdCursor as usize)) as OSMesg, Blocking::NoBlock);
	sAudioCmdLastRefreshPos = sAudioCmdCursor;
}

unsafe fn eu_execute_audio_cmds(bounds: u16) {
	let [end, mut pos] = bounds.to_le_bytes();
	while pos != end {
		let cmd = &mut euAudioCmd[pos as usize];
		pos = pos.wrapping_add(1);

		if let Some(seqPlayer) = gSequencePlayers.get_mut(cmd.u.s.player as usize) {
			if (cmd.u.s.op & 0x80) != 0 {
				eu_global_audio_cmd(cmd);
			} else if (cmd.u.s.op & 0x40) != 0 {
				match cmd.u.s.op {
					PLAYER_CMD_SET_FADE_VOLUME_SCALE => {
						seqPlayer.fadeVolumeScale = cmd.u2.as_f32;
						seqPlayer.flags |= SequencePlayerFlags::RecalculateVolume;
					}
					PLAYER_CMD_SET_TEMPO => seqPlayer.tempo = cmd.u2.as_u32 as u16 * TATUMS_PER_BEAT as u16,
					PLAYER_CMD_SET_TRANSPOSITION => seqPlayer.transposition = cmd.u2.as_i8.v as i16,
					PLAYER_CMD_SET_VARIATION => {
						assert_eq!(cmd.u.s.arg3, 0);
						seqPlayer.seqVariationEu = cmd.u2.as_i8.v;
					}
					_ => eprintln!("Error: undefined port command: 0x{:02X}", cmd.u.s.op),
				}
			} else if seqPlayer.flags.contains(SequencePlayerFlags::Enabled) && cmd.u.s.arg2 < CHANNELS_MAX as u8 {
				let chan = seqPlayer.channels[cmd.u.s.arg2 as usize];
				if chan != std::ptr::addr_of_mut!(gSequenceChannelNone) {
					let chan = &mut *chan;
					match cmd.u.s.op {
						CHANNEL_CMD_SET_VOLUME_SCALE => {
							chan.volumeScale = cmd.u2.as_f32;
							chan.changes |= SequenceChannelChanges::Volume;
						}
						CHANNEL_CMD_SET_VOLUME => {
							chan.volume = cmd.u2.as_f32;
							chan.changes |= SequenceChannelChanges::Volume;
						}
						CHANNEL_CMD_SET_PAN => {
							chan.newPan = cmd.u2.as_u8.v;
							chan.changes |= SequenceChannelChanges::Pan;
						}
						CHANNEL_CMD_SET_FREQ_SCALE => {
							chan.freqScale = cmd.u2.as_f32;
							chan.changes |= SequenceChannelChanges::FreqScale;
						}
						CHANNEL_CMD_SET_REVERB_VOLUME => chan.reverbVol = cmd.u2.as_u8.v,
						CHANNEL_CMD_SET_PORT => if let Some(port) = chan.soundScriptIO.get_mut(cmd.u.s.arg3 as usize) {
							*port = cmd.u2.as_i8.v;
						}
						// --
						CHANNEL_CMD_STOP_SOMETHING => chan.flags.set(SequenceChannelFlags::StopSomething2, cmd.u2.as_u8.v != 0),
						_ => {}
					}
				}
			}
		}

		cmd.u.s.op = 0;
	}
}

#[inline]
pub unsafe fn port_eu_init() {
	port_eu_init_queues();
}
