use super::*;

macro_rules! sound_bits {
	// Fast path
	($bank:expr, $soundID:expr, $priority:expr, []) => {
		SoundBits::new($bank, $soundID, $priority)
	};
	($bank:expr, $soundID:expr, $priority:expr, [$($flags:ident),+ $(,)?]) => {
		add_flags!(SoundBits::new($bank, $soundID, $priority), $($flags,)*)
	};
}

macro_rules! add_flags {
	($base:expr,) => ($base);
	($base:expr, $flag:ident, $($tail:tt)*) => (add_flags!(
		$base.add_flag(SoundBits::$flag)
	, $($tail)*));
}

pub const SOUND_BANK_ACTION:   u8 = 0;
pub const SOUND_BANK_MOVING:   u8 = 1;
pub const SOUND_BANK_VOICE:    u8 = 2;
pub const SOUND_BANK_GENERAL:  u8 = 3;
pub const SOUND_BANK_ENV:      u8 = 4;
pub const SOUND_BANK_OBJ:      u8 = 5;
pub const SOUND_BANK_AIR:      u8 = 6;
pub const SOUND_BANK_MENU:     u8 = 7;
pub const SOUND_BANK_GENERAL2: u8 = 8;
pub const SOUND_BANK_OBJ2:     u8 = 9;
pub const SOUND_BANK_COUNT:    usize = 10;

pub const SOUND_BANKS_ALL_BITS: u16 = !0;
pub const SOUND_BANKS_ALL: u16 = if SOUND_BANK_COUNT == 16 { !0 } else { (1u16 << SOUND_BANK_COUNT) - 1 };

macro_rules! bank_mask {
	($mask:ident = [$($banks:ident),+ $(,)?]) => {
		pub const $mask: u16 = $(
			(1 << $banks)
		)|+;
	};
}

bank_mask!(SOUND_BANKS_FOREGROUND = [SOUND_BANK_ACTION, SOUND_BANK_VOICE, SOUND_BANK_MENU]);
pub const SOUND_BANKS_BACKGROUND: u16 = SOUND_BANKS_ALL & !SOUND_BANKS_FOREGROUND;
bank_mask!(SOUND_BANKS_DISABLED_DURING_INTRO_CUTSCENE = [
	SOUND_BANK_ENV,
	SOUND_BANK_OBJ,
	SOUND_BANK_GENERAL2,
	SOUND_BANK_OBJ2,
]);
bank_mask!(SOUND_BANKS_DISABLED_AFTER_CREDITS = [
	SOUND_BANK_ACTION,
	SOUND_BANK_MOVING,
	SOUND_BANK_VOICE,
	SOUND_BANK_GENERAL,
]);

/*
 * The table below defines all sounds that exist in the game, and which flags
 * they are used with. If a sound is used with multiple sets of flags (e.g.
 * different priorities), they are gives distinguishing suffixes.
 * A _2 suffix means the sound is the same despite a different sound ID.
 */

//// Terrain sounds

// Terrain-dependent moving sounds; a value 0-7 is added to the sound ID before
// playing. See higher up for the different terrain types.
pub const SOUND_ACTION_TERRAIN_JUMP:            SoundBits = /* 0x04008080 */ sound_bits!(SOUND_BANK_ACTION,   0x00, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_TERRAIN_LANDING:         SoundBits = /* 0x04088080 */ sound_bits!(SOUND_BANK_ACTION,   0x08, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_TERRAIN_STEP:            SoundBits = /* 0x06108080 */ sound_bits!(SOUND_BANK_ACTION,   0x10, 0x80, [SOUND_VIBRATO, SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_TERRAIN_BODY_HIT_GROUND: SoundBits = /* 0x04188080 */ sound_bits!(SOUND_BANK_ACTION,   0x18, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_TERRAIN_STEP_TIPTOE:     SoundBits = /* 0x06208080 */ sound_bits!(SOUND_BANK_ACTION,   0x20, 0x80, [SOUND_VIBRATO, SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_TERRAIN_STUCK_IN_GROUND: SoundBits = /* 0x04488080 */ sound_bits!(SOUND_BANK_ACTION,   0x48, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_TERRAIN_HEAVY_LANDING:   SoundBits = /* 0x04608080 */ sound_bits!(SOUND_BANK_ACTION,   0x60, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

pub const SOUND_ACTION_METAL_JUMP:              SoundBits = /* 0x04289080 */ sound_bits!(SOUND_BANK_ACTION,   0x28, 0x90, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_METAL_LANDING:           SoundBits = /* 0x04299080 */ sound_bits!(SOUND_BANK_ACTION,   0x29, 0x90, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_METAL_STEP:              SoundBits = /* 0x042A9080 */ sound_bits!(SOUND_BANK_ACTION,   0x2A, 0x90, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_METAL_HEAVY_LANDING:     SoundBits = /* 0x042B9080 */ sound_bits!(SOUND_BANK_ACTION,   0x2B, 0x90, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_CLAP_HANDS_COLD:         SoundBits = /* 0x062C0080 */ sound_bits!(SOUND_BANK_ACTION,   0x2C, 0x00, [SOUND_VIBRATO, SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_HANGING_STEP:            SoundBits = /* 0x042DA080 */ sound_bits!(SOUND_BANK_ACTION,   0x2D, 0xA0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_QUICKSAND_STEP:          SoundBits = /* 0x042E0080 */ sound_bits!(SOUND_BANK_ACTION,   0x2E, 0x00, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_METAL_STEP_TIPTOE:       SoundBits = /* 0x042F9080 */ sound_bits!(SOUND_BANK_ACTION,   0x2F, 0x90, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_UNKNOWN430:              SoundBits = /* 0x0430C080 */ sound_bits!(SOUND_BANK_ACTION,   0x30, 0xC0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_UNKNOWN431:              SoundBits = /* 0x04316080 */ sound_bits!(SOUND_BANK_ACTION,   0x31, 0x60, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_UNKNOWN432:              SoundBits = /* 0x04328080 */ sound_bits!(SOUND_BANK_ACTION,   0x32, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_SWIM:                    SoundBits = /* 0x04338080 */ sound_bits!(SOUND_BANK_ACTION,   0x33, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_UNKNOWN434:              SoundBits = /* 0x04348080 */ sound_bits!(SOUND_BANK_ACTION,   0x34, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_THROW:                   SoundBits = /* 0x04358080 */ sound_bits!(SOUND_BANK_ACTION,   0x35, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_KEY_SWISH:               SoundBits = /* 0x04368080 */ sound_bits!(SOUND_BANK_ACTION,   0x36, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_SPIN:                    SoundBits = /* 0x04378080 */ sound_bits!(SOUND_BANK_ACTION,   0x37, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_TWIRL:                   SoundBits = /* 0x04388080 */ sound_bits!(SOUND_BANK_ACTION,   0x38, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // same sound as spin
pub const SOUND_ACTION_CLIMB_UP_TREE:           SoundBits = /* 0x043A8080 */ sound_bits!(SOUND_BANK_ACTION,   0x3A, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_CLIMB_DOWN_TREE:         SoundBits = /* 0x003B0000 */ sound_bits!(SOUND_BANK_ACTION,   0x3B, 0x00, []); // unverified, unused
pub const SOUND_ACTION_UNK3C:                   SoundBits = /* 0x003C0000 */ sound_bits!(SOUND_BANK_ACTION,   0x3C, 0x00, []); // unverified, unused
pub const SOUND_ACTION_UNKNOWN43D:              SoundBits = /* 0x043D8080 */ sound_bits!(SOUND_BANK_ACTION,   0x3D, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_UNKNOWN43E:              SoundBits = /* 0x043E8080 */ sound_bits!(SOUND_BANK_ACTION,   0x3E, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_PAT_BACK:                SoundBits = /* 0x043F8080 */ sound_bits!(SOUND_BANK_ACTION,   0x3F, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_BRUSH_HAIR:              SoundBits = /* 0x04408080 */ sound_bits!(SOUND_BANK_ACTION,   0x40, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_CLIMB_UP_POLE:           SoundBits = /* 0x04418080 */ sound_bits!(SOUND_BANK_ACTION,   0x41, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_METAL_BONK:              SoundBits = /* 0x04428080 */ sound_bits!(SOUND_BANK_ACTION,   0x42, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_UNSTUCK_FROM_GROUND:     SoundBits = /* 0x04438080 */ sound_bits!(SOUND_BANK_ACTION,   0x43, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_HIT:                     SoundBits = /* 0x0444C080 */ sound_bits!(SOUND_BANK_ACTION,   0x44, 0xC0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_HIT_2:                   SoundBits = /* 0x0444B080 */ sound_bits!(SOUND_BANK_ACTION,   0x44, 0xB0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_HIT_3:                   SoundBits = /* 0x0444A080 */ sound_bits!(SOUND_BANK_ACTION,   0x44, 0xA0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_BONK:                    SoundBits = /* 0x0445A080 */ sound_bits!(SOUND_BANK_ACTION,   0x45, 0xA0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_SHRINK_INTO_BBH:         SoundBits = /* 0x0446A080 */ sound_bits!(SOUND_BANK_ACTION,   0x46, 0xA0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_SWIM_FAST:               SoundBits = /* 0x0447A080 */ sound_bits!(SOUND_BANK_ACTION,   0x47, 0xA0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_METAL_JUMP_WATER:        SoundBits = /* 0x04509080 */ sound_bits!(SOUND_BANK_ACTION,   0x50, 0x90, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_METAL_LAND_WATER:        SoundBits = /* 0x04519080 */ sound_bits!(SOUND_BANK_ACTION,   0x51, 0x90, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_METAL_STEP_WATER:        SoundBits = /* 0x04529080 */ sound_bits!(SOUND_BANK_ACTION,   0x52, 0x90, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_UNK53:                   SoundBits = /* 0x00530000 */ sound_bits!(SOUND_BANK_ACTION,   0x53, 0x00, []); // unverified, unused
pub const SOUND_ACTION_UNK54:                   SoundBits = /* 0x00540000 */ sound_bits!(SOUND_BANK_ACTION,   0x54, 0x00, []); // unverified, unused
pub const SOUND_ACTION_UNK55:                   SoundBits = /* 0x00550000 */ sound_bits!(SOUND_BANK_ACTION,   0x55, 0x00, []); // unverified, unused
pub const SOUND_ACTION_FLYING_FAST:             SoundBits = /* 0x04568080 */ sound_bits!(SOUND_BANK_ACTION,   0x56, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // "swoop"?, unverified
pub const SOUND_ACTION_TELEPORT:                SoundBits = /* 0x0457C080 */ sound_bits!(SOUND_BANK_ACTION,   0x57, 0xC0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_UNKNOWN458:              SoundBits = /* 0x0458A080 */ sound_bits!(SOUND_BANK_ACTION,   0x58, 0xA0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_BOUNCE_OFF_OBJECT:       SoundBits = /* 0x0459B080 */ sound_bits!(SOUND_BANK_ACTION,   0x59, 0xB0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_SIDE_FLIP_UNK:           SoundBits = /* 0x045A8080 */ sound_bits!(SOUND_BANK_ACTION,   0x5A, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_READ_SIGN:               SoundBits = /* 0x045BFF80 */ sound_bits!(SOUND_BANK_ACTION,   0x5B, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_ACTION_UNKNOWN45C:              SoundBits = /* 0x045C8080 */ sound_bits!(SOUND_BANK_ACTION,   0x5C, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_UNK5D:                   SoundBits = /* 0x005D0000 */ sound_bits!(SOUND_BANK_ACTION,   0x5D, 0x00, []); // unverified, unused
pub const SOUND_ACTION_INTRO_UNK45E:            SoundBits = /* 0x045E8080 */ sound_bits!(SOUND_BANK_ACTION,   0x5E, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_ACTION_INTRO_UNK45F:            SoundBits = /* 0x045F8080 */ sound_bits!(SOUND_BANK_ACTION,   0x5F, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified

//// Moving Sound Effects

// Terrain-dependent moving sounds; a value 0-7 is added to the sound ID before
// playing. See higher up for the different terrain types.
pub const SOUND_MOVING_TERRAIN_SLIDE:           SoundBits = /* 0x14000000 */ sound_bits!(SOUND_BANK_MOVING,   0x00, 0x00, [SOUND_NO_PRIORITY_LOSS]);
pub const SOUND_MOVING_TERRAIN_RIDING_SHELL:    SoundBits = /* 0x14200000 */ sound_bits!(SOUND_BANK_MOVING,   0x20, 0x00, [SOUND_NO_PRIORITY_LOSS]);

pub const SOUND_MOVING_LAVA_BURN:               SoundBits = /* 0x14100000 */ sound_bits!(SOUND_BANK_MOVING,   0x10, 0x00, [SOUND_NO_PRIORITY_LOSS]); // ?
pub const SOUND_MOVING_SLIDE_DOWN_POLE:         SoundBits = /* 0x14110000 */ sound_bits!(SOUND_BANK_MOVING,   0x11, 0x00, [SOUND_NO_PRIORITY_LOSS]); // ?
pub const SOUND_MOVING_SLIDE_DOWN_TREE:         SoundBits = /* 0x14128000 */ sound_bits!(SOUND_BANK_MOVING,   0x12, 0x80, [SOUND_NO_PRIORITY_LOSS]);
pub const SOUND_MOVING_QUICKSAND_DEATH:         SoundBits = /* 0x14140000 */ sound_bits!(SOUND_BANK_MOVING,   0x14, 0x00, [SOUND_NO_PRIORITY_LOSS]);
pub const SOUND_MOVING_SHOCKED:                 SoundBits = /* 0x14160000 */ sound_bits!(SOUND_BANK_MOVING,   0x16, 0x00, [SOUND_NO_PRIORITY_LOSS]);
pub const SOUND_MOVING_FLYING:                  SoundBits = /* 0x14170000 */ sound_bits!(SOUND_BANK_MOVING,   0x17, 0x00, [SOUND_NO_PRIORITY_LOSS]);
pub const SOUND_MOVING_ALMOST_DROWNING:         SoundBits = /* 0x1C180000 */ sound_bits!(SOUND_BANK_MOVING,   0x18, 0x00, [SOUND_NO_PRIORITY_LOSS, SOUND_CONSTANT_FREQUENCY]);
pub const SOUND_MOVING_AIM_CANNON:              SoundBits = /* 0x1D192000 */ sound_bits!(SOUND_BANK_MOVING,   0x19, 0x20, [SOUND_NO_VOLUME_LOSS, SOUND_NO_PRIORITY_LOSS, SOUND_CONSTANT_FREQUENCY]);
pub const SOUND_MOVING_UNK1A:                   SoundBits = /* 0x101A0000 */ sound_bits!(SOUND_BANK_MOVING,   0x1A, 0x00, []); // ?, unused
pub const SOUND_MOVING_RIDING_SHELL_LAVA:       SoundBits = /* 0x14280000 */ sound_bits!(SOUND_BANK_MOVING,   0x28, 0x00, [SOUND_NO_PRIORITY_LOSS]);

//// Mario Sound Effects

// A random number 0-2 is added to the sound ID before playing, producing Yah/Wah/Hoo
pub const SOUND_MARIO_YAH_WAH_HOO:              SoundBits = /* 0x24008080 */ sound_bits!(SOUND_BANK_VOICE,    0x00, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

pub const SOUND_MARIO_HOOHOO:                   SoundBits = /* 0x24038080 */ sound_bits!(SOUND_BANK_VOICE,    0x03, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_YAHOO:                    SoundBits = /* 0x24048080 */ sound_bits!(SOUND_BANK_VOICE,    0x04, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_UH:                       SoundBits = /* 0x24058080 */ sound_bits!(SOUND_BANK_VOICE,    0x05, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_HRMM:                     SoundBits = /* 0x24068080 */ sound_bits!(SOUND_BANK_VOICE,    0x06, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_WAH2:                     SoundBits = /* 0x24078080 */ sound_bits!(SOUND_BANK_VOICE,    0x07, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_WHOA:                     SoundBits = /* 0x2408C080 */ sound_bits!(SOUND_BANK_VOICE,    0x08, 0xC0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_EEUH:                     SoundBits = /* 0x24098080 */ sound_bits!(SOUND_BANK_VOICE,    0x09, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_ATTACKED:                 SoundBits = /* 0x240AFF80 */ sound_bits!(SOUND_BANK_VOICE,    0x0A, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_OOOF:                     SoundBits = /* 0x240B8080 */ sound_bits!(SOUND_BANK_VOICE,    0x0B, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_OOOF2:                    SoundBits = /* 0x240BD080 */ sound_bits!(SOUND_BANK_VOICE,    0x0B, 0xD0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_HERE_WE_GO:               SoundBits = /* 0x240C8080 */ sound_bits!(SOUND_BANK_VOICE,    0x0C, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_YAWNING:                  SoundBits = /* 0x240D8080 */ sound_bits!(SOUND_BANK_VOICE,    0x0D, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_SNORING1:                 SoundBits = /* 0x240E8080 */ sound_bits!(SOUND_BANK_VOICE,    0x0E, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_SNORING2:                 SoundBits = /* 0x240F8080 */ sound_bits!(SOUND_BANK_VOICE,    0x0F, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_WAAAOOOW:                 SoundBits = /* 0x2410C080 */ sound_bits!(SOUND_BANK_VOICE,    0x10, 0xC0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_HAHA:                     SoundBits = /* 0x24118080 */ sound_bits!(SOUND_BANK_VOICE,    0x11, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_HAHA_2:                   SoundBits = /* 0x2411F080 */ sound_bits!(SOUND_BANK_VOICE,    0x11, 0xF0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_UH2:                      SoundBits = /* 0x2413D080 */ sound_bits!(SOUND_BANK_VOICE,    0x13, 0xD0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_UH2_2:                    SoundBits = /* 0x24138080 */ sound_bits!(SOUND_BANK_VOICE,    0x13, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_ON_FIRE:                  SoundBits = /* 0x2414A080 */ sound_bits!(SOUND_BANK_VOICE,    0x14, 0xA0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_DYING:                    SoundBits = /* 0x2415FF80 */ sound_bits!(SOUND_BANK_VOICE,    0x15, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_MARIO_PANTING_COLD:             SoundBits = /* 0x24168080 */ sound_bits!(SOUND_BANK_VOICE,    0x16, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

// A random number 0-2 is added to the sound ID before playing
pub const SOUND_MARIO_PANTING:                  SoundBits = /* 0x24188080 */ sound_bits!(SOUND_BANK_VOICE,    0x18, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

pub const SOUND_MARIO_COUGHING1:                SoundBits = /* 0x241B8080 */ sound_bits!(SOUND_BANK_VOICE,    0x1B, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_COUGHING2:                SoundBits = /* 0x241C8080 */ sound_bits!(SOUND_BANK_VOICE,    0x1C, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_COUGHING3:                SoundBits = /* 0x241D8080 */ sound_bits!(SOUND_BANK_VOICE,    0x1D, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_PUNCH_YAH:                SoundBits = /* 0x241E8080 */ sound_bits!(SOUND_BANK_VOICE,    0x1E, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_PUNCH_HOO:                SoundBits = /* 0x241F8080 */ sound_bits!(SOUND_BANK_VOICE,    0x1F, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_MAMA_MIA:                 SoundBits = /* 0x24208080 */ sound_bits!(SOUND_BANK_VOICE,    0x20, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_OKEY_DOKEY:               SoundBits = /* 0x20210000 */ sound_bits!(SOUND_BANK_VOICE,    0x21, 0x00, []); // unused
pub const SOUND_MARIO_GROUND_POUND_WAH:         SoundBits = /* 0x24228080 */ sound_bits!(SOUND_BANK_VOICE,    0x22, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_DROWNING:                 SoundBits = /* 0x2423F080 */ sound_bits!(SOUND_BANK_VOICE,    0x23, 0xF0, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_PUNCH_WAH:                SoundBits = /* 0x24248080 */ sound_bits!(SOUND_BANK_VOICE,    0x24, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

//// Mario Sound Effects (US/EU only)
pub const SOUND_PEACH_DEAR_MARIO:               SoundBits = /* 0x2428FF80 */ sound_bits!(SOUND_BANK_VOICE,    0x28, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

// A random number 0-4 is added to the sound ID before playing, producing one of
// Yahoo! (60% chance), Waha! (20%), or Yippee! (20%).
pub const SOUND_MARIO_YAHOO_WAHA_YIPPEE:        SoundBits = /* 0x242B8080 */ sound_bits!(SOUND_BANK_VOICE,    0x2B, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

pub const SOUND_MARIO_DOH:                      SoundBits = /* 0x24308080 */ sound_bits!(SOUND_BANK_VOICE,    0x30, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_GAME_OVER:                SoundBits = /* 0x2431FF80 */ sound_bits!(SOUND_BANK_VOICE,    0x31, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_HELLO:                    SoundBits = /* 0x2432FF80 */ sound_bits!(SOUND_BANK_VOICE,    0x32, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_PRESS_START_TO_PLAY:      SoundBits = /* 0x2433FFA0 */ sound_bits!(SOUND_BANK_VOICE,    0x33, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_NO_ECHO, SOUND_DISCRETE]);
pub const SOUND_MARIO_TWIRL_BOUNCE:             SoundBits = /* 0x24348080 */ sound_bits!(SOUND_BANK_VOICE,    0x34, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_SNORING3:                 SoundBits = /* 0x2435FF80 */ sound_bits!(SOUND_BANK_VOICE,    0x35, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_SO_LONGA_BOWSER:          SoundBits = /* 0x24368080 */ sound_bits!(SOUND_BANK_VOICE,    0x36, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_MARIO_IMA_TIRED:                SoundBits = /* 0x24378080 */ sound_bits!(SOUND_BANK_VOICE,    0x37, 0x80, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

//// Princess Peach Sound Effects (US/EU only)
pub const SOUND_PEACH_MARIO:                    SoundBits = /* 0x2438FF80 */ sound_bits!(SOUND_BANK_VOICE,    0x38, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_PEACH_POWER_OF_THE_STARS:       SoundBits = /* 0x2439FF80 */ sound_bits!(SOUND_BANK_VOICE,    0x39, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_PEACH_THANKS_TO_YOU:            SoundBits = /* 0x243AFF80 */ sound_bits!(SOUND_BANK_VOICE,    0x3A, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_PEACH_THANK_YOU_MARIO:          SoundBits = /* 0x243BFF80 */ sound_bits!(SOUND_BANK_VOICE,    0x3B, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_PEACH_SOMETHING_SPECIAL:        SoundBits = /* 0x243CFF80 */ sound_bits!(SOUND_BANK_VOICE,    0x3C, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_PEACH_BAKE_A_CAKE:              SoundBits = /* 0x243DFF80 */ sound_bits!(SOUND_BANK_VOICE,    0x3D, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_PEACH_FOR_MARIO:                SoundBits = /* 0x243EFF80 */ sound_bits!(SOUND_BANK_VOICE,    0x3E, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);
pub const SOUND_PEACH_MARIO2:                   SoundBits = /* 0x243FFF80 */ sound_bits!(SOUND_BANK_VOICE,    0x3F, 0xFF, [SOUND_NO_PRIORITY_LOSS, SOUND_DISCRETE]);

//// General Sound Effects
pub const SOUND_GENERAL_ACTIVATE_CAP_SWITCH:      SoundBits = /* 0x30008080 */ sound_bits!(SOUND_BANK_GENERAL,  0x00, 0x80, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_FLAME_OUT:                SoundBits = /* 0x30038080 */ sound_bits!(SOUND_BANK_GENERAL,  0x03, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_OPEN_WOOD_DOOR:           SoundBits = /* 0x3004C080 */ sound_bits!(SOUND_BANK_GENERAL,  0x04, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_CLOSE_WOOD_DOOR:          SoundBits = /* 0x3005C080 */ sound_bits!(SOUND_BANK_GENERAL,  0x05, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_OPEN_IRON_DOOR:           SoundBits = /* 0x3006C080 */ sound_bits!(SOUND_BANK_GENERAL,  0x06, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_CLOSE_IRON_DOOR:          SoundBits = /* 0x3007C080 */ sound_bits!(SOUND_BANK_GENERAL,  0x07, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BUBBLES:                  SoundBits = /* 0x30080000 */ sound_bits!(SOUND_BANK_GENERAL,  0x08, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_MOVING_WATER:             SoundBits = /* 0x30090080 */ sound_bits!(SOUND_BANK_GENERAL,  0x09, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_SWISH_WATER:              SoundBits = /* 0x300A0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x0A, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_QUIET_BUBBLE:             SoundBits = /* 0x300B0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x0B, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_VOLCANO_EXPLOSION:        SoundBits = /* 0x300C8080 */ sound_bits!(SOUND_BANK_GENERAL,  0x0C, 0x80, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_QUIET_BUBBLE2:            SoundBits = /* 0x300D0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x0D, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_CASTLE_TRAP_OPEN:         SoundBits = /* 0x300E8080 */ sound_bits!(SOUND_BANK_GENERAL,  0x0E, 0x80, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_WALL_EXPLOSION:           SoundBits = /* 0x300F0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x0F, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_COIN:                     SoundBits = /* 0x38118080 */ sound_bits!(SOUND_BANK_GENERAL,  0x11, 0x80, [SOUND_CONSTANT_FREQUENCY, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_COIN_WATER:               SoundBits = /* 0x38128080 */ sound_bits!(SOUND_BANK_GENERAL,  0x12, 0x80, [SOUND_CONSTANT_FREQUENCY, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_SHORT_STAR:               SoundBits = /* 0x30160090 */ sound_bits!(SOUND_BANK_GENERAL,  0x16, 0x00, [SOUND_LOWER_BACKGROUND_MUSIC, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BIG_CLOCK:                SoundBits = /* 0x30170080 */ sound_bits!(SOUND_BANK_GENERAL,  0x17, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_LOUD_POUND:               SoundBits = /* 0x30180000 */ sound_bits!(SOUND_BANK_GENERAL,  0x18, 0x00, []); // _TERRAIN?, unverified, unused
pub const SOUND_GENERAL_LOUD_POUND2:              SoundBits = /* 0x30190000 */ sound_bits!(SOUND_BANK_GENERAL,  0x19, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_SHORT_POUND1:             SoundBits = /* 0x301A0000 */ sound_bits!(SOUND_BANK_GENERAL,  0x1A, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_SHORT_POUND2:             SoundBits = /* 0x301B0000 */ sound_bits!(SOUND_BANK_GENERAL,  0x1B, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_SHORT_POUND3:             SoundBits = /* 0x301C0000 */ sound_bits!(SOUND_BANK_GENERAL,  0x1C, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_SHORT_POUND4:             SoundBits = /* 0x301D0000 */ sound_bits!(SOUND_BANK_GENERAL,  0x1D, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_SHORT_POUND5:             SoundBits = /* 0x301E0000 */ sound_bits!(SOUND_BANK_GENERAL,  0x1E, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_SHORT_POUND6:             SoundBits = /* 0x301F0000 */ sound_bits!(SOUND_BANK_GENERAL,  0x1F, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_OPEN_CHEST:               SoundBits = /* 0x31208080 */ sound_bits!(SOUND_BANK_GENERAL,  0x20, 0x80, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]);
pub const SOUND_GENERAL_CLAM_SHELL1:              SoundBits = /* 0x31228080 */ sound_bits!(SOUND_BANK_GENERAL,  0x22, 0x80, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOX_LANDING:              SoundBits = /* 0x30240080 */ sound_bits!(SOUND_BANK_GENERAL,  0x24, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOX_LANDING_2:            SoundBits = /* 0x32240080 */ sound_bits!(SOUND_BANK_GENERAL,  0x24, 0x00, [SOUND_VIBRATO, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNKNOWN1:                 SoundBits = /* 0x30250080 */ sound_bits!(SOUND_BANK_GENERAL,  0x25, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNKNOWN1_2:               SoundBits = /* 0x32250080 */ sound_bits!(SOUND_BANK_GENERAL,  0x25, 0x00, [SOUND_VIBRATO, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_CLAM_SHELL2:              SoundBits = /* 0x30264080 */ sound_bits!(SOUND_BANK_GENERAL,  0x26, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_CLAM_SHELL3:              SoundBits = /* 0x30274080 */ sound_bits!(SOUND_BANK_GENERAL,  0x27, 0x40, [SOUND_DISCRETE]); // unverified
// This one differs between JP and other versions by a factor of SOUND_NO_VOLUME_LOSS:
pub const SOUND_GENERAL_PAINTING_EJECT:           SoundBits = /* 0x39280080 */ sound_bits!(SOUND_BANK_GENERAL,  0x28, 0x00, [SOUND_NO_VOLUME_LOSS, SOUND_CONSTANT_FREQUENCY, SOUND_DISCRETE]);
pub const SOUND_GENERAL_LEVEL_SELECT_CHANGE:      SoundBits = /* 0x302B0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x2B, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_PLATFORM:                 SoundBits = /* 0x302D8080 */ sound_bits!(SOUND_BANK_GENERAL,  0x2D, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_DONUT_PLATFORM_EXPLOSION: SoundBits = /* 0x302E2080 */ sound_bits!(SOUND_BANK_GENERAL,  0x2E, 0x20, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_BOWSER_BOMB_EXPLOSION:    SoundBits = /* 0x312F0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x2F, 0x00, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]);
pub const SOUND_GENERAL_COIN_SPURT:               SoundBits = /* 0x30300080 */ sound_bits!(SOUND_BANK_GENERAL,  0x30, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_COIN_SPURT_2:             SoundBits = /* 0x38300080 */ sound_bits!(SOUND_BANK_GENERAL,  0x30, 0x00, [SOUND_CONSTANT_FREQUENCY, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_COIN_SPURT_EU:            SoundBits = /* 0x38302080 */ sound_bits!(SOUND_BANK_GENERAL,  0x30, 0x20, [SOUND_CONSTANT_FREQUENCY, SOUND_DISCRETE]); // unverifiedpub const
pub const SOUND_GENERAL_EXPLOSION6:               SoundBits = /* 0x30310000 */ sound_bits!(SOUND_BANK_GENERAL,  0x31, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_UNK32:                    SoundBits = /* 0x30320000 */ sound_bits!(SOUND_BANK_GENERAL,  0x32, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_BOAT_TILT1:               SoundBits = /* 0x30344080 */ sound_bits!(SOUND_BANK_GENERAL,  0x34, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOAT_TILT2:               SoundBits = /* 0x30354080 */ sound_bits!(SOUND_BANK_GENERAL,  0x35, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_COIN_DROP:                SoundBits = /* 0x30364080 */ sound_bits!(SOUND_BANK_GENERAL,  0x36, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNKNOWN3_LOWPRIO:         SoundBits = /* 0x30370080 */ sound_bits!(SOUND_BANK_GENERAL,  0x37, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNKNOWN3:                 SoundBits = /* 0x30378080 */ sound_bits!(SOUND_BANK_GENERAL,  0x37, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNKNOWN3_2:               SoundBits = /* 0x38378080 */ sound_bits!(SOUND_BANK_GENERAL,  0x37, 0x80, [SOUND_CONSTANT_FREQUENCY, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_PENDULUM_SWING:           SoundBits = /* 0x30380080 */ sound_bits!(SOUND_BANK_GENERAL,  0x38, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_CHAIN_CHOMP1:             SoundBits = /* 0x30390080 */ sound_bits!(SOUND_BANK_GENERAL,  0x39, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_CHAIN_CHOMP2:             SoundBits = /* 0x303A0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x3A, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_DOOR_TURN_KEY:            SoundBits = /* 0x303B0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x3B, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_MOVING_IN_SAND:           SoundBits = /* 0x303C0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x3C, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNKNOWN4_LOWPRIO:         SoundBits = /* 0x303D0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x3D, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNKNOWN4:                 SoundBits = /* 0x303D8080 */ sound_bits!(SOUND_BANK_GENERAL,  0x3D, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_MOVING_PLATFORM_SWITCH:   SoundBits = /* 0x303E0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x3E, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_CAGE_OPEN:                SoundBits = /* 0x303FA080 */ sound_bits!(SOUND_BANK_GENERAL,  0x3F, 0xA0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_QUIET_POUND1_LOWPRIO:     SoundBits = /* 0x30400080 */ sound_bits!(SOUND_BANK_GENERAL,  0x40, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_QUIET_POUND1:             SoundBits = /* 0x30404080 */ sound_bits!(SOUND_BANK_GENERAL,  0x40, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BREAK_BOX:                SoundBits = /* 0x3041C080 */ sound_bits!(SOUND_BANK_GENERAL,  0x41, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_DOOR_INSERT_KEY:          SoundBits = /* 0x30420080 */ sound_bits!(SOUND_BANK_GENERAL,  0x42, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_QUIET_POUND2:             SoundBits = /* 0x30430080 */ sound_bits!(SOUND_BANK_GENERAL,  0x43, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BIG_POUND:                SoundBits = /* 0x30440080 */ sound_bits!(SOUND_BANK_GENERAL,  0x44, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNK45:                    SoundBits = /* 0x30450080 */ sound_bits!(SOUND_BANK_GENERAL,  0x45, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNK46_LOWPRIO:            SoundBits = /* 0x30460080 */ sound_bits!(SOUND_BANK_GENERAL,  0x46, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_UNK46:                    SoundBits = /* 0x30468080 */ sound_bits!(SOUND_BANK_GENERAL,  0x46, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_CANNON_UP:                SoundBits = /* 0x30478080 */ sound_bits!(SOUND_BANK_GENERAL,  0x47, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_GRINDEL_ROLL:             SoundBits = /* 0x30480080 */ sound_bits!(SOUND_BANK_GENERAL,  0x48, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_EXPLOSION7:               SoundBits = /* 0x30490000 */ sound_bits!(SOUND_BANK_GENERAL,  0x49, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_SHAKE_COFFIN:             SoundBits = /* 0x304A0000 */ sound_bits!(SOUND_BANK_GENERAL,  0x4A, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_RACE_GUN_SHOT:            SoundBits = /* 0x314D4080 */ sound_bits!(SOUND_BANK_GENERAL,  0x4D, 0x40, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_STAR_DOOR_OPEN:           SoundBits = /* 0x304EC080 */ sound_bits!(SOUND_BANK_GENERAL,  0x4E, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_STAR_DOOR_CLOSE:          SoundBits = /* 0x304FC080 */ sound_bits!(SOUND_BANK_GENERAL,  0x4F, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_POUND_ROCK:               SoundBits = /* 0x30560080 */ sound_bits!(SOUND_BANK_GENERAL,  0x56, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_STAR_APPEARS:             SoundBits = /* 0x3057FF90 */ sound_bits!(SOUND_BANK_GENERAL,  0x57, 0xFF, [SOUND_LOWER_BACKGROUND_MUSIC, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_COLLECT_1UP:              SoundBits = /* 0x3058FF80 */ sound_bits!(SOUND_BANK_GENERAL,  0x58, 0xFF, [SOUND_DISCRETE]);
pub const SOUND_GENERAL_BUTTON_PRESS_LOWPRIO:     SoundBits = /* 0x305A0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5A, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BUTTON_PRESS:             SoundBits = /* 0x305A4080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5A, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BUTTON_PRESS_2_LOWPRIO:   SoundBits = /* 0x315A0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5A, 0x00, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BUTTON_PRESS_2:           SoundBits = /* 0x315A4080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5A, 0x40, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_ELEVATOR_MOVE:            SoundBits = /* 0x305B0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5B, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_ELEVATOR_MOVE_2:          SoundBits = /* 0x315B0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5B, 0x00, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_SWISH_AIR:                SoundBits = /* 0x305C0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5C, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_SWISH_AIR_2:              SoundBits = /* 0x315C0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5C, 0x00, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_HAUNTED_CHAIR:            SoundBits = /* 0x305D0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5D, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_SOFT_LANDING:             SoundBits = /* 0x305E0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5E, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_HAUNTED_CHAIR_MOVE:       SoundBits = /* 0x305F0080 */ sound_bits!(SOUND_BANK_GENERAL,  0x5F, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOWSER_PLATFORM:          SoundBits = /* 0x30628080 */ sound_bits!(SOUND_BANK_GENERAL,  0x62, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOWSER_PLATFORM_2:        SoundBits = /* 0x31628080 */ sound_bits!(SOUND_BANK_GENERAL,  0x62, 0x80, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_HEART_SPIN:               SoundBits = /* 0x3064C080 */ sound_bits!(SOUND_BANK_GENERAL,  0x64, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_POUND_WOOD_POST:          SoundBits = /* 0x3065C080 */ sound_bits!(SOUND_BANK_GENERAL,  0x65, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_WATER_LEVEL_TRIG:         SoundBits = /* 0x30668080 */ sound_bits!(SOUND_BANK_GENERAL,  0x66, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_SWITCH_DOOR_OPEN:         SoundBits = /* 0x3067A080 */ sound_bits!(SOUND_BANK_GENERAL,  0x67, 0xA0, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_RED_COIN:                 SoundBits = /* 0x30689080 */ sound_bits!(SOUND_BANK_GENERAL,  0x68, 0x90, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BIRDS_FLY_AWAY:           SoundBits = /* 0x30690080 */ sound_bits!(SOUND_BANK_GENERAL,  0x69, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_METAL_POUND:              SoundBits = /* 0x306B8080 */ sound_bits!(SOUND_BANK_GENERAL,  0x6B, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOING1:                   SoundBits = /* 0x306C4080 */ sound_bits!(SOUND_BANK_GENERAL,  0x6C, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOING2_LOWPRIO:           SoundBits = /* 0x306D2080 */ sound_bits!(SOUND_BANK_GENERAL,  0x6D, 0x20, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOING2:                   SoundBits = /* 0x306D4080 */ sound_bits!(SOUND_BANK_GENERAL,  0x6D, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_YOSHI_WALK:               SoundBits = /* 0x306E2080 */ sound_bits!(SOUND_BANK_GENERAL,  0x6E, 0x20, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_ENEMY_ALERT1:             SoundBits = /* 0x306F3080 */ sound_bits!(SOUND_BANK_GENERAL,  0x6F, 0x30, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_YOSHI_TALK:               SoundBits = /* 0x30703080 */ sound_bits!(SOUND_BANK_GENERAL,  0x70, 0x30, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_SPLATTERING:              SoundBits = /* 0x30713080 */ sound_bits!(SOUND_BANK_GENERAL,  0x71, 0x30, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOING3:                   SoundBits = /* 0x30720000 */ sound_bits!(SOUND_BANK_GENERAL,  0x72, 0x00, []); // unverified, unused
pub const SOUND_GENERAL_GRAND_STAR:               SoundBits = /* 0x30730080 */ sound_bits!(SOUND_BANK_GENERAL,  0x73, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_GRAND_STAR_JUMP:          SoundBits = /* 0x30740080 */ sound_bits!(SOUND_BANK_GENERAL,  0x74, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_BOAT_ROCK:                SoundBits = /* 0x30750080 */ sound_bits!(SOUND_BANK_GENERAL,  0x75, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_GENERAL_VANISH_SFX:               SoundBits = /* 0x30762080 */ sound_bits!(SOUND_BANK_GENERAL,  0x76, 0x20, [SOUND_DISCRETE]); // unverified

//// Environment Sound Effects
pub const SOUND_ENV_WATERFALL1:                 SoundBits = /* 0x40000000 */ sound_bits!(SOUND_BANK_ENV,      0x00, 0x00, []); // unverified
pub const SOUND_ENV_WATERFALL2:                 SoundBits = /* 0x40010000 */ sound_bits!(SOUND_BANK_ENV,      0x01, 0x00, []); // unverified
pub const SOUND_ENV_ELEVATOR1:                  SoundBits = /* 0x40020000 */ sound_bits!(SOUND_BANK_ENV,      0x02, 0x00, []); // unverified
pub const SOUND_ENV_DRONING1:                   SoundBits = /* 0x41030000 */ sound_bits!(SOUND_BANK_ENV,      0x03, 0x00, [SOUND_NO_VOLUME_LOSS]); // unverified
pub const SOUND_ENV_DRONING2:                   SoundBits = /* 0x40040000 */ sound_bits!(SOUND_BANK_ENV,      0x04, 0x00, []); // unverified
pub const SOUND_ENV_WIND1:                      SoundBits = /* 0x40050000 */ sound_bits!(SOUND_BANK_ENV,      0x05, 0x00, []); // unverified
pub const SOUND_ENV_MOVING_SAND_SNOW:           SoundBits = /* 0x40060000 */ sound_bits!(SOUND_BANK_ENV,      0x06, 0x00, []); // unverified, unused
pub const SOUND_ENV_UNK07:                      SoundBits = /* 0x40070000 */ sound_bits!(SOUND_BANK_ENV,      0x07, 0x00, []); // unverified, unused
pub const SOUND_ENV_ELEVATOR2:                  SoundBits = /* 0x40080000 */ sound_bits!(SOUND_BANK_ENV,      0x08, 0x00, []); // unverified
pub const SOUND_ENV_WATER:                      SoundBits = /* 0x40090000 */ sound_bits!(SOUND_BANK_ENV,      0x09, 0x00, []); // unverified
pub const SOUND_ENV_UNKNOWN2:                   SoundBits = /* 0x400A0000 */ sound_bits!(SOUND_BANK_ENV,      0x0A, 0x00, []); // unverified
pub const SOUND_ENV_BOAT_ROCKING1:              SoundBits = /* 0x400B0000 */ sound_bits!(SOUND_BANK_ENV,      0x0B, 0x00, []); // unverified
pub const SOUND_ENV_ELEVATOR3:                  SoundBits = /* 0x400C0000 */ sound_bits!(SOUND_BANK_ENV,      0x0C, 0x00, []); // unverified
pub const SOUND_ENV_ELEVATOR4:                  SoundBits = /* 0x400D0000 */ sound_bits!(SOUND_BANK_ENV,      0x0D, 0x00, []); // unverified
pub const SOUND_ENV_ELEVATOR4_2:                SoundBits = /* 0x410D0000 */ sound_bits!(SOUND_BANK_ENV,      0x0D, 0x00, [SOUND_NO_VOLUME_LOSS]); // unverified
pub const SOUND_ENV_MOVINGSAND:                 SoundBits = /* 0x400E0000 */ sound_bits!(SOUND_BANK_ENV,      0x0E, 0x00, []); // unverified
pub const SOUND_ENV_MERRY_GO_ROUND_CREAKING:    SoundBits = /* 0x400F4000 */ sound_bits!(SOUND_BANK_ENV,      0x0F, 0x40, []); // unverified
pub const SOUND_ENV_WIND2:                      SoundBits = /* 0x40108000 */ sound_bits!(SOUND_BANK_ENV,      0x10, 0x80, []); // unverified
pub const SOUND_ENV_UNK12:                      SoundBits = /* 0x40120000 */ sound_bits!(SOUND_BANK_ENV,      0x12, 0x00, []); // unverified, unused
pub const SOUND_ENV_SLIDING:                    SoundBits = /* 0x40130000 */ sound_bits!(SOUND_BANK_ENV,      0x13, 0x00, []); // unverified
pub const SOUND_ENV_STAR:                       SoundBits = /* 0x40140010 */ sound_bits!(SOUND_BANK_ENV,      0x14, 0x00, [SOUND_LOWER_BACKGROUND_MUSIC]); // unverified
pub const SOUND_ENV_MOVING_BIG_PLATFORM:        SoundBits = /* 0x41150000 */ sound_bits!(SOUND_BANK_ENV,      0x15, 0x00, [SOUND_NO_VOLUME_LOSS]); // unverified
pub const SOUND_ENV_WATER_DRAIN:                SoundBits = /* 0x41160000 */ sound_bits!(SOUND_BANK_ENV,      0x16, 0x00, [SOUND_NO_VOLUME_LOSS]); // unverified
pub const SOUND_ENV_METAL_BOX_PUSH:             SoundBits = /* 0x40178000 */ sound_bits!(SOUND_BANK_ENV,      0x17, 0x80, []); // unverified
pub const SOUND_ENV_SINK_QUICKSAND:             SoundBits = /* 0x40188000 */ sound_bits!(SOUND_BANK_ENV,      0x18, 0x80, []); // unverified

//// Object Sound Effects
pub const SOUND_OBJ_SUSHI_SHARK_WATER_SOUND:    SoundBits = /* 0x50008080 */ sound_bits!(SOUND_BANK_OBJ,      0x00, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ_MRI_SHOOT:                  SoundBits = /* 0x50010080 */ sound_bits!(SOUND_BANK_OBJ,      0x01, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BABY_PENGUIN_WALK:          SoundBits = /* 0x50020080 */ sound_bits!(SOUND_BANK_OBJ,      0x02, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BOWSER_WALK:                SoundBits = /* 0x50030080 */ sound_bits!(SOUND_BANK_OBJ,      0x03, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BOWSER_TAIL_PICKUP:         SoundBits = /* 0x50050080 */ sound_bits!(SOUND_BANK_OBJ,      0x05, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BOWSER_DEFEATED:            SoundBits = /* 0x50060080 */ sound_bits!(SOUND_BANK_OBJ,      0x06, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BOWSER_SPINNING:            SoundBits = /* 0x50070080 */ sound_bits!(SOUND_BANK_OBJ,      0x07, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BOWSER_INHALING:            SoundBits = /* 0x50080080 */ sound_bits!(SOUND_BANK_OBJ,      0x08, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BIG_PENGUIN_WALK:           SoundBits = /* 0x50098080 */ sound_bits!(SOUND_BANK_OBJ,      0x09, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BOO_BOUNCE_TOP:             SoundBits = /* 0x500A0080 */ sound_bits!(SOUND_BANK_OBJ,      0x0A, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BOO_LAUGH_SHORT:            SoundBits = /* 0x500B0080 */ sound_bits!(SOUND_BANK_OBJ,      0x0B, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_THWOMP:                     SoundBits = /* 0x500CA080 */ sound_bits!(SOUND_BANK_OBJ,      0x0C, 0xA0, [SOUND_DISCRETE]);
pub const SOUND_OBJ_CANNON1:                    SoundBits = /* 0x500DF080 */ sound_bits!(SOUND_BANK_OBJ,      0x0D, 0xF0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_CANNON2:                    SoundBits = /* 0x500EF080 */ sound_bits!(SOUND_BANK_OBJ,      0x0E, 0xF0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_CANNON3:                    SoundBits = /* 0x500FF080 */ sound_bits!(SOUND_BANK_OBJ,      0x0F, 0xF0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_JUMP_WALK_WATER:            SoundBits = /* 0x50120000 */ sound_bits!(SOUND_BANK_OBJ,      0x12, 0x00, []); // unverified, unused
pub const SOUND_OBJ_UNKNOWN2:                   SoundBits = /* 0x50130080 */ sound_bits!(SOUND_BANK_OBJ,      0x13, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_MRI_DEATH:                  SoundBits = /* 0x50140080 */ sound_bits!(SOUND_BANK_OBJ,      0x14, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_POUNDING1:                  SoundBits = /* 0x50155080 */ sound_bits!(SOUND_BANK_OBJ,      0x15, 0x50, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_POUNDING1_HIGHPRIO:         SoundBits = /* 0x50158080 */ sound_bits!(SOUND_BANK_OBJ,      0x15, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_WHOMP_LOWPRIO:              SoundBits = /* 0x50166080 */ sound_bits!(SOUND_BANK_OBJ,      0x16, 0x60, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KING_BOBOMB:                SoundBits = /* 0x50168080 */ sound_bits!(SOUND_BANK_OBJ,      0x16, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BULLY_METAL:                SoundBits = /* 0x50178080 */ sound_bits!(SOUND_BANK_OBJ,      0x17, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_BULLY_EXPLODE:              SoundBits = /* 0x5018A080 */ sound_bits!(SOUND_BANK_OBJ,      0x18, 0xA0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_BULLY_EXPLODE_2:            SoundBits = /* 0x5118A080 */ sound_bits!(SOUND_BANK_OBJ,      0x18, 0xA0, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_POUNDING_CANNON:            SoundBits = /* 0x501A5080 */ sound_bits!(SOUND_BANK_OBJ,      0x1A, 0x50, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_BULLY_WALK:                 SoundBits = /* 0x501B3080 */ sound_bits!(SOUND_BANK_OBJ,      0x1B, 0x30, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_UNKNOWN3:                   SoundBits = /* 0x501D8080 */ sound_bits!(SOUND_BANK_OBJ,      0x1D, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_UNKNOWN4:                   SoundBits = /* 0x501EA080 */ sound_bits!(SOUND_BANK_OBJ,      0x1E, 0xA0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_BABY_PENGUIN_DIVE:          SoundBits = /* 0x501F4080 */ sound_bits!(SOUND_BANK_OBJ,      0x1F, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ_GOOMBA_WALK:                SoundBits = /* 0x50200080 */ sound_bits!(SOUND_BANK_OBJ,      0x20, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_UKIKI_CHATTER_LONG:         SoundBits = /* 0x50210080 */ sound_bits!(SOUND_BANK_OBJ,      0x21, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_MONTY_MOLE_ATTACK:          SoundBits = /* 0x50220080 */ sound_bits!(SOUND_BANK_OBJ,      0x22, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_EVIL_LAKITU_THROW:          SoundBits = /* 0x50222080 */ sound_bits!(SOUND_BANK_OBJ,      0x22, 0x20, [SOUND_DISCRETE]);
pub const SOUND_OBJ_UNK23:                      SoundBits = /* 0x50230000 */ sound_bits!(SOUND_BANK_OBJ,      0x23, 0x00, []); // unverified, unused
pub const SOUND_OBJ_DYING_ENEMY1:               SoundBits = /* 0x50244080 */ sound_bits!(SOUND_BANK_OBJ,      0x24, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ_CANNON4:                    SoundBits = /* 0x50254080 */ sound_bits!(SOUND_BANK_OBJ,      0x25, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_DYING_ENEMY2:               SoundBits = /* 0x50260000 */ sound_bits!(SOUND_BANK_OBJ,      0x26, 0x00, []); // unverified, unused
pub const SOUND_OBJ_BOBOMB_WALK:                SoundBits = /* 0x50270080 */ sound_bits!(SOUND_BANK_OBJ,      0x27, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_SOMETHING_LANDING:          SoundBits = /* 0x50288080 */ sound_bits!(SOUND_BANK_OBJ,      0x28, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_DIVING_IN_WATER:            SoundBits = /* 0x5029A080 */ sound_bits!(SOUND_BANK_OBJ,      0x29, 0xA0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_SNOW_SAND1:                 SoundBits = /* 0x502A0080 */ sound_bits!(SOUND_BANK_OBJ,      0x2A, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_SNOW_SAND2:                 SoundBits = /* 0x502B0080 */ sound_bits!(SOUND_BANK_OBJ,      0x2B, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_DEFAULT_DEATH:              SoundBits = /* 0x502C8080 */ sound_bits!(SOUND_BANK_OBJ,      0x2C, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BIG_PENGUIN_YELL:           SoundBits = /* 0x502D0080 */ sound_bits!(SOUND_BANK_OBJ,      0x2D, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_WATER_BOMB_BOUNCING:        SoundBits = /* 0x502E8080 */ sound_bits!(SOUND_BANK_OBJ,      0x2E, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ_GOOMBA_ALERT:               SoundBits = /* 0x502F0080 */ sound_bits!(SOUND_BANK_OBJ,      0x2F, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_WIGGLER_JUMP:               SoundBits = /* 0x502F6080 */ sound_bits!(SOUND_BANK_OBJ,      0x2F, 0x60, [SOUND_DISCRETE]);
pub const SOUND_OBJ_STOMPED:                    SoundBits = /* 0x50308080 */ sound_bits!(SOUND_BANK_OBJ,      0x30, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_UNKNOWN6:                   SoundBits = /* 0x50310080 */ sound_bits!(SOUND_BANK_OBJ,      0x31, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_DIVING_INTO_WATER:          SoundBits = /* 0x50324080 */ sound_bits!(SOUND_BANK_OBJ,      0x32, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_PIRANHA_PLANT_SHRINK:       SoundBits = /* 0x50334080 */ sound_bits!(SOUND_BANK_OBJ,      0x33, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KOOPA_THE_QUICK_WALK:       SoundBits = /* 0x50342080 */ sound_bits!(SOUND_BANK_OBJ,      0x34, 0x20, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KOOPA_WALK:                 SoundBits = /* 0x50350080 */ sound_bits!(SOUND_BANK_OBJ,      0x35, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BULLY_WALKING:              SoundBits = /* 0x50366080 */ sound_bits!(SOUND_BANK_OBJ,      0x36, 0x60, [SOUND_DISCRETE]);
pub const SOUND_OBJ_DORRIE:                     SoundBits = /* 0x50376080 */ sound_bits!(SOUND_BANK_OBJ,      0x37, 0x60, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BOWSER_LAUGH:               SoundBits = /* 0x50388080 */ sound_bits!(SOUND_BANK_OBJ,      0x38, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ_UKIKI_CHATTER_SHORT:        SoundBits = /* 0x50390080 */ sound_bits!(SOUND_BANK_OBJ,      0x39, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_UKIKI_CHATTER_IDLE:         SoundBits = /* 0x503A0080 */ sound_bits!(SOUND_BANK_OBJ,      0x3A, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_UKIKI_STEP_DEFAULT:         SoundBits = /* 0x503B0080 */ sound_bits!(SOUND_BANK_OBJ,      0x3B, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_UKIKI_STEP_LEAVES:          SoundBits = /* 0x503C0080 */ sound_bits!(SOUND_BANK_OBJ,      0x3C, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KOOPA_TALK:                 SoundBits = /* 0x503DA080 */ sound_bits!(SOUND_BANK_OBJ,      0x3D, 0xA0, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KOOPA_DAMAGE:               SoundBits = /* 0x503EA080 */ sound_bits!(SOUND_BANK_OBJ,      0x3E, 0xA0, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KLEPTO1:                    SoundBits = /* 0x503F4080 */ sound_bits!(SOUND_BANK_OBJ,      0x3F, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_KLEPTO2:                    SoundBits = /* 0x50406080 */ sound_bits!(SOUND_BANK_OBJ,      0x40, 0x60, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_KING_BOBOMB_TALK:           SoundBits = /* 0x50410080 */ sound_bits!(SOUND_BANK_OBJ,      0x41, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KING_BOBOMB_JUMP:           SoundBits = /* 0x50468080 */ sound_bits!(SOUND_BANK_OBJ,      0x46, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KING_WHOMP_DEATH:           SoundBits = /* 0x5147C080 */ sound_bits!(SOUND_BANK_OBJ,      0x47, 0xC0, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]);
pub const SOUND_OBJ_BOO_LAUGH_LONG:             SoundBits = /* 0x50480080 */ sound_bits!(SOUND_BANK_OBJ,      0x48, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_EEL:                        SoundBits = /* 0x504A0080 */ sound_bits!(SOUND_BANK_OBJ,      0x4A, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_EEL_2:                      SoundBits = /* 0x524A0080 */ sound_bits!(SOUND_BANK_OBJ,      0x4A, 0x00, [SOUND_VIBRATO, SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_EYEROK_SHOW_EYE:            SoundBits = /* 0x524B0080 */ sound_bits!(SOUND_BANK_OBJ,      0x4B, 0x00, [SOUND_VIBRATO, SOUND_DISCRETE]);
pub const SOUND_OBJ_MR_BLIZZARD_ALERT:          SoundBits = /* 0x504C0080 */ sound_bits!(SOUND_BANK_OBJ,      0x4C, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_SNUFIT_SHOOT:               SoundBits = /* 0x504D0080 */ sound_bits!(SOUND_BANK_OBJ,      0x4D, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_SKEETER_WALK:               SoundBits = /* 0x504E0080 */ sound_bits!(SOUND_BANK_OBJ,      0x4E, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_WALKING_WATER:              SoundBits = /* 0x504F0080 */ sound_bits!(SOUND_BANK_OBJ,      0x4F, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_BIRD_CHIRP3:                SoundBits = /* 0x50514000 */ sound_bits!(SOUND_BANK_OBJ,      0x51, 0x40, []);
pub const SOUND_OBJ_PIRANHA_PLANT_APPEAR:       SoundBits = /* 0x50542080 */ sound_bits!(SOUND_BANK_OBJ,      0x54, 0x20, [SOUND_DISCRETE]);
pub const SOUND_OBJ_FLAME_BLOWN:                SoundBits = /* 0x50558080 */ sound_bits!(SOUND_BANK_OBJ,      0x55, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ_MAD_PIANO_CHOMPING:         SoundBits = /* 0x52564080 */ sound_bits!(SOUND_BANK_OBJ,      0x56, 0x40, [SOUND_VIBRATO, SOUND_DISCRETE]);
pub const SOUND_OBJ_BOBOMB_BUDDY_TALK:          SoundBits = /* 0x50584080 */ sound_bits!(SOUND_BANK_OBJ,      0x58, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ_SPINY_UNK59:                SoundBits = /* 0x50591080 */ sound_bits!(SOUND_BANK_OBJ,      0x59, 0x10, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_WIGGLER_HIGH_PITCH:         SoundBits = /* 0x505C4080 */ sound_bits!(SOUND_BANK_OBJ,      0x5C, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ_HEAVEHO_TOSSED:             SoundBits = /* 0x505D4080 */ sound_bits!(SOUND_BANK_OBJ,      0x5D, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ_WIGGLER_DEATH:              SoundBits = /* 0x505E0000 */ sound_bits!(SOUND_BANK_OBJ,      0x5E, 0x00, []); // unverified, unused
pub const SOUND_OBJ_BOWSER_INTRO_LAUGH:         SoundBits = /* 0x505F8090 */ sound_bits!(SOUND_BANK_OBJ,      0x5F, 0x80, [SOUND_LOWER_BACKGROUND_MUSIC, SOUND_DISCRETE]);
pub const SOUND_OBJ_ENEMY_DEATH_HIGH:           SoundBits = /* 0x5060B080 */ sound_bits!(SOUND_BANK_OBJ,      0x60, 0xB0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_ENEMY_DEATH_LOW:            SoundBits = /* 0x5061B080 */ sound_bits!(SOUND_BANK_OBJ,      0x61, 0xB0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_SWOOP_DEATH:                SoundBits = /* 0x5062B080 */ sound_bits!(SOUND_BANK_OBJ,      0x62, 0xB0, [SOUND_DISCRETE]);
pub const SOUND_OBJ_KOOPA_FLYGUY_DEATH:         SoundBits = /* 0x5063B080 */ sound_bits!(SOUND_BANK_OBJ,      0x63, 0xB0, [SOUND_DISCRETE]);
pub const SOUND_OBJ_POKEY_DEATH:                SoundBits = /* 0x5063C080 */ sound_bits!(SOUND_BANK_OBJ,      0x63, 0xC0, [SOUND_DISCRETE]);
pub const SOUND_OBJ_SNOWMAN_BOUNCE:             SoundBits = /* 0x5064C080 */ sound_bits!(SOUND_BANK_OBJ,      0x64, 0xC0, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_SNOWMAN_EXPLODE:            SoundBits = /* 0x5065D080 */ sound_bits!(SOUND_BANK_OBJ,      0x65, 0xD0, [SOUND_DISCRETE]);
pub const SOUND_OBJ_POUNDING_LOUD:              SoundBits = /* 0x50684080 */ sound_bits!(SOUND_BANK_OBJ,      0x68, 0x40, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_MIPS_RABBIT:                SoundBits = /* 0x506A0080 */ sound_bits!(SOUND_BANK_OBJ,      0x6A, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_MIPS_RABBIT_WATER:          SoundBits = /* 0x506C0080 */ sound_bits!(SOUND_BANK_OBJ,      0x6C, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_OBJ_EYEROK_EXPLODE:             SoundBits = /* 0x506D0080 */ sound_bits!(SOUND_BANK_OBJ,      0x6D, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_CHUCKYA_DEATH:              SoundBits = /* 0x516E0080 */ sound_bits!(SOUND_BANK_OBJ,      0x6E, 0x00, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]);
pub const SOUND_OBJ_WIGGLER_TALK:               SoundBits = /* 0x506F0080 */ sound_bits!(SOUND_BANK_OBJ,      0x6F, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ_WIGGLER_ATTACKED:           SoundBits = /* 0x50706080 */ sound_bits!(SOUND_BANK_OBJ,      0x70, 0x60, [SOUND_DISCRETE]);
pub const SOUND_OBJ_WIGGLER_LOW_PITCH:          SoundBits = /* 0x50712080 */ sound_bits!(SOUND_BANK_OBJ,      0x71, 0x20, [SOUND_DISCRETE]);
pub const SOUND_OBJ_SNUFIT_SKEETER_DEATH:       SoundBits = /* 0x5072C080 */ sound_bits!(SOUND_BANK_OBJ,      0x72, 0xC0, [SOUND_DISCRETE]);
pub const SOUND_OBJ_BUBBA_CHOMP:                SoundBits = /* 0x50734080 */ sound_bits!(SOUND_BANK_OBJ,      0x73, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ_ENEMY_DEFEAT_SHRINK:        SoundBits = /* 0x50744080 */ sound_bits!(SOUND_BANK_OBJ,      0x74, 0x40, [SOUND_DISCRETE]);

pub const SOUND_AIR_BOWSER_SPIT_FIRE:           SoundBits = /* 0x60000000 */ sound_bits!(SOUND_BANK_AIR,      0x00, 0x00, []);
pub const SOUND_AIR_UNK01:                      SoundBits = /* 0x60010000 */ sound_bits!(SOUND_BANK_AIR,      0x01, 0x00, []); // ?, unused
pub const SOUND_AIR_LAKITU_FLY:                 SoundBits = /* 0x60028000 */ sound_bits!(SOUND_BANK_AIR,      0x02, 0x80, []);
pub const SOUND_AIR_LAKITU_FLY_HIGHPRIO:        SoundBits = /* 0x6002FF00 */ sound_bits!(SOUND_BANK_AIR,      0x02, 0xFF, []);
pub const SOUND_AIR_AMP_BUZZ:                   SoundBits = /* 0x60034000 */ sound_bits!(SOUND_BANK_AIR,      0x03, 0x40, []);
pub const SOUND_AIR_BLOW_FIRE:                  SoundBits = /* 0x60048000 */ sound_bits!(SOUND_BANK_AIR,      0x04, 0x80, []);
pub const SOUND_AIR_BLOW_WIND:                  SoundBits = /* 0x60044000 */ sound_bits!(SOUND_BANK_AIR,      0x04, 0x40, []);
pub const SOUND_AIR_ROUGH_SLIDE:                SoundBits = /* 0x60050000 */ sound_bits!(SOUND_BANK_AIR,      0x05, 0x00, []);
pub const SOUND_AIR_HEAVEHO_MOVE:               SoundBits = /* 0x60064000 */ sound_bits!(SOUND_BANK_AIR,      0x06, 0x40, []);
pub const SOUND_AIR_UNK07:                      SoundBits = /* 0x60070000 */ sound_bits!(SOUND_BANK_AIR,      0x07, 0x00, []); // ?, unused
pub const SOUND_AIR_BOBOMB_LIT_FUSE:            SoundBits = /* 0x60086000 */ sound_bits!(SOUND_BANK_AIR,      0x08, 0x60, []);
pub const SOUND_AIR_HOWLING_WIND:               SoundBits = /* 0x60098000 */ sound_bits!(SOUND_BANK_AIR,      0x09, 0x80, []);
pub const SOUND_AIR_CHUCKYA_MOVE:               SoundBits = /* 0x600A4000 */ sound_bits!(SOUND_BANK_AIR,      0x0A, 0x40, []);
pub const SOUND_AIR_PEACH_TWINKLE:              SoundBits = /* 0x600B4000 */ sound_bits!(SOUND_BANK_AIR,      0x0B, 0x40, []);
pub const SOUND_AIR_CASTLE_OUTDOORS_AMBIENT:    SoundBits = /* 0x60104000 */ sound_bits!(SOUND_BANK_AIR,      0x10, 0x40, []);

//// Menu Sound Effects
pub const SOUND_MENU_CHANGE_SELECT:             SoundBits = /* 0x7000F880 */ sound_bits!(SOUND_BANK_MENU,     0x00, 0xF8, [SOUND_DISCRETE]);
pub const SOUND_MENU_REVERSE_PAUSE:             SoundBits = /* 0x70010000 */ sound_bits!(SOUND_BANK_MENU,     0x01, 0x00, []); // unverified, unused
pub const SOUND_MENU_PAUSE:                     SoundBits = /* 0x7002F080 */ sound_bits!(SOUND_BANK_MENU,     0x02, 0xF0, [SOUND_DISCRETE]);
pub const SOUND_MENU_PAUSE_HIGHPRIO:            SoundBits = /* 0x7002FF80 */ sound_bits!(SOUND_BANK_MENU,     0x02, 0xFF, [SOUND_DISCRETE]);
pub const SOUND_MENU_PAUSE_2:                   SoundBits = /* 0x7003FF80 */ sound_bits!(SOUND_BANK_MENU,     0x03, 0xFF, [SOUND_DISCRETE]);
pub const SOUND_MENU_MESSAGE_APPEAR:            SoundBits = /* 0x70040080 */ sound_bits!(SOUND_BANK_MENU,     0x04, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_MESSAGE_DISAPPEAR:         SoundBits = /* 0x70050080 */ sound_bits!(SOUND_BANK_MENU,     0x05, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_CAMERA_ZOOM_IN:            SoundBits = /* 0x70060080 */ sound_bits!(SOUND_BANK_MENU,     0x06, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_CAMERA_ZOOM_OUT:           SoundBits = /* 0x70070080 */ sound_bits!(SOUND_BANK_MENU,     0x07, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_PINCH_MARIO_FACE:          SoundBits = /* 0x70080080 */ sound_bits!(SOUND_BANK_MENU,     0x08, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_LET_GO_MARIO_FACE:         SoundBits = /* 0x70090080 */ sound_bits!(SOUND_BANK_MENU,     0x09, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_HAND_APPEAR:               SoundBits = /* 0x700A0080 */ sound_bits!(SOUND_BANK_MENU,     0x0A, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_HAND_DISAPPEAR:            SoundBits = /* 0x700B0080 */ sound_bits!(SOUND_BANK_MENU,     0x0B, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_UNK0C:                     SoundBits = /* 0x700C0080 */ sound_bits!(SOUND_BANK_MENU,     0x0C, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_MENU_POWER_METER:               SoundBits = /* 0x700D0080 */ sound_bits!(SOUND_BANK_MENU,     0x0D, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_MENU_CAMERA_BUZZ:               SoundBits = /* 0x700E0080 */ sound_bits!(SOUND_BANK_MENU,     0x0E, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_CAMERA_TURN:               SoundBits = /* 0x700F0080 */ sound_bits!(SOUND_BANK_MENU,     0x0F, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_UNK10:                     SoundBits = /* 0x70100000 */ sound_bits!(SOUND_BANK_MENU,     0x10, 0x00, []); // unverified, unused
pub const SOUND_MENU_CLICK_FILE_SELECT:         SoundBits = /* 0x70110080 */ sound_bits!(SOUND_BANK_MENU,     0x11, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_MESSAGE_NEXT_PAGE:         SoundBits = /* 0x70130080 */ sound_bits!(SOUND_BANK_MENU,     0x13, 0x00, [SOUND_DISCRETE]); // unverified
pub const SOUND_MENU_COIN_ITS_A_ME_MARIO:       SoundBits = /* 0x70140080 */ sound_bits!(SOUND_BANK_MENU,     0x14, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_YOSHI_GAIN_LIVES:          SoundBits = /* 0x70150080 */ sound_bits!(SOUND_BANK_MENU,     0x15, 0x00, [SOUND_DISCRETE]);
pub const SOUND_MENU_ENTER_PIPE:                SoundBits = /* 0x7016A080 */ sound_bits!(SOUND_BANK_MENU,     0x16, 0xA0, [SOUND_DISCRETE]);
pub const SOUND_MENU_EXIT_PIPE:                 SoundBits = /* 0x7017A080 */ sound_bits!(SOUND_BANK_MENU,     0x17, 0xA0, [SOUND_DISCRETE]);
pub const SOUND_MENU_BOWSER_LAUGH:              SoundBits = /* 0x70188080 */ sound_bits!(SOUND_BANK_MENU,     0x18, 0x80, [SOUND_DISCRETE]);
pub const SOUND_MENU_ENTER_HOLE:                SoundBits = /* 0x71198080 */ sound_bits!(SOUND_BANK_MENU,     0x19, 0x80, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]);
pub const SOUND_MENU_CLICK_CHANGE_VIEW:         SoundBits = /* 0x701A8080 */ sound_bits!(SOUND_BANK_MENU,     0x1A, 0x80, [SOUND_DISCRETE]); // unverified
pub const SOUND_MENU_CAMERA_UNUSED1:            SoundBits = /* 0x701B0000 */ sound_bits!(SOUND_BANK_MENU,     0x1B, 0x00, []); // unverified, unused
pub const SOUND_MENU_CAMERA_UNUSED2:            SoundBits = /* 0x701C0000 */ sound_bits!(SOUND_BANK_MENU,     0x1C, 0x00, []); // unverified, unused
pub const SOUND_MENU_MARIO_CASTLE_WARP:         SoundBits = /* 0x701DB080 */ sound_bits!(SOUND_BANK_MENU,     0x1D, 0xB0, [SOUND_DISCRETE]); // unverified
pub const SOUND_MENU_STAR_SOUND:                SoundBits = /* 0x701EFF80 */ sound_bits!(SOUND_BANK_MENU,     0x1E, 0xFF, [SOUND_DISCRETE]);
pub const SOUND_MENU_THANK_YOU_PLAYING_MY_GAME: SoundBits = /* 0x701FFF80 */ sound_bits!(SOUND_BANK_MENU,     0x1F, 0xFF, [SOUND_DISCRETE]);
pub const SOUND_MENU_READ_A_SIGN:               SoundBits = /* 0x70200000 */ sound_bits!(SOUND_BANK_MENU,     0x20, 0x00, []); // unverified, unused
pub const SOUND_MENU_EXIT_A_SIGN:               SoundBits = /* 0x70210000 */ sound_bits!(SOUND_BANK_MENU,     0x21, 0x00, []); // unverified, unused
pub const SOUND_MENU_MARIO_CASTLE_WARP2:        SoundBits = /* 0x70222080 */ sound_bits!(SOUND_BANK_MENU,     0x22, 0x20, [SOUND_DISCRETE]); // unverified
pub const SOUND_MENU_STAR_SOUND_OKEY_DOKEY:     SoundBits = /* 0x7023FF80 */ sound_bits!(SOUND_BANK_MENU,     0x23, 0xFF, [SOUND_DISCRETE]);
pub const SOUND_MENU_STAR_SOUND_LETS_A_GO:      SoundBits = /* 0x7024FF80 */ sound_bits!(SOUND_BANK_MENU,     0x24, 0xFF, [SOUND_DISCRETE]);

// US/EU only; an index between 0-7 or 0-4 is added to the sound ID before
// playing, producing the same sound with different pitch.
pub const SOUND_MENU_COLLECT_RED_COIN:          SoundBits = /* 0x78289080 */ sound_bits!(SOUND_BANK_MENU,     0x28, 0x90, [SOUND_CONSTANT_FREQUENCY, SOUND_DISCRETE]);
pub const SOUND_MENU_COLLECT_SECRET:            SoundBits = /* 0x70302080 */ sound_bits!(SOUND_BANK_MENU,     0x30, 0x20, [SOUND_DISCRETE]);

// Channel 8 loads sounds from the same place as channel 3, making it possible
// to play two channel 3 sounds at once (since just one sound from each channel
// can play at a given time).
pub const SOUND_GENERAL2_BOBOMB_EXPLOSION:      SoundBits = /* 0x802E2080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x2E, 0x20, [SOUND_DISCRETE]);
pub const SOUND_GENERAL2_PURPLE_SWITCH:         SoundBits = /* 0x803EC080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x3E, 0xC0, [SOUND_DISCRETE]);
pub const SOUND_GENERAL2_ROTATING_BLOCK_CLICK:  SoundBits = /* 0x80400080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x40, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL2_SPINDEL_ROLL:          SoundBits = /* 0x80482080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x48, 0x20, [SOUND_DISCRETE]);
pub const SOUND_GENERAL2_PYRAMID_TOP_SPIN:      SoundBits = /* 0x814BE080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x4B, 0xE0, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]);
pub const SOUND_GENERAL2_PYRAMID_TOP_EXPLOSION: SoundBits = /* 0x814CF080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x4C, 0xF0, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]);
pub const SOUND_GENERAL2_BIRD_CHIRP2:           SoundBits = /* 0x80504000 */ sound_bits!(SOUND_BANK_GENERAL2, 0x50, 0x40, []);
pub const SOUND_GENERAL2_SWITCH_TICK_FAST:      SoundBits = /* 0x8054F010 */ sound_bits!(SOUND_BANK_GENERAL2, 0x54, 0xF0, [SOUND_LOWER_BACKGROUND_MUSIC]);
pub const SOUND_GENERAL2_SWITCH_TICK_SLOW:      SoundBits = /* 0x8055F010 */ sound_bits!(SOUND_BANK_GENERAL2, 0x55, 0xF0, [SOUND_LOWER_BACKGROUND_MUSIC]);
pub const SOUND_GENERAL2_STAR_APPEARS:          SoundBits = /* 0x8057FF90 */ sound_bits!(SOUND_BANK_GENERAL2, 0x57, 0xFF, [SOUND_LOWER_BACKGROUND_MUSIC, SOUND_DISCRETE]);
pub const SOUND_GENERAL2_ROTATING_BLOCK_ALERT:  SoundBits = /* 0x80590080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x59, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL2_BOWSER_EXPLODE:        SoundBits = /* 0x80600080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x60, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL2_BOWSER_KEY:            SoundBits = /* 0x80610080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x61, 0x00, [SOUND_DISCRETE]);
pub const SOUND_GENERAL2_1UP_APPEAR:            SoundBits = /* 0x8063D080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x63, 0xD0, [SOUND_DISCRETE]);
pub const SOUND_GENERAL2_RIGHT_ANSWER:          SoundBits = /* 0x806AA080 */ sound_bits!(SOUND_BANK_GENERAL2, 0x6A, 0xA0, [SOUND_DISCRETE]);

// Channel 9 loads sounds from the same place as channel 5.
pub const SOUND_OBJ2_BOWSER_ROAR:               SoundBits = /* 0x90040080 */ sound_bits!(SOUND_BANK_OBJ2,     0x04, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_PIRANHA_PLANT_BITE:        SoundBits = /* 0x90105080 */ sound_bits!(SOUND_BANK_OBJ2,     0x10, 0x50, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_PIRANHA_PLANT_DYING:       SoundBits = /* 0x90116080 */ sound_bits!(SOUND_BANK_OBJ2,     0x11, 0x60, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_BOWSER_PUZZLE_PIECE_MOVE:  SoundBits = /* 0x90192080 */ sound_bits!(SOUND_BANK_OBJ2,     0x19, 0x20, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_BULLY_ATTACKED:            SoundBits = /* 0x901C0080 */ sound_bits!(SOUND_BANK_OBJ2,     0x1C, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_KING_BOBOMB_DAMAGE:        SoundBits = /* 0x91424080 */ sound_bits!(SOUND_BANK_OBJ2,     0x42, 0x40, [SOUND_NO_VOLUME_LOSS, SOUND_DISCRETE]);
pub const SOUND_OBJ2_SCUTTLEBUG_WALK:           SoundBits = /* 0x90434080 */ sound_bits!(SOUND_BANK_OBJ2,     0x43, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_SCUTTLEBUG_ALERT:          SoundBits = /* 0x90444080 */ sound_bits!(SOUND_BANK_OBJ2,     0x44, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_BABY_PENGUIN_YELL:         SoundBits = /* 0x90450080 */ sound_bits!(SOUND_BANK_OBJ2,     0x45, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_SWOOP:                     SoundBits = /* 0x90490080 */ sound_bits!(SOUND_BANK_OBJ2,     0x49, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_BIRD_CHIRP1:               SoundBits = /* 0x90524000 */ sound_bits!(SOUND_BANK_OBJ2,     0x52, 0x40, []);
pub const SOUND_OBJ2_LARGE_BULLY_ATTACKED:      SoundBits = /* 0x90570080 */ sound_bits!(SOUND_BANK_OBJ2,     0x57, 0x00, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_EYEROK_SOUND_SHORT:        SoundBits = /* 0x935A0080 */ sound_bits!(SOUND_BANK_OBJ2,     0x5A, 0x00, [SOUND_NO_VOLUME_LOSS, SOUND_VIBRATO, SOUND_DISCRETE]);
pub const SOUND_OBJ2_WHOMP_SOUND_SHORT:         SoundBits = /* 0x935AC080 */ sound_bits!(SOUND_BANK_OBJ2,     0x5A, 0xC0, [SOUND_NO_VOLUME_LOSS, SOUND_VIBRATO, SOUND_DISCRETE]);
pub const SOUND_OBJ2_EYEROK_SOUND_LONG:         SoundBits = /* 0x925B0080 */ sound_bits!(SOUND_BANK_OBJ2,     0x5B, 0x00, [SOUND_VIBRATO, SOUND_DISCRETE]);
pub const SOUND_OBJ2_BOWSER_TELEPORT:           SoundBits = /* 0x90668080 */ sound_bits!(SOUND_BANK_OBJ2,     0x66, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_MONTY_MOLE_APPEAR:         SoundBits = /* 0x90678080 */ sound_bits!(SOUND_BANK_OBJ2,     0x67, 0x80, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_BOSS_DIALOG_GRUNT:         SoundBits = /* 0x90694080 */ sound_bits!(SOUND_BANK_OBJ2,     0x69, 0x40, [SOUND_DISCRETE]);
pub const SOUND_OBJ2_MRI_SPINNING:              SoundBits = /* 0x906B0080 */ sound_bits!(SOUND_BANK_OBJ2,     0x6B, 0x00, [SOUND_DISCRETE]);
