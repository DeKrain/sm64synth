#![allow(dead_code)]

pub const VAL_DIFF: u16 = 60000;

macro_rules! levels {
	/*(level: $level:ident) => {
		$crate::levels::levels!(stub: nop, level: $level)
	};
	(stub: $stub:ident, level: $level:ident) => {*/
	($levels:ident) => {
		$levels! {
			stub ("",               LEVEL_UNKNOWN_1,        COURSE_NONE,                                 20000,    0x00, 0x00, 0x00, _,         _)
			stub ("",               LEVEL_UNKNOWN_2,        COURSE_NONE,                                 20000,    0x00, 0x00, 0x00, _,         _)
			stub ("",               LEVEL_UNKNOWN_3,        COURSE_NONE,                                 20000,    0x00, 0x00, 0x00, _,         _)
			level("TERESA OBAKE",   LEVEL_BBH,              COURSE_BBH,      bbh,              spooky,   28000,    0x28, 0x28, 0x28, sDynBbh,   sCamBBH)
			level("YYAMA1 % YSLD1", LEVEL_CCM,              COURSE_CCM,      ccm,              snow,     17000,    0x10, 0x38, 0x38, _,         sCamCCM)
			level("SELECT ROOM",    LEVEL_CASTLE,           COURSE_NONE,     castle_inside,    inside,   20000,    0x20, 0x20, 0x30, _,         sCamCastle)
			level("HORROR DUNGEON", LEVEL_HMC,              COURSE_HMC,      hmc,              cave,     16000,    0x28, 0x28, 0x28, sDynHmc,   sCamHMC)
			level("SABAKU % PYRMD", LEVEL_SSL,              COURSE_SSL,      ssl,              generic,  15000,    0x08, 0x30, 0x30, _,         sCamSSL)
			level("BATTLE FIELD",   LEVEL_BOB,              COURSE_BOB,      bob,              generic,  15000,    0x08, 0x08, 0x08, _,         _)
			level("YUKIYAMA2",      LEVEL_SL,               COURSE_SL,       sl,               snow,     14000,    0x10, 0x28, 0x28, _,         sCamSL)
			level("POOL KAI",       LEVEL_WDW,              COURSE_WDW,      wdw,              grass,    17000,    0x10, 0x18, 0x18, sDynWdw,   _)
			level("WTDG % TINBOTU", LEVEL_JRB,              COURSE_JRB,      jrb,              water,    20000,    0x10, 0x18, 0x18, sDynJrb,   _)
			level("BIG WORLD",      LEVEL_THI,              COURSE_THI,      thi,              grass,    20000,    0x0c, 0x0c, 0x20, _,         sCamTHI)
			level("CLOCK TOWER",    LEVEL_TTC,              COURSE_TTC,      ttc,              machine,  18000,    0x18, 0x18, 0x18, _,         _)
			level("RAINBOW CRUISE", LEVEL_RR,               COURSE_RR,       rr,               sky,      20000,    0x20, 0x20, 0x20, _,         sCamRR)
			level("MAIN MAP",       LEVEL_CASTLE_GROUNDS,   COURSE_NONE,     castle_grounds,   outside,  25000,    0x08, 0x08, 0x08, _,         _)
			level("EXT1 YOKO SCRL", LEVEL_BITDW,            COURSE_BITDW,    bitdw,            sky,      16000,    0x28, 0x28, 0x28, _,         _)
			level("EXT7 HORI MINI", LEVEL_VCUTM,            COURSE_VCUTM,    vcutm,            outside,  30000,    0x28, 0x28, 0x28, _,         _)
			level("EXT2 TIKA LAVA", LEVEL_BITFS,            COURSE_BITFS,    bitfs,            sky,      16000,    0x28, 0x28, 0x28, _,         _)
			level("EXT9 SUISOU",    LEVEL_SA,               COURSE_SA,       sa,               inside,   20000,    0x10, 0x10, 0x10, _,         _)
			level("EXT3 HEAVEN",    LEVEL_BITS,             COURSE_BITS,     bits,             sky,      16000,    0x28, 0x28, 0x28, _,         _)
			level("FIREB1 % INVLC", LEVEL_LLL,              COURSE_LLL,      lll,              fire,     22000,    0x08, 0x30, 0x30, _,         _)
			level("WATER LAND",     LEVEL_DDD,              COURSE_DDD,      ddd,              water,    17000,    0x10, 0x20, 0x20, sDynDdd,   _)
			level("MOUNTAIN",       LEVEL_WF,               COURSE_WF,       wf,               grass,    13000,    0x08, 0x08, 0x08, _,         _)
			level("ENDING",         LEVEL_ENDING,           COURSE_CAKE_END, ending,           generic,  20000,    0x00, 0x00, 0x00, _,         _)
			level("URANIWA",        LEVEL_CASTLE_COURTYARD, COURSE_NONE,     castle_courtyard, outside,  20000,    0x08, 0x08, 0x08, _,         _)
			level("EXT4 MINI SLID", LEVEL_PSS,              COURSE_PSS,      pss,              mountain, 20000,    0x28, 0x28, 0x28, _,         _)
			level("IN THE FALL",    LEVEL_COTMC,            COURSE_COTMC,    cotmc,            cave,     18000,    0x28, 0x28, 0x28, _,         sCamCotMC)
			level("EXT6 MARIO FLY", LEVEL_TOTWC,            COURSE_TOTWC,    totwc,            sky,      20000,    0x20, 0x20, 0x20, _,         _)
			level("KUPPA1",         LEVEL_BOWSER_1,         COURSE_BITDW,    bowser_1,         generic,  VAL_DIFF, 0x40, 0x40, 0x40, _,         _)
			level("EXT8 BLUE SKY",  LEVEL_WMOTR,            COURSE_WMOTR,    wmotr,            generic,  20000,    0x28, 0x28, 0x28, _,         _)
			stub ("",               LEVEL_UNKNOWN_32,       COURSE_NONE,                                 20000,    0x70, 0x00, 0x00, _,         _)
			level("KUPPA2",         LEVEL_BOWSER_2,         COURSE_BITFS,    bowser_2,         fire,     VAL_DIFF, 0x40, 0x40, 0x40, _,         _)
			level("KUPPA3",         LEVEL_BOWSER_3,         COURSE_BITS,     bowser_3,         generic,  VAL_DIFF, 0x40, 0x40, 0x40, _,         _)
			stub ("",               LEVEL_UNKNOWN_35,       COURSE_NONE,                                 20000,    0x00, 0x00, 0x00, _,         _)
			level("DONKEY % SLID2", LEVEL_TTM,              COURSE_TTM,      ttm,              mountain, 15000,    0x08, 0x08, 0x08, _,         _)
			stub ("",               LEVEL_UNKNOWN_37,       COURSE_NONE,                                 20000,    0x00, 0x00, 0x00, _,         _)
			stub ("",               LEVEL_UNKNOWN_38,       COURSE_NONE,                                 20000,    0x00, 0x00, 0x00, sDynUnk38, _)
		}
	};
}

pub(crate) use levels;

macro_rules! lvls {
	(+[$($defs:tt)*]) => {
		enum _Levels {
			$($defs,)*
		}
		$(pub const $defs: usize = _Levels::$defs as usize;)*
		pub const LEVEL_MAX: usize = LEVEL_COUNT - 1;
	};
	(+[$($head:tt)*] ^ id $id:ident $($tail:tt)*) => (lvls!{+[$($head)* $id] $($tail)*});
	(+[$($head:tt)*] ^ stub ($_0:tt, $id:ident, $_2:tt, $_3:tt, $_4:tt, $_5:tt, $_6:tt, $_7:tt, $_8:tt) $($tail:tt)*) => (lvls!{+[$($head)*] ^ id $id $($tail)*});
	(+[$($head:tt)*] ^ level ($_0:tt, $id:ident, $_2:tt, $_3:tt, $_4:tt, $_5:tt, $_6:tt, $_7:tt, $_8:tt, $_9:tt, $_10:tt) $($tail:tt)*) => (lvls!{+[$($head)*] ^ id $id $($tail)*});
	[$(
		$type:ident $args:tt
	)*] => {
		lvls! {
			+[]
			^ id LEVEL_NONE
			$(^ $type $args)*
			^ id LEVEL_COUNT
		}
	};
}

levels!(lvls);


/*pub const LEVEL_COUNT: usize = {
	macro_rules! counter {
		[$($type:ident ($($args:tt)*))*] => ([(/* null level */), $({ nop! { $type } },)*]);
	}
	levels!(counter).len()
};*/
