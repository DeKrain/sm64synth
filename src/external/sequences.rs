macro_rules! seq {
	{$($(#[$attr:meta])* $seq:ident)*} => {
		#[repr(u8)]
		enum SeqId {
			$($seq,)*
		}
		$($(#[$attr])* pub const $seq: u8 = SeqId::$seq as u8;)*
	};
}
seq! {
	SEQ_SOUND_PLAYER
	SEQ_EVENT_CUTSCENE_COLLECT_STAR
	SEQ_MENU_TITLE_SCREEN
	SEQ_LEVEL_GRASS
	SEQ_LEVEL_INSIDE_CASTLE
	SEQ_LEVEL_WATER
	SEQ_LEVEL_HOT
	SEQ_LEVEL_BOSS_KOOPA
	SEQ_LEVEL_SNOW
	SEQ_LEVEL_SLIDE
	SEQ_LEVEL_SPOOKY
	SEQ_EVENT_PIRANHA_PLANT
	SEQ_LEVEL_UNDERGROUND
	SEQ_MENU_STAR_SELECT
	SEQ_EVENT_POWERUP
	SEQ_EVENT_METAL_CAP
	SEQ_EVENT_KOOPA_MESSAGE
	SEQ_LEVEL_KOOPA_ROAD
	SEQ_EVENT_HIGH_SCORE
	SEQ_EVENT_MERRY_GO_ROUND
	SEQ_EVENT_RACE
	SEQ_EVENT_CUTSCENE_STAR_SPAWN
	SEQ_EVENT_BOSS
	SEQ_EVENT_CUTSCENE_COLLECT_KEY
	SEQ_EVENT_ENDLESS_STAIRS
	SEQ_LEVEL_BOSS_KOOPA_FINAL
	SEQ_EVENT_CUTSCENE_CREDITS
	SEQ_EVENT_SOLVE_PUZZLE
	SEQ_EVENT_TOAD_MESSAGE
	SEQ_EVENT_PEACH_MESSAGE
	SEQ_EVENT_CUTSCENE_INTRO
	SEQ_EVENT_CUTSCENE_VICTORY
	SEQ_EVENT_CUTSCENE_ENDING
	SEQ_MENU_FILE_SELECT
	SEQ_EVENT_CUTSCENE_LAKITU

	/// Total number of sequences
	SEQ_COUNT
}

crate::support::static_assert!(SEQ_COUNT < 0x80, "Too many sequences");

/// The base sequence ID.
pub const SEQ_BASE_ID: u8 = 0x7F;

/// Flag indicating a variation of the base sequence.
pub const SEQ_VARIATION: u8 = 0x80;
