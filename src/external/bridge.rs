extern "C" {
	pub static gCurrAreaIndex: i16;

	pub static gCurrLevelNum: i16;

	pub static gMarioCurrentRoom: i16;

	pub fn gMarioState_getPosition<'a>() -> &'a [f32; 3];
}
