use crate::{
	structs::*,
	load::*,
	heap::*,
	data::*,
};

use std::ptr;

impl Note {
	pub unsafe fn set_vel_pan_reverb(&mut self, mut velocity: f32, pan: u8, reverbVol: u8) {
		use NoteSubEuFlags as SF;
		let sub = &mut self.noteSubEu;
		let pan = pan & 0x7F;

		let (volLeft, volRight);
		if sub.flags.contains(SF::StereoHeadsetEffects) && gSoundMode == SoundMode::Headset {
			let mut smallPanIndex = (pan >> 3) as usize;
			smallPanIndex = smallPanIndex.min(gHeadsetPanQuantization.len() - 1);
			sub.headsetPanLeft = gHeadsetPanQuantization[smallPanIndex] as u8;
			sub.headsetPanRight = gHeadsetPanQuantization[gHeadsetPanQuantization.len() - 1 - smallPanIndex] as u8;
			sub.flags -= SF::StereoStrongLeft | SF::StereoStrongRight;
			sub.flags |= SF::UsesHeadsetPanEffects;

			volLeft = gHeadsetPanVolume[pan as usize];
			volRight = gHeadsetPanVolume[0x7F - pan as usize];
		} else if sub.flags.contains(SF::StereoHeadsetEffects) && gSoundMode == SoundMode::Stereo {
			sub.flags -= SF::StereoStrongLeft | SF::StereoStrongRight;
			sub.headsetPanLeft = 0;
			sub.headsetPanRight = 0;
			sub.flags -= SF::UsesHeadsetPanEffects;

			volLeft = gStereoPanVolume[pan as usize];
			volRight = gStereoPanVolume[0x7F - pan as usize];
			if pan < 0x20 {
				sub.flags |= SF::StereoStrongLeft;
			} else if pan > 0x60 {
				sub.flags |= SF::StereoStrongRight;
			}
		} else if gSoundMode == SoundMode::Mono {
			// 0.707 = ~ 1/sqrt(2)
			volLeft = 0.707;
			volRight = 0.707;
		} else {
			volLeft = gDefaultPanVolume[pan as usize];
			volRight = gDefaultPanVolume[0x7F - pan as usize];
		}

		if velocity < 0.0 {
			eprintln!("Warning: volume negetive ({velocity})");
			velocity = 0.0;
		} else if velocity > 32767.0 {
			eprintln!("Warning: volume overflow ({velocity})");
			velocity = 32767.0;
		}

		sub.targetVolLeft = (((velocity * volLeft) as i32 & 0xffff) >> 5) as u16;
		sub.targetVolRight = (((velocity * volRight) as i32 & 0xffff) >> 5) as u16;

		// @bug for the change to UQ0.7, the if statement should also have been changed accordingly
		if sub.reverbVol != reverbVol {
			sub.reverbVol = reverbVol;
			sub.flags |= SF::EnvMixerNeedsInit;
		} else {
			sub.flags.set(SF::EnvMixerNeedsInit, sub.flags.contains(SF::NeedsInit));
		}
	}

	fn set_resampling_rate(&mut self, mut resamplingRateInput: f32) {
		const MAX_RESAMPLING_RATE: f32 = 1.99996;

		if resamplingRateInput < 0.0 {
			eprintln!("Error: setptch: negative pitch ({resamplingRateInput})");
			resamplingRateInput = 0.0;
		}

		let resamplingRate = if resamplingRateInput < 2.0 {
			self.noteSubEu.flags -= NoteSubEuFlags::HasTwoAdpcmParts;
			resamplingRateInput.min(MAX_RESAMPLING_RATE)
		} else {
			self.noteSubEu.flags |= NoteSubEuFlags::HasTwoAdpcmParts;
			(resamplingRateInput * 0.5).min(MAX_RESAMPLING_RATE)
		};

		self.noteSubEu.resamplingRateFixedPoint = (resamplingRate * 32768.0) as i32 as u16;
	}
}

impl Instrument {
	pub fn get_audio_bank_sound(&self, semitone: i32) -> &AudioBankSound {
		if semitone < self.normalRangeLo as i32 {
			&self.lowNotesSound
		} else if semitone <= self.normalRangeHi as i32 {
			&self.normalNotesSound
		} else {
			&self.highNotesSound
		}
	}
}

pub unsafe fn get_instrument_inner(bankId: i32, instId: i32) -> *mut Instrument {
	if !gBankLoadStatus[bankId as usize].is_load_complete() {
		eprintln!("Error: no bank {bankId}");
		gAudioErrorFlags = bankId + 0x10000000;
		return ptr::null_mut();
	}

	if instId >= (*gCtlEntries.add(bankId as usize)).numInstruments as i32 {
		eprintln!("Error: instrument out of range ({}; count {})",
			instId, (*gCtlEntries.add(bankId as usize)).numInstruments);
		gAudioErrorFlags = ((bankId << 8) + instId) + 0x3000000;
		return ptr::null_mut();
	}

	let inst = (*gCtlEntries.add(bankId as usize)).instruments.add(instId as usize).read();
	if inst.is_null() {
		eprintln!("Error: instrument undefined (bank={bankId} inst={instId})");
		gAudioErrorFlags = ((bankId << 8) + instId) + 0x1000000;
		return ptr::null_mut();
	}

	if gBankLoadedPool.contains_object(inst.cast()) {
		inst
	} else {
		eprintln!("Error: bad voice pointer {inst:p} (bank={bankId} inst={instId})");
		gAudioErrorFlags = ((bankId << 8) + instId) + 0x2000000;
		ptr::null_mut()
	}
}

pub unsafe fn get_drum(bankId: i32, drumId: i32) -> *mut Drum {
	if !gBankLoadStatus[bankId as usize].is_load_complete() {
		eprintln!("Error: no bank {bankId}");
		gAudioErrorFlags = bankId + 0x10000000;
		return ptr::null_mut();
	}

	if drumId >= (*gCtlEntries.add(bankId as usize)).numDrums as i32 {
		eprintln!("Error: drum instrument out of range ({}; count {})",
			drumId, (*gCtlEntries.add(bankId as usize)).numDrums);
		gAudioErrorFlags = ((bankId << 8) + drumId) + 0x4000000;
		return ptr::null_mut();
	}

	let drum = (*gCtlEntries.add(bankId as usize)).drums.add(drumId as usize).read();
	if drum.is_null() {
		eprintln!("Error: drum instrument undefined (bank={bankId} drum={drumId})");
		gAudioErrorFlags = ((bankId << 8) + drumId) + 0x5000000;
	}
	drum
}

impl Note {
	unsafe fn init(&mut self) {
		self.playbackState.adsr.init(
			if (*self.playbackState.parentLayer).adsr.releaseRate == 0 {
				(*(*self.playbackState.parentLayer).seqChannel).adsr.envelope
			} else {
				(*self.playbackState.parentLayer).adsr.envelope
			},
		&mut self.playbackState.adsrVolScale);

		self.playbackState.adsr.state = AdsrStateState::Initial;
		self.noteSubEu = gDefaultNoteSub;
	}

	pub unsafe fn disable(&mut self) {
		if self.noteSubEu.flags.contains(NoteSubEuFlags::NeedsInit) {
			self.noteSubEu.flags -= NoteSubEuFlags::NeedsInit;
		} else {
			self.set_vel_pan_reverb(0.0, 0x40, 0);
		}
		self.playbackState.priority = NotePriority::Disabled;
		self.playbackState.parentLayer = SequenceChannelLayer::NO_LAYER;
		self.playbackState.prevParentLayer = SequenceChannelLayer::NO_LAYER;
		self.noteSubEu.flags -= NoteSubEuFlags::Enabled | NoteSubEuFlags::Finished;
	}
}

pub unsafe fn process_notes() {
	'main_loop: for note in std::slice::from_raw_parts_mut(gNotes, gMaxSimultaneousNotes as usize) {
		let playbackState = &mut *ptr::addr_of_mut!(note.playbackState);
		'd: { if playbackState.parentLayer != SequenceChannelLayer::NO_LAYER {
			'c: { if !(*playbackState.parentLayer).flags.contains(SequenceChannelLayerFlags::Enabled)
				&& playbackState.priority >= NotePriority::Min
			{
				break 'c;
			} else if (*(*playbackState.parentLayer).seqChannel).seqPlayer.is_null() {
				eprintln!("Warning: channel is seperated from group");
				(*(*playbackState.parentLayer).seqChannel).disable();
				playbackState.priority = NotePriority::Stopping;
				continue 'main_loop;
			} else if (*(*(*playbackState.parentLayer).seqChannel).seqPlayer).flags.contains(SequencePlayerFlags::Muted) {
				if (*(*playbackState.parentLayer).seqChannel).muteBehavior.intersects(
					MuteBehaviorFlags::StopScript | MuteBehaviorFlags::StopNotes
				) {
					break 'c;
				}
			}
			break 'd; }

			(*playbackState.parentLayer).note_release();
			AudioListItem::remove(&mut note.listItem);
			(*note.listItem.pool).decaying.push_front(&mut note.listItem);
			playbackState.priority = NotePriority::Stopping;
		} else if playbackState.priority >= NotePriority::Min {
			continue 'main_loop;
		} }

		if playbackState.priority != NotePriority::Disabled {
			let noteSubEu = &mut *ptr::addr_of_mut!(note.noteSubEu);
			if playbackState.priority == NotePriority::Stopping || noteSubEu.flags.contains(NoteSubEuFlags::Finished) {
				if playbackState.adsr.state == AdsrStateState::Disabled || noteSubEu.flags.contains(NoteSubEuFlags::Finished) {
					if playbackState.wantedParentLayer != SequenceChannelLayer::NO_LAYER {
						note.disable();
						let wantedParentLayer = unsafe { &mut *playbackState.wantedParentLayer };
						if !wantedParentLayer.seqChannel.is_null() {
							note.init_for_layer(wantedParentLayer);
							note.vibrato_init();
							AudioListItem::remove(&mut note.listItem);
							(*note.listItem.pool).active.push_back(&mut note.listItem);
							playbackState.wantedParentLayer = SequenceChannelLayer::NO_LAYER;
						} else {
							eprintln!("Error: wait track disappeared :OOO");
							note.disable();
							AudioListItem::remove(&mut note.listItem);
							(*note.listItem.pool).disabled.push_back(&mut note.listItem);
							playbackState.wantedParentLayer = SequenceChannelLayer::NO_LAYER;
							continue;
						}
					} else {
						note.disable();
						AudioListItem::remove(&mut note.listItem);
						(*note.listItem.pool).disabled.push_back(&mut note.listItem);
						continue;
					}
				}
			} else if playbackState.adsr.state == AdsrStateState::Disabled {
				note.disable();
				AudioListItem::remove(&mut note.listItem);
				(*note.listItem.pool).disabled.push_back(&mut note.listItem);
				continue;
			}

			let scale = playbackState.adsr.update();
			note.vibrato_update();
			let attributes = &playbackState.attributes;
			let (mut frequency, mut velocity, pan, reverbVol, bookOffset);
			if playbackState.priority == NotePriority::Stopping {
				frequency = attributes.freqScale;
				velocity = attributes.velocity;
				pan = attributes.pan;
				reverbVol = attributes.reverbVol;
				bookOffset = noteSubEu.book_offset();
			} else {
				let parent = unsafe { &*playbackState.parentLayer };
				frequency = parent.noteFreqScale;
				velocity = parent.noteVelocity;
				pan = parent.notePan;
				reverbVol = (*parent.seqChannel).reverbVol;
				bookOffset = (*parent.seqChannel).bookOffset & 7;
			}

			frequency *= playbackState.vibratoFreqScale * playbackState.portamentoFreqScale;
			frequency *= gAudioBufferParameters.resampleRate;
			velocity *= scale * scale;
			note.set_resampling_rate(frequency);
			note.set_vel_pan_reverb(velocity, pan, reverbVol);
			noteSubEu.set_book_offset(bookOffset);
		}
	}
}

impl SequenceChannelLayer {
	unsafe fn decay_release_internal(&mut self, target: AdsrStateState) {
		if Self::NO_LAYER == self {
			return;
		}

		let Some(note) = self.note.as_mut() else {
			return;
		};

		let attributes = &mut note.playbackState.attributes;
		if note.playbackState.wantedParentLayer == self {
			note.playbackState.wantedParentLayer = Self::NO_LAYER;
		}

		if note.playbackState.parentLayer != self {
			if note.playbackState.parentLayer == Self::NO_LAYER &&
				note.playbackState.wantedParentLayer == Self::NO_LAYER &&
				note.playbackState.prevParentLayer == self &&
				target != AdsrStateState::Decay
			{
				note.playbackState.adsr.fadeOutVel = gAudioBufferParameters.updatesPerFrameInv;
				note.playbackState.adsr.action |= AdsrAction::Release;
			}

			return;
		}

		self.status = SoundLoadStatus::NotLoaded;
		if note.playbackState.adsr.state != AdsrStateState::Decay {
			attributes.freqScale = self.noteFreqScale;
			attributes.velocity = self.noteVelocity;
			attributes.pan = self.notePan;
			if let Some(chan) = self.seqChannel.as_ref() {
				attributes.reverbVol = chan.reverbVol;
			}
			note.playbackState.priority = NotePriority::Stopping;

			note.playbackState.prevParentLayer = note.playbackState.parentLayer;
			note.playbackState.parentLayer = Self::NO_LAYER;
			if target == AdsrStateState::Release {
				note.playbackState.adsr.fadeOutVel = gAudioBufferParameters.updatesPerFrameInv;
				note.playbackState.adsr.action |= AdsrAction::Release;
			} else { // Decay
				note.playbackState.adsr.action |= AdsrAction::Decay;
				note.playbackState.adsr.fadeOutVel = if self.adsr.releaseRate == 0 {
					(*self.seqChannel).adsr.releaseRate
				} else {
					self.adsr.releaseRate
				} as f32 * gAudioBufferParameters.unkUpdatesPerFrameScaled;
				note.playbackState.adsr.sustain = ((*self.seqChannel).adsr.sustain as f32 * note.playbackState.adsr.current) / 256.0;
			}
		}

		if target == AdsrStateState::Decay {
			AudioListItem::remove(&mut note.listItem);
			(*note.listItem.pool).decaying.push_front(&mut note.listItem);
		}
	}

	#[inline]
	pub unsafe fn note_decay(&mut self) {
		self.decay_release_internal(AdsrStateState::Decay);
	}

	#[inline]
	pub unsafe fn note_release(&mut self) {
		self.decay_release_internal(AdsrStateState::Release);
	}
}

fn build_synthetic_wave(note: &mut Note, layer: &mut SequenceChannelLayer, mut waveId: u8) -> u8 {
	if waveId < 0x80 {
		eprintln!("Error: bad voice ID {waveId}");
		waveId = 0x80;
	}

	let mut freqScale = layer.freqScale;
	if layer.portamento.mode != 0 && 0.0 < layer.portamento.extent {
		freqScale *= layer.portamento.extent + 1.0;
	}

	let (octave, ratio) =
	if freqScale < 1.0 { (0, 1.0465) }
	else if freqScale < 2.0 { (1, 0.52325) }
	else if freqScale < 4.0 { (2, 0.26263) } // @DeKrain: this isn't 1.0465 / 4, which is 0.261625
	else { (3, 0.13081) };

	layer.freqScale *= ratio;
	note.playbackState.waveId = waveId;
	note.playbackState.octave = octave;
	note.noteSubEu.sound.samples = &gWaveSamples[waveId as usize - 0x80][octave as usize * WAVE_CYCLE_SAMPLES as usize];
	octave
}

pub unsafe fn init_synthetic_wave(note: &mut Note, layer: &mut SequenceChannelLayer) {
	let mut waveId = layer.instrOrWave;
	if waveId == 0xff {
		waveId = (*layer.seqChannel).instrOrWave as u8;
	}
	let prevOctave = note.playbackState.octave;
	let octave = build_synthetic_wave(note, layer, waveId);
	note.synthesisState.samplePosInt =
		note.synthesisState.samplePosInt *
		gOctaveScalar[octave as usize] as u32 /
		gOctaveScalar[prevOctave as usize] as u32;
}

impl AudioListItem {
	fn init_note_list(&mut self) {
		self.prev = self;
		self.next = self;
		self.u.count = 0;
	}
}

impl NotePool {
	pub fn init_note_lists(&mut self) {
		self.disabled.init_note_list();
		self.decaying.init_note_list();
		self.releasing.init_note_list();
		self.active.init_note_list();
		self.disabled.pool = self;
		self.decaying.pool = self;
		self.releasing.pool = self;
		self.active.pool = self;
	}
}

pub unsafe fn init_note_free_list() {
	gNoteFreeLists.init_note_lists();
	for note in std::slice::from_raw_parts_mut(gNotes, gMaxSimultaneousNotes as usize) {
		note.listItem.u.value_note = note;
		note.listItem.prev = ptr::null_mut();
		gNoteFreeLists.disabled.push_back(&mut note.listItem);
	}
}

impl NotePool {
	pub unsafe fn clear(&mut self) {
		for (source, dest) in [
			(&mut self.disabled, &mut gNoteFreeLists.disabled),
			(&mut self.decaying, &mut gNoteFreeLists.decaying),
			(&mut self.releasing, &mut gNoteFreeLists.releasing),
			(&mut self.active, &mut gNoteFreeLists.active),
		] {
			loop {
				let cur = source.next;
				if cur == source {
					break;
				}
				let Some(cur) = cur.as_mut() else {
					eprintln!("Error: deallocated voice is null");
					break;
				};
				AudioListItem::remove(cur);
				dest.push_back(cur);
			}
		}
	}

	pub unsafe fn fill(&mut self, count: u32) {
		let mut idx = 0;
		self.clear();
		for (source, dest) in [
			(&mut self.disabled, &mut gNoteFreeLists.disabled),
			(&mut self.decaying, &mut gNoteFreeLists.decaying),
			(&mut self.releasing, &mut gNoteFreeLists.releasing),
			(&mut self.active, &mut gNoteFreeLists.active),
		] {
			loop {
				if idx == count {
					return;
				}
				let note: *mut Note = source.pop_back();
				let Some(note) = note.as_mut() else {
					break;
				};
				dest.push_back(&mut note.listItem);
				idx += 1;
			}
		}
		eprintln!("Error: voice alloc desired count ({count}) greater than factual ({idx})");
	}
}

// idk why the implementation is split between here and seqplayer
impl AudioListItem {
	pub unsafe fn push_front(&mut self, item: &mut AudioListItem) {
		if !item.prev.is_null() {
			panic!("Error: item is already in a list");
		}
		item.prev = self;
		item.next = self.next;
		unsafe { (*self.next).prev = item; }
		self.next = item;
		self.u.count += 1;
		item.pool = self.pool;
	}

	pub unsafe fn remove(item: &mut AudioListItem) {
		if item.prev.is_null() {
			eprintln!("Error: item is already detached from any list");
		} else {
			(*item.prev).next = item.next;
			(*item.next).prev = item.prev;
			item.prev = ptr::null_mut();
			// This was not originally there, but I added it for completeness.
			item.next = ptr::null_mut();
		}
	}

	unsafe fn pop_note_with_lower_priority(&mut self, limit: NotePriority) -> *mut Note {
		if ptr::eq(self.next, self) {
			return ptr::null_mut();
		}

		let mut cur = self.next;
		let mut best = cur;
		while cur != self {
			if (*(*best).u.value_note).playbackState.priority >= (*(*cur).u.value_note).playbackState.priority {
				best = cur;
			}
			cur = (*cur).next;
		}
		if best.is_null() {
			return ptr::null_mut();
		}
		if limit <= (*(*best).u.value_note).playbackState.priority {
			return ptr::null_mut();
		}
		let best = &mut *best;
		Self::remove(best);
		best.u.value_note
	}
}

impl Note {
	unsafe fn init_for_layer(&mut self, layer: &mut SequenceChannelLayer) {
		let channel = &mut *layer.seqChannel;
		self.playbackState.prevParentLayer = SequenceChannelLayer::NO_LAYER;
		self.playbackState.parentLayer = layer;
		self.playbackState.priority = channel.notePriority;
		layer.flags |= SequenceChannelLayerFlags::NotePropertiesNeedInit;
		// "loaded"
		layer.status = SoundLoadStatus::Discardable;
		layer.note = self;
		layer.noteVelocity = 0.0;
		self.init();
		let mut instId = layer.instrOrWave as i16;
		if instId == 0xff {
			instId = channel.instrOrWave;
		}
		self.noteSubEu.sound.audioBankSound = layer.sound;

		self.noteSubEu.flags.set(NoteSubEuFlags::IsSyntheticWave, instId >= 0x80);
		if self.noteSubEu.flags.contains(NoteSubEuFlags::IsSyntheticWave) {
			println!("Debug: synthetic wave");
			build_synthetic_wave(self, layer, instId as u8);
		}
		self.noteSubEu.bankId = channel.bankId;
		self.noteSubEu.flags.set(NoteSubEuFlags::StereoHeadsetEffects, channel.flags.contains(SequenceChannelFlags::StereoHeadsetEffects));
		self.noteSubEu.set_reverb_index(channel.reverbIndex & 3);
	}

	unsafe fn switch_parent_layer(&mut self, layer: &mut SequenceChannelLayer) {
		(*self.playbackState.parentLayer).note_release();
		self.playbackState.wantedParentLayer = layer;
	}

	unsafe fn release_and_take_ownership(&mut self, layer: &mut SequenceChannelLayer) {
		self.playbackState.wantedParentLayer = layer;
		self.playbackState.priority = NotePriority::Stopping;
		self.playbackState.adsr.fadeOutVel = gAudioBufferParameters.updatesPerFrameInv;
		self.playbackState.adsr.action |= AdsrAction::Release;
	}
}

impl NotePool {
	unsafe fn alloc_note_from_disabled(&mut self, layer: &mut SequenceChannelLayer) -> *mut Note {
		let note: *mut Note = self.disabled.pop_back();
		if let Some(note) = note.as_mut() {
			note.init_for_layer(layer);
			self.active.push_front(&mut note.listItem);
		}
		note
	}

	unsafe fn alloc_note_from_decaying(&mut self, layer: &mut SequenceChannelLayer) -> *mut Note {
		let note: *mut Note = self.decaying.pop_back();
		if let Some(note) = note.as_mut() {
			note.release_and_take_ownership(layer);
			self.releasing.push_back(&mut note.listItem);
		}
		note
	}

	unsafe fn alloc_note_from_active(&mut self, layer: &mut SequenceChannelLayer) -> *mut Note {
		let note = self.active.pop_note_with_lower_priority((*layer.seqChannel).notePriority);
		if let Some(note) = note.as_mut() {
			note.switch_parent_layer(layer);
			self.releasing.push_back(&mut note.listItem);
		} else {
			//eprintln!("Note: lower priority note not found");
		}
		note
	}
}

pub unsafe fn alloc_note(layer: &mut SequenceChannelLayer) -> *mut Note {
	let channel = &mut *layer.seqChannel;
	let policy = channel.noteAllocPolicy;

	if policy.contains(NoteAllocPolicy::Layer) {
		if let Some(note) = layer.note.as_mut() {
			if note.playbackState.prevParentLayer == layer
				&& note.playbackState.wantedParentLayer == SequenceChannelLayer::NO_LAYER
			{
				note.release_and_take_ownership(layer);
				AudioListItem::remove(&mut note.listItem);
				(*note.listItem.pool).releasing.push_back(&mut note.listItem);
				return note;
			}
		}
	}

	match {
		let channelPool = &mut channel.notePool;
		let playerPool = &mut (*channel.seqPlayer).notePool;
		if policy.contains(NoteAllocPolicy::Channel) {
			// Channel
			channelPool.alloc_note_from_disabled(layer).as_mut()
			.or_else(|| channelPool.alloc_note_from_decaying(layer).as_mut())
			.or_else(|| channelPool.alloc_note_from_active(layer).as_mut())
		} else if policy.contains(NoteAllocPolicy::Seq) {
			// Channel + Player
			channelPool.alloc_note_from_disabled(layer).as_mut()
			.or_else(|| playerPool.alloc_note_from_disabled(layer).as_mut())
			.or_else(|| channelPool.alloc_note_from_decaying(layer).as_mut())
			.or_else(|| playerPool.alloc_note_from_decaying(layer).as_mut())
			.or_else(|| channelPool.alloc_note_from_active(layer).as_mut())
			.or_else(|| playerPool.alloc_note_from_active(layer).as_mut())
		} else if policy.contains(NoteAllocPolicy::GlobalFreeList) {
			// Global
			gNoteFreeLists.alloc_note_from_disabled(layer).as_mut()
			.or_else(|| gNoteFreeLists.alloc_note_from_decaying(layer).as_mut())
			.or_else(|| gNoteFreeLists.alloc_note_from_active(layer).as_mut())
		} else {
			// Channel + Player + Global
			channelPool.alloc_note_from_disabled(layer).as_mut()
			.or_else(|| playerPool.alloc_note_from_disabled(layer).as_mut())
			.or_else(|| gNoteFreeLists.alloc_note_from_disabled(layer).as_mut())
			.or_else(|| channelPool.alloc_note_from_decaying(layer).as_mut())
			.or_else(|| playerPool.alloc_note_from_decaying(layer).as_mut())
			.or_else(|| gNoteFreeLists.alloc_note_from_decaying(layer).as_mut())
			.or_else(|| channelPool.alloc_note_from_active(layer).as_mut())
			.or_else(|| playerPool.alloc_note_from_active(layer).as_mut())
			.or_else(|| gNoteFreeLists.alloc_note_from_active(layer).as_mut())
		}
	} {
		Some(note) => note,
		None => {
			eprintln!("Warning: dropping voice");
			layer.status = SoundLoadStatus::NotLoaded;
			ptr::null_mut()
		}
	}
}

// pub fn reclaim_notes();

pub unsafe fn note_init_all() {
	for note in std::slice::from_raw_parts_mut(gNotes, gMaxSimultaneousNotes as usize) {
		note.noteSubEu = gZeroNoteSub;
		note.playbackState.priority = NotePriority::Disabled;
		note.playbackState.parentLayer = SequenceChannelLayer::NO_LAYER;
		note.playbackState.wantedParentLayer = SequenceChannelLayer::NO_LAYER;
		note.playbackState.prevParentLayer = SequenceChannelLayer::NO_LAYER;
		note.playbackState.waveId = 0;
		note.playbackState.attributes.velocity = 0.0;
		note.playbackState.adsrVolScale = 0;
		note.playbackState.adsr.state = AdsrStateState::Disabled;
		note.playbackState.adsr.action = AdsrAction::empty();
		note.playbackState.vibratoState.active = false;
		note.playbackState.portamento.cur = 0.0;
		note.playbackState.portamento.speed = 0.0;
		note.synthesisState.synthesisBuffers = gNotesAndBuffersPool.alloc(std::mem::size_of::<NoteSynthesisBuffers>() as u32);
	}
}

/* impl Note {
	pub fn init_volume(&mut self);
	pub fn set_frequency(&mut self, frequency: f32);
	pub fn enable(&mut self);
} */
