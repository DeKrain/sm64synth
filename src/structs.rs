use crate::{
	support::*,
	ultra::*,
};
use std::mem::MaybeUninit;

pub const SEQUENCE_PLAYERS: usize = 4;
pub const SEQUENCE_CHANNELS: usize = 48;
pub const SEQUENCE_LAYERS: usize = 64;

pub const LAYERS_MAX: usize = 4;
pub const CHANNELS_MAX: usize = 16;

pub const TATUMS_PER_BEAT: i16 = 48;
pub const TEMPO_SCALE: i16 = TATUMS_PER_BEAT;

#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum SoundLoadStatus {
	NotLoaded = 0,
	InProgress = 1,
	Complete = 2,
	Discardable = 3,
	Unk4 = 4,
	Unk5 = 5,
}

impl SoundLoadStatus {
	#[inline]
	pub const fn is_load_complete(self) -> bool {
		// Casting must be done explicity because the derive implementations
		// currently aren't const
		self as u8 >= Self::Complete as u8
	}
}

#[repr(u8)]
pub enum Codec {
	ADPCM,
	S8,
	Skip,
}

/// A node in a circularly linked list. Each node is either a head or an item:
/// - Items can be either detached (prev = NULL), or attached to a list.
///   'value' points to something of interest.
/// - List heads are always attached; if a list is empty, its head points
///   to itself. 'count' contains the size of the list.
/// If the list holds notes, 'pool' points back to the pool where it lives.
/// Otherwise, that member is NULL.
pub struct AudioListItem {
	pub prev: *mut AudioListItem,
	pub next: *mut AudioListItem,
	pub u: AudioListItem_Union,
	pub pool: *mut NotePool,
}

pub union AudioListItem_Union {
	pub value: *mut (),
	pub value_note: *mut Note,
	pub value_layer: *mut SequenceChannelLayer,
	pub count: i32,
}

pub struct NotePool {
	pub disabled: AudioListItem,
	pub decaying: AudioListItem,
	pub releasing: AudioListItem,
	pub active: AudioListItem,
}

pub struct VibratoState {
	pub seqChannel: *mut SequenceChannel,
	pub time: u32,
	// NOTE: this varies between JP+US & EU+SH (represented)
	/// This always points into a sine wave cycle.
	/// Regardless of the wave, it's one of the [`gWaveSamples`][crate::data::gWaveSamples].
	pub curve: *const i16,
	pub extent: f32,
	pub rate: f32,
	pub active: bool,

	pub rateChangeTimer: u16,
	pub extentChangeTimer: u16,
	pub delay: u16,
}

/// Pitch sliding by up to one octave in the positive direction. Negative
/// direction is "supported" by setting extent to be negative. The code
/// extrapolates exponentially in the wrong direction in that case, but that
/// doesn't prevent seqplayer from doing it, AFAICT.
#[repr(C)]
#[derive(Clone, Copy)]
pub struct Portamento {
	/// Bit 7 denotes something; the rest are an index 0-5
	pub mode: u8,
	pub cur: f32,
	pub speed: f32,
	pub extent: f32,
}

impl Portamento {
	pub const fn mode_split(&self) -> (bool, u8) {
		(self.mode & 0x80 != 0, self.mode & 0x7F)
	}
}

/// ADSR envelope part stored in sequence data.
/// Envelope parts are stored in Big Endian.
#[repr(C)]
pub struct AdsrEnvelope {
	_delay: i16,
	_arg: i16,
}

impl AdsrEnvelope {
	// Special values for delay (instructions)
	pub const DELAY_DISABLE: i16 = 0;
	pub const DELAY_HANG: i16 = -1;
	/// Takes `arg`: envelope index
	pub const DELAY_GOTO: i16 = -2;
	pub const DELAY_RESTART: i16 = -3;

	#[inline(always)]
	pub const fn new(delay: i16, arg: i16) -> Self {
		AdsrEnvelope { _delay: delay.to_be(), _arg: arg.to_be() }
	}

	#[inline(always)]
	pub const fn delay(&self) -> i16 {
		self._delay.to_be()
	}

	#[inline(always)]
	pub const fn arg(&self) -> i16 {
		self._arg.to_be()
	}
}

#[repr(C)]
pub struct AdpcmLoop {
	pub start: u32,
	pub end: u32,
	pub count: u32,
	// TODO: Do I need this?
	_pad: u32,
	/// only exists if count != 0. 8-byte aligned
	pub state: [i16; 16],
}

#[repr(C)]
pub struct AdpcmBook {
	pub order: u32,
	pub npredictors: u32,
	/// size 8 * order * npredictors. 8-byte aligned
	pub book: DSArray<i16>,
}

#[repr(C)]
pub struct AudioBankSample {
	pub unused: u8,
	pub loaded: u8,
	/// Offset within bank of gSoundDataSamples
	pub sampleAddr: *mut u8,
	pub r#loop: *mut AdpcmLoop,
	pub book: *mut AdpcmBook,
	pub sampleSize: u32,
}

impl AudioBankSample {
	pub const LOAD_STATE_LOADED_BIT: u8 = 1;
	pub const LOAD_STATE_ALLOC_BIT: u8 = 0x80;
}

#[repr(C)]
pub struct AudioBankSound {
	pub sample: *mut AudioBankSample,
	/// frequency scale factor
	pub tuning: f32,
}

#[repr(C)]
pub struct Instrument {
	pub loaded: bool,
	pub normalRangeLo: u8,
	pub normalRangeHi: u8,
	pub releaseRate: u8,
	pub envelope: *mut AdsrEnvelope,
	pub lowNotesSound: AudioBankSound,
	pub normalNotesSound: AudioBankSound,
	pub highNotesSound: AudioBankSound,
}

#[repr(C)]
pub struct Drum {
	pub releaseRate: u8,
	pub pan: u8,
	pub loaded: bool,
	pub sound: AudioBankSound,
	pub envelope: *mut AdsrEnvelope,
}

#[repr(C)]
pub struct AudioBank {
	pub drums: *mut *mut Drum,
	pub instruments: DSArray<*mut Instrument>,
}

#[repr(C)]
pub struct CtlEntry {
	pub unused: u8,
	pub numInstruments: u8,
	pub numDrums: u8,
	pub instruments: *mut *mut Instrument,
	pub drums: *mut *mut Drum,
}

pub struct M64ScriptState {
	pub pc: *const u8,
	pub stack: [*const u8; 4],
	pub remLoopIters: [u8; 4],
	pub depth: u8,
}

bitflags::bitflags! {
	#[derive(Clone, Copy)]
	pub struct SequencePlayerFlags: u8 {
		const Enabled = 1 << 0;
		const Finished = 1 << 1;
		const Muted = 1 << 2;
		const SeqDmaInProgress = 1 << 3;
		const BankDmaInProgress = 1 << 4;
		const RecalculateVolume = 1 << 5;
	}

	#[derive(Clone, Copy)]
	pub struct MuteBehaviorFlags: u8 {
		/// Stop processing sequence/channel scripts.
		const StopScript = 0x80;
		/// Prevent further notes from playing.
		const StopNotes = 0x40;
		/// Lower volume, by default to half.
		const Soften = 0x20;
	}
}

#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SequencePlayerStateEU {
	FadingNormal,
	FadingIn,
	FadeOut,
}

impl SequencePlayerStateEU {
	const MAX_VALUE: u8 = 2;

	pub(crate) const unsafe fn from_u8(val: u8) -> Self {
		assert!(val <= Self::MAX_VALUE);
		std::mem::transmute(val)
	}
}

/// Also known as a Group, according to debug strings.
pub struct SequencePlayer {
	pub flags: SequencePlayerFlags,
	pub state: SequencePlayerStateEU,
	pub noteAllocPolicy: NoteAllocPolicy,
	pub muteBehavior: MuteBehaviorFlags,
	pub seqId: u8,
	pub defaultBank: u8,
	pub loadingBankId: u8,
	pub seqVariationEu: i8,
	pub tempo: u16, // tatums per minute
	pub tempoAcc: u16,
	pub transposition: i16,
	pub delay: u16,
	pub fadeRemainingFrames: u16,
	pub fadeTimerUnkEu: u16,
	pub seqData: *mut u8,
	pub fadeVolume: f32,
	pub fadeVelocity: f32,
	pub volume: f32,
	pub muteVolumeScale: f32,
	pub fadeVolumeScale: f32,
	pub appliedFadeVolume: f32,

	pub channels: [*mut SequenceChannel; CHANNELS_MAX],
	pub scriptState: M64ScriptState,
	pub shortNoteVelocityTable: *const u8,
	pub shortNoteDurationTable: *const u8,
	pub notePool: NotePool,
	pub seqDmaMesgQueue: MaybeUninit<OSMesgQueue>,
	pub seqDmaMesg: OSMesg,
	pub seqDmaIoMesg: OSIoMesg,
	pub bankDmaMesgQueue: MaybeUninit<OSMesgQueue>,
	pub bankDmaMesg: OSMesg,
	pub bankDmaIoMesg: OSIoMesg,
	pub bankDmaCurrMemAddr: *mut u8,
	pub bankDmaCurrDevAddr: DevPtrRO<u8>,
	pub bankDmaRemaining: isize,
}

#[derive(Clone, Copy)]
pub struct AdsrSettings {
	pub releaseRate: u8,
	pub sustain: u8,
	pub envelope: *const AdsrEnvelope,
}

pub struct AdsrState {
	pub action: AdsrAction,
	pub state: AdsrStateState,
	pub envIndex: i16,
	pub delay: i16,
	pub sustain: f32,
	pub velocity: f32,
	pub fadeOutVel: f32,
	pub current: f32,
	pub target: f32,
	pub envelope: *const AdsrEnvelope,
}

bitflags::bitflags! {
	pub struct AdsrAction: u8 {
		const Release = 0x10;
		const Decay = 0x20;
		const Hang = 0x40;
	}
}

// Awkward name, but whatever
#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum AdsrStateState {
	Disabled = 0,
	Initial = 1,
	StartLoop = 2,
	Loop = 3,
	Fade = 4,
	Hang = 5,
	Decay = 6,
	Release = 7,
	Sustain = 8,
}

bitflags::bitflags! {
	#[derive(Clone, Copy)]
	pub struct ReverbBitsData: u8 {
		const UsesHeadsetPanEffects = 1 << 3;
		const StereoHeadsetEffects = 3 << 4;
		const StrongRight = 1 << 6;
		const StrongLeft = 1 << 7;
	}
}

pub union ReverbBits {
	pub s: ReverbBitsData,
	pub asByte: u8,
}

pub struct ReverbInfo {
	pub reverbVol: u8,
	/// UQ4.4, although 0 <= x < 1 is rounded up to 1
	pub synthesisVolume: u8,
	pub pan: u8,
	pub reverbBits: ReverbBits,
	pub freqScale: f32,
	pub velocity: f32,
	pub filter: *mut i16,
}

pub struct NoteAttributes {
	pub reverbVol: u8,
	pub pan: u8,
	pub freqScale: f32,
	pub velocity: f32,
}

bitflags::bitflags! {
	pub struct SequenceChannelFlags: u8 {
		const Enabled = 1 << 0;
		const Finished = 1 << 1;
		const StopScript = 1 << 2;
		/// Sets SequenceChannelLayer.stopSomething
		const StopSomething2 = 1 << 3;
		const HasInstrument = 1 << 4;
		const StereoHeadsetEffects = 1 << 5;
		/// Notes specify duration and velocity
		const LargeNotes = 1 << 6;
	}

	pub struct SequenceChannelChanges: u8 {
		const FreqScale = 1 << 0;
		const Volume = 1 << 1;
		const Pan = 1 << 2;
	}

	/// Mask bits denoting where to allocate notes from, according to a channel's
	/// noteAllocPolicy. Despite being checked as bitmask bits, the bits are not
	/// orthogonal; rather, the smallest bit wins, except for NOTE_ALLOC_LAYER,
	/// which *is* orthogonal to the other. SEQ implicitly includes CHANNEL.
	/// If none of the Channel/Seq/GlobalFreeList bits are set, all three locations
	/// are tried.
	#[derive(Clone, Copy)]
	pub struct NoteAllocPolicy: u8 {
		const Layer = 1 << 0;
		const Channel = 1 << 1;
		const Seq = 1 << 2;
		const GlobalFreeList = 1 << 3;
	}
}

/*
#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum NotePriority {
	Disabled,
	Stopping,
	Min,
	Default,
}

impl NotePriority {
	const MAX_VALUE: u8 = 3;

	pub unsafe fn from_u8(val: u8) -> Self {
		assert!(val <= Self::MAX_VALUE);
		std::mem::transmute(val)
	}
}*/

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct NotePriority(u8);

impl NotePriority {
	pub const Disabled: Self = Self(0);
	pub const Stopping: Self = Self(1);
	pub const Min: Self = Self(2);
	pub const Default: Self = Self(3);

	pub unsafe fn from_u8(val: u8) -> Self {
		assert!(val <= 0xF);
		NotePriority(val)
	}
}

impl std::fmt::Debug for NotePriority {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match *self {
			NotePriority::Disabled => f.write_str("Disabled"),
			NotePriority::Stopping => f.write_str("Stopping"),
			NotePriority::Min => f.write_str("2 (min)"),
			NotePriority::Default => f.write_str("3 (default)"),
			NotePriority(pri) => std::fmt::Display::fmt(&pri, f),
		}
	}
}

// Also known as a SubTrack, according to debug strings.
// Confusingly, a SubTrack is a container of Tracks.
pub struct SequenceChannel {
	pub flags: SequenceChannelFlags,
	pub changes: SequenceChannelChanges,
	pub noteAllocPolicy: NoteAllocPolicy,
	pub muteBehavior: MuteBehaviorFlags,
	/// UQ0.8
	pub reverbVol: u8,
	pub notePriority: NotePriority,
	pub bankId: u8,
	pub reverbIndex: u8,
	pub bookOffset: u8,
	pub newPan: u8,
	/// Proportion of pan that comes from the channel (0..128)
	pub panChannelWeight: u8,

	pub vibratoRateStart: u16,
	pub vibratoExtentStart: u16,
	pub vibratoRateTarget: u16,
	pub vibratoExtentTarget: u16,
	pub vibratoRateChangeDelay: u16,
	pub vibratoExtentChangeDelay: u16,
	pub vibratoDelay: u16,
	pub delay: u16,
	/// Either 0 (none), instrument index + 1, or 0x80..=0x83 for sawtooth/triangle/sine/square waves.
	pub instrOrWave: i16,
	pub transposition: i16,
	pub volumeScale: f32,
	pub volume: f32,
	pub pan: u16,
	pub appliedVolume: f32,
	pub freqScale: f32,
	pub dynTable: *mut [u8; 2],
	// noteUnused: *mut Note,
	// layerUnused: *mut SequenceChannelLayer,
	pub instrument: *mut Instrument,
	pub seqPlayer: *mut SequencePlayer,
	pub layers: [*mut SequenceChannelLayer; LAYERS_MAX],
	/// Bridge between sound script and audio lib.
	/// For player 2:
	/// - `[0]` contains enabled
	/// - `[4]` contains sound ID
	/// - `[5]` contains reverb adjustment
	pub soundScriptIO: [i8; 8],
	pub scriptState: M64ScriptState,
	pub adsr: AdsrSettings,
	pub notePool: NotePool,
}

bitflags::bitflags! {
	pub struct SequenceChannelLayerFlags: u8 {
		const Enabled = 1 << 0;
		const Finished = 1 << 1;
		const StopSomething = 1 << 2;
		/// Keep the same note for consecutive notes with the same sound
		const ContinuousNotes = 1 << 3;
		// --
		const NotePropertiesNeedInit = 1 << 5;
		const IgnoreDrumPan = 1 << 6;
	}
}

/// Also known as a Track, according to debug strings.
pub struct SequenceChannelLayer {
	pub flags: SequenceChannelLayerFlags,
	pub instrOrWave: u8,
	pub status: SoundLoadStatus,
	pub noteDuration: u8,
	pub portamentoTargetNote: u8,
	/// 0x00..0x80
	pub pan: u8,
	pub notePan: u8,
	pub portamento: Portamento,
	pub adsr: AdsrSettings,
	pub portamentoTime: u16,
	/// Number of semitones added to play commands.
	///
	/// M64 instruction encoding only allows referring to the limited range
	/// 0..=0x3f; this makes 0x40..=0x7f accessible as well.
	pub transposition: i16,
	pub freqScale: f32,
	pub velocitySquare: f32,
	pub noteVelocity: f32,
	pub noteFreqScale: f32,
	pub shortNoteDefaultPlayPercentage: i16,
	/// More like fraction
	pub playProcentage: i16,
	pub delay: i16,
	pub duration: i16,
	// delayUnused: i16,
	pub note: *mut Note,
	pub instrument: *mut Instrument,
	pub sound: *const AudioBankSound,
	pub seqChannel: *mut SequenceChannel,
	pub scriptState: M64ScriptState,
	pub listItem: AudioListItem,
}

impl SequenceChannelLayer {
	pub const NO_LAYER: *mut Self = !0usize as *mut _;
}

pub struct NoteSynthesisState {
	pub restart: bool,
	pub sampleDmaIndex: u8,
	pub prevHeadsetPanRight: u8,
	pub prevHeadsetPanLeft: u8,
	pub samplePosFrac: u16,
	pub samplePosInt: u32,
	pub synthesisBuffers: *mut NoteSynthesisBuffers,
	// Notice different ordering wrt. prevHeadsetPan
	/// Q1.15
	pub curVolLeft: u16,
	/// Q1.15
	pub curVolRight: u16,
}

pub struct NotePlaybackState {
	pub priority: NotePriority,
	pub waveId: u8,
	pub octave: u8,
	pub adsrVolScale: i16,
	pub portamentoFreqScale: f32,
	pub vibratoFreqScale: f32,
	pub prevParentLayer: *mut SequenceChannelLayer,
	pub parentLayer: *mut SequenceChannelLayer,
	pub wantedParentLayer: *mut SequenceChannelLayer,
	pub attributes: NoteAttributes,
	pub adsr: AdsrState,
	pub portamento: Portamento,
	pub vibratoState: VibratoState,
}

bitflags::bitflags! {
	#[derive(Clone, Copy)]
	pub struct NoteSubEuFlags: u16 {
		const Enabled = 1 << 0;
		const NeedsInit = 1 << 1;
		const Finished = 1 << 2;
		const EnvMixerNeedsInit = 1 << 3;
		const StereoStrongRight = 1 << 4;
		const StereoStrongLeft = 1 << 5;
		const StereoHeadsetEffects = 1 << 6;
		const UsesHeadsetPanEffects = 1 << 7;
		const ReverbIndex = 7 << 8;
		const BookOffset = 7 << 11;
		const IsSyntheticWave = 1 << 14;
		const HasTwoAdpcmParts = 1 << 15;
	}
}

#[derive(Clone, Copy)]
pub struct NoteSubEu {
	pub flags: NoteSubEuFlags,
	pub bankId: u8,
	pub headsetPanRight: u8,
	pub headsetPanLeft: u8,
	/// Q1.7
	pub reverbVol: u8,
	/// UQ0.10
	pub targetVolLeft: u16,
	/// UQ0.10
	pub targetVolRight: u16,
	pub resamplingRateFixedPoint: u16,
	pub sound: NoteSubEuSound,
}

impl NoteSubEu {
	#[inline]
	pub fn reverb_index(&self) -> u8 {
		((self.flags & NoteSubEuFlags::ReverbIndex).bits() >> 8) as u8
	}

	#[inline]
	pub fn set_reverb_index(&mut self, reverbIndex: u8) {
		self.flags -= NoteSubEuFlags::ReverbIndex;
		self.flags |= NoteSubEuFlags::from_bits_retain(((reverbIndex & 7) as u16) << 8);
	}

	#[inline]
	pub fn book_offset(&self) -> u8 {
		((self.flags & NoteSubEuFlags::BookOffset).bits() >> 11) as u8
	}

	#[inline]
	pub fn set_book_offset(&mut self, bookOffset: u8) {
		self.flags -= NoteSubEuFlags::BookOffset;
		self.flags |= NoteSubEuFlags::from_bits_retain(((bookOffset & 7) as u16) << 11);
	}
}

#[derive(Clone, Copy)]
pub union NoteSubEuSound {
	pub samples: *const i16,
	pub audioBankSound: *const AudioBankSound,
}

pub struct Note {
	pub listItem: AudioListItem,
	pub synthesisState: NoteSynthesisState,
	pub playbackState: NotePlaybackState,
	pub noteSubEu: NoteSubEu,
}

use crate::ultra64_abi as bufs;

pub struct NoteSynthesisBuffers {
	pub adpcmDecState: bufs::ADPCM_STATE,
	pub finalResampleState: bufs::RESAMPLE_STATE,
	pub mixEnvelopeState: bufs::ENVMIX_STATE,
	pub panResampleState: bufs::RESAMPLE_STATE,
	pub panSamplesBuffer: [i16; 0x20],
	pub dummyResampleState: bufs::RESAMPLE_STATE,
}

pub struct ReverbSettingsEU {
	pub downsampleRate: u8,
	/// To by multiplied by 64 (<< 6)
	pub windowSize: u8,
	pub gain: u16,
}

pub struct AudioSessionSettingsEU {
	pub frequency: u32,
	pub unk1: u8,
	pub maxSimultaneousNotes: u8,
	// numReverbs got integrated into reverbSettings
	// pub numReverbs: u8,
	pub unk2: u8,
	pub reverbSettings: &'static [ReverbSettingsEU],
	pub volume: u16,
	pub unk3: u16,
	pub persistentSeqMem: u32,
	pub persistentBankMem: u32,
	pub temporarySeqMem: u32,
	pub temporaryBankMem: u32,
}

#[cfg(any())]
pub struct AudioSessionSettings {
	pub frequency: u32,
	pub maxSimultaneousNotes: u8,
	pub reverbDownsampleRate: u8,
	pub reverbWindowSize: u16,
	pub reverbGain: u16,
	pub volume: u16,
	pub persistentSeqMem: u32,
	pub persistentBankMem: u32,
	pub temporarySeqMem: u32,
	pub temporaryBankMem: u32,
}

pub struct AudioBufferParametersEU {
	/// Audio frames per vsync?
	pub presetUnk4: u16,
	pub frequency: u16,
	pub aiFrequency: u16,
	pub samplesPerFrameTarget: u16,
	pub maxAiBufferLength: u16,
	pub minAiBufferLength: u16,
	pub updatesPerFrame: u16,
	pub samplesPerUpdate: u16,
	pub samplesPerUpdateMax: u16,
	pub samplesPerUpdateMin: u16,
	/// contains 32000.0 / frequency
	pub resampleRate: f32,
	/// 1.0 / updatesPerFrame
	pub updatesPerFrameInv: f32,
	/// 3.0 / (1280.0 * updatesPerFrame)
	pub unkUpdatesPerFrameScaled: f32,
}

pub mod eu_cmd {
	#[repr(C)]
	pub struct EuAudioCmd {
		pub u: Part1,
		pub u2: Part2,
	}

	#[repr(C)]
	pub union Part1 {
		pub s: ArgOp,
		pub first: i32,
	}

	#[cfg(target_endian = "big")]
	#[repr(C)]
	#[derive(Clone, Copy)]
	pub struct ArgOp {
		pub op: u8,
		pub player: u8,
		pub arg2: u8,
		pub arg3: u8,
	}

	#[cfg(target_endian = "little")]
	#[repr(C)]
	#[derive(Clone, Copy)]
	pub struct ArgOp {
		pub arg3: u8,
		pub arg2: u8,
		pub player: u8,
		pub op: u8,
	}

	#[repr(C)]
	pub union Part2 {
		pub as_i32: i32,
		pub as_u32: u32,
		pub as_f32: f32,

		pub as_u8: AsU8_32,
		pub as_i8: AsI8_32,
	}

	#[cfg(target_endian = "big")]
	#[repr(C)]
	#[derive(Clone, Copy)]
	pub struct AsU8_32 {
		pub v: u8,
		_pad: [u8; 3],
	}

	#[cfg(target_endian = "big")]
	#[repr(C)]
	#[derive(Clone, Copy)]
	pub struct AsI8_32 {
		pub v: i8,
		_pad: [u8; 3],
	}

	#[cfg(target_endian = "little")]
	#[repr(C)]
	#[derive(Clone, Copy)]
	pub struct AsU8_32 {
		_pad: [u8; 3],
		pub v: u8,
	}

	#[cfg(target_endian = "little")]
	#[repr(C)]
	#[derive(Clone, Copy)]
	pub struct AsI8_32 {
		_pad: [u8; 3],
		pub v: i8,
	}
}

pub use eu_cmd::EuAudioCmd;
