use crate::support::DSArray;

// NOT Shindou layout
#[repr(C)]
pub struct ALSeqFile {
	pub revision: u16,
	pub seqCount: u16,
	pub seqArray: DSArray<ALSeqData>,
}

// DEFINITELY NOT Shindou layout
#[repr(C)]
pub struct ALSeqData {
	pub offset: *mut u8,
	pub len: u32,
}

pub unsafe fn alSeqFileNew(f: &mut ALSeqFile, base: *const ()) {
	let base = base as *mut u8;
	for i in 0..f.seqCount as isize {
		let ptr = &mut (*f.seqArray.as_mut_ptr().offset(i)).offset;
		*ptr = base.offset((*ptr) as isize);
	}
}
