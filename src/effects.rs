use crate::{
	structs::*,
	load::*,
	data::*,
};

impl SequencePlayer {
	pub unsafe fn process_sound(&mut self) {
		use SequencePlayerFlags as F;
		if self.fadeRemainingFrames != 0 {
			self.fadeVolume += self.fadeVelocity;
			self.flags |= F::RecalculateVolume;
			self.fadeVolume = self.fadeVolume.clamp(0.0, 1.0);
			self.fadeRemainingFrames -= 1;
			if self.fadeRemainingFrames == 0 {
				if self.state == SequencePlayerStateEU::FadeOut {
					self.disable();
					return;
				}
			}
		}

		let recalculateVolume = self.flags.contains(F::RecalculateVolume);
		if recalculateVolume {
			self.appliedFadeVolume = self.fadeVolume * self.fadeVolumeScale;
			self.flags -= F::RecalculateVolume;
		}

		for channel in self.channels {
			if channel != std::ptr::addr_of_mut!(gSequenceChannelNone) && (*channel).flags.contains(SequenceChannelFlags::Enabled) {
				(*channel).process_sound(recalculateVolume);
			}
		}
	}
}

impl Note {
	pub unsafe fn vibrato_update(&mut self) {
		if self.playbackState.portamento.mode != 0 {
			self.playbackState.portamentoFreqScale = self.playbackState.portamento.get_freq_scale();
		}
		if self.playbackState.vibratoState.active && self.playbackState.parentLayer != SequenceChannelLayer::NO_LAYER {
			self.playbackState.vibratoFreqScale = self.playbackState.vibratoState.get_freq_scale();
		}
	}

	pub unsafe fn vibrato_init(&mut self) {
		self.playbackState.vibratoFreqScale = 1.0;
		self.playbackState.portamentoFreqScale = 1.0;

		let vib = &mut self.playbackState.vibratoState;
		vib.active = true;
		vib.time = 0;
		vib.curve = gWaveSamples[WAVE_SINE].as_ptr();
		vib.seqChannel = (*self.playbackState.parentLayer).seqChannel;
		let seqChannel = unsafe { &mut *vib.seqChannel };
		vib.extentChangeTimer = seqChannel.vibratoExtentChangeDelay;
		if vib.extentChangeTimer == 0 {
			vib.extent = seqChannel.vibratoExtentTarget as f32;
		} else {
			vib.extent = seqChannel.vibratoExtentStart as f32;
		}

		vib.rateChangeTimer = seqChannel.vibratoRateChangeDelay;
		if vib.rateChangeTimer == 0 {
			vib.rate = seqChannel.vibratoRateTarget as f32;
		} else {
			vib.rate = seqChannel.vibratoRateStart as f32;
		}

		vib.delay = seqChannel.vibratoDelay;

		self.playbackState.portamento = (*self.playbackState.parentLayer).portamento;
	}
}

impl AdsrState {
	pub fn init(&mut self, envelope: *const AdsrEnvelope, volOut: *mut i16) {
		self.action = AdsrAction::empty();
		self.state = AdsrStateState::Disabled;
		self.delay = 0;
		self.envelope = envelope;
		self.current = 0.0;
		let _ = volOut;
	}

	pub unsafe fn update(&mut self) -> f32 {
		use AdsrStateState as State;
		let mut state = self.state;
		loop {
			match state {
				State::Disabled => return 0.0,
				State::Initial => {
					if self.action.contains(AdsrAction::Hang) {
						self.state = State::Hang;
						break;
					} else {
						state = State::StartLoop;
						continue;
					}
				}
				State::StartLoop => {
					self.envIndex = 0;
					self.state = State::Loop;
					state = State::Loop;
					continue;
				}
				State::Loop => {
					let env = &*self.envelope.offset(self.envIndex as isize);
					self.delay = env.delay();
					match self.delay {
						AdsrEnvelope::DELAY_DISABLE => self.state = State::Disabled,
						AdsrEnvelope::DELAY_HANG => self.state = State::Hang,
						AdsrEnvelope::DELAY_GOTO => self.envIndex = env.arg(),
						AdsrEnvelope::DELAY_RESTART => self.state = State::Initial,
						_ => {
							if self.delay >= 4 {
								self.delay = (self.delay as i32 * gAudioBufferParameters.updatesPerFrame as i32 / 4) as i16;
							}
							self.target = env.arg() as f32 / 32767.0;
							self.target *= self.target;
							self.velocity = (self.target - self.current) / self.delay as f32;
							self.state = State::Fade;
							self.envIndex += 1;
						}
					}
					if self.state != State::Fade {
						break;
					} else {
						state = State::Fade;
						continue;
					}
				}
				State::Fade => {
					self.current += self.velocity;
					self.delay -= 1;
					if self.delay <= 0 {
						self.state = State::Loop;
					}
					state = State::Hang;
					continue;
				}
				State::Hang => break,
				State::Decay | State::Release => {
					self.current -= self.fadeOutVel;
					if self.sustain != 0.0 && state == State::Decay {
						if self.current < self.sustain {
							self.current = self.sustain;
							self.delay = 0x80;
							self.state = State::Sustain;
						}
						break;
					}

					if self.current < 0.0 {
						self.current = 0.0;
						self.state = State::Disabled;
					}

					break;
				}
				State::Sustain => {
					self.delay -= 1;
					if self.delay == 0 {
						self.state = State::Release;
					}
					break;
				}
			}
		}

		if self.action.contains(AdsrAction::Decay) {
			self.state = State::Decay;
			self.action -= AdsrAction::Decay;
		}

		if self.action.contains(AdsrAction::Release) {
			self.state = State::Release;
			self.action -= AdsrAction::Release;
		}

		if self.current < 0.0 {
			0.0
		} else if self.current > 1.0 {
			eprintln!("Warning: envelope overflow: {}", self.current);
			1.0
		} else {
			self.current
		}
	}
}


impl SequenceChannel {
	unsafe fn process_sound(&mut self, recalculateVolume: bool) {
		use SequenceChannelChanges as C;

		if recalculateVolume || self.changes.contains(C::Volume) {
			let mut channelVolume = self.volume * self.volumeScale * (*self.seqPlayer).appliedFadeVolume;
			if (*self.seqPlayer).flags.contains(SequencePlayerFlags::Muted) && self.muteBehavior.contains(MuteBehaviorFlags::Soften) {
				channelVolume *= (*self.seqPlayer).muteVolumeScale;
			}
			self.appliedVolume = channelVolume;
		}

		if self.changes.contains(C::Pan) {
			self.pan = self.newPan as u16 * self.panChannelWeight as u16;
		}

		for layer in self.layers {
			if layer.is_null() || !(*layer).flags.contains(SequenceChannelLayerFlags::Enabled) || (*layer).note.is_null() {
				continue;
			}
			let layer = unsafe { &mut *layer };
			if layer.flags.contains(SequenceChannelLayerFlags::NotePropertiesNeedInit) {
				layer.noteFreqScale = layer.freqScale * self.freqScale;
				layer.noteVelocity = layer.velocitySquare * self.appliedVolume;
				layer.notePan = ((self.pan + layer.pan as u16 * (0x80 - self.panChannelWeight) as u16) >> 7) as u8;
				layer.flags -= SequenceChannelLayerFlags::NotePropertiesNeedInit;
			} else {
				if self.changes.contains(C::FreqScale) {
					layer.noteFreqScale = layer.freqScale * self.freqScale;
				}
				if recalculateVolume || self.changes.contains(C::Volume) {
					layer.noteVelocity = layer.velocitySquare * self.appliedVolume;
				}
				if self.changes.contains(C::Pan) {
					layer.notePan = ((self.pan + layer.pan as u16 * (0x80 - self.panChannelWeight) as u16) >> 7) as u8;
				}
			}
		}

		self.changes = C::empty();
	}
}

impl Portamento {
	fn get_freq_scale(&mut self) -> f32 {
		self.cur += self.speed;
		let v0 = (self.cur as usize).min(0x7F);
		1.0 + self.extent * (gPitchBendFrequencyScale[v0 + 0x80] - 1.0)
	}
}

impl VibratoState {
	fn get_pitch_change(&mut self) -> i16 {
		self.time += self.rate as u32;
		let index = (self.time >> 10) & WAVE_CYCLE_SAMPLES_MASK as u32;
		unsafe { self.curve.add(index as usize).read() >> 8 }
	}

	unsafe fn get_freq_scale(&mut self) -> f32 {
		if self.delay != 0 {
			self.delay -= 1;
			return 1.0;
		}

		let seqChannel = unsafe { &mut *self.seqChannel };

		macro_rules! updateTimer {
			($var:ident $ChangeTimer:ident $vibratoTarget:ident $vibratoChangeDelay:ident) => {
				if self.$ChangeTimer != 0 {
					if self.$ChangeTimer == 1 {
						self.$var = seqChannel.$vibratoTarget as f32;
					} else {
						self.$var += (seqChannel.$vibratoTarget as f32 - self.$var) / self.$ChangeTimer as f32;
					}

					self.$ChangeTimer -= 1;
				} else if seqChannel.$vibratoTarget as i32 != self.$var as i32 {
					self.$ChangeTimer = seqChannel.$vibratoChangeDelay;
					if self.$ChangeTimer == 0 {
						self.$var = seqChannel.$vibratoTarget as f32;
					}
				}
			}
		}

		updateTimer!(extent extentChangeTimer vibratoExtentTarget vibratoExtentChangeDelay);
		updateTimer!(rate rateChangeTimer vibratoRateStart vibratoRateChangeDelay);

		if self.extent == 0.0 {
			return 1.0;
		}

		let pitchChange = self.get_pitch_change() as i32;
		let extent = self.extent / 4096.0;
		let result = 1.0 + extent * (gPitchBendFrequencyScale[(pitchChange + 0x80) as usize] - 1.0);
		result
	}
}
