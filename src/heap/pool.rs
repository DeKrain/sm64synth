use crate::support::{DynThin, PtrExt};
use crate::structs::*;
use super::*;

#[repr(align(16))]
pub struct Aligned16<T>(T);

const AUDIO_HEAP_SIZE: usize = 0x2c500;
const AUDIO_INIT_POOL_SIZE: usize = 0x2c00;

#[no_mangle] pub static gAudioHeapSize: usize = 2*AUDIO_HEAP_SIZE;
#[no_mangle] pub static gAudioInitPoolSize: usize = 2*AUDIO_INIT_POOL_SIZE;

//const AUDIO_HEAP_INIT_SIZE: usize = 2*0x31200 - 0x3800;
const AUDIO_HEAP_INIT_SIZE: usize = 2*AUDIO_HEAP_SIZE;
#[no_mangle] static mut gAudioHeap: Aligned16<[u8; AUDIO_HEAP_INIT_SIZE]> = Aligned16([0; AUDIO_HEAP_INIT_SIZE]);

#[inline]
pub(crate) unsafe fn zero_heap() {
	std::ptr::addr_of_mut!(gAudioHeap.0).cast::<u8>().write_bytes(0, gAudioHeapSize);
}

#[no_mangle] pub static mut gAudioSessionPool: SoundAllocPool = SoundAllocPool::INIT;
#[no_mangle] pub static mut gAudioInitPool: SoundAllocPool = SoundAllocPool::INIT;
#[no_mangle] pub static mut gNotesAndBuffersPool: SoundAllocPool = SoundAllocPool::INIT;

#[no_mangle] pub static mut gSeqAndBankPool: SoundAllocPool = SoundAllocPool::INIT;
#[no_mangle] pub static mut gPersistentCommonPool: SoundAllocPool = SoundAllocPool::INIT;
#[no_mangle] pub static mut gTemporaryCommonPool: SoundAllocPool = SoundAllocPool::INIT;

#[no_mangle] pub static mut gSeqLoadedPool: SoundMultiPool = SoundMultiPool::INIT;
#[no_mangle] pub static mut gBankLoadedPool: SoundMultiPool = SoundMultiPool::INIT;
#[no_mangle] pub static mut gUnusedLoadedPool: SoundMultiPool = SoundMultiPool::INIT;

pub struct SoundAllocPool {
	pub(crate) start: *mut u8,
	pub(crate) cur: *mut u8,
	pub(crate) size: u32,
	pub(crate) numAllocatedEntries: u32,
}

impl SoundAllocPool {
	pub const INIT: Self = Self { start: 0 as _, cur: 0 as _, size: 0, numAllocatedEntries: 0 };

	pub fn init(&mut self, memAddr: *mut u8, mut size: usize) {
		let alignedAddr = ((memAddr as usize + 0xF) & !0xF) as *mut u8;
		size -= alignedAddr as usize - memAddr as usize;

		self.start = alignedAddr;
		self.cur = alignedAddr;
		self.size = size as u32;
		self.numAllocatedEntries = 0;
	}

	#[inline]
	pub unsafe fn alloc<T: ?Sized + DynThin>(&mut self, size: u32) -> *mut T {
		self.alloc_raw(size).cast_thin()
	}

	unsafe fn alloc_raw(&mut self, size: u32) -> *mut () {
		let alignedSize = (size + 0xF) & !0xF;

		let start = self.cur;
		if start.add(alignedSize as usize) <= self.start.add(self.size as usize) {
			start.write_bytes(0, alignedSize as usize);
			self.cur = self.cur.offset(alignedSize as isize);
		} else {
			return std::ptr::null_mut();
		}

		self.numAllocatedEntries += 1;
		start.cast()
	}

	#[inline]
	pub unsafe fn alloc_uninitialized<T: ?Sized + DynThin>(self: &mut SoundAllocPool, size: u32) -> *mut T {
		self.alloc_uninitialized_raw(size).cast_thin()
	}

	unsafe fn alloc_uninitialized_raw(&mut self, size: u32) -> *mut () {
		let alignedSize = (size + 0xF) & !0xF;

		let start = self.cur;
		if start.offset(alignedSize as isize) <= self.start.offset(self.size as isize) {
			self.cur = self.cur.offset(alignedSize as isize);
		} else {
			return std::ptr::null_mut();
		}

		self.numAllocatedEntries += 1;
		start.cast()
	}

	#[inline]
	fn area_as_ptr_range(&self) -> std::ops::Range<*const ()> {
		self.start.cast() .. unsafe { self.start.add(self.size as usize) }.cast()
	}
}

pub struct PersintentPool {
	numEntries: u32,
	pool: SoundAllocPool,
	entries: [SeqBankOrEntry; 32],
}

impl PersintentPool {
	pub const INIT: Self = Self { numEntries: 0, pool: SoundAllocPool::INIT, entries: [SeqBankOrEntry::INIT; 32] };

	pub unsafe fn clear(&mut self) {
		self.pool.numAllocatedEntries = 0;
		self.pool.cur = self.pool.start;
		self.numEntries = 0;
	}
}

pub struct TemporaryPool {
	pub nextSide: u32,
	pool: SoundAllocPool,
	pub entries: [SeqBankOrEntry; 2],
}

impl TemporaryPool {
	pub const INIT: Self = Self { nextSide: 0, pool: SoundAllocPool::INIT, entries: [SeqBankOrEntry::INIT; 2] };

	pub unsafe fn clear(&mut self) {
		self.pool.numAllocatedEntries = 0;
		self.pool.cur = self.pool.start;
		self.nextSide = 0;
		self.entries[0].ptr = self.pool.start;
		self.entries[1].ptr = self.pool.start.offset(self.pool.size as isize);
		self.entries[0].id = -1;
		self.entries[1].id = -1;
	}
}

pub struct SoundMultiPool {
	persistent: PersintentPool,
	pub temporary: TemporaryPool,
}

impl SoundMultiPool {
	pub const INIT: Self = Self { persistent: PersintentPool::INIT, temporary: TemporaryPool::INIT };

	/// Checks if the given pointer (only start address) is a part of this pool.
	// Originally as part of playback::get_instrument_inner, moved here as it's pool specific
	#[inline]
	pub fn contains_object(&self, ptr: *const ()) -> bool {
		/*(
			self.persistent.pool.start.cast_const().cast() <= ptr
			&& ptr <= self.persistent.pool.start.add(self.persistent.pool.size as usize).cast_const().cast()
		) || (
			self.temporary.pool.start.cast_const().cast() <= ptr
			&& ptr <= self.temporary.pool.start.add(self.temporary.pool.size as usize).cast_const().cast()
		)*/

		// Clean solutions ftw!
		self.persistent.pool.area_as_ptr_range().contains(&ptr) ||
		self.temporary.pool.area_as_ptr_range().contains(&ptr)
	}
}

pub(crate) struct PoolSplit {
	pub wantSeq: u32,
	pub wantBank: u32,
	pub wantUnused: u32,
	pub wantCustom: u32,
}

pub(crate) struct PoolSplit2 {
	wantPersistent: u32,
	wantTemporary: u32,
}

impl PoolSplit {
	pub const INIT: Self = Self { wantSeq: 0, wantBank: 0, wantUnused: 0, wantCustom: 0 };
}

impl PoolSplit2 {
	pub const INIT: Self = Self { wantPersistent: 0, wantTemporary: 0 };
}

static mut sSessionPoolSplit: PoolSplit = PoolSplit::INIT;
static mut sSeqAndBankPoolSplit: PoolSplit2 = PoolSplit2::INIT;
static mut sPersistentCommonPoolSplit: PoolSplit = PoolSplit::INIT;
static mut sTemporaryCommonPoolSplit: PoolSplit = PoolSplit::INIT;

pub struct SeqBankOrEntry {
	ptr: *mut u8,
	size: u32,
	//poolIndex: i16,
	//pub id: i16,
	pub id: i32,
}

impl SeqBankOrEntry {
	pub const INIT: Self = Self { ptr: std::ptr::null_mut(), size: 0, id: 0 };
}

pub unsafe fn sound_init_main_pools(sizeForAudioInitPool: usize) {
	gAudioInitPool.init(gAudioHeap.0.as_mut_ptr(), sizeForAudioInitPool);
	gAudioSessionPool.init(gAudioHeap.0.as_mut_ptr().add(sizeForAudioInitPool), gAudioHeapSize - sizeForAudioInitPool);
}

unsafe fn session_pools_init(a: &PoolSplit) {
	gAudioSessionPool.cur = gAudioSessionPool.start;
	gNotesAndBuffersPool.init(gAudioSessionPool.alloc_uninitialized(a.wantSeq), a.wantSeq as usize);
	gSeqAndBankPool.init(gAudioSessionPool.alloc_uninitialized(a.wantCustom), a.wantCustom as usize);
}

unsafe fn seq_and_bank_pool_init(a: &PoolSplit2) {
	gSeqAndBankPool.cur = gSeqAndBankPool.start;
	gPersistentCommonPool.init(gSeqAndBankPool.alloc_uninitialized(a.wantPersistent), a.wantPersistent as usize);
	gTemporaryCommonPool.init(gSeqAndBankPool.alloc_uninitialized(a.wantTemporary), a.wantTemporary as usize);
}

unsafe fn persistent_pools_init(a: &PoolSplit) {
	gPersistentCommonPool.cur = gPersistentCommonPool.start;
	gSeqLoadedPool.persistent.pool.init(gPersistentCommonPool.alloc_uninitialized(a.wantSeq), a.wantSeq as usize);
	gBankLoadedPool.persistent.pool.init(gPersistentCommonPool.alloc_uninitialized(a.wantBank), a.wantBank as usize);
	gUnusedLoadedPool.persistent.pool.init(gPersistentCommonPool.alloc_uninitialized(a.wantUnused),
	              a.wantUnused as usize);
	gSeqLoadedPool.persistent.clear();
	gBankLoadedPool.persistent.clear();
	gUnusedLoadedPool.persistent.clear();
}

unsafe fn temporary_pools_init(a: &PoolSplit) {
	gTemporaryCommonPool.cur = gTemporaryCommonPool.start;
	gSeqLoadedPool.temporary.pool.init(gTemporaryCommonPool.alloc_uninitialized(a.wantSeq), a.wantSeq as usize);
	gBankLoadedPool.temporary.pool.init(gTemporaryCommonPool.alloc_uninitialized(a.wantBank), a.wantBank as usize);
	gUnusedLoadedPool.temporary.pool.init(gTemporaryCommonPool.alloc_uninitialized(a.wantUnused),
	              a.wantUnused as usize);
	gSeqLoadedPool.temporary.clear();
	gBankLoadedPool.temporary.clear();
	gUnusedLoadedPool.temporary.clear();
}

pub(super) unsafe fn reset_session_memory(preset: &crate::structs::AudioSessionSettingsEU) {
	let persistentMem = 2*(preset.persistentBankMem + preset.persistentSeqMem);
	let temporaryMem = 2*(preset.temporaryBankMem + preset.temporarySeqMem);
	let totalMem = persistentMem + temporaryMem;
	let wantMisc = gAudioSessionPool.size - totalMem - 0x100;
	sSessionPoolSplit.wantSeq = wantMisc;
	sSessionPoolSplit.wantCustom = totalMem;
	session_pools_init(&sSessionPoolSplit);
	sSeqAndBankPoolSplit.wantPersistent = persistentMem;
	sSeqAndBankPoolSplit.wantTemporary = temporaryMem;
	seq_and_bank_pool_init(&sSeqAndBankPoolSplit);
	sPersistentCommonPoolSplit.wantSeq = 2*preset.persistentSeqMem;
	sPersistentCommonPoolSplit.wantBank = 2*preset.persistentBankMem;
	sPersistentCommonPoolSplit.wantUnused = 0;
	persistent_pools_init(&sPersistentCommonPoolSplit);
	sTemporaryCommonPoolSplit.wantSeq = 2*preset.temporarySeqMem;
	sTemporaryCommonPoolSplit.wantBank = 2*preset.temporaryBankMem;
	sTemporaryCommonPoolSplit.wantUnused = 0;
	temporary_pools_init(&sTemporaryCommonPoolSplit);
}

pub enum AllocBankSeqMode {
	UseTemporary = 0,
	UsePersistent = 1,
	UsePersistent_FallbackTemporary = 2,
}

pub unsafe fn alloc_bank_or_seq(pool: &mut SoundMultiPool, elementCount: u32, size: u32, mode: AllocBankSeqMode, id: i32) -> *mut () {
	const nullID: i32 = -1;
	if let AllocBankSeqMode::UseTemporary = mode {
		let (table, isSound) = if (pool as *const SoundMultiPool) == std::ptr::addr_of!(gSeqLoadedPool) {
			(gSeqLoadStatus.as_mut_ptr(), false)
		} else {
			(gBankLoadStatus.as_mut_ptr(), true)
		};
		let tp = &mut pool.temporary;

		let firstVal = if tp.entries[0].id == nullID {
			SoundLoadStatus::NotLoaded
		} else {
			*table.add(tp.entries[0].id as usize)
		};
		let secondVal = if tp.entries[1].id == nullID {
			SoundLoadStatus::NotLoaded
		} else {
			*table.add(tp.entries[1].id as usize)
		};

		if firstVal == SoundLoadStatus::NotLoaded {
			tp.nextSide = 0;
		} else if secondVal == SoundLoadStatus::NotLoaded {
			tp.nextSide = 1;
		} else {
			eprintln!("Warning: no free autoseq area");
			match (firstVal, secondVal) {
				(SoundLoadStatus::Discardable, SoundLoadStatus::Discardable) => {
					// Use the opposite side from last time.
				}
				(SoundLoadStatus::Discardable, _) => {
					tp.nextSide = 0;
				}
				(_, SoundLoadStatus::Discardable) => {
					tp.nextSide = 1;
				}
				_ => {
					eprintln!("Warning: no stop auto area. Trying to force stop one");
					if firstVal != SoundLoadStatus::InProgress {
						tp.nextSide = 0;
					} else if secondVal != SoundLoadStatus::InProgress {
						tp.nextSide = 1;
					} else {
						eprintln!("Two sides are leading... alloc cancelled!");
						return std::ptr::null_mut();
					}
				}
			}
		}

		let pool = &mut tp.pool;
		if tp.entries[tp.nextSide as usize].id != nullID {
			*table.add(tp.entries[tp.nextSide as usize].id as usize) = SoundLoadStatus::NotLoaded;
			if isSound {
				discard_bank(tp.entries[tp.nextSide as usize].id as u8);
			}
		}

		let ret;
		if tp.nextSide == 0 {
			tp.entries[0].ptr = pool.start;
			tp.entries[0].id = id;
			tp.entries[0].size = size;

			pool.cur = pool.start.add(size as usize);
			if tp.entries[1].ptr < pool.cur {
				eprintln!("Warning: before area overlaid after");

				// Throw out the entry on the other side if it doesn't fit.
				// (possible @bug: what if it's currently being loaded?)
				*table.add(tp.entries[1].id as usize) = SoundLoadStatus::NotLoaded;

				if isSound {
					discard_bank(tp.entries[1].id as u8);
				} else {
					discard_sequence(tp.entries[1].id as u8);
				}

				tp.entries[1].id = nullID;
				tp.entries[1].ptr = pool.start.add(pool.size as usize);
			}

			ret = tp.entries[0].ptr;
		} else {
			tp.entries[1].ptr = pool.start.add(pool.size as usize - size as usize - 0x10);
			tp.entries[1].id = id;
			tp.entries[1].size = size;

			if tp.entries[1].ptr < pool.cur {
				eprintln!("Warning: after area overlaid before");

				*table.add(tp.entries[0].id as usize) = SoundLoadStatus::NotLoaded;

				if isSound {
					discard_bank(tp.entries[0].id as u8);
				} else {
					discard_sequence(tp.entries[0].id as u8);
				}

				tp.entries[0].id = nullID;
				pool.cur = pool.start;
			}

			ret = tp.entries[1].ptr;
		}

		// Switch sides for next time in case both entries are SoundLoadStatus::Discardable.
		tp.nextSide ^= 1;

		return ret.cast();
	}

	let ret = pool.persistent.pool.alloc(elementCount * size);
	pool.persistent.entries[pool.persistent.numEntries as usize].ptr = ret;

	if ret.is_null() {
		match mode {
			AllocBankSeqMode::UsePersistent_FallbackTemporary => {
				eprintln!("Stay heap overflow");
				return alloc_bank_or_seq(pool, elementCount, size, AllocBankSeqMode::UseTemporary, id);
			}
			AllocBankSeqMode::UsePersistent => {
				eprintln!("Stay heap overflow (required {:X})", elementCount * size);
				return std::ptr::null_mut();
			}
			AllocBankSeqMode::UseTemporary => unreachable!(),
		}
	}

	// TODO: why is this guaranteed to write <= 32 entries...?
	// Because the buffer is small enough that more don't fit?
	pool.persistent.entries[pool.persistent.numEntries as usize].id = id;
	pool.persistent.entries[pool.persistent.numEntries as usize].size = size;
	let ret = pool.persistent.entries[pool.persistent.numEntries as usize].ptr.cast();
	pool.persistent.numEntries += 1;
	ret
}

pub unsafe fn get_bank_or_seq(pool: &mut SoundMultiPool, arg1: i32, id: i32) -> *mut () {
	let temporary = &mut pool.temporary;
	if arg1 == 0 {
		// Try not to overwrite sound that we have just accessed, by setting nextSide appropriately.
		if temporary.entries[0].id == id {
			temporary.nextSide = 1;
			return temporary.entries[0].ptr.cast();
		} else if temporary.entries[1].id == id {
			temporary.nextSide = 0;
			return temporary.entries[1].ptr.cast();
		}
		eprintln!("Error: audio heap uninit for ID {id}");
		std::ptr::null_mut()
	} else {
		let persistent = &pool.persistent;
		for entry in &persistent.entries[..persistent.numEntries as usize] {
			if id == entry.id {
				// println!("Cache hit {id} at stay {index}");
				return entry.ptr.cast();
			}
		}

		if arg1 == 2 {
			get_bank_or_seq(pool, 0, id)
		} else {
			std::ptr::null_mut()
		}
	}
}
