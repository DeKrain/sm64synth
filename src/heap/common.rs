use crate::{
	ultra::*,
	structs::*,
	load::*,
	data::*,
	synthesis::*,
	playback::*,
};
use super::*;

use std::mem::size_of;
use std::sync::atomic::{Ordering, AtomicU8};

// #[no_mangle] pub static mut gReverbDownsampleRate: i8 = 0;
// #[no_mangle] pub static mut sReverbDownsampleRateLog: u8 = 0;

#[no_mangle] pub static mut gBankLoadStatus: [SoundLoadStatus; 0x40] = [SoundLoadStatus::NotLoaded; 0x40];
#[no_mangle] pub static mut gSeqLoadStatus: [SoundLoadStatus; 0x100] = [SoundLoadStatus::NotLoaded; 0x100];

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u8)]
pub enum AudioResetStatus {
	Complete,
	StepResetSession,
	StepClearBuffers2,
	StepClearBuffers,
	StepReleaseAdsr,
	StepStopPlayers,
}

pub struct AudioResetStatusCell(AtomicU8);

impl AudioResetStatusCell {
	const INIT: Self = AudioResetStatusCell(AtomicU8::new(0));

	pub fn get(&self) -> AudioResetStatus {
		unsafe { std::mem::transmute(self.0.load(Ordering::SeqCst)) }
	}

	pub fn set(&self, status: AudioResetStatus) {
		self.0.store(status as u8, Ordering::SeqCst);
	}
}

#[no_mangle] pub static gAudioResetStatus: AudioResetStatusCell = AudioResetStatusCell::INIT;
#[no_mangle] pub static mut gAudioResetPresetIdToLoad: u8 = 0;
#[no_mangle] pub static mut gAudioResetFadeOutFramesLeft: u32 = 0;

macro_rules! align16 {
	($val: expr) => {
		($val + 0xF) & !0xF
	};
}

pub enum ResetState {
	Ready,
	InProgress,
}

pub unsafe fn audio_shut_down_and_reset_step() -> ResetState {
	use AudioResetStatus::*;
	match gAudioResetStatus.get() {
		StepStopPlayers => {
			for player in &mut gSequencePlayers {
				player.disable();
			}
			gAudioResetFadeOutFramesLeft = 4;
			gAudioResetStatus.set(StepReleaseAdsr);
		}
		StepReleaseAdsr => {
			if gAudioResetFadeOutFramesLeft != 0 {
				gAudioResetFadeOutFramesLeft -= 1;
				decrease_reverb_gain();
			} else {
				for note in std::slice::from_raw_parts_mut(gNotes, gMaxSimultaneousNotes as usize) {
					if note.noteSubEu.flags.contains(NoteSubEuFlags::Enabled) && note.playbackState.adsr.state != AdsrStateState::Disabled {
						note.playbackState.adsr.fadeOutVel = gAudioBufferParameters.updatesPerFrameInv;
						note.playbackState.adsr.action |= AdsrAction::Release;
					}
				}
				gAudioResetFadeOutFramesLeft = 16;
				gAudioResetStatus.set(StepClearBuffers);
			}
		}
		StepClearBuffers => {
			if gAudioResetFadeOutFramesLeft != 0 {
				gAudioResetFadeOutFramesLeft -= 1;
				decrease_reverb_gain();
			} else {
				for buf in gAiBuffers {
					buf.cast::<u8>().write_bytes(0, AIBUFFER_LEN);
				}
				gAudioResetFadeOutFramesLeft = 4;
				gAudioResetStatus.set(StepClearBuffers2);
			}
		}
		StepClearBuffers2 => {
			// clear_curr_ai_buffer();
			if gAudioResetFadeOutFramesLeft != 0 {
				gAudioResetFadeOutFramesLeft -= 1;
			} else {
				gAudioResetStatus.set(StepResetSession);
				// func_sh_802f23ec()
			}
		}
		StepResetSession => {
			audio_reset_session();
			gAudioResetStatus.set(Complete);
		}
		Complete => {}
	}
	if gAudioResetStatus.get() < StepClearBuffers {
		ResetState::Ready
	} else {
		ResetState::InProgress
	}
}

pub unsafe fn audio_reset_session(/*preset: &AudioSessionSettings*/) {
	let preset = &gAudioSessionPresets[gAudioResetPresetIdToLoad as usize];

	gSampleDmaNumListItems = 0;
	gAudioBufferParameters.frequency = preset.frequency as u16;
	gAudioBufferParameters.aiFrequency = osAiSetFrequency(preset.frequency) as u16;
	gAudioBufferParameters.samplesPerFrameTarget = align16!(preset.frequency / gRefreshRate) as u16;
	gAudioBufferParameters.minAiBufferLength = gAudioBufferParameters.samplesPerFrameTarget - 0x10;
	gAudioBufferParameters.maxAiBufferLength = gAudioBufferParameters.samplesPerFrameTarget + 0x10;
	gAudioBufferParameters.updatesPerFrame = (gAudioBufferParameters.samplesPerFrameTarget + 0x10) / 160 + 1;
	gAudioBufferParameters.samplesPerUpdate = (gAudioBufferParameters.samplesPerFrameTarget / gAudioBufferParameters.updatesPerFrame) & 0xfff8;
	gAudioBufferParameters.samplesPerUpdateMax = gAudioBufferParameters.samplesPerUpdate + 8;
	gAudioBufferParameters.samplesPerUpdateMin = gAudioBufferParameters.samplesPerUpdate - 8;
	gAudioBufferParameters.resampleRate = 32000.0 / gAudioBufferParameters.frequency as f32;
	gAudioBufferParameters.unkUpdatesPerFrameScaled = (3.0 / 1280.0) / gAudioBufferParameters.updatesPerFrame as f32;
	gAudioBufferParameters.updatesPerFrameInv = 1.0 / gAudioBufferParameters.updatesPerFrame as f32;

	gMaxSimultaneousNotes = preset.maxSimultaneousNotes as i32;
	gVolume = preset.volume as i16;
	gTempoInternalToExternal = (gAudioBufferParameters.updatesPerFrame as f32 * 2880000.0 / gTatumsPerBeat as f32 / gAproxRefreshInterval) as u32 as i16;

	gAudioBufferParameters.presetUnk4 = preset.unk1 as u16;
	gAudioBufferParameters.samplesPerFrameTarget *= gAudioBufferParameters.presetUnk4;
	gAudioBufferParameters.maxAiBufferLength *= gAudioBufferParameters.presetUnk4;
	gAudioBufferParameters.minAiBufferLength *= gAudioBufferParameters.presetUnk4;
	gAudioBufferParameters.updatesPerFrame *= gAudioBufferParameters.presetUnk4;

	gMaxAudioCmds = gMaxSimultaneousNotes * 0x10 * gAudioBufferParameters.updatesPerFrame as i32 + (preset.reverbSettings.len() * 0x20) as i32 + 0x300;

	/*
		reverbWindowSize = preset.reverbWindowSize as i32;
		gAiFrequency = osAiSetFrequency(preset.frequency);
		gMaxSimultaneousNotes = preset.maxSimultaneousNotes as i32;
		gSamplesPerFrameTarget = ((gAiFrequency / 60) + 0xf) & !0xf;
		gReverbDownsampleRate = preset.reverbDownsampleRate as i8;
		sReverbDownsampleRateLog = match gReverbDownsampleRate {
			1 => 0,
			2 => 1,
			4 => 2,
			8 => 3,
			16 => 4,
			_ => 0,
		};
		gReverbDownsampleRate = preset.reverbDownsampleRate as i8;
		gVolume = preset.volume as i16;
		gMinAiBufferLength = gSamplesPerFrameTarget - 0x10;
		let updatesPerFrame: i8 = gSamplesPerFrameTarget / 160 + 1;
		gAudioUpdatesPerFrame = gSamplesPerFrameTarget / 160 + 1;
		/* Compute conversion ratio from the internal unit tatums/tick to the
		external beats/minute (JP) or tatums/minute (US). In practice this is
		300 on JP and 14360 on US. */
		gTempoInternalToExternal = (updatesPerFrame * 2880000.0 / gTatumsPerBeat / 16.713) as u32;
		gMaxAudioCmds = gMaxSimultaneousNotes * 20 * updatesPerFrame + 320;
	*/

	reset_session_memory(preset);

	reset_bank_and_seq_load_status();
	gNotes = gNotesAndBuffersPool.alloc(gMaxSimultaneousNotes as u32 * size_of::<Note>() as u32);
	note_init_all();
	init_note_free_list();

	gNoteSubsEu = gNotesAndBuffersPool.alloc(gAudioBufferParameters.updatesPerFrame as u32 * gMaxSimultaneousNotes as u32 * size_of::<NoteSubEu>() as u32);
	gAudioCmdBuffers.fill_with(||
		gNotesAndBuffersPool.alloc(gMaxAudioCmds as u32 * size_of::<crate::ultra64_abi::Acmd>() as u32));
	for reverb in &mut gSynthesisReverbs {
		reverb.useReverb = false;
	}
	gNumSynthesisReverbs = preset.reverbSettings.len() as u8;
	for (reverb, reverbSettings) in std::iter::zip(&mut gSynthesisReverbs, preset.reverbSettings) {
		reverb.windowSize = (reverbSettings.windowSize as u16) << 6;
		reverb.downsampleRate = reverbSettings.downsampleRate;
		reverb.reverbGain = reverbSettings.gain;
		reverb.useReverb = false;
		reverb.ringBufferLeft = gNotesAndBuffersPool.alloc(reverb.windowSize as u32 * 2);
		reverb.ringBufferRight = gNotesAndBuffersPool.alloc(reverb.windowSize as u32 * 2);
		reverb.nextRingBufferPos = 0;
		reverb.curFrame = 0;
		reverb.bufSizePerChannel = reverb.windowSize as u32;
		reverb.framesLeftToIgnore = 2;
		if reverb.downsampleRate != 1 {
			reverb.resampleFlags = crate::ultra64_abi::A_INIT;
			reverb.resampleRate = 0x8000 / reverb.downsampleRate as u16;
			reverb.resampleStateLeft = gNotesAndBuffersPool.alloc((16 * size_of::<i16>()) as u32);
			reverb.resampleStateRight = gNotesAndBuffersPool.alloc((16 * size_of::<i16>()) as u32);
			for i in 0..gAudioBufferParameters.updatesPerFrame as usize {
				for items in &mut reverb.items {
					let mem = gNotesAndBuffersPool.alloc(DEFAULT_LEN_2CH as u32);
					items[i].toDownsampleLeft = mem;
					items[i].toDownsampleRight = mem.cast::<u8>().add(DEFAULT_LEN_1CH).cast();
				}
			}
		}
	}

	init_sample_dma_buffers(gMaxSimultaneousNotes);

	build_vol_rampings_table(gAudioBufferParameters.samplesPerUpdate as u32);
}


unsafe fn build_vol_rampings_table(len: u32) {
	let k = len / 8;
	let mut step = 0;
	for i in 0..0x400 {
		let d = if step == 0 { 1f64 } else { step as f64 };

		gLeftVolRampings [0][i] = kth_root(      d, k - 1) as f32;
		gRightVolRampings[0][i] = kth_root(1.0 / d, k - 1) as f32 * 0x10000 as f32;
		gLeftVolRampings [1][i] = kth_root(      d, k    ) as f32;
		gRightVolRampings[1][i] = kth_root(1.0 / d, k    ) as f32 * 0x10000 as f32;
		gLeftVolRampings [2][i] = kth_root(      d, k + 1) as f32;
		gRightVolRampings[2][i] = kth_root(1.0 / d, k + 1) as f32 * 0x10000 as f32;

		step += 0x20;
	}

	/// Computes `d ** (1/k)`, assuming `k` is in `9..=24`.
	fn kth_root(d: f64, k: u32) -> f64 {
		if d == 0.0 {
			return 1.0;
		}
		
		let mut root = 1.5;
		for _ in 0..64 {
			let next = root_newton_step(root, k, d);
			let diff = (next - root).abs();

			root = next;
			if diff < 1e-7 {
				break;
			}
		}
		root
	}

	/// Computes a newton's method step for f(x) = x**k - d.
	/// Assumes `k` is in range `9..=24`
	fn root_newton_step(x: f64, k: u32, d: f64) -> f64 {
		let x2 = x * x;
		let x4 = x2 * x2;
		let x8 = x4 * x4;
		let degree = k - 9;

		let mut deriv = x8;
		if degree & 1 != 0 {
			deriv *= x;
		}
		if degree & 2 != 0 {
			deriv *= x2;
		}
		if degree & 4 != 0 {
			deriv *= x4;
		}
		if degree & 8 != 0 {
			deriv *= x8;
		}
		let fx = deriv * x - d;
		deriv = k as f64 * deriv;
		x - fx / deriv
	}
}

unsafe fn reset_bank_and_seq_load_status() {
	gBankLoadStatus.fill(SoundLoadStatus::NotLoaded);
	gSeqLoadStatus.fill(SoundLoadStatus::NotLoaded);
}

pub(super) unsafe fn discard_bank(bankId: u8) {
	println!("Discard bank");
	for note in std::slice::from_raw_parts_mut(gNotes, gMaxSimultaneousNotes as usize) {
		if note.noteSubEu.bankId != bankId {
			continue;
		}
		eprintln!("Warning: killing note on bank discard");
		if note.playbackState.priority >= NotePriority::Min {
			eprintln!("Killing voice {} (ID {}) {:?}",
				note.playbackState.waveId,
				bankId,
				note.playbackState.priority);
			eprintln!("Warning: running sequence's data disappeared");
			(*note.playbackState.parentLayer).flags -= SequenceChannelLayerFlags::Enabled;
			(*note.playbackState.parentLayer).flags |= SequenceChannelLayerFlags::Finished;
		}
		note.disable();
		AudioListItem::remove(&mut note.listItem);
		gNoteFreeLists.disabled.push_back(&mut note.listItem);
	}
}

pub(super) unsafe fn discard_sequence(seqId: u8) {
	println!("Discard sequence");
	for player in &mut gSequencePlayers {
		if player.flags.contains(SequencePlayerFlags::Enabled) && player.seqId == seqId {
			player.disable();
		}
	}
}

unsafe fn decrease_reverb_gain() {
	for reverb in &mut gSynthesisReverbs[..gNumSynthesisReverbs as usize] {
		reverb.reverbGain -= reverb.reverbGain / 8;
	}
}
