//! Ultra 64 Audio Binary Interface.
//!
//! This is based on the official SGI SDK.

#![allow(dead_code)]

pub mod mixer;

use std::ptr::NonNull;

// Audio commands
const A_SPNOOP      : u32 = 0;
const A_ADPCM       : u32 = 1;
const A_CLEARBUFF   : u32 = 2;
const A_ENVMIXER    : u32 = 3;
const A_LOADBUFF    : u32 = 4;
const A_RESAMPLE    : u32 = 5;
const A_SAVEBUFF    : u32 = 6;
const A_SEGMENT     : u32 = 7;
const A_SETBUFF     : u32 = 8;
const A_SETVOL      : u32 = 9;
const A_DMEMMOVE    : u32 = 10;
const A_LOADADPCM   : u32 = 11;
const A_MIXER       : u32 = 12;
const A_INTERLEAVE  : u32 = 13;
const A_POLEF       : u32 = 14;
const A_SETLOOP     : u32 = 15;

pub type AudioFlags = u8;

pub const A_INIT    : AudioFlags = 0x01;
pub const A_CONTINUE: AudioFlags = 0x00;
pub const A_LOOP    : AudioFlags = 0x02;
pub const A_OUT     : AudioFlags = 0x02;
pub const A_LEFT    : AudioFlags = 0x02;
pub const A_RIGHT   : AudioFlags = 0x00;
pub const A_VOL     : AudioFlags = 0x04;
pub const A_RATE    : AudioFlags = 0x00;
pub const A_AUX     : AudioFlags = 0x08;
pub const A_NOAUX   : AudioFlags = 0x00;
pub const A_MAIN    : AudioFlags = 0x00;
pub const A_MIX     : AudioFlags = 0x10;

#[repr(C)]
pub struct Aadpcm {
	pub cmd: u8,
	pub flags: u8,
	pub gain: u16,
	pub addr: u32,
}

#[repr(C, align(8))]
pub struct Acmd(u32, usize);

/// ADPCM State
pub type ADPCM_STATE = [i16; 0x10];

/// Pole Filter State
pub type POLEF_STATE = [i16; 4];

/// Resampler State
pub type RESAMPLE_STATE = [i16; 0x10];

/// Enveloper/Mixer state
pub type ENVMIX_STATE = [i16; 0x28];

#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct AudioCmdPtr(NonNull<Acmd>);

impl AudioCmdPtr {
	#[inline(always)]
	pub const unsafe fn new(ptr: NonNull<Acmd>) -> Self {
		Self(ptr)
	}
	
	#[inline(always)]
	pub const fn get(self) -> NonNull<Acmd> {
		self.0
	}

	unsafe fn push_cmd(&mut self, cmd: Acmd) {
		*self.0.as_ptr() = cmd;
		self.0 = NonNull::new_unchecked(self.0.as_ptr().add(1));
	}
}

impl std::ops::Deref for AudioCmdPtr {
	type Target = NonNull<Acmd>;

	#[inline(always)]
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

macro_rules! word {
	{
		$(
			$value:expr ; $shift:expr ; $width:expr
		),+ $(,)?
	} => {
		($(
			($value as u32 & ((1u32 << $width) - 1)) << $shift
		)|+) as _
	};
}

impl AudioCmdPtr {
	/// Decompress ADPCM data.
	///
	/// Possible flags:
	/// - A_INIT
	/// - A_LOOP
	///
	/// ADPCM decompression works on a block of 16 (uncompressed) samples.
	/// The previous 2 samples and 9 bytes of input are decompressed to
	/// 16 new samples using the code book previously loaded.
	///
	/// Before the algorithm starts, the previous 16 samples are loaded according to flag:
	/// - `A_INIT`: all zeros
	/// - `A_LOOP`: the address set by aSetLoop
	/// - no flags: the DRAM address in the s parameter
	/// These 16 samples are immediately copied to the destination address.
	///
	/// The result of "count" bytes will be written after these 16 initial samples.
	/// The last 16 samples written to the destination will also be written to
	/// the state address in DRAM.
	pub unsafe fn aADPCMdec(&mut self, f: AudioFlags, s: *mut ADPCM_STATE) {
		self.push_cmd(Acmd(word! {
			A_ADPCM ; 24 ; 8,
			f ; 16 ; 8,
		}, s as usize));
	}

	// Not used in SM64.
	pub unsafe fn aPoleFilter(&mut self, flags: AudioFlags, g: u16, s: *mut POLEF_STATE) {
		self.push_cmd(Acmd(word! {
			A_POLEF ; 24 ; 8,
			flags ; 16 ; 8,
			g ; 0 ; 16,
		}, s as usize));
	}

	/// Clears DMEM data, at `addr` and `count` bytes, by writing zeros.
	///
	/// Note: `count` is rounded up to the nearest multiple of 16 bytes.
	pub unsafe fn aClearBuffer(&mut self, addr: u32, count: usize) {
		self.push_cmd(Acmd(word! {
			A_CLEARBUFF ; 24 ; 8,
			addr ; 0 ; 24,
		}, count));
	}

	/**
	Mixes an envelope with mono sound into 2 or 4 channels.
	Possible flags: A_INIT, A_AUX (indicates that 4 channels should be used).

	Before this command, call:
	```ignore
	cmd.aSetBuffer(0, inBuf, dryLeft, count);
	cmd.aSetBuffer(A_AUX, dryRight, wetLeft, wetRight);
	```

	The first time (`A_INIT` is set), volume also needs to be set:
	```ignore
	cmd.aSetVolume(A_VOL | A_LEFT, initialVolumeLeft, 0, 0);
	cmd.aSetVolume(A_VOL | A_RIGHT, initialVolumeRight, 0, 0);
	cmd.aSetVolume32(A_RATE | A_LEFT, targetVolumeLeft, rampLeft);
	cmd.aSetVolume32(A_RATE | A_RIGHT, targetVolumeRight, rampRight);
	cmd.aSetVolume(A_AUX, dryVolume, 0, wetVolume);
	```

	This command will now mix samples in inBuf into the destination buffers (dry and wet),
	but with the volume increased (or decreased) from initial volumes to target volumes,
	with the specified ramp rate. Once the target volume is reached, the volume stays
	at that level. Before the samples are finally mixed (added) into the destination
	buffers (dry and wet), the volume is changed according to dryVolume and wetVolume.

	**Note:** count will be rounded up to the nearest multiple of 16 bytes.

	**Note:** the wet channels are used for reverb.
	*/
	pub unsafe fn aEnvMixer(&mut self, flags: AudioFlags, state: *mut ENVMIX_STATE) {
		self.push_cmd(Acmd(word! {
			A_ENVMIXER ; 24 ; 8,
			flags ; 16 ; 8,
		}, state as usize));
	}

	/**
	Interleaves two mono channels into stereo.
	
	First call:
	```ignore
	cmd.aSetBuffer(0, 0, output, count)
	```
	
	The count refers to the size of each input. Hence 2 * count bytes will be written out.
	A left sample will be placed before the right sample.
	
	Note: count will be rounded up to the nearest multiple of 16 bytes.
	*/
	pub unsafe fn aInterleave(&mut self, left: u16, right: u16) {
		self.push_cmd(Acmd(word! {
			A_INTERLEAVE ; 24 ; 8
		}, word! {
			left ; 16 ; 16,
			right ; 0 ; 16,
		}));
	}

	/// Loads a buffer from DRAM to DMEM.
	///
	/// First call:
	/// ```ignore
	/// cmd.aSetBuffer(0, inp, 0, count)
	/// ```
	///
	/// The in parameter to aSetBuffer is the destination in DMEM and the
	/// `memAddr` parameter to this command is the source in DRAM.
	pub unsafe fn aLoadBuffer(&mut self, memAddr: *const ()) {
		self.push_cmd(Acmd(word! {
			A_LOADBUFF ; 24 ; 8
		}, memAddr as usize));
	}

	/**
	Mixes audio.
	Possible flags: no flags used, although parameter present.

	First call:
	```ignore
	cmd.aSetBuffer(0, 0, 0, count);
	```

	Input and output addresses are taken from the `inp` and `out` parameters, respectively.
	The volume with which the input is changed is taken from the `gain` parameter.
	After the volume of the input samples have been changed, the result is added to the output.

	**Note:** count will be rounded up to the nearest multiple of 32 bytes.
	*/
	pub unsafe fn aMix(&mut self, flags: AudioFlags, gain: u16, inp: u16, out: u16) {
		self.push_cmd(Acmd(word! {
			A_MIXER ; 24 ; 8,
			flags ; 16 ; 8,
			gain ; 0 ; 16,
		}, word! {
			inp ; 16 ; 16,
			out ; 0 ; 16,
		}));
	}

	// Not present in the audio microcode.
	/*pub unsafe fn aPan(&mut self, flags: AudioFlags, d: u16, s: *mut ()) {
		self.push_cmd(Acmd(word! {
			A_PAN ; 24 ; 8,
			flags ; 16 ; 8,
			d ; 0 ; 16,
		}, s as usize));
	}*/

	/**
	Resamples audio.
	Possible flags: A_INIT, A_OUT? (not used in SM64).

	First call:
	```ignore
	cmd.aSetBuffer(0, inp, out, count)
	```

	This command resamples the audio using the given frequency ratio (pitch)
	using a filter that uses a window of 4 source samples. This can be used
	either for just resampling audio to be able to be played back at a different
	sample rate, or to change the pitch if the result is played back at
	the same sample rate as the input.

	The frequency ratio is given in UQ1.15 fixed point format.
	For no change in frequency, use pitch 0x8000.
	For 1 octave up or downsampling to (roughly) half number of samples, use pitch 0xffff.
	For 1 octave down or upsampling to double as many samples, use pitch 0x4000.

	Note: count represents the number of output sample bytes and is rounded up to
	the nearest multiple of 16 bytes.

	The state consists of the four following source samples when the algorithm stopped as
	well as a fractional position, and is initialized to all zeros if A_INIT is given.
	Otherwise it is loaded from DRAM at address s.

	The algorithm starts by writing the four source samples from the state (or zero)
	to just before the input address given. It then creates one output sample by examining
	the four next source samples and then moving the source position zero or more
	samples forward. The first output sample (when A_INIT is given) is always 0.

	When "count" bytes have been written, the following four source samples
	are written to the state in DRAM as well as a fractional position.
	*/
	pub unsafe fn aResample(&mut self, flags: AudioFlags, pitch: u16, samples: *mut RESAMPLE_STATE) {
		self.push_cmd(Acmd(word! {
			A_RESAMPLE ; 24 ; 8,
			flags ; 16 ; 8,
			pitch ; 0 ; 16,
		}, samples as usize));
	}

	/// Stores a buffer in DMEM to DRAM.
	///
	/// First call:
	/// ```ignore
	/// cmd.aSetBuffer(0, 0, out, count)
	/// ```
	///
	/// The out parameter to aSetBuffer is the source in DMEM and the
	/// s parameter to this command is the destination in DRAM.
	pub unsafe fn aSaveBuffer(&mut self, memAddr: *mut ()) {
		self.push_cmd(Acmd(word! {
			A_SAVEBUFF ; 24 ; 8
		}, memAddr as usize));
	}

	/// Sets up an entry in the segment table.
	///
	/// The `seg` parameter is a segment index, 0 to 15.
	/// The `base` parameter is the base offset.
	pub unsafe fn aSegment(&mut self, seg: u8, base: u32) {
		self.push_cmd(Acmd(word! {
			A_SEGMENT ; 24 ; 8
		}, word! {
			seg ; 24 ; 8,
			base ; 0 ; 24,
		}))
	}

	/// Sets internal DMEM buffer addresses used for later commands.
	/// See each command for how to use aSetBuffer.
	pub unsafe fn aSetBuffer(&mut self, flags: AudioFlags, inp: u16, out: u16, count: u16) {
		self.push_cmd(Acmd(word! {
			A_SETBUFF ; 24 ; 8,
			flags ; 16 ; 8,
			inp ; 0 ; 16,
		}, word! {
			out ; 16 ; 16,
			count ; 0 ; 16,
		}));
	}

	/// Sets internal volume parameters.
	/// See aEnvMixer for more info.
	pub unsafe fn aSetVolume(&mut self, flags: AudioFlags, v: u16, t: u16, r: u16) {
		self.push_cmd(Acmd(word! {
			A_SETVOL ; 24 ; 8,
			flags ; 16 ; 16,
			v ; 0 ; 16,
		}, word! {
			t ; 16 ; 16,
			r ; 0 ; 16,
		}));
	}

	/// Sets internal volume parameters.
	/// See aEnvMixer for more info.
	///
	/// ## Note
	/// This is a version of aSetVolume which takes a single 32-bit parameter
	/// instead of two 16-bit ones. According to AziAudio, it is used to set
	/// ramping values when neither bit 4 nor bit 8 is set in the flags parameter.
	/// It does not appear in the official abi.h header.
	pub unsafe fn aSetVolume32(&mut self, flags: AudioFlags, v: u16, tr: u32) {
		self.push_cmd(Acmd(word! {
			A_SETVOL ; 24 ; 8,
			flags ; 16 ; 16,
			v ; 0 ; 16,
		},
			// Equivalent to taking higher 16-bits as `t` & lower 16-bits as `r`
			tr as usize
		));
	}

	/// Sets the address to ADPCM loop state.
	///
	/// The a parameter is a DRAM address.
	/// See aADPCMdec for more info.
	pub unsafe fn aSetLoop(&mut self, ptr: *const ADPCM_STATE) {
		self.push_cmd(Acmd(word! {
			A_SETLOOP ; 24 ; 8
		}, ptr as usize));
	}

	/**
	Copies memory in DMEM.

	Copies `count` bytes from address `source` to address `destination`.

	Note: count is rounded up to the nearest multiple of 16 bytes.

	Note: This acts as memcpy where 16 bytes are moved at a time, therefore
	if input and output overlap, output address should be less than input address.
	*/
	pub unsafe fn aDMEMMove(&mut self, source: u32, destination: u16, count: u16) {
		self.push_cmd(Acmd(word! {
			A_DMEMMOVE ; 24 ; 8,
			source ; 0 ; 24,
		}, word! {
			destination ; 16 ; 16,
			count ; 0 ; 16,
		}));
	}

	/**
	Loads ADPCM book from DRAM into DMEM.

	This command loads ADPCM table entries from DRAM to DMEM.

	The `count` parameter should be a multiple of 16 bytes.
	The `ptr` parameter is a DRAM address.
	*/
	pub unsafe fn aLoadADPCM(&mut self, count: u32, book_addr: *const i16) {
		self.push_cmd(Acmd(word! {
			A_LOADADPCM ; 24 ; 8,
			count ; 0 ; 24,
		}, book_addr as usize));
	}
}
