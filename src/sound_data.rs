macro_rules! import {
	($symbol:ident = $name:literal) => {
		#[no_mangle] static $symbol: [u8; include_data_lebe!(@byte_size concat!("sound_data/", $name))] = include_data_lebe!(concat!("sound_data/", $name));
	};
}

import!(gSoundDataDefinition = "sound_data.ctl");
import!(gSoundDataSamples = "sound_data.tbl");
import!(gMusicData = "sequences");
import!(gBankSetsData = "bank_sets");
