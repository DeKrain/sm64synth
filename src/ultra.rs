#![allow(dead_code)]

use std::sync::{Mutex, MutexGuard, Condvar};
use crate::support::DevPtr;

pub fn osAiSetFrequency(freq: u32) -> i32 {
	// Using the definitions from EU/SH version of the game.
	// The PC Port uses MPAL value instead of PAL.
	const osViClockPAL: i32 = 0x02F5B2D2;
	const osViClockMPAL: i32 = 0x02E6025C;
	const osViClockNTSC: i32 = 0x02E6D354;
	use osViClockMPAL as osViClock;

	let a1 = ((osViClock as f32) / (freq as f32) + 0.5) as u32;
	if a1 < 0x84 {
		return -1;
	}

	let mut a2 = ((a1 / 66) & 0xFF) as i32;
	if a2 > 16 {
		a2 = 16;
	}
	osViClock / (a1 as i32)
}

pub type OsId = i32;
pub type OsPri = i32;

pub type OSMesg = *mut ();

struct __OSThreadProfile {
	flag: u32,
	count: u32,
	time: u64,
}

pub struct OSThread {
	next: *mut OSThread,
	priority: OsPri,
	queue: *mut *mut OSThread,
	tlnext: *mut OSThread,
	state: u16,
	flags: u16,
	id: OsId,
	fp: i32,
	thprof: *mut __OSThreadProfile,
	context: __OSThreadContext,
}

/// Originally stored register contents
struct __OSThreadContext {}

struct QueueState {
	/// Contains number of valid messages.
	validCount: u32,
	/// Points to the first valid message.
	first: u32,
}

pub struct OSMesgQueue {
	/// Queue to store threads blocked on empty mailboxes (receive).
	//mtQueue: *mut OSThread,
	push_sem: Condvar,
	/// Queue to store threads blocked on full mailboxes (send).
	//fullQueue: *mut OSThread,
	pop_sem: Condvar,

	state: Mutex<QueueState>,
	/// Contains total count of messages.
	msgCount: u32,
	/// Points to message buffer array.
	msg: *mut OSMesg,
}

unsafe impl Sync for OSMesgQueue {}

impl OSMesgQueue {
	/// Waits for the queue to receive a message.
	unsafe fn wait_empty<'l>(&self, mut lock: MutexGuard<'l, QueueState>) -> MutexGuard<'l, QueueState> {
		//panic!("Blocking empty");
		println!("Entering empty block");
		while lock.validCount == 0 {
			lock = self.push_sem.wait(lock).unwrap();
		}
		println!("Finished empty block");
		lock
	}

	/// Waits for the queue to make a room for the next message.
	unsafe fn wait_full<'l>(&self, mut lock: MutexGuard<'l, QueueState>) -> MutexGuard<'l, QueueState> {
		//panic!("Blocking full");
		println!("Entering full block");
		while lock.validCount >= self.msgCount {
			lock = self.pop_sem.wait(lock).unwrap();
		}
		println!("Finished full block");
		lock
	}
}

struct OSIoMesgHdr {
	r#type: u16,
	pri: u8,
	status: u8,
	retQueue: *mut OSMesgQueue,
}

pub struct OSIoMesg {
	hdr: OSIoMesgHdr,
	dramAddr: *mut (),
	devAddr: usize,
	size: usize,
	// From the official definition.
	piHandle: *mut OSPiHandle,
}

struct OSPiHandle {
	next: *mut OSPiHandle,
	r#type: u8,
	latency: u8,
	pageSize: u8,
	relDuration: u8,
	pulse: u8,
	domain: u8,
	baseAddress: u32,
	speed: u32,
	transferInfo: __OSTranxInfo,
}

struct __OSTranxInfo {
	cmdType: u32,
	transferMode: u16,
	blockNum: u16,
	sectorNum: i32,
	devAddr: usize,
	errStatus: u32,
	bmCtlShadow: u32,
	seqCtlShadow: u32,
	block: [__OSBlockInfo; 2],
}

struct __OSBlockInfo {
	dramAddr: *mut (),
	C2Addr: *mut (),
	sectorSize: u32,
	C1ErrNum: u32,
	C1ErrSector: [u32; 4],
}

struct OSTask {}

pub struct SPTask {
	task: OSTask,
	msgQueue: *mut OSMesgQueue,
	msg: OSMesg,
	state: SPTaskState,
}

impl SPTask {
	pub const INIT: SPTask = SPTask { task: OSTask {}, msgQueue: std::ptr::null_mut(), msg: std::ptr::null_mut(), state: SPTaskState::NotStarted };
}

pub enum SPTaskState {
	NotStarted,
	Running,
	Interrupted,
	Finished,
	FinishedDp,
}

pub enum Blocking {
	NoBlock = 0,
	Block = 1,
}

/// Signifies failure to send a message because the queue is full.
#[derive(Debug)]
pub struct QueueFull;

/// Signifies failure to receiver a message because the queue is empty.
#[derive(Debug)]
pub struct QueueEmpty;


pub unsafe fn osCreateMesgQueue(queue: *mut OSMesgQueue, messages: *mut [OSMesg]) {
	//queue.mtQueue = std::ptr::null_mut();
	//queue.fullQueue = std::ptr::null_mut();
	queue.write(OSMesgQueue {
		push_sem: Condvar::new(),
		pop_sem: Condvar::new(),
		state: Mutex::new(QueueState {
			validCount: 0,
			first: 0,
		}),
		msgCount: (*messages).len().try_into().expect("Message buffer is too large"),
		msg: messages.cast(),
	});
}

pub unsafe fn osRecvMesg(queue: &OSMesgQueue, msg: *mut OSMesg, blocking: Blocking) -> Result<(), QueueEmpty> {
	let mut lock = queue.state.lock().unwrap();

	if lock.validCount == 0 {
		match blocking {
			Blocking::NoBlock => return Err(QueueEmpty),
			Blocking::Block => lock = queue.wait_empty(lock),
		}
	}

	if let Some(msg) = msg.as_mut() {
		*msg = queue.msg.add(lock.first as usize).read();
	}
	lock.validCount -= 1;
	lock.first = if lock.validCount == 0 {
		0
	} else {
		(lock.first + 1) % queue.msgCount
	};

	queue.pop_sem.notify_one();
	Ok(())
}

pub unsafe fn osSendMesg(queue: &OSMesgQueue, msg: OSMesg, blocking: Blocking) -> Result<(), QueueFull> {
	let mut lock = queue.state.lock().unwrap();

	if lock.validCount >= queue.msgCount {
		match blocking {
			Blocking::NoBlock => return Err(QueueFull),
			Blocking::Block => lock = queue.wait_full(lock),
		}
	}

	let index = (lock.first + lock.validCount) % queue.msgCount;
	queue.msg.add(index as usize).write(msg);
	lock.validCount += 1;

	queue.push_sem.notify_one();
	Ok(())
}


#[allow(unused_variables)]
pub unsafe fn osInvalDCache(addr: *mut (), size: usize) {}

pub enum MesgPriority {
	Normal,
	High,
}

pub enum TransferDirection {
	/// device => RAM
	Read,
	/// device <= RAM
	Write,
}

// Technically returns either ok or error status, but none of users care.
pub unsafe fn osPiStartDma(
	mesg: &mut OSIoMesg,
	priority: MesgPriority,
	direction: TransferDirection,
	devAddr: DevPtr<u8>,
	vAddr: *mut u8,
	nBytes: usize,
	mesg_queue: &OSMesgQueue,
) {
	let _ = mesg;
	let _ = priority;
	match direction {
		TransferDirection::Read => std::ptr::copy_nonoverlapping(devAddr as *const u8, vAddr, nBytes),
		TransferDirection::Write => std::ptr::copy_nonoverlapping(vAddr, devAddr as *mut u8, nBytes),
	}
	// Send finish message
	let _ = osSendMesg(mesg_queue, std::ptr::null_mut(), Blocking::NoBlock);
}
