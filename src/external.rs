//#![deny(missing_docs)]

mod levels;
use levels::{levels, LEVEL_COUNT, LEVEL_MAX, LEVEL_DDD, LEVEL_JRB, LEVEL_WDW, VAL_DIFF};

pub mod sounds;
pub use sounds::{SOUND_BANKS_ALL, SOUND_BANKS_ALL_BITS, SOUND_BANKS_FOREGROUND, SOUND_BANKS_BACKGROUND, SOUND_BANKS_DISABLED_DURING_INTRO_CUTSCENE, SOUND_BANKS_DISABLED_AFTER_CREDITS};
pub use sounds::{SOUND_BANK_ACTION, SOUND_BANK_MOVING, SOUND_BANK_VOICE, SOUND_BANK_GENERAL, SOUND_BANK_ENV, SOUND_BANK_OBJ, SOUND_BANK_AIR, SOUND_BANK_MENU, SOUND_BANK_GENERAL2, SOUND_BANK_OBJ2, SOUND_BANK_COUNT};

mod port_eu;
use port_eu::*;

mod bridge;
use bridge::*;

mod sequences;
pub use sequences::*;

use crate::{
	support::*,
	ultra::*,
	structs::*,
	load::*,
	data::gAudioRandom,
};

pub(crate) use port_eu::port_eu_init;
pub use port_eu::{create_next_audio_buffer, create_next_audio_frame_task};
pub use crate::load::{audio_init, SoundMode};
pub use crate::data::AI_SAMPLE_RATE;

/// Level background music
pub const SEQ_PLAYER_LEVEL: usize = 0;
/// Misc music like the puzzle jingle
pub const SEQ_PLAYER_ENV: usize = 1;
/// Sound effects
pub const SEQ_PLAYER_SFX: usize = 2;

// N.B. sound banks are different from the audio banks referred to in other
// files. We should really fix our naming to be less ambiguous...
const MAX_BACKGROUND_MUSIC_QUEUE_SIZE: usize = 6;
const MAX_CHANNELS_PER_SOUND_BANK: usize = 1;

const SEQUENCE_NONE: u8 = 0xFF;

#[derive(Clone, Copy)]
struct Sound {
	soundBits: SoundBits,
	position: *const f32,
}

struct ChannelVolumeScaleFade {
	velocity: f32,
	target: u8,
	current: f32,
	remainingFrames: u16,
}

#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq)]
enum SoundStatus {
	Stopped,
	Waiting,
	Playing,
}

impl SoundStatus {
	const MAX_VALUE: u8 = 2;

	pub(crate) const unsafe fn from_u8(val: u8) -> Self {
		assert!(val <= Self::MAX_VALUE);
		std::mem::transmute(val)
	}
}

#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct SoundBits(u32, sound_bits_impl::Construct);

#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct SoundFlag(u32, sound_bits_impl::Construct);

mod sound_bits_impl {
use super::*;

#[derive(Clone, Copy)]
pub(super) struct Construct(());

impl SoundFlag {
	#[inline(always)]
	const unsafe fn make(bits: u32) -> Self {
		Self(bits, Construct(()))
	}
}

impl SoundBits {
	#[inline(always)]
	const unsafe fn make(bits: u32) -> Self {
		Self(bits, Construct(()))
	}

	/// This setting contains no sound.
	pub const NO_SOUND: SoundBits = unsafe { SoundBits::make(0) };

	/// Checks for a given sound flag.
	/// If multiple flags are set, it will check if any are set.
	#[inline(always)]
	pub const fn contains(self, flag: SoundFlag) -> bool {
		(self.0 & flag.0) != 0
	}

	/// Checks if set to [`NO_SOUND`].
	///
	/// [`NO_SOUND`]: Self::NO_SOUND
	#[inline(always)]
	pub const fn is_no_sound(self) -> bool {
		self.0 == 0
	}

	#[cfg(any())]
	/// Converts sound status into sound bits.
	#[inline(always)]
	pub const fn from_status(status: SoundStatus) -> Self {
		unsafe { SoundBits::make(status as u32) }
	}

	/// Replaces the status with a new one.
	#[inline]
	pub(super) fn set_status(&mut self, status: SoundStatus) {
		self.0 = (self.0 & !Self::SOUNDARGS_MASK_STATUS) | status as u8 as u32;
	}

	#[inline]
	pub const fn new(bank: u8, soundID: u8, priority: u8) -> Self {
		unsafe { SoundBits::make(
			SoundStatus::Waiting as u32
			| (((bank as u32) << Self::SOUNDARGS_SHIFT_BANK) & Self::SOUNDARGS_MASK_BANK)
			| (((soundID as u32) << Self::SOUNDARGS_SHIFT_SOUNDID) & Self::SOUNDARGS_MASK_SOUNDID)
			| (((priority as u32) << Self::SOUNDARGS_SHIFT_PRIORITY) & Self::SOUNDARGS_MASK_PRIORITY)
		) }
	}

	#[inline]
	pub const fn add_flag(self, flag: SoundFlag) -> Self {
		// This is safe as flag can only contain a valid value of a flag
		unsafe { SoundBits::make(self.0 | flag.0) }
	}
}

macro_rules! sound_bits_fields {
	{
		^extract $conv_type:ty = $name:ident $mask_name:ident $shift_name:ident
	} => {
		#[doc = stringify!("Extract `", $name, "` from the bits.")]
		#[inline(always)]
		pub(super) const unsafe fn $name(self) -> $conv_type {
			<$conv_type>::from_u8(((self.0 & Self::$mask_name) >> Self::$shift_name) as u8)
		}
	};
	{
		^extract = $name:ident $mask_name:ident $shift_name:ident
	} => {
		#[doc = stringify!("Extract `", $name, "` from the bits.")]
		#[inline(always)]
		pub(super) const fn $name(self) -> u8 {
			((self.0 & Self::$mask_name) >> Self::$shift_name) as u8
		}
	};
	[$(
		$name:ident $((as $ty:ty))? $name_bits:ident $mask_name:ident = $mask_val:expr; $shift_name:ident = $shift_val:expr;
	)*] => {$(
		const $mask_name: u32 = $mask_val;
		const $shift_name: u8 = $shift_val;

		sound_bits_fields!(^extract $($ty)? =$name $mask_name $shift_name);

		#[doc = stringify!("Similar to [`", $name, "`][Self::", $name, "], but without shifting & converting values.")]
		#[inline(always)]
		pub const fn $name_bits(self) -> u32 {
			self.0 & Self::$mask_name
		}
	)*};
}

impl SoundBits {
	sound_bits_fields! {
		bank     bank_bits     SOUNDARGS_MASK_BANK     = 0xF0000000; SOUNDARGS_SHIFT_BANK     = 28;
		sound_id sound_id_bits SOUNDARGS_MASK_SOUNDID  = 0x00FF0000; SOUNDARGS_SHIFT_SOUNDID  = 16;
		priority priority_bits SOUNDARGS_MASK_PRIORITY = 0x0000FF00; SOUNDARGS_SHIFT_PRIORITY = 8;
		status(as SoundStatus) status_bits SOUNDARGS_MASK_STATUS = 0x0000000F; SOUNDARGS_SHIFT_STATUS = 0;
	}
}

macro_rules! sound_bits_flags {
	[$(
		$(#[$attr:meta])*
		$getter:ident $flag:ident = $flag_val:expr;
	)*] => {$(
		pub const $flag: SoundFlag = unsafe { SoundFlag::make($flag_val) };

		$(#[$attr])*
		#[inline(always)]
		pub const fn $getter(self) -> bool {
			self.contains(Self::$flag)
		}
	)*};
}

impl SoundBits {
	sound_bits_flags! {
		//// Audio lower bitflags

		/// Lower volume of background music while playing
		should_lower_background_music SOUND_LOWER_BACKGROUND_MUSIC = 0x10;
		/// Disable level reverb. Not in JP
		has_no_echo SOUND_NO_ECHO = 0x20;
		/// Every call to play_sound restarts the sound. If not
		/// set, the sound is continuous and play_sound should be
		/// called at least every other frame to keep it playing
		is_discrete SOUND_DISCRETE = 0x80;

		//// Audio playback bitflags

		/// No volume loss with distance
		has_no_volume_loss SOUND_NO_VOLUME_LOSS = 0x1000000;
		/// Randomly alter frequency each audio frame
		has_vibrato SOUND_VIBRATO = 0x2000000;
		/// Do not prioritize closer sounds
		does_not_lose_priority SOUND_NO_PRIORITY_LOSS = 0x4000000;
		/// Frequency is not affected by distance or speed. If
		/// not set, frequency will increase with distance.
		/// For sounds in SOUND_BANK_MOVING, frequency will
		/// further increase with speed, and volume will
		/// decrease at slow speeds.
		is_constant_frequency SOUND_CONSTANT_FREQUENCY = 0x8000000;
	}
}
}

/*impl std::ops::BitOr for SoundBits {
	type Output = Self;
	#[inline(always)]
	fn bitor(self, rhs: Self) -> Self {
		Self(self.0 | rhs.0)
	}
}

impl std::ops::BitAnd for SoundBits {
	type Output = Self;
	#[inline(always)]
	fn bitand(self, rhs: Self) -> Self {
		Self(self.0 & rhs.0)
	}
}*/

struct SoundCharacteristics {
	x: *const f32,
	y: *const f32,
	z: *const f32,
	distance: f32,
	priority: u32,
	/// Packed bits, same as first arg to play_sound.
	soundBits: SoundBits,
	soundStatus: SoundStatus,
	freshness: u8,
	prev: u8,
	next: u8,
}

// Also the number of frames a discrete sound can be in the WAITING state before being deleted
const SOUND_MAX_FRESHNESS: u8 = 10;

#[derive(Clone, Copy)]
struct SequenceQueueItem {
	seqId: u8,
	priority: u8,
}

static mut sGameLoopTicked: bool = false;

//// Dialog sounds
// The US difference is the sound for Dialog 37 ("I win! You lose! Ha ha ha ha!
// You're no slouch, but I'm a better sledder! Better luck next time!"), spoken
// by Koopa instead of the penguin in JP.
const DIALOG_COUNT: usize = 170;

#[repr(u8)]
#[derive(Clone, Copy, PartialEq)]
enum DialogSpeaker {
	UKIKI,
	TUXIE,
	/// Bowser Intro / Doors Laugh
	BOWS1,
	KOOPA,
	KBOMB,
	BOO,
	BOMB,
	/// Bowser Battle Laugh
	BOWS2,
	GRUNT,
	WIGLR,
	YOSHI,

	None = 0xFF,
}

static sDialogSpeaker: [DialogSpeaker; DIALOG_COUNT] = {
	use DialogSpeaker::*;
	// In JP version, this is KOOPA.
	use TUXIE as DIFF;
	[
		None,  BOMB,  BOMB,  BOMB,  BOMB,  KOOPA, KOOPA, KOOPA, None,  KOOPA,
		None,  None,  None,  None,  None,  None,  None,  KBOMB, None,  None,
		None,  BOWS1, BOWS1, BOWS1, BOWS1, BOWS1, BOWS1, BOWS1, BOWS1, BOWS1,
		None,  None,  None,  None,  None,  None,  None,  DIFF,  None,  None,
		None,  KOOPA, None,  None,  None,  None,  None,  BOMB,  None,  None,
		None,  None,  None,  None,  None,  TUXIE, TUXIE, TUXIE, TUXIE, TUXIE,
		None,  None,  None,  None,  None,  None,  None,  BOWS2, None,  None,
		None,  None,  None,  None,  None,  None,  None,  None,  None,  UKIKI,
		UKIKI, None,  None,  None,  None,  BOO,   None,  None,  None,  None,
		BOWS2, None,  BOWS2, BOWS2, None,  None,  None,  None,  BOO,   BOO,
		UKIKI, UKIKI, None,  None,  None,  BOMB,  BOMB,  BOO,   BOO,   None,
		None,  None,  None,  None,  GRUNT, GRUNT, KBOMB, GRUNT, GRUNT, None,
		None,  None,  None,  None,  None,  None,  None,  None,  KBOMB, None,
		None,  None,  TUXIE, None,  None,  None,  None,  None,  None,  None,
		None,  None,  None,  None,  None,  None,  None,  None,  None,  None,
		WIGLR, WIGLR, WIGLR, None,  None,  None,  None,  None,  None,  None,
		None,  YOSHI, None,  None,  None,  None,  None,  None,  WIGLR, None,
	]
};

static sDialogSpeakerVoice: [SoundBits; 11] = [
	sounds::SOUND_OBJ_UKIKI_CHATTER_LONG,
	sounds::SOUND_OBJ_BIG_PENGUIN_YELL,
	sounds::SOUND_OBJ_BOWSER_INTRO_LAUGH,
	sounds::SOUND_OBJ_KOOPA_TALK,
	sounds::SOUND_OBJ_KING_BOBOMB_TALK,
	sounds::SOUND_OBJ_BOO_LAUGH_LONG,
	sounds::SOUND_OBJ_BOBOMB_BUDDY_TALK,
	sounds::SOUND_OBJ_BOWSER_LAUGH,
	sounds::SOUND_OBJ2_BOSS_DIALOG_GRUNT,
	sounds::SOUND_OBJ_WIGGLER_TALK,
	sounds::SOUND_GENERAL_YOSHI_TALK,
	// US and JP versions have 4 more empty slots
];

static mut sSoundRequestProcessingHead: u8 = 0;
static mut sSoundRequestCursor: u8 = 0;

type Dyn = u16;

// Music dynamic tables. A dynamic describes which volumes to apply to which
// channels of a sequence (I think?), and different parts of a level can have
// different dynamics. Each table below specifies first the sequence to apply
// the dynamics to, then a bunch of conditions for when each dynamic applies
// (e.g. "only if Mario's X position is between 100 and 300"), and finally a
// fallback dynamic. Due to the encoding of the tables, the conditions must
// come in the same order as the macros.

const MARIO_X_GE: Dyn = 0;
const MARIO_Y_GE: Dyn = 1;
const MARIO_Z_GE: Dyn = 2;
const MARIO_X_LT: Dyn = 3;
const MARIO_Y_LT: Dyn = 4;
const MARIO_Z_LT: Dyn = 5;
const MARIO_IS_IN_AREA: Dyn = 6;
const MARIO_IS_IN_ROOM: Dyn = 7;

macro_rules! LEVEL_AREA_INDEX{
	($levelNum:expr, $areaNum:expr) => (($levelNum << 4) + $areaNum);
}

const BBH_OUTSIDE_ROOM: Dyn = 13;
const BBH_NEAR_MERRY_GO_ROUND_ROOM: Dyn = 10;
const AREA_DDD_WHIRLPOOL: Dyn = LEVEL_AREA_INDEX!(LEVEL_DDD as Dyn, 1);
const AREA_DDD_SUB: Dyn = LEVEL_AREA_INDEX!(LEVEL_DDD as Dyn, 2);
const AREA_JRB_SHIP: Dyn = LEVEL_AREA_INDEX!(LEVEL_JRB as Dyn, 2);
const AREA_WDW_MAIN: Dyn = LEVEL_AREA_INDEX!(LEVEL_WDW as Dyn, 1);
const AREA_WDW_TOWN: Dyn = LEVEL_AREA_INDEX!(LEVEL_WDW as Dyn, 2);

macro_rules! dyn_def {
	{$(
		static $name:ident: [Dyn; _] = [ $($def:tt)* ];
	)*} => {$(
		static $name: [Dyn; { dyn_def!(^ len [$($def)*]) }] = dyn_def!(^ elems [$($def)*]);
	)*};

	//(^ len [$($def:tt)*]) => (dyn_def!(^ elems [$($def)*]).len());
	(^ len [$($def:tt)*]) => (dyn_def!(^ len_elems [] [$($def)*]));
	(^ len_elems [$($l:tt)*] []) => {[$($l,)*].len()};
	{
		^ len_elems [$($l:tt)*]
		[
			DYN1 $_dyn:tt,
			$($def:tt)*
		]
	} => {dyn_def! { ^ len_elems [$($l)* () ()] [$($def)*]}};
	{
		^ len_elems [$($l:tt)*]
		[
			DYN2 $_dyn:tt,
			$($def:tt)*
		]
	} => {dyn_def! { ^ len_elems [$($l)* () () ()] [$($def)*]}};
	{
		^ len_elems [$($l:tt)*]
		[
			DYN3 $_dyn:tt,
			$($def:tt)*
		]
	} => {dyn_def! { ^ len_elems [$($l)* () () () ()] [$($def)*]}};
	{
		^ len_elems [$($l:tt)*]
		[
			$_expr:expr,
			$($def:tt)*
		]
	} => {dyn_def! { ^ len_elems [$($l)* ()] [$($def)*]}};

	{
		^ elems
		[
			DYN1($cond1:expr, $val1:expr, $res:expr),
			$($def:tt)*
		]
		$([$($prev:tt)*])?
	} => {dyn_def! {
		^elems
		[$($def)*]
		[
			$($($prev)*)?
			(1u16 << (15 - $cond1)) | $res, $val1 as u16,
		]
	}};
	{
		^ elems
		[
			DYN2($cond1:expr, $val1:expr, $cond2:expr, $val2:expr, $res:expr),
			$($def:tt)*
		]
		$([$($prev:tt)*])?
	} => {dyn_def! {
		^elems
		[$($def)*]
		[
			$($($prev)*)?
			(1u16 << (15 - $cond1)) | (1u16 << (15 - $cond2)) | $res,
			$val1 as i16 as u16, $val2 as i16 as u16,
		]
	}};
	{
		^ elems
		[
			DYN3($cond1:expr, $val1:expr, $cond2:expr, $val2:expr, $cond3:expr, $val3:expr, $res:expr),
			$($def:tt)*
		]
		$([$($prev:tt)*])?
	} => {dyn_def! {
		^elems
		[$($def)*]
		[
			$($($prev)*)?
			(1u16 << (15 - $cond1)) | (1u16 << (15 - $cond2)) | (1u16 << (15 - $cond3)) | $res,
			$val1 as i16 as u16, $val2 as i16 as u16, $val3 as i16 as u16,
		]
	}};
	{
		^ elems
		[
			$val:expr,
			$($def:tt)*
		]
		$([$($prev:tt)*])?
	} => {dyn_def! {
		^elems
		[$($def)*]
		[
			$($($prev)*)?
			$val,
		]
	}};
	{
		^ elems
		[]
		$([$($prev:tt)*])?
	} => {[
		$($($prev)*)?
	]};
}

dyn_def! {
	static sDynBbh: [Dyn; _] = [
		SEQ_LEVEL_SPOOKY as Dyn, DYN1(MARIO_IS_IN_ROOM, BBH_OUTSIDE_ROOM, 6),
		DYN1(MARIO_IS_IN_ROOM, BBH_NEAR_MERRY_GO_ROUND_ROOM, 6), 5,
	];
	static sDynDdd: [Dyn; _] = [
		SEQ_LEVEL_WATER as Dyn,
		DYN2(MARIO_X_LT, -800, MARIO_IS_IN_AREA, AREA_DDD_WHIRLPOOL & 0xf, 0),
		DYN3(MARIO_Y_GE, -2000, MARIO_X_LT, 470, MARIO_IS_IN_AREA, AREA_DDD_WHIRLPOOL & 0xf, 0),
		DYN2(MARIO_Y_GE, 100, MARIO_IS_IN_AREA, AREA_DDD_SUB & 0xf, 2),
		1,
	];
	static sDynJrb: [Dyn; _] = [
		SEQ_LEVEL_WATER as Dyn,
		DYN2(MARIO_Y_GE, 945, MARIO_X_LT, -5260, 0),
		DYN1(MARIO_IS_IN_AREA, AREA_JRB_SHIP & 0xf, 0),
		DYN1(MARIO_Y_GE, 1000, 0),
		DYN2(MARIO_Y_GE, -3100, MARIO_Z_LT, -900, 2),
		1,
		5, // bogus entry, ignored (was JRB originally intended to have spooky music?)
	];
	static sDynWdw: [Dyn; _] = [
		SEQ_LEVEL_UNDERGROUND as Dyn, DYN2(MARIO_Y_LT, -670, MARIO_IS_IN_AREA, AREA_WDW_MAIN & 0xf, 4),
		DYN1(MARIO_IS_IN_AREA, AREA_WDW_TOWN & 0xf, 4), 3,
	];
	static sDynHmc: [Dyn; _] = [
		SEQ_LEVEL_UNDERGROUND as Dyn, DYN2(MARIO_X_GE, 0, MARIO_Y_LT, -203, 4),
		DYN2(MARIO_X_LT, 0, MARIO_Y_LT, -2400, 4), 3,
	];
	static sDynUnk38: [Dyn; _] = [
		SEQ_LEVEL_UNDERGROUND as Dyn,
		DYN1(MARIO_IS_IN_AREA, 1, 3),
		DYN1(MARIO_IS_IN_AREA, 2, 4),
		DYN1(MARIO_IS_IN_AREA, 3, 7),
		0,
	];
}
static sDynNone: [Dyn; 2] = [SEQ_SOUND_PLAYER as Dyn, 0];

static mut sCurrentMusicDynamic: u8 = 0xFF;
static mut sBackgroundMusicForDynamics: u8 = 0xFF;

static sLevelDynamics: [&'static DSArray<Dyn>; LEVEL_COUNT] = {
	macro_rules! gen {
		(^ id _) => (gen!(^ id sDynNone));
		(^ id $id:ident) => (unsafe { DSArray::new_non_null(std::ptr::NonNull::new_unchecked($id.as_ptr().cast_mut())) });
		(^ stub ($_0:tt, $_1:tt, $_2:tt, $_3:tt, $_4:tt, $_5:tt, $_6:tt, $leveldyn:tt, $_8:tt)) => (gen!(^ id $leveldyn));
		(^ level ($_0:tt, $_1:tt, $_2:tt, $_3:tt, $_4:tt, $_5:tt, $_6:tt, $_7:tt, $_8:tt, $leveldyn:tt, $_10:tt)) => (gen!(^ id $leveldyn));
		[$(
			$type:ident $args:tt
		)*] => {[
			gen!(^ id _), // LEVEL_NONE
			$(gen!(^ $type $args),)*
		]};
	}
	levels! { gen }
};

type DynamicCondition = Dyn;

struct DynamicPart {
	condition: DynamicCondition,
	volScale: u16,
	duration: u16,
}
type MusicDynamic = [DynamicPart; 2];

static sMusicDynamics: [MusicDynamic; 8] = [
	[DynamicPart { condition: 0x0000, volScale: 0x7F, duration: 100}, DynamicPart { condition: 0x0E43, volScale: 0, duration: 100}], // SEQ_LEVEL_WATER
	[DynamicPart { condition: 0x0003, volScale: 0x7F, duration: 100}, DynamicPart { condition: 0x0E40, volScale: 0, duration: 100}], // SEQ_LEVEL_WATER
	[DynamicPart { condition: 0x0E43, volScale: 0x7F, duration: 200}, DynamicPart { condition: 0x0000, volScale: 0, duration: 200}], // SEQ_LEVEL_WATER
	[DynamicPart { condition: 0x02FF, volScale: 0x7F, duration: 100}, DynamicPart { condition: 0x0100, volScale: 0, duration: 100}], // SEQ_LEVEL_UNDERGROUND
	[DynamicPart { condition: 0x03F7, volScale: 0x7F, duration: 100}, DynamicPart { condition: 0x0008, volScale: 0, duration: 100}], // SEQ_LEVEL_UNDERGROUND
	[DynamicPart { condition: 0x0070, volScale: 0x7F, duration: 010}, DynamicPart { condition: 0x0000, volScale: 0, duration: 100}], // SEQ_LEVEL_SPOOKY
	[DynamicPart { condition: 0x0000, volScale: 0x7F, duration: 100}, DynamicPart { condition: 0x0070, volScale: 0, duration: 010}], // SEQ_LEVEL_SPOOKY
	[DynamicPart { condition: 0xFFFF, volScale: 0x7F, duration: 100}, DynamicPart { condition: 0x0000, volScale: 0, duration: 100}], // any (unused)
];

static sLevelAreaReverbs: [[u8; 3]; LEVEL_COUNT] = {
	macro_rules! gen {
		(^ echo $a:literal $b:literal $c:literal) => ([$a, $b, $c]);
		(^ stub ($_0:tt, $_1:tt, $_2:tt, $_3:tt, $echo1:tt, $echo2:tt, $echo3:tt, $_7:tt, $_8:tt)) => (gen!(^ echo $echo1 $echo2 $echo3));
		(^ level ($_0:tt, $_1:tt, $_2:tt, $_3:tt, $_4:tt, $_5:tt, $echo1:tt, $echo2:tt, $echo3:tt, $_9:tt, $_10:tt)) => (gen!(^ echo $echo1 $echo2 $echo3));
		[$(
			$type:ident $args:tt
		)*] => {[
			gen!(^ echo 0x00 0x00 0x00), // LEVEL_NONE
			$(gen!(^ $type $args),)*
		]};
	}
	levels! { gen }
};

static sLevelAcousticReaches: [u16; LEVEL_COUNT] = {
	macro_rules! gen {
		(^ stub ($_0:tt, $_1:tt, $_2:tt, $volume:tt, $_4:tt, $_5:tt, $_6:tt, $_7:tt, $_8:tt)) => ($volume);
		(^ level ($_0:tt, $_1:tt, $_2:tt, $_3:tt, $_4:tt, $volume:tt, $_6:tt, $_7:tt, $_8:tt, $_9:tt, $_10:tt)) => ($volume);
		[$(
			$type:ident $args:tt
		)*] => {[
			20000, // LEVEL_NONE
			$(gen!(^ $type $args),)*
		]};
	}
	levels! { gen }
};


const AUDIO_MAX_DISTANCE: f32 = 22000.0;
const LOW_VOLUME_REVERB: f32 = 40.0;
const VOLUME_RANGE_UNK1: f32 = 0.9;
const VOLUME_RANGE_UNK2: f32 = 0.8;

/// Default volume for background music sequences (playing on player 0).
///
/// (Only used on JP and US.)
#[allow(dead_code)]
static sBackgroundMusicDefaultVolume: [u8; SEQ_COUNT as usize] = [
	127, // SEQ_SOUND_PLAYER
	80,  // SEQ_EVENT_CUTSCENE_COLLECT_STAR
	80,  // SEQ_MENU_TITLE_SCREEN
	75,  // SEQ_LEVEL_GRASS
	70,  // SEQ_LEVEL_INSIDE_CASTLE
	75,  // SEQ_LEVEL_WATER
	75,  // SEQ_LEVEL_HOT
	75,  // SEQ_LEVEL_BOSS_KOOPA
	70,  // SEQ_LEVEL_SNOW
	65,  // SEQ_LEVEL_SLIDE
	80,  // SEQ_LEVEL_SPOOKY
	65,  // SEQ_EVENT_PIRANHA_PLANT
	85,  // SEQ_LEVEL_UNDERGROUND
	75,  // SEQ_MENU_STAR_SELECT
	65,  // SEQ_EVENT_POWERUP
	70,  // SEQ_EVENT_METAL_CAP
	65,  // SEQ_EVENT_KOOPA_MESSAGE
	70,  // SEQ_LEVEL_KOOPA_ROAD
	70,  // SEQ_EVENT_HIGH_SCORE
	65,  // SEQ_EVENT_MERRY_GO_ROUND
	80,  // SEQ_EVENT_RACE
	70,  // SEQ_EVENT_CUTSCENE_STAR_SPAWN
	85,  // SEQ_EVENT_BOSS
	75,  // SEQ_EVENT_CUTSCENE_COLLECT_KEY
	75,  // SEQ_EVENT_ENDLESS_STAIRS
	85,  // SEQ_LEVEL_BOSS_KOOPA_FINAL
	70,  // SEQ_EVENT_CUTSCENE_CREDITS
	80,  // SEQ_EVENT_SOLVE_PUZZLE
	80,  // SEQ_EVENT_TOAD_MESSAGE
	70,  // SEQ_EVENT_PEACH_MESSAGE
	75,  // SEQ_EVENT_CUTSCENE_INTRO
	80,  // SEQ_EVENT_CUTSCENE_VICTORY
	70,  // SEQ_EVENT_CUTSCENE_ENDING
	65,  // SEQ_MENU_FILE_SELECT
	0,   // SEQ_EVENT_CUTSCENE_LAKITU (not in JP)
];

static mut sCurrentBackgroundMusicSeqId: u8 = 0xFF;
static mut sMusicDynamicDelay: u8 = 0;
static mut sSoundBankUsedListBack: [u8; SOUND_BANK_COUNT] = [0; SOUND_BANK_COUNT];
static mut sSoundBankFreeListFront: [u8; SOUND_BANK_COUNT] = [1; SOUND_BANK_COUNT];
/// Only used for debugging.
static mut sNumSoundsInBank: [u8; SOUND_BANK_COUNT] = [0; SOUND_BANK_COUNT];
static sMaxChannelsForSoundBank: [u8; SOUND_BANK_COUNT] = [1; SOUND_BANK_COUNT];

static sNumSoundsPerBank: [u8; SOUND_BANK_COUNT] = [
	0x70, 0x30, 0x40, 0x80, 0x20, 0x80, 0x20, 0x40, 0x80, 0x80,
];

// sBackgroundMusicMaxTargetVolume and sBackgroundMusicTargetVolume use the 0x80
// bit to indicate that they are set, and the rest of the bits for the actual value.
const TARGET_VOLUME_IS_PRESENT_FLAG: u8 = 0x80;
const TARGET_VOLUME_VALUE_MASK: u8 = 0x7f;
const TARGET_VOLUME_UNSET: u8 = 0x00;

#[no_mangle] pub static gGlobalSoundSource: [f32; 3] = [0.0; 3];
static mut sSoundBankDisabled: [bool; 16] = [false; 16];
static mut sPresetIdSoundMode: u8 = 0;
static mut sHasStartedFadeOut: bool = false;
static mut sSoundBanksThatLowerBackgroundMusic: u16 = 0;
static mut sBackgroundMusicMaxTargetVolume: u8 = TARGET_VOLUME_UNSET;
static mut sCurrentEnvironmentSeqId: u8 = 0;
static mut sEnvironmentTargetVolume: u8 = 0;
static mut sEnvironmentMusicChangeDelay: u8 = 0;
static mut sBackgroundMusicQueueSize: u8 = 0;

//// .bss

zeroed! {
	static mut sSoundRequests: [Sound; 0x100];
	// Curiously, this has size 3, despite SEQUENCE_PLAYERS == 4 on EU
	static mut sChannelFades: [[ChannelVolumeScaleFade; CHANNELS_MAX]; 3];
	static mut sUsedChannelsForSoundBank: [u8; SOUND_BANK_COUNT];
	/// Index into sSoundBanks.
	static mut sCurrentSound: [[u8; MAX_CHANNELS_PER_SOUND_BANK]; SOUND_BANK_COUNT];

	/**
	For each sound bank, a pool containing the currently active sounds for that bank.
	The free and used slots in these pools are linked lists. The element `sSoundBanks[bank][0]`
	is used as a list header for the used list, and never holds an actual sound. See also
	sSoundBankUsedListBack and sSoundBankFreeListFront.
	*/
	static mut sSoundBanks: [[SoundCharacteristics; 40]; SOUND_BANK_COUNT];

	static mut sSoundMovingSpeed: [u8; SOUND_BANK_COUNT];
	static mut sBackgroundMusicTargetVolume: u8;
	static mut sLowerBackgroundMusicVolume: bool;
	static mut sBackgroundMusicQueue: [SequenceQueueItem; MAX_BACKGROUND_MUSIC_QUEUE_SIZE];
}

use std::mem::MaybeUninit;

/// Signals the beginning of creating a new frame. Unused.
static OSMesgQueueFrame: MaybeUninit<OSMesgQueue> = MaybeUninit::uninit();
/// Synchronize audio command buffers.
static OSMesgQueueCmdSync: MaybeUninit<OSMesgQueue> = MaybeUninit::uninit();
/// Audio preset to load on the next frame.
static OSMesgQueuePreset: MaybeUninit<OSMesgQueue> = MaybeUninit::uninit();
/// Informs the preset has been fully loaded.
static OSMesgQueuePresetLoaded: MaybeUninit<OSMesgQueue> = MaybeUninit::uninit();

// `mut` not strictly necessary, but at least it should be close to other statics, so whatever
// ...unless I make it a const
/*const OSMesgQueues: [&mut OSMesgQueue; 4] = unsafe {[
	&mut OSMesgQueue0, &mut OSMesgQueue1, &mut OSMesgQueue2, &mut OSMesgQueue3
]};*/

// Nevermind. Let's just address queues directly by purpose!

zeroed! {
	static mut euAudioCmd: [EuAudioCmd; 0x100];

	static mut OSMesg0: OSMesg;
	// Prevents UB from occuring by colliding with OSMesg2
	static mut OSMesg1: [OSMesg; 4];
	static mut OSMesg2: OSMesg;
	static mut OSMesg3: OSMesg;
}


//// Functions, finally, woo!

#[inline]
fn v3mag(vec: [f32; 3]) -> f32 {
	(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]).sqrt()
}

// This is a debug function which is almost entirely optimized away,
// except for loops, string literals, and a read of a volatile variable.
// The string literals have allowed it to be partially reconstructed.
//  fn unused_8031E4F0();
//  fn unused_8031E568();

/// Fade out a sequence player.
///
/// Called from threads:
/// - thread5_game_loop
unsafe fn audio_reset_session_eu(presetId: i32) {
	let mut mesg = std::mem::MaybeUninit::<OSMesg>::uninit();
	let _ = osRecvMesg(OSMesgQueuePresetLoaded.assume_init_ref(), mesg.as_mut_ptr(), Blocking::NoBlock);
	osSendMesg(OSMesgQueuePreset.assume_init_ref(), presetId as OSMesg, Blocking::NoBlock).unwrap();
	osRecvMesg(OSMesgQueuePresetLoaded.assume_init_ref(), mesg.as_mut_ptr(), Blocking::Block).unwrap();
	println!("Received preset response");
	if mesg.assume_init_read() as i32 != presetId {
		println!("Received wrong preset");
		osRecvMesg(OSMesgQueuePresetLoaded.assume_init_ref(), mesg.as_mut_ptr(), Blocking::Block).unwrap();
		println!("Received wrong preset epilogue");
	}
}

/// Called from threads:
/// - thread5_game_loop
unsafe fn seq_player_fade_to_percentage_of_volume(player: usize, fadeDuration: i32, percentage: u8) {
	let seqPlayer = &mut gSequencePlayers[player];
	if seqPlayer.state == SequencePlayerStateEU::FadeOut {
		return;
	}

	let targetVolume = percentage as f32 / 100.0 * seqPlayer.fadeVolume;
	seqPlayer.volume = seqPlayer.fadeVolume;
	seqPlayer.fadeRemainingFrames = 0;
	if fadeDuration == 0 {
		seqPlayer.fadeVolume = targetVolume;
		return;
	}
	seqPlayer.fadeVelocity = (targetVolume - seqPlayer.fadeVolume) / fadeDuration as f32;
	seqPlayer.state = SequencePlayerStateEU::FadingNormal;
	seqPlayer.fadeRemainingFrames = fadeDuration as u16;
}

/// Called from threads:
/// - thread3_main
/// - thread4_sound
/// - thread5_game_loop
unsafe fn seq_player_fade_to_normal_volume(player: usize, fadeDuration: i32) {
	let seqPlayer = &mut gSequencePlayers[player];
	if seqPlayer.state == SequencePlayerStateEU::FadeOut {
		return;
	}

	seqPlayer.fadeRemainingFrames = 0;
	if fadeDuration == 0 {
		seqPlayer.fadeVolume = seqPlayer.volume;
		return;
	}
	seqPlayer.fadeVelocity = (seqPlayer.volume - seqPlayer.fadeVolume) / fadeDuration as f32;
	seqPlayer.state = SequencePlayerStateEU::FadingNormal;
	seqPlayer.fadeRemainingFrames = fadeDuration as u16;
}

/// Called from threads:
/// - thread3_main
/// - thread4_sound
/// - thread5_game_loop
unsafe fn seq_player_fade_to_target_volume(player: usize, fadeDuration: i32, targetVolume: u8) {
	let seqPlayer = &mut gSequencePlayers[player];
	seqPlayer.fadeRemainingFrames = 0;
	if fadeDuration == 0 {
		seqPlayer.fadeVolume = targetVolume as f32 / 127.0;
		return;
	}

	seqPlayer.fadeVelocity = ((targetVolume as f32 / 127.0) - seqPlayer.fadeVolume) / fadeDuration as f32;
	seqPlayer.state = SequencePlayerStateEU::FadingNormal;
	seqPlayer.fadeRemainingFrames = fadeDuration as u16;
}

/// Called from threads:
/// - thread5_game_loop
unsafe fn maybe_tick_game_sound() {
	if sGameLoopTicked {
		update_game_sound();
		sGameLoopTicked = false;
	}
	eu_flush_audio_commands();
}

/* unsafe fn func_eu_802e9bec(player: usize, channelIndex: usize, stopSomething2: bool) {
	send_audio_cmd_byte(0x08000000 | (player as u32) << 16 | (channel as u32) << 8, stopSomething2 as i8);
} */

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn play_sound(soundBits: SoundBits, pos: *const f32) {
	sSoundRequests[sSoundRequestCursor as usize].soundBits = soundBits;
	sSoundRequests[sSoundRequestCursor as usize].position = pos;
	sSoundRequestCursor = sSoundRequestCursor.wrapping_add(1);
	if sSoundRequestCursor == sSoundRequestProcessingHead {
		panic!("Sound request queue overflow!");
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn process_sound_request(bits: SoundBits, pos: *const f32) {
	let bankId = bits.bank() as usize;
	let soundId = bits.sound_id();
	if soundId >= sNumSoundsPerBank[bankId] || sSoundBankDisabled[bankId] {
		return;
	}

	let mut counter: usize = 0;
	let mut soundIndex = sSoundBanks[bankId][0].next as usize;
	while soundIndex != 0xff && soundIndex != 0 {
		let sound = &mut sSoundBanks[bankId][soundIndex];
		// If an existing sound from the same source exists in the bank, then we should either
		// interrupt that sound and replace it with the new sound, or we should drop the new sound.
		if sound.x == pos {
			// If the existing sound has lower or equal priority, then we should replace it.
			// Otherwise the new sound will be dropped.
			if sound.soundBits.priority_bits() <= bits.priority_bits() {
				// If the existing sound is discrete or is a different continuous sound, then
				// interrupt it and play the new sound instead.
				// Otherwise the new sound is continuous and equals the existing sound, so we just
				// need to update the sound's freshness.
				if sound.soundBits.is_discrete()
					|| bits.sound_id_bits() != sound.soundBits.sound_id_bits()
				{
					update_background_music_after_sound(bankId, soundIndex);
					sound.soundBits = bits;
					// In practice, the starting status is always WAITING
					sound.soundStatus = bits.status();
				}

				// Reset freshness:
				// - For discrete sounds, this gives the sound SOUND_MAX_FRESHNESS frames to play
				//   before it gets deleted for being stale
				// - For continuous sounds, this gives it another 2 frames before play_sound must
				//   be called again to keep it playing
				sound.freshness = SOUND_MAX_FRESHNESS;
			}

			// Prevent allocating a new node - if the existing sound had higher piority, then the
			// new sound will be dropped.
			soundIndex = 0;
		} else {
			soundIndex = sound.next as usize;
		}
		counter += 1;
	}

	if counter == 0 {
		sSoundMovingSpeed[bankId] = 32;
	}

	// If free list has more than one element remaining
	if sSoundBanks[bankId][sSoundBankFreeListFront[bankId] as usize].next != 0xff && soundIndex != 0 {
		// Allocate from free list
		soundIndex = sSoundBankFreeListFront[bankId] as usize;

		let dist = {
			let pos = pos.cast::<[f32; 3]>().read();
			v3mag(pos)
		};
		let bank = &mut sSoundBanks[bankId][soundIndex];
		bank.x = pos;
		bank.y = pos.add(1);
		bank.z = pos.add(2);
		bank.distance = dist;
		bank.soundBits = bits;
		// In practice, the starting status is always WAITING
		bank.soundStatus = bits.status();
		bank.freshness = SOUND_MAX_FRESHNESS;

		// Append to end of used list and pop from front of free list
		bank.prev = sSoundBankUsedListBack[bankId];
		sSoundBanks[bankId][sSoundBankUsedListBack[bankId] as usize].next = sSoundBankFreeListFront[bankId];
		sSoundBankUsedListBack[bankId] = sSoundBankFreeListFront[bankId];
		sSoundBankFreeListFront[bankId] = sSoundBanks[bankId][sSoundBankFreeListFront[bankId] as usize].next;
		sSoundBanks[bankId][sSoundBankFreeListFront[bankId] as usize].prev = 0xff;
		bank.next = 0xff;
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn process_all_sound_requests() {
	while sSoundRequestCursor != sSoundRequestProcessingHead {
		let sound = sSoundRequests[sSoundRequestProcessingHead as usize];
		process_sound_request(sound.soundBits, sound.position);
		sSoundRequestProcessingHead = sSoundRequestProcessingHead.wrapping_add(1);
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn delete_sound_from_bank(bank: usize, soundIndex: usize) {
	if sSoundBankUsedListBack[bank] == soundIndex as u8 {
		// Remove from end of used list
		sSoundBankUsedListBack[bank] = sSoundBanks[bank][soundIndex].prev;
	} else {
		// Set sound.next.prev to sound.prev
		sSoundBanks[bank][sSoundBanks[bank][soundIndex].next as usize].prev = sSoundBanks[bank][soundIndex].prev;
	}

	// Set sound.prev.next to sound.next
	sSoundBanks[bank][sSoundBanks[bank][soundIndex].prev as usize].next = sSoundBanks[bank][soundIndex].next;

	// Push to front of free list
	sSoundBanks[bank][soundIndex].next = sSoundBankFreeListFront[bank];
	sSoundBanks[bank][soundIndex].prev = 0xff;
	sSoundBanks[bank][sSoundBankFreeListFront[bank] as usize].prev = soundIndex as u8;
	sSoundBankFreeListFront[bank] = soundIndex as u8;
}

/// Called from threads:
/// - thread3_main
/// - thread4_sound
/// - thread5_game_loop
unsafe fn update_background_music_after_sound(bank: usize, soundIndex: usize) {
	if sSoundBanks[bank][soundIndex].soundBits.should_lower_background_music() {
		sSoundBanksThatLowerBackgroundMusic &= !(1 << bank);
		begin_background_music_fade(50);
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn select_current_sounds(bankId: usize) {
	let mut liveSoundPriorities = [0x1000_0000_u32; 16];
	let mut liveSoundIndices = [0xffu8; 16];
	// let mut liveSoundStatuses = [0xffu8; 16];
	let mut numSoundsInBank: u8 = 0;

	// Delete stale sounds and prioritize remaining sounds into the liveSound arrays.
	let mut soundIndex = sSoundBanks[bankId][0].next as usize;
	while soundIndex != 0xff {
		let mut latestSoundIndex = soundIndex;
		let bank = &mut sSoundBanks[bankId][soundIndex];

		// If a discrete sound goes 10 frames without being played (because it is too low
		// priority), then mark it for deletion.
		//if (bank.soundBits & (SoundBits::SOUND_DISCRETE | SoundBits(SoundBits::SOUNDARGS_MASK_STATUS))) == (SoundBits::SOUND_DISCRETE | SoundBits::from_status(SoundStatus::Waiting))
		if bank.soundBits.is_discrete() && bank.soundBits.status() == SoundStatus::Waiting {
			if bank.freshness == 0 {
				bank.soundBits = SoundBits::NO_SOUND;
			}
			bank.freshness = bank.freshness.wrapping_sub(1);
		}
		// If a continuous sound goes 2 frames without play_sound being called, then mark it for
		// deletion.
		else if !bank.soundBits.is_discrete() {
			let freshness = bank.freshness;
			bank.freshness = bank.freshness.wrapping_sub(1);
			if freshness == SOUND_MAX_FRESHNESS - 2 {
				update_background_music_after_sound(bankId, soundIndex);
				bank.soundBits = SoundBits::NO_SOUND;
			}
		}

		// If a sound was marked for deletion and hasn't started playing yet, delete it now.
		if bank.soundBits.is_no_sound() && bank.soundStatus == SoundStatus::Waiting {
			// Since the current sound will be deleted, the next iteration should process
			// sound.prev.next
			latestSoundIndex = bank.prev as usize;
			bank.soundStatus = SoundStatus::Stopped;
			delete_sound_from_bank(bankId, soundIndex);
		}

		// If the current sound was not just deleted, consider it as a candidate for the currently
		// playing sound.
		if bank.soundStatus != SoundStatus::Stopped && soundIndex == latestSoundIndex {
			// Recompute distance each frame since the sound's position may have changed
			bank.distance = v3mag([*bank.x, *bank.y, *bank.z]);

			let requestedPriority = bank.soundBits.priority();
			// Recompute priority, possibly based on the sound's source position relative to the camera.
			// (Note that the sound's priority is the opposite of requestedPriority; lower is more important)
			bank.priority = if bank.soundBits.does_not_lose_priority() {
				0x4c * (0xff - requestedPriority) as u32
			} else if *bank.z > 0.0 {
				bank.distance as u32 + (*bank.z / 6.0) as u32
					+ 0x4c * (0xff - requestedPriority) as u32
			} else {
				bank.distance as u32 + 0x4c * (0xff - requestedPriority) as u32
			};

			// Insert the sound into the liveSound arrays, keeping the arrays sorted by priority.
			// If more than sMaxChannelsForSoundBank[bankId] sounds are live, then the
			// sound with lowest priority will be removed from the arrays.
			// In practice sMaxChannelsForSoundBank is always 1, so this code is overly general.
			let mut i = 0;
			while i < sMaxChannelsForSoundBank[bankId] as usize {
				// If the correct position is found
				if liveSoundPriorities[i] < bank.priority {
					i += 1;
					continue;
				}

				// Shift remaining sounds to the right
				for j in (i + 1..sMaxChannelsForSoundBank[bankId] as usize).rev() {
					liveSoundPriorities[j] = liveSoundPriorities[j - 1];
					liveSoundIndices[j] = liveSoundIndices[j - 1];
					// liveSoundStatuses[j] = liveSoundStatuses[j - 1];
				}
				// Insert the sound at index i
				liveSoundPriorities[i] = bank.priority;
				liveSoundIndices[i] = soundIndex as u8;
				// liveSoundStatuses[i] = bank.soundStatus as u8; // unused
				// Break
				i = sMaxChannelsForSoundBank[bankId] as usize;
				i += 1;
			}

			numSoundsInBank += 1;
		}

		soundIndex = sSoundBanks[bankId][latestSoundIndex].next as usize;
	}

	sNumSoundsInBank[bankId] = numSoundsInBank;
	sUsedChannelsForSoundBank[bankId] = sMaxChannelsForSoundBank[bankId];

	// Remove any sounds from liveSoundIndices that are already playing.
	// Stop any currently playing sounds that are not in liveSoundIndices.
	for i in 0..sUsedChannelsForSoundBank[bankId] as usize {
		// Check if sCurrentSound[bank][i] is present in the liveSound arrays.
		soundIndex = 0;
		while soundIndex < sUsedChannelsForSoundBank[bankId] as usize {
			if liveSoundIndices[soundIndex] != 0xff && sCurrentSound[bankId][i] == liveSoundIndices[soundIndex] {
				// If found, remove it from liveSoundIndices
				liveSoundIndices[soundIndex] = 0xff;
				// Break
				soundIndex = 0xfe;
			}
			soundIndex += 1;
		}

		// If it is not present in the liveSound arrays, then stop playing it
		if soundIndex == 0xff || sCurrentSound[bankId][i] == 0xff {
			continue;
		}
		soundIndex = sCurrentSound[bankId][i] as usize;
		let bank = &mut sSoundBanks[bankId][soundIndex];

		// If the sound was marked for deletion and is playing, delete it
		if bank.soundBits.is_no_sound() {
			if bank.soundStatus == SoundStatus::Playing {
				bank.soundStatus = SoundStatus::Stopped;
				delete_sound_from_bank(bankId, soundIndex);
			}
		}

		//let isDiscreteAndStatus = (bank.soundBits & (SoundBits::SOUND_DISCRETE | SoundBits::SOUNDARGS_MASK_STATUS)) as u32;
		let (isDiscrete, status) = (bank.soundBits.is_discrete(), bank.soundBits.status());

		if status == SoundStatus::Playing && bank.soundStatus != SoundStatus::Stopped {
			// If the sound is discrete and is playing, then delete it
			//if isDiscreteAndStatus >= (SoundBits::SOUND_DISCRETE | SoundStatus::Playing) && bank.soundStatus != SoundStatus::Stopped
			if isDiscrete {
				update_background_music_after_sound(bankId, soundIndex);
				bank.soundBits = SoundBits::NO_SOUND;
				bank.soundStatus = SoundStatus::Stopped;
				delete_sound_from_bank(bankId, soundIndex);
			}
			// If the sound is continuous and is playing, then stop playing it but don't delete it.
			// (A continuous sound shouldn't be deleted until it stops being requested)
			//else if isDiscreteAndStatus == SoundStatus::Playing as u32 && bank.soundStatus != SoundStatus::Stopped
			else {
				bank.soundStatus = SoundStatus::Waiting;
			}
		}

		sCurrentSound[bankId][i] = 0xff;
	}

	// Start playing the remaining sounds from liveSoundIndices.
	for soundIndex in 0..sUsedChannelsForSoundBank[bankId] as usize {
		if liveSoundIndices[soundIndex] != 0xff {
			let mut i = 0;
			while i < sUsedChannelsForSoundBank[bankId] as usize {
				if sCurrentSound[bankId][i] == 0xff {
					sCurrentSound[bankId][i] = liveSoundIndices[soundIndex];

					// Set (soundBits & status) to WAITING (soundStatus will be updated
					// shortly after in update_game_sound)
					sSoundBanks[bankId][liveSoundIndices[soundIndex] as usize].soundBits.set_status(SoundStatus::Waiting);

					// Doesn't do anything
					liveSoundIndices[i] = 0xff;
					// Break
					i = 0xfe;
				}
				i += 1;
			}
		}
	}
}

/**
Given x and z coordinates, return the pan. This is a value nominally between
0 and 1 that represents the audio direction.

### Pan
- `0.0` - fully left
- `0.5` - center pan
- `1.0` - fully right

### Called from threads
- thread4_sound
- thread5_game_loop (EU only)

This function doesn't access any global data (and thus is not unsafe), so
thread information is not necessary, but it's included here for completness.
*/
fn get_sound_pan(x: f32, z: f32) -> f32 {
	let absX = x.abs().min(AUDIO_MAX_DISTANCE);
	let absZ = z.abs().min(AUDIO_MAX_DISTANCE);

	// There are 4 panning equations (12-hr clock used for angles)
	// 1. (0,0) fully-centered pan
	// 2. far right pan: between 1:30 and 4:30
	// 3. far left pan: between 7:30 and 10:30
	// 4. center pan: between 4:30 and 7:30 or between 10:30 and 1:30
	if x == 0.0 && z == 0.0 {
		// x and z being 0 results in a center pan
		0.5
	} else if x >= 0.0 && absX >= absZ {
		// far right pan
		1.0 - (2.0 * AUDIO_MAX_DISTANCE - absX) / (3.0 * (2.0 * AUDIO_MAX_DISTANCE - absZ))
	} else if x < 0.0 && absX > absZ {
		// far left pan
		(2.0 * AUDIO_MAX_DISTANCE - absX) / (3.0 * (2.0 * AUDIO_MAX_DISTANCE - absZ))
	} else {
		// center pan

		// @bug (JP PU sound glitch) If |x|, |z| > AUDIO_MAX_DISTANCE, we'll
		// end up in this case, and pan may be set to something outside of [0,1]
		// since x is not clamped. On JP, this can lead to an out-of-bounds
		// float read in note_set_vel_pan_reverb when x is highly negative,
		// causing console crashes when that float is a nan or denormal.
		0.5 + x / (6.0 * absZ)
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn get_sound_volume(bankId: usize, soundIndex: usize, volumeRange: f32) -> f32 {
	let div = if bankId < 3 { 2 } else { 3 };
	let bank = &sSoundBanks[bankId][soundIndex];
	let mut intensity;
	if !bank.soundBits.has_no_volume_loss() {
		// Intensity linearly lowers from 1 at the camera to 1 - volumeRange at maxSoundDistance,
		// then it goes from 1 - volumeRange at maxSoundDistance to 0 at AUDIO_MAX_DISTANCE
		intensity = if bank.distance > AUDIO_MAX_DISTANCE {
			0.0
		} else {
			let maxSoundDistance: f32 = (sLevelAcousticReaches[gCurrLevelNum as usize] / div) as f32;
			if maxSoundDistance < bank.distance {
				((AUDIO_MAX_DISTANCE - bank.distance) / (AUDIO_MAX_DISTANCE - maxSoundDistance))
					* (1.0 - volumeRange)
			} else {
				1.0 - bank.distance / maxSoundDistance * volumeRange
			}
		};

		if bank.soundBits.has_vibrato() {
			if intensity >= 0.08 {
				intensity -= (gAudioRandom & 0xf) as f32 / 192.0;
			}
		}
	} else {
		intensity = 1.0;
	}

	// Rise quadratically from 1 - volumeRange to 1
	volumeRange * intensity * intensity + 1.0 - volumeRange
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn get_sound_freq_scale(bankId: usize, soundIndex: usize) -> f32 {
	let bank = &sSoundBanks[bankId][soundIndex];
	let mut amount;
	if !bank.soundBits.is_constant_frequency() {
		amount = bank.distance / AUDIO_MAX_DISTANCE;
		if bank.soundBits.has_vibrato() {
			amount += (gAudioRandom & 0xff) as f32 / 64.0;
		}
	} else {
		amount = 0.0;
	}

	// Goes from 1 at the camera to 1 + 1/15 at AUDIO_MAX_DISTANCE (and continues rising
	// farther than that)
	amount / 15.0 + 1.0
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn get_sound_reverb(bankId: usize, soundIndex: usize, channelIndex: usize) -> u8 {
	// Disable level reverb if NO_ECHO is set
	let (level, area);
	if sSoundBanks[bankId][soundIndex].soundBits.has_no_echo() {
		level = 0;
		area = 0;
	} else {
		level = (gCurrLevelNum as usize).min(LEVEL_MAX);
		area = ((gCurrAreaIndex - 1) as usize).min(2);
	}

	// reverb = reverb adjustment + level reverb + a volume-dependent value
	// The volume-dependent value is 0 when volume is at maximum, and raises to
	// LOW_VOLUME_REVERB when the volume is 0
	let reverb =
		(*gSequencePlayers[SEQ_PLAYER_SFX].channels[channelIndex]).soundScriptIO[5] as u8
		+ sLevelAreaReverbs[level][area]
		+ ((1.0 - (*gSequencePlayers[SEQ_PLAYER_SFX].channels[channelIndex]).volume) * LOW_VOLUME_REVERB) as u8;

	reverb.min(0x7f)
}

/**
Called from the game loop thread to inform the audio thread that a new game
frame has started.

Called from threads:
- thread5_game_loop
*/
#[no_mangle]
pub unsafe extern "C" fn audio_signal_game_loop_tick() {
	sGameLoopTicked = true;
	maybe_tick_game_sound();
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU and SH only)
unsafe fn update_game_sound() {
	process_all_sound_requests();
	process_level_music_dynamics();

	if gSequencePlayers[SEQ_PLAYER_SFX].channels[0] == std::ptr::addr_of_mut!(gSequenceChannelNone) {
		return;
	}

	let mut channelIndex: u8 = 0;
	for bankId in 0..SOUND_BANK_COUNT {
		select_current_sounds(bankId);

		for soundIndex in sCurrentSound[bankId] {
			let soundIndex = soundIndex as usize;
			if soundIndex < 0xff && sSoundBanks[bankId][soundIndex].soundStatus != SoundStatus::Stopped {
				let bank = &mut sSoundBanks[bankId][soundIndex];
				// Freeze channel index to prevent accidental misuse.
				let channelIndex = channelIndex;
				let channel = &mut *gSequencePlayers[SEQ_PLAYER_SFX].channels[channelIndex as usize];

				let soundStatus = bank.soundBits.status();
				let soundId = bank.soundBits.sound_id();
				bank.soundStatus = soundStatus;

				let big_switch = |bank: &mut SoundCharacteristics| {
					match bankId as u8 {
						SOUND_BANK_MOVING if !bank.soundBits.is_constant_frequency() => {
							send_audio_cmd_f32(
								//0x0202_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_VOLUME, SEQ_PLAYER_SFX as u8, channelIndex),
								{
									let s = get_sound_volume(bankId, soundIndex, VOLUME_RANGE_UNK1);
									if sSoundMovingSpeed[bankId] > 8 {
										s
									} else {
										s * ((sSoundMovingSpeed[bankId] as f32 + 8.0) / 16.0)
									}
							});
							send_audio_cmd_byte(
								//0x0302_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_PAN, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_pan(*bank.x, *bank.z) as i8,
							);
							send_audio_cmd_f32(
								//0x0402_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_FREQ_SCALE, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_freq_scale(bankId, soundIndex)
									+ sSoundMovingSpeed[bankId] as f32 /
										if bank.soundBits.sound_id_bits() == sounds::SOUND_MOVING_FLYING.sound_id_bits() { 80.0 } else { 400.0 },
							);
							send_audio_cmd_byte(
								//0x0502_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_REVERB_VOLUME, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_reverb(bankId, soundIndex, channelIndex as usize) as i8,
							);
						}
						SOUND_BANK_MOVING | SOUND_BANK_MENU => {
							send_audio_cmd_f32(
								//0x0202_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_VOLUME, SEQ_PLAYER_SFX as u8, channelIndex),
								1.0);
							send_audio_cmd_byte(
								//0x0302_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_PAN, SEQ_PLAYER_SFX as u8, channelIndex),
								64);
							send_audio_cmd_f32(
								//0x0402_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_FREQ_SCALE, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_freq_scale(bankId, soundIndex),
							);
						}
						SOUND_BANK_ACTION | SOUND_BANK_VOICE => {
							send_audio_cmd_byte(
								//0x0502_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_REVERB_VOLUME, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_reverb(bankId, soundIndex, channelIndex as usize) as i8,
							);
							send_audio_cmd_f32(
								//0x0202_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_VOLUME, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_volume(bankId, soundIndex, VOLUME_RANGE_UNK1),
							);
							send_audio_cmd_byte(
								//0x0302_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_PAN, SEQ_PLAYER_SFX as u8, channelIndex),
								(get_sound_pan(*bank.x, *bank.z) * 127.0 + 0.5) as i8,
							);
							send_audio_cmd_f32(
								//0x0402_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_FREQ_SCALE, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_freq_scale(bankId, soundIndex),
							);
						}
						SOUND_BANK_GENERAL | SOUND_BANK_ENV | SOUND_BANK_OBJ | SOUND_BANK_AIR | SOUND_BANK_GENERAL2 | SOUND_BANK_OBJ2 => {
							send_audio_cmd_byte(
								//0x0502_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_REVERB_VOLUME, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_reverb(bankId, soundIndex, channelIndex as usize) as i8,
							);
							send_audio_cmd_f32(
								//0x0202_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_VOLUME, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_volume(bankId, soundIndex, VOLUME_RANGE_UNK2),
							);
							send_audio_cmd_byte(
								//0x0302_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_PAN, SEQ_PLAYER_SFX as u8, channelIndex),
								(get_sound_pan(*bank.x, *bank.z) * 127.0 + 0.5) as i8,
							);
							send_audio_cmd_f32(
								//0x0402_0000 | (channelIndex << 8),
								build_channel_cmd(CHANNEL_CMD_SET_FREQ_SCALE, SEQ_PLAYER_SFX as u8, channelIndex),
								get_sound_freq_scale(bankId, soundIndex),
							);
						}
						_ => unreachable!("Invalid sound bank"),
					}
				};

				if soundStatus == SoundStatus::Waiting {
					if bank.soundBits.should_lower_background_music() {
						sSoundBanksThatLowerBackgroundMusic |= 1 << bankId;
						begin_background_music_fade(50);
					}

					// Set sound status to PLAYING
					bank.soundBits.0 += 1;
					bank.soundStatus = SoundStatus::Playing;

					// Begin playing the sound
					channel.soundScriptIO[4] = soundId as i8;
					channel.soundScriptIO[0] = 1;

					big_switch(bank);
				}

				// If the sound was marked for deletion (bits set to NO_SOUND), then stop playing it
				// and delete it.
				else if channel.layers[0].is_null() {
					update_background_music_after_sound(bankId, soundIndex);
					bank.soundStatus = SoundStatus::Stopped;
					delete_sound_from_bank(bankId, soundIndex);
				} else if soundStatus == SoundStatus::Stopped &&
					!(*channel.layers[0])
						.flags.contains(SequenceChannelLayerFlags::Finished) {
					update_background_music_after_sound(bankId, soundIndex);
					channel.soundScriptIO[0] = 0;
					delete_sound_from_bank(bankId, soundIndex);
				}

				// If sound has finished playing, then delete it.
				// @bug JP: See https://www.youtube.com/watch?v=QetyTgbQxcw
				else if !(*channel.layers[0]).flags.contains(SequenceChannelLayerFlags::Enabled) {
					update_background_music_after_sound(bankId, soundIndex);
					bank.soundStatus = SoundStatus::Stopped;
					delete_sound_from_bank(bankId, soundIndex);
				} else {
					big_switch(bank);
				}
			}

			// Increment to the next channel that this bank owns.
			channelIndex += 1;
		}

		// Increment to the first channel index of the next bank.
		// (In practice sUsedChannelsForSoundBank[i] = sMaxChannelsForSoundBank[i] = 1, so this
		// doesn't do anything.)
		channelIndex += sMaxChannelsForSoundBank[bankId] - sUsedChannelsForSoundBank[bankId];
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop
unsafe fn seq_player_play_sequence(player: usize, seqId: u8, fadeTimer: u16) {
	if player == SEQ_PLAYER_LEVEL {
		sCurrentBackgroundMusicSeqId = seqId & SEQ_BASE_ID;
		sBackgroundMusicForDynamics = SEQUENCE_NONE;
		sCurrentMusicDynamic = 0xff;
		sMusicDynamicDelay = 2;
	}

	for fade in &mut sChannelFades[player] {
		fade.remainingFrames = 0;
	}

	//send_audio_cmd_byte(0x4600_0000 | (((player & 0xff) as u32) << 16), (seqId & SEQ_VARIATION) as i8);
	send_audio_cmd_byte(build_player_cmd(PLAYER_CMD_SET_VARIATION, player as u8), (seqId & SEQ_VARIATION) as i8);
	//send_audio_cmd_u32(0x8200_0000 | (((player & 0xff) as u32) << 16) | (((seqId & SEQ_BASE_ID) as u32) << 8), fadeTimer as u32);
	send_audio_cmd_u32(build_player_cmd_seq(EU_CMD_LOAD_SEQ, player as u8, seqId & SEQ_BASE_ID), fadeTimer as u32);

	if player == SEQ_PLAYER_LEVEL {
		let targetVolume = begin_background_music_fade(0);
		if targetVolume != 0xff {
			gSequencePlayers[SEQ_PLAYER_LEVEL].fadeVolumeScale = targetVolume as f32 / 127.0;
		}
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn seq_player_fade_out(player: u8, fadeDuration: u16) {
	if player == SEQ_PLAYER_LEVEL as u8 {
		sCurrentBackgroundMusicSeqId = SEQUENCE_NONE;
	}

	//send_audio_cmd_u32(0x8300_0000 | (((player & 0xff) as u32) << 16), fadeDuration as u32);
	send_audio_cmd_u32(build_player_cmd(EU_CMD_FADE_OUT_PLAYER, player), fadeDuration as u32);
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn fade_volume_scale(player: usize, targetScale: u8, fadeDuration: u16) {
	for idx in 0..CHANNELS_MAX {
		fade_channel_volume_scale(player, idx, targetScale, fadeDuration);
	}
}

/// Called from threads:
/// - thread3_main
/// - thread4_sound
/// - thread5_game_loop
unsafe fn fade_channel_volume_scale(player: usize, channelIndex: usize, targetScale: u8, fadeDuration: u16) {
	let channel = gSequencePlayers[player].channels[channelIndex];
	if channel != std::ptr::addr_of_mut!(gSequenceChannelNone) {
		let fade = &mut sChannelFades[player][channelIndex];
		fade.remainingFrames = fadeDuration;
		fade.velocity = ( targetScale as f32 / 127.0 - (*channel).volumeScale) / fadeDuration as f32;
		fade.target = targetScale;
		fade.current = (*channel).volumeScale;
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn process_channel_fades(player: usize) {
	for chan in 0..CHANNELS_MAX {
		if gSequencePlayers[player].channels[chan] != std::ptr::addr_of_mut!(gSequenceChannelNone) && sChannelFades[player][chan].remainingFrames != 0 {
			let fade = &mut sChannelFades[player][chan];
			fade.current += fade.velocity;
			send_audio_cmd_f32(
				//0x0100_0000 | (((player & 0xff) as u32) << 16) | (((chan & 0xff) as u32) << 8),
				build_channel_cmd(CHANNEL_CMD_SET_VOLUME_SCALE, player as u8, chan as u8),
				fade.current);
			fade.remainingFrames -= 1;
			if fade.remainingFrames == 0 {
				send_audio_cmd_f32(
					//0x0100_0000 | (((player & 0xff) as u32) << 16) | (((chan & 0xff) as u32) << 8),
					build_channel_cmd(CHANNEL_CMD_SET_VOLUME_SCALE, player as u8, chan as u8),
					fade.target as f32 / 127.0);
			}
		}
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn process_level_music_dynamics() {
	process_channel_fades(SEQ_PLAYER_LEVEL);
	process_channel_fades(SEQ_PLAYER_SFX);
	process_environment_music();

	if sMusicDynamicDelay != 0 {
		sMusicDynamicDelay -= 1;
	} else {
		sBackgroundMusicForDynamics = sCurrentBackgroundMusicSeqId;
	}

	let currLevelDynamics = sLevelDynamics[gCurrLevelNum as usize];

	if sBackgroundMusicForDynamics as u16 != currLevelDynamics.get(0) {
		return;
	}

	let mut conditionBits: u16 = currLevelDynamics.get(1) & 0xff00;
	let mut musicDynIndex = (currLevelDynamics.get(1) & 0xff) as u8;

	let mut conditionValues: [i16; 8] = [0; 8];
	let mut conditionTypes: [u8; 8] = [0; 8];

	let mut i: u8 = 2;
	while conditionBits & 0xff00 != 0 {
		let mut j: u8 = 0;
		let mut condIndex: u8 = 0;
		let mut bit: u16 = 0x8000;
		while j < 8 {
			if conditionBits & bit != 0 {
				conditionValues[condIndex as usize] = currLevelDynamics.get(i as usize) as i16;
				i += 1;
				conditionTypes[condIndex as usize] = j;
				condIndex += 1;
			}

			j += 1;
			bit >>= 1;
		}

		j = 0;
		while j < condIndex {
			struct Ordering(u8);
			impl Ordering {
				pub const Less: Ordering = Ordering(1 << 0);
				pub const Greater: Ordering = Ordering(1 << 1);
				pub const Equal: Ordering = Ordering(1 << 2);

				#[inline(always)]
				pub fn has(self, variant: cmp::Ordering) -> bool {
					self.0 & match variant {
						cmp::Ordering::Less => Self::Less,
						cmp::Ordering::Equal => Self::Equal,
						cmp::Ordering::Greater => Self::Greater,
					}.0 != 0
				}
			}
			impl std::ops::BitOr for Ordering {
				type Output = Self;
				#[inline(always)]
				fn bitor(self, rhs: Self) -> Self {
					Ordering(self.0 | rhs.0)
				}
			}
			use std::cmp;
			let (val, ord): (i16, _) = match conditionTypes[j as usize] as Dyn {
				MARIO_X_GE => (gMarioState_getPosition()[0] as i16, Ordering::Greater | Ordering::Equal),
				MARIO_Y_GE => (gMarioState_getPosition()[1] as i16, Ordering::Greater | Ordering::Equal),
				MARIO_Z_GE => (gMarioState_getPosition()[2] as i16, Ordering::Greater | Ordering::Equal),
				MARIO_X_LT => (gMarioState_getPosition()[0] as i16, Ordering::Less),
				MARIO_Y_LT => (gMarioState_getPosition()[1] as i16, Ordering::Less),
				MARIO_Z_LT => (gMarioState_getPosition()[2] as i16, Ordering::Less),
				MARIO_IS_IN_AREA => (gCurrAreaIndex, Ordering::Equal),
				MARIO_IS_IN_ROOM => (gMarioCurrentRoom, Ordering::Equal),
				_ => unreachable!("Invalid condition"),
			};

			/*let skip = match (val.cmp(&conditionValues[j as usize]), ord) {
				(cmp::Ordering::Equal, Ordering::Equal) => false,
				(cmp::Ordering::Greater | cmp::Ordering::Equal, Ordering::GreaterOrEqual) => false,
				(cmp::Ordering::Less, Ordering::Less) => false,
				_ => true,
			};*/
			if !ord.has(val.cmp(&conditionValues[j as usize])) {
				j = condIndex + 1;
			}

			j += 1;
		}

		if j == condIndex {
			// The area matches. Break out of the loop.
			conditionBits = 0;
		} else {
			conditionBits = currLevelDynamics.get(i as usize) & 0xff00;
			musicDynIndex = (currLevelDynamics.get(i as usize) & 0xff) as u8;
			i += 1;
		}
	}

	if sCurrentMusicDynamic != musicDynIndex {
		let r#dyn = &sMusicDynamics[musicDynIndex as usize];

		let mut tempBits: DynamicCondition = 1;
		let dur = if sCurrentMusicDynamic == 0xff {
			[1, 1]
		} else {
			[r#dyn[0].duration, r#dyn[1].duration]
		};

		for channelIndex in 0..CHANNELS_MAX {
			conditionBits = tempBits;
			//tempBits = 0;
			if r#dyn[0].condition & conditionBits != 0 {
				fade_channel_volume_scale(SEQ_PLAYER_LEVEL, channelIndex, r#dyn[0].volScale as u8, dur[0]);
			}
			if r#dyn[1].condition & conditionBits != 0 {
				fade_channel_volume_scale(SEQ_PLAYER_LEVEL, channelIndex, r#dyn[1].volScale as u8, dur[1]);
			}
			tempBits = conditionBits << 1;
		}

		sCurrentMusicDynamic = musicDynIndex;
	}
}

// fn unused_8031FED0(player: usize, bits: u32, arg2: i8);

/**
Lower a sequence player's volume over fadeDuration frames.
If player is SEQ_PLAYER_LEVEL (background music), the given percentage is ignored
and a max target volume of 40 is used.

Called from threads:
- thread5_game_loop
*/
#[no_mangle]
pub unsafe extern "C" fn seq_player_lower_volume(player: usize, fadeDuration: u16, percentage: u8) {
	if player == SEQ_PLAYER_LEVEL {
		sLowerBackgroundMusicVolume = true;
		begin_background_music_fade(fadeDuration);
	} else if gSequencePlayers[player].flags.contains(SequencePlayerFlags::Enabled) {
		seq_player_fade_to_percentage_of_volume(player, fadeDuration as i32, percentage);
	}
}

/**
Remove the lowered volume constraint set by seq_player_lower_volume.
If player is SEQ_PLAYER_LEVEL (background music), the music won't necessarily
raise back to normal volume if other constraints have been set, e.g.
sBackgroundMusicTargetVolume.

Called from threads:
- thread5_game_loop
*/
#[no_mangle]
pub unsafe extern "C" fn seq_player_unlower_volume(player: usize, fadeDuration: u16) {
	// Should this be in condition block?
	sLowerBackgroundMusicVolume = false;
	if player == SEQ_PLAYER_LEVEL {
		if gSequencePlayers[player].state != SequencePlayerStateEU::FadingIn {
			begin_background_music_fade(fadeDuration);
		}
	} else if gSequencePlayers[player].flags.contains(SequencePlayerFlags::Enabled) {
		seq_player_fade_to_normal_volume(player, fadeDuration as i32);
	}
}

/**
Begin a volume fade to adjust the background music to the correct volume.
The target volume is determined by global variables like sBackgroundMusicTargetVolume
and sLowerBackgroundMusicVolume.
If none of the relevant global variables are set, then the default background music
volume for the sequence is used.

Called from threads:
- thread3_main
- thread4_sound
- thread5_game_loop
*/
unsafe fn begin_background_music_fade(fadeDuration: u16) -> u8 {
	let mut targetVolume: u8 = 0xff;

	if sCurrentBackgroundMusicSeqId == SEQUENCE_NONE || sCurrentBackgroundMusicSeqId == SEQ_EVENT_CUTSCENE_CREDITS {
		return 0xff;
	}

	if gSequencePlayers[SEQ_PLAYER_LEVEL].volume == 0.0 && fadeDuration != 0 {
		gSequencePlayers[SEQ_PLAYER_LEVEL].volume = gSequencePlayers[SEQ_PLAYER_LEVEL].fadeVolume;
	}

	if sBackgroundMusicTargetVolume != TARGET_VOLUME_UNSET {
		targetVolume = sBackgroundMusicTargetVolume & TARGET_VOLUME_VALUE_MASK;
	}

	if sBackgroundMusicMaxTargetVolume != TARGET_VOLUME_UNSET {
		let maxTargetVolume = sBackgroundMusicMaxTargetVolume & TARGET_VOLUME_VALUE_MASK;
		if targetVolume > maxTargetVolume {
			targetVolume = maxTargetVolume;
		}
	}

	if sLowerBackgroundMusicVolume && targetVolume > 40 {
		targetVolume = 40;
	}

	if sSoundBanksThatLowerBackgroundMusic != 0 && targetVolume > 20 {
		targetVolume = 20;
	}

	if gSequencePlayers[SEQ_PLAYER_LEVEL].flags.contains(SequencePlayerFlags::Enabled) {
		if targetVolume != 0xff {
			seq_player_fade_to_target_volume(SEQ_PLAYER_LEVEL, fadeDuration as i32, targetVolume);
		} else {
			seq_player_fade_to_normal_volume(SEQ_PLAYER_LEVEL, fadeDuration as i32);
		}
	}

	targetVolume
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn set_audio_muted(muted: bool) {
	let bits = build_global_cmd(if muted { EU_CMD_MUTE_PLAYERS } else { EU_CMD_UNMUTE_PLAYERS });
	//for _ in 0..SEQUENCE_PLAYERS {
		send_audio_cmd_u32(bits, 0);
	//}
}

/// Called from threads:
/// - thread4_sound
#[no_mangle]
pub unsafe extern "C" fn sound_init() {
	for bank in &mut sSoundBanks {
		for sound in bank {
			sound.soundStatus = SoundStatus::Stopped;
		}
	}

	// Remove current sounds
	unsafe {
		let std::ops::Range { start, end } = sCurrentSound.as_mut_ptr_range();
		let start = start.cast::<u8>();
		let size = end.cast::<u8>().offset_from(start);
		start.write_bytes(0xff, size as usize);
	}

	sSoundBankUsedListBack.fill(0);
	sSoundBankFreeListFront.fill(1);
	sNumSoundsInBank.fill(0);

	for bank in &mut sSoundBanks {
		// Set used list to empty
		bank[0].prev = 0xff;
		bank[0].next = 0xff;

		let bank_len = bank.len();

		// Set free list to contain every sound slot
		for idx in 1..bank_len - 1 {
			bank[idx].prev = (idx - 1) as u8;
			bank[idx].next = (idx + 1) as u8;
		}

		let [.., last] = bank;
		last.prev = (bank_len - 2) as u8;
		last.next = 0xff;
	}

	for player_group in &mut sChannelFades {
		for fade in player_group {
			fade.remainingFrames = 0;
		}
	}

	for seq in &mut sBackgroundMusicQueue {
		seq.priority = 0;
	}

	sound_banks_enable(SEQ_PLAYER_SFX, SOUND_BANKS_ALL_BITS);

	sBackgroundMusicTargetVolume = TARGET_VOLUME_UNSET;
	sLowerBackgroundMusicVolume = false;
	sSoundBanksThatLowerBackgroundMusic = 0;
	sCurrentBackgroundMusicSeqId = 0xff;
	gSoundMode = SoundMode::Stereo;
	sBackgroundMusicQueueSize = 0;
	sBackgroundMusicMaxTargetVolume = TARGET_VOLUME_UNSET;
	sCurrentEnvironmentSeqId = 0;
	sEnvironmentTargetVolume = 0;
	sSoundRequestProcessingHead = 0;
	sSoundRequestCursor = 0;
}

/* struct PlayingSound {
	numPlayingSounds: u8,
	numSoundsInBank: u8,
	soundId: u8,
}

/// Unused
fn get_currently_playing_sound(bank: usize) -> PlayingSound; */

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn stop_sound(soundBits: SoundBits, pos: *const f32) {
	let bank = soundBits.bank() as usize;
	let mut soundIndex = sSoundBanks[bank][0].next as usize;
	while soundIndex != 0xff {
		let sound = &mut sSoundBanks[bank][soundIndex];

		// If sound has same id and source position pointer...
		if soundBits.sound_id_bits() == sound.soundBits.sound_id_bits() && sound.x == pos {
			// ...mark sound for deletion.
			update_background_music_after_sound(bank, soundIndex);
			sound.soundBits = SoundBits::NO_SOUND;
			soundIndex = 0xff; // break
		} else {
			soundIndex = sound.next as usize;
		}
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn stop_sounds_from_source(pos: *const f32) {
	for (bankId, bank) in sSoundBanks.iter_mut().enumerate() {
		let mut soundIndex = bank[0].next as usize;
		while soundIndex != 0xff {
			let sound = &mut bank[soundIndex];
			if sound.x == pos {
				update_background_music_after_sound(bankId, soundIndex);
				sound.soundBits = SoundBits::NO_SOUND;
			}
			soundIndex = sound.next as usize;
		}
	}
}

/// Called from threads:
/// - thread3_main
/// - thread5_game_loop
unsafe fn stop_sounds_in_bank(bankId: u8) {
	let bank = &mut sSoundBanks[bankId as usize];
	let mut soundIndex = bank[0].next as usize;
	while soundIndex != 0xff {
		update_background_music_after_sound(bankId as usize, soundIndex);
		bank[soundIndex].soundBits = SoundBits::NO_SOUND;
		soundIndex = bank[soundIndex].next as usize;
	}
}

/**
Stops sounds in all of the sound banks that predominantly consist of continuous
sounds. Misses some specific continuous sounds in other banks like bird chirping
and the ticking sound after pressing a switch.

Called from threads:
- thread3_main
- thread5_game_loop
*/
#[no_mangle]
pub unsafe extern "C" fn stop_sounds_in_continuous_banks() {
	stop_sounds_in_bank(SOUND_BANK_MOVING);
	stop_sounds_in_bank(SOUND_BANK_ENV);
	stop_sounds_in_bank(SOUND_BANK_AIR);
}

/// Called from threads:
/// - thread3_main
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn sound_banks_disable(player: usize, mut bankMask: u16) {
	let _ = player;
	for bank in &mut sSoundBankDisabled {
		if bankMask & 1 != 0 {
			*bank = true;
		}
		bankMask >>= 1;
	}
}

/// Called from threads:
/// - thread5_game_loop
unsafe fn disable_all_sequence_players() {
	for player in &mut gSequencePlayers {
		player.disable();
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn sound_banks_enable(player: usize, mut bankMask: u16) {
	let _ = player;
	for bank in &mut sSoundBankDisabled {
		if bankMask & 1 != 0 {
			*bank = false;
		}
		bankMask >>= 1;
	}
}

// fn unused_803209D8(player: usize, channelIndex: usize, stopSomething2: bool) -> bool;

/**
Set the moving speed for a sound bank, which may affect the volume and pitch
of the sound.

Called from threads:
- thread5_game_loop
*/
#[no_mangle]
pub unsafe extern "C" fn set_sound_moving_speed(bank: usize, speed: u8) {
	sSoundMovingSpeed[bank] = speed;
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn play_dialog_sound(mut dialogId: usize) {
	if dialogId >= DIALOG_COUNT {
		dialogId = 0;
	}
	let dialogId = dialogId;

	let speaker = sDialogSpeaker[dialogId];
	if speaker != DialogSpeaker::None {
		play_sound(sDialogSpeakerVoice[speaker as u8 as usize], gGlobalSoundSource.as_ptr());

		// Play music during bowser message that appears when first entering the
		// castle or when trying to enter a door without enough stars.
		if speaker == DialogSpeaker::BOWS1 {
			seq_player_play_sequence(SEQ_PLAYER_ENV, SEQ_EVENT_KOOPA_MESSAGE, 0);
		}
	}

	// "You've stepped on the (Wing|Metal|Vanish) Cap Switch"
	if let 10..=12 = dialogId {
		play_puzzle_jingle();
	}
}

#[inline(always)]
pub const fn sequence_args(priority: u8, seqId: u8) -> u16 {
	u16::from_le_bytes([seqId, priority])
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn play_music(player: usize, seqArgs: u16, fadeTimer: u16) {
	let [seqId, priority] = seqArgs.to_le_bytes();

	// Except for the background music player, we don't support queued
	// sequences. Just play them immediately, stopping any old sequence.
	if player != SEQ_PLAYER_LEVEL {
		seq_player_play_sequence(player, seqId, fadeTimer);
		return;
	}

	// Abort if the queue is already full.
	if sBackgroundMusicQueueSize as usize >= MAX_BACKGROUND_MUSIC_QUEUE_SIZE {
		return;
	}

	// If already in the queue, abort, after first restarting the sequence if
	// it is first, and handling disabled music somehow.
	// (That handling probably ought to occur even when the queue is full...)
	for seq in &sBackgroundMusicQueue[..sBackgroundMusicQueueSize as usize] {
		if seq.seqId == seqId {
			if std::ptr::eq(seq, &sBackgroundMusicQueue[0]) {
				seq_player_play_sequence(SEQ_PLAYER_LEVEL, seqId, fadeTimer);
			} else if !gSequencePlayers[SEQ_PLAYER_LEVEL].flags.contains(SequencePlayerFlags::Enabled) {
				stop_background_music(seqId);
			}
			return;
		}
	}

	// Find the next sequence slot by priority.
	let mut foundIndex = 0;
	for idx in 0..sBackgroundMusicQueueSize {
		if sBackgroundMusicQueue[idx as usize].priority <= priority {
			foundIndex = idx;
			break;
		}
	}

	// If the sequence ends up first in the queue, start it, and make space for one more entry in the queue.
	if foundIndex == 0 {
		seq_player_play_sequence(SEQ_PLAYER_LEVEL, seqId, fadeTimer);
		sBackgroundMusicQueueSize += 1;
	}

	// Move all items up in queue, throwing away the last one if we didn't put the new sequence first.
	/*for idx in (foundIndex + 1..sBackgroundMusicQueueSize).rev() {
		let &mut [ref prev, ref mut cur] = &mut sBackgroundMusicQueue[idx as usize - 1..];
		/*cur.priority = prev.priority;
		cur.seqId = prev.seqId;*/
		*cur = *prev;
	}*/
	sBackgroundMusicQueue.copy_within(foundIndex as usize..sBackgroundMusicQueueSize as usize - 1, foundIndex as usize + 1);

	// Insert item into queue.
	sBackgroundMusicQueue[foundIndex as usize] = SequenceQueueItem {
		seqId, priority,
	};
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn stop_background_music(seqId: u8) {
	if sBackgroundMusicQueueSize == 0 {
		return;
	}

	// If sequence is not found, remove an empty queue item (the next empty queue slot).
	let mut foundIndex = sBackgroundMusicQueueSize as usize;

	// Search for the sequence.
	for idx in 0..sBackgroundMusicQueueSize as usize {
		if sBackgroundMusicQueue[idx].seqId != seqId {
			continue;
		}

		// Remove sequence from queue. If it was first, play the next one, or fade out the music.
		sBackgroundMusicQueueSize -= 1;
		if idx == 0 {
			if sBackgroundMusicQueueSize != 0 {
				seq_player_play_sequence(SEQ_PLAYER_LEVEL, sBackgroundMusicQueue[1].seqId, 0);
			} else {
				seq_player_fade_out(SEQ_PLAYER_LEVEL as u8, 20);
			}
		}
		foundIndex = idx;
		break;
	}

	// Move later slots down.
	sBackgroundMusicQueue.copy_within(foundIndex + 1..sBackgroundMusicQueueSize as usize + 1, foundIndex);

	// @bug? If the sequence queue is full and we attempt to stop a sequence
	// that isn't in the queue, this writes out of bounds. Can that happen?
	sBackgroundMusicQueue[sBackgroundMusicQueueSize as usize].priority = 0;
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn fadeout_background_music(seqId: u8, fadeDuration: u16) {
	if sBackgroundMusicQueueSize != 0 && sBackgroundMusicQueue[0].seqId == seqId {
		seq_player_fade_out(SEQ_PLAYER_LEVEL as u8, fadeDuration);
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn drop_queued_background_music() {
	if sBackgroundMusicQueueSize != 0 {
		sBackgroundMusicQueueSize = 1;
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn get_current_background_music() -> u16 {
	if sBackgroundMusicQueueSize != 0 {
		let SequenceQueueItem { seqId, priority } = sBackgroundMusicQueue[0];
		u16::from_le_bytes([seqId, priority])
	} else {
		!0
	}
}

/// Called from threads:
/// - thread4_sound
/// - thread5_game_loop (EU only)
unsafe fn process_environment_music() {
	if sEnvironmentMusicChangeDelay != 0 {
		sEnvironmentMusicChangeDelay -= 1;
	}

	if gSequencePlayers[SEQ_PLAYER_ENV].flags.contains(SequencePlayerFlags::Enabled)
			|| sBackgroundMusicMaxTargetVolume == TARGET_VOLUME_UNSET || sEnvironmentMusicChangeDelay != 0 {
		return;
	}

	sBackgroundMusicMaxTargetVolume = TARGET_VOLUME_UNSET;
	begin_background_music_fade(50);

	if sBackgroundMusicTargetVolume != TARGET_VOLUME_UNSET {
		if sCurrentEnvironmentSeqId == SEQ_EVENT_MERRY_GO_ROUND || sCurrentEnvironmentSeqId == SEQ_EVENT_PIRANHA_PLANT {
			seq_player_play_sequence(SEQ_PLAYER_ENV, sCurrentEnvironmentSeqId, 1);
			if sEnvironmentTargetVolume != 0xff {
				seq_player_fade_to_target_volume(SEQ_PLAYER_ENV, 1, sEnvironmentTargetVolume);
			}
		}
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn play_secondary_music(seqId: u8, bgMusicVolume: u8, volume: u8, fadeDuration: u16) {
	if sCurrentBackgroundMusicSeqId == 0xff || sCurrentBackgroundMusicSeqId == SEQ_MENU_TITLE_SCREEN {
		return;
	}

	if sBackgroundMusicTargetVolume == TARGET_VOLUME_UNSET {
		sBackgroundMusicTargetVolume = bgMusicVolume | TARGET_VOLUME_IS_PRESENT_FLAG;
		begin_background_music_fade(fadeDuration);
		seq_player_play_sequence(SEQ_PLAYER_ENV, seqId, fadeDuration >> 1);
		if volume < 0x80 {
			seq_player_fade_to_target_volume(SEQ_PLAYER_ENV, fadeDuration as i32, volume);
		}
		sEnvironmentTargetVolume = volume;
		sCurrentEnvironmentSeqId = seqId;
	} else if volume != 0xff {
		sBackgroundMusicTargetVolume = bgMusicVolume | TARGET_VOLUME_IS_PRESENT_FLAG;
		begin_background_music_fade(fadeDuration);
		seq_player_fade_to_target_volume(SEQ_PLAYER_ENV, fadeDuration as i32, volume);
		sEnvironmentTargetVolume = volume;
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn fade_out_environment_music(fadeDuration: u16) {
	if sBackgroundMusicTargetVolume == TARGET_VOLUME_UNSET {
		return;
	}

	sBackgroundMusicTargetVolume = TARGET_VOLUME_UNSET;
	sCurrentEnvironmentSeqId = 0;
	sEnvironmentTargetVolume = 0;
	begin_background_music_fade(fadeDuration);
	seq_player_fade_out(SEQ_PLAYER_ENV as u8, fadeDuration);
}

/// Called from threads:
/// - thread3_main
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn stop_all_sounds(fadeDuration: u16) {
	if sHasStartedFadeOut {
		return;
	}

	if gSequencePlayers[SEQ_PLAYER_LEVEL].flags.contains(SequencePlayerFlags::Enabled) {
		send_audio_cmd_u32(build_player_cmd(EU_CMD_FADE_OUT_PLAYER, SEQ_PLAYER_LEVEL as u8), fadeDuration as u32);
	}
	if gSequencePlayers[SEQ_PLAYER_ENV].flags.contains(SequencePlayerFlags::Enabled) {
		send_audio_cmd_u32(build_player_cmd(EU_CMD_FADE_OUT_PLAYER, SEQ_PLAYER_ENV as u8), fadeDuration as u32);
	}

	for bank in 0..SOUND_BANK_COUNT {
		if bank as u8 != SOUND_BANK_MENU {
			fade_channel_volume_scale(SEQ_PLAYER_SFX, bank, 0, fadeDuration / 16);
		}
	}

	sHasStartedFadeOut = true;
}

macro_rules! play_env {
	{$({
		$name:ident,
		seq: $seq:expr,
		target_vol: $target_vol:expr,
	})*} => {$(
		/// Called from threads:
		/// - thread5_game_loop
		#[no_mangle]
		pub unsafe extern "C" fn $name() {
			seq_player_play_sequence(SEQ_PLAYER_ENV, $seq, 0);
			sBackgroundMusicMaxTargetVolume = $target_vol | TARGET_VOLUME_IS_PRESENT_FLAG;
			sEnvironmentMusicChangeDelay = 2;
			begin_background_music_fade(50);
		}
	)*};
}

play_env! {
	{
		play_course_clear,
		seq: SEQ_EVENT_CUTSCENE_COLLECT_STAR,
		target_vol: 0,
	}
	{
		play_peachs_jingle,
		seq: SEQ_EVENT_PEACH_MESSAGE,
		target_vol: 0,
	}
	{
		play_puzzle_jingle,
		seq: SEQ_EVENT_SOLVE_PUZZLE,
		target_vol: 20,
	}
	{
		play_star_fanfare,
		seq: SEQ_EVENT_HIGH_SCORE,
		target_vol: 20,
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn play_power_star_jingle(keep_music_playing: bool) {
	if !keep_music_playing {
		sBackgroundMusicTargetVolume = 0;
	}
	seq_player_play_sequence(SEQ_PLAYER_ENV, SEQ_EVENT_CUTSCENE_STAR_SPAWN, 0);
	sBackgroundMusicMaxTargetVolume = 20 | TARGET_VOLUME_IS_PRESENT_FLAG;
	sEnvironmentMusicChangeDelay = 2;
	begin_background_music_fade(50);
}

play_env! {
	{
		play_race_fanfare,
		seq: SEQ_EVENT_RACE,
		target_vol: 20,
	}
	{
		play_toads_jingle,
		seq: SEQ_EVENT_TOAD_MESSAGE,
		target_vol: 20,
	}
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn sound_reset(mut presetId: u8) {
	if presetId >= 8 {
		presetId = 0;
		eprintln!("Warning: preset {presetId} is out of range");
	}
	let presetId = presetId;

	sGameLoopTicked = false;
	disable_all_sequence_players();
	sound_init();
	audio_reset_session_eu(presetId as i32);
	if presetId != 7 {
		preload_sequence(SEQ_EVENT_SOLVE_PUZZLE, PreloadMask::Banks | PreloadMask::Sequence);
		preload_sequence(SEQ_EVENT_PEACH_MESSAGE, PreloadMask::Banks | PreloadMask::Sequence);
		preload_sequence(SEQ_EVENT_CUTSCENE_STAR_SPAWN, PreloadMask::Banks | PreloadMask::Sequence);
	}
	seq_player_play_sequence(SEQ_PLAYER_SFX, SEQ_SOUND_PLAYER, 0);
	sPresetIdSoundMode = (sPresetIdSoundMode & 0xf0) | presetId;
	gSoundMode = SoundMode::from_u8(sPresetIdSoundMode >> 4);
	sHasStartedFadeOut = false;
}

/// Called from threads:
/// - thread5_game_loop
#[no_mangle]
pub unsafe extern "C" fn audio_set_sound_mode(soundMode: SoundMode) {
	sPresetIdSoundMode = (sPresetIdSoundMode & 0xf) | ((soundMode as u8) << 4);
	gSoundMode = soundMode;
}

/// This is a convenience function that starts the player for sound effects.
#[no_mangle]
pub unsafe extern "C" fn sound_effects_start() {
	play_music(SEQ_PLAYER_SFX, sequence_args(0, SEQ_SOUND_PLAYER), 0);
}
