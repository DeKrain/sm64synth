// We split the decoder and command representation to
// allow using different versions.

use super::*;

use sm64_synth_macros::m64_opcode_def_ext;

#[cfg(any())]
macro_rules! version_opcode_def {
	{
		$version:ident $command_type:ident :
		$(
			$opcode:literal = $variant:expr ;
		)*
		Packed(
			$packed_command_type:ident
			mask $mask:literal
			test($($test:tt)*)
		) {
			$($packed_opcode:literal = $packed_variant:expr ; )*
		}
	} => {
		fn $version(op: u8) -> $packed_command_type {
			if op $($test)* {
				let arg = op & !$mask;
				use $packed_command_type::*;
				match op & $mask {
					$($packed_opcode => $packed_variant(arg),)*
					_ => panic!("Unknown M64 opcode"),
				}
			} else {
				use $command_type::*;
				match op {
					$($opcode => $packed_command_type::Standalone($variant),)*
					_ => panic!("Unknown M64 opcode"),
				}
			}
		}
	};
	{
		$version:ident $command_type:ident :
		$(
			$opcode:literal = $variant:expr ;
		)*
	} => {
		fn $version(op: u8) -> $command_type {
			use $command_type::*;
			match op {
				$($opcode => $variant,)*
				_ => panic!("Unknown M64 opcode"),
			}
		}
	};
}

#[cfg(any())]
macro_rules! version_opcode_def_ext {
	{
		$command_type:ty :
		Versions {
			jp: $jp_fn:ident,
			us: $us_fn:ident,
			eu: $eu_fn:ident,
			sh: $sh_fn:ident,
		}

		$($commands:tt)*
	} => {
		version_opcode_def_ext_preproc!{
			[jp:$jp_fn ($command_type)]
			{ $($commands)* } {}
		}
		version_opcode_def_ext_preproc!{
			[us:$us_fn ($command_type)]
			{ $($commands)* } {}
		}
		version_opcode_def_ext_preproc!{
			[eu:$eu_fn ($command_type)]
			{ $($commands)* } {}
		}
		version_opcode_def_ext_preproc!{
			[sh:$sh_fn ($command_type)]
			{ $($commands)* } {}
		}
	};
}

#[cfg(any())]
macro_rules! version_opcode_def_ext_preproc {
	/* Template:
	{
		[$ver:ident : $fn:ident ($cmd_ty:ty)]
		{
			$($commands:tt)*
		}
		{
			$($output:tt)*
		}
	} => {};
	*/

	{
		$pre:tt
		{
			@ $cond:tt $body:tt
			$($commands:tt)*
		}
		$output:tt
	} => (version_opcode_def_ext_preproc! {
		$pre
		Cond $cond $body
		{ $($commands)* }
		$output
	});
	// For lack of token comparisons in MBEs, list all variants
	{
		[jp : $fn:ident ($cmd_ty:ty)]
		Cond(jp $(, $($cond_tail:tt)*)?) $body:tt
		$commands:tt
		$output:tt
	} => (version_opcode_def_ext_preproc! {
		[jp : $fn:ident ($cmd_ty:ty)]
		CondSat $body
		$commands
		$output
	});
	{
		[us : $fn:ident ($cmd_ty:ty)]
		Cond(us $(, $($cond_tail:tt)*)?) $body:tt
		$commands:tt
		$output:tt
	} => (version_opcode_def_ext_preproc! {
		[us : $fn:ident ($cmd_ty:ty)]
		CondSat $body
		$commands
		$output
	});
	{
		[eu : $fn:ident ($cmd_ty:ty)]
		Cond(eu $(, $($cond_tail:tt)*)?) $body:tt
		$commands:tt
		$output:tt
	} => (version_opcode_def_ext_preproc! {
		[eu : $fn:ident ($cmd_ty:ty)]
		CondSat $body
		$commands
		$output
	});
	{
		[sh : $fn:ident ($cmd_ty:ty)]
		Cond(sh $(, $($cond_tail:tt)*)?) $body:tt
		$commands:tt
		$output:tt
	} => (version_opcode_def_ext_preproc! {
		[sh : $fn:ident ($cmd_ty:ty)]
		CondSat $body
		$commands
		$output
	});
	// Fallback case
	{
		$head:tt
		Cond($cond:tt $(, $($cond_tail:tt)*)?) $body:tt
		$commands:tt
		$output:tt
	} => (version_opcode_def_ext_preproc! {
		$head
		Cond($($($cond_tail)*)?) $body
		$commands
		$output
	});
	// Failed case
	{
		$head:tt
		Cond() $body:tt
		$commands:tt
		$output:tt
	} => (version_opcode_def_ext_preproc! {
		$head
		$commands
		$output
	});
	// Success case
	{
		$head:tt
		CondSat { $($body:tt)* }
		{ $($commands:tt)* }
		$output:tt
	} => (version_opcode_def_ext_preproc! {
		$head
		{
			$($body)*
			$($commands)*
		}
		$output
	});

	// Other tokens
	{
		$head:tt
		{
			$token:tt
			$($commands:tt)*
		}
		{
			$($output:tt)*
		}
	} => (version_opcode_def_ext_preproc! {
		$head
		{
			$($commands)*
		}
		{
			$($output)*
			$token
		}
	});

	// Final case
	{
		$head:tt
		{}
		$output:tt
	} => (version_opcode_def_ext_gen! {
		$head
		$output
	});
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum M64Version {
	/// JP
	Japanese,
	/// US
	American,
	/// EU
	European,
	/// SH
	JapaneseShindou,
}

/// This controls the version of sound sequence format (seperate from engine version,
/// but some features will not match 1 to 1).
/// It's currently set to `US` by default.
const M64_GAME_VERSION: M64Version = M64Version::American;

impl M64Version {
	pub fn decode_layer_op(self, op: u8) -> M64LayerCommand {
		match self {
			M64Version::Japanese => decode_jp_layer_op(op),
			M64Version::American => decode_us_layer_op(op),
			M64Version::European => decode_eu_layer_op(op),
			M64Version::JapaneseShindou => decode_sh_layer_op(op),
		}
	}

	pub fn decode_channel_op(self, op: u8) -> M64ChannelCommand {
		match self {
			M64Version::Japanese => decode_jp_channel_op(op),
			M64Version::American => decode_us_channel_op(op),
			M64Version::European => decode_eu_channel_op(op),
			M64Version::JapaneseShindou => decode_sh_channel_op(op),
		}
	}

	pub fn decode_player_op(self, op: u8) -> M64PlayerCommand {
		match self {
			M64Version::Japanese => decode_jp_player_op(op),
			M64Version::American => decode_us_player_op(op),
			M64Version::European => decode_eu_player_op(op),
			M64Version::JapaneseShindou => decode_sh_player_op(op),
		}
	}
}

pub enum JumpCond {
	Uncond,
	IfZero,
	// Unused
	//IfNonZero,
	IfNegative,
	// Only in absolute
	IfNonNegative,
}

pub enum M64LayerCommandStandalone {
	End,
	Call,
	LoopStart,
	LoopEnd,
	Jump,
	JumpRelative,
	SetShortNoteVelocity,
	SetPan,
	Transpose,
	SetShortNoteDuration,
	ContinuousNotesOn,
	ContinuousNotesOff,
	SetShortNoteDefaultPlayPercentage,
	SetInstrument,
	SetPortamento,
	DisablePortamento,
	SetEnvelope,
	IgnoreDrumPan,
}

pub enum M64LayerCommand {
	// Looping commands
	Standalone(M64LayerCommandStandalone),
	SetShortNoteVelocityFromTable(u8),
	SetShortNoteDurationFromTable(u8),

	// Pausing commands
	Delay,
	Note0(u8),
	Note1(u8),
	Note2(u8),
}

m64_opcode_def_ext! {
	M64LayerCommand:
	Versions {
		jp: decode_jp_layer_op,
		us: decode_us_layer_op,
		eu: decode_eu_layer_op,
		sh: decode_sh_layer_op,
	}

	Test(<= 0xC0) {
		//Return(Standalone(Break))
		Test(== 0xC0) Return(M64LayerCommand::Delay)
		Packed(mask 0xC0) {
			0x00 = Note0;
			0x40 = Note1;
			0x80 = Note2;
		}
	}

	Match(0xD0 ..= 0xEF) {
		Packed(mask 0xF0) {
			0xD0 = SetShortNoteVelocityFromTable;
			0xE0 = SetShortNoteDurationFromTable;
		}
	}

	Standalone(M64LayerCommandStandalone) {
		0xFF = End;
		0xFC = Call;
		0xF8 = LoopStart;
		0xF7 = LoopEnd;
		0xFB = Jump;
		@(eu, sh) {
			0xF4 = JumpRelative;
		}

		0xC1 = SetShortNoteVelocity;
		// JP/US interpret panning differently from EU/SH
		0xCA = SetPan;
		0xC2 = Transpose;
		0xC9 = SetShortNoteDuration;
		0xC4 = ContinuousNotesOn;
		0xC5 = ContinuousNotesOff;
		0xC3 = SetShortNoteDefaultPlayPercentage;
		// EU/SH extends this to waves
		0xC6 = SetInstrument;
		0xC7 = SetPortamento;
		0xC8 = DisablePortamento;
		@(eu, sh) {
			0xCB = SetEnvelope;
			0xCC = IgnoreDrumPan;
		}
	}
}

impl SequenceChannelLayer {
	pub(super) unsafe fn process_script(&mut self) {
		use SequenceChannelLayerFlags as F;
		if !self.flags.contains(F::Enabled) {
			return;
		}

		if self.delay > 1 {
			self.delay -= 1;
			if !self.flags.contains(F::StopSomething) && self.delay <= self.duration {
				self.note_decay();
				self.flags |= F::StopSomething;
			}
			return;
		}

		if !self.flags.contains(F::ContinuousNotes) {
			self.note_decay();
		}

		if matches!(self.portamento.mode_split().1, PORTAMENTO_MODE_ONE_SHOT_TARGET | PORTAMENTO_MODE_ONE_SHOT_NOTE) {
			self.portamento.mode = 0;
		}

		let seqChannel = unsafe { &mut *self.seqChannel };
		let seqPlayer = unsafe { &mut *seqChannel.seqPlayer };
		self.flags |= F::NotePropertiesNeedInit;

		enum NoteCmd {
			Note0,
			Note1,
			Note2,
		}
		enum ContinueCmd {
			Delay,
			Note(NoteCmd, u8),
		}

		let state = &mut *ptr::addr_of_mut!(self.scriptState);
		let cmd = loop {
			let cmd = state.read_u8();
			let cmd = M64_GAME_VERSION.decode_layer_op(cmd);
			use M64LayerCommand::{*, Standalone as S};
			use M64LayerCommandStandalone::*;

			match cmd {
				Delay => break ContinueCmd::Delay,
				Note0(v) => break ContinueCmd::Note(NoteCmd::Note0, v),
				Note1(v) => break ContinueCmd::Note(NoteCmd::Note1, v),
				Note2(v) => break ContinueCmd::Note(NoteCmd::Note2, v),

				S(End) => {
					// Layer end
					// Return or end of script
					if state.depth == 0 { 
						// N.B. this function call is *not* inlined even though it's
						// within the same file, unlike in the rest of this function.
						self.disable();
						return;
					}
					state.depth -= 1;
					state.pc = state.stack[state.depth as usize];
				}
				S(Call) => {
					// Call
					debug_assert!(state.depth < 4, "Stack overflow!");
					let offset = state.read_u16();
					state.stack[state.depth as usize] = state.pc;
					state.depth += 1;
					state.pc = seqPlayer.seqData.add(offset as usize);
				}
				S(LoopStart) => {
					// Loop start, N iterations (256 if N = 0)
					debug_assert!(state.depth < 4, "Stack overflow!");
					state.remLoopIters[state.depth as usize] = state.read_u8();
					state.stack[state.depth as usize] = state.pc;
					state.depth += 1;
				}
				S(LoopEnd) => {
					// Loop end
					if { let p = &mut state.remLoopIters[state.depth as usize - 1]; *p -= 1; *p } != 0 {
						state.pc = state.stack[state.depth as usize - 1];
					} else {
						state.depth -= 1;
					}
				}
				S(Jump) => {
					// Jump
					let offset = state.read_u16();
					state.pc = seqPlayer.seqData.add(offset as usize);
				}
				S(JumpRelative) => {
					// Relative jump
					let offset = state.read_i8() as isize;
					state.pc = state.pc.offset(offset);
				}
				S(SetShortNoteVelocity) => {
					// Set short note velocity
					let vel = state.read_u8() as f32;
					self.velocitySquare = vel * vel;
				}
				S(SetPan) => {
					// Set pan
					self.pan = state.read_u8();
				}
				S(Transpose) => {
					// Transpose
					self.transposition = state.read_u8() as i16;
				}
				S(SetShortNoteDuration) => {
					// Set short note duration
					self.noteDuration = state.read_u8();
				}
				S(ContinuousNotesOn) => {
					// Continuous notes ON
					self.flags |= F::ContinuousNotes;
					self.note_decay();
				}
				S(ContinuousNotesOff) => {
					// Continuous notes OFF
					self.flags -= F::ContinuousNotes;
					self.note_decay();
				}
				S(SetShortNoteDefaultPlayPercentage) => {
					// Set short note default play percentage
					self.shortNoteDefaultPlayPercentage = state.read_compressed_u16() as i16;
				}
				S(SetInstrument) => {
					// Set instrument
					let cmd = state.read_u8();
					if cmd >= 0x7F {
						if cmd == 0x7F {
							self.instrOrWave = 0;
						} else {
							self.instrOrWave = cmd;
							self.instrument = ptr::null_mut();
						}

						if cmd == 0xFF {
							self.adsr.releaseRate = 0;
						}
					} else {
						self.instrOrWave = seqChannel.get_instrument(cmd, &mut self.instrument, &mut self.adsr);
						if self.instrOrWave == 0 {
							eprintln!("Warning: cannot change instrument: {cmd}");
							self.instrOrWave = 0xFF;
						}
					}
				}
				S(SetPortamento) => {
					// Portamento
					self.portamento.mode = state.read_u8();

					let mut target = (state.read_u8() as i16
						+ seqChannel.transposition
						+ self.transposition
						+ seqPlayer.transposition) as u8;

					if target >= 0x80 {
						target = 0;
					}

					self.portamentoTargetNote = target;

					if self.portamento.mode_split().0 {
						self.portamentoTime = state.read_u8() as u16;
					} else {
						self.portamentoTime = state.read_compressed_u16();
					}
				}
				S(DisablePortamento) => {
					// Disable portamento
					self.portamento.mode = 0;
				}
				S(SetEnvelope) => {
					// Set envelope
					let address = state.read_u16() as usize;
					self.adsr.envelope = seqPlayer.seqData.add(address).cast();
					self.adsr.releaseRate = state.read_u8();
				}
				S(IgnoreDrumPan) => {
					// Ignore drum pan
					self.flags |= F::IgnoreDrumPan;
				}

				SetShortNoteVelocityFromTable(index) => {
					// Set short note velocity from table
					let vel = *seqPlayer.shortNoteVelocityTable.add(index as usize) as f32;
					self.velocitySquare = vel * vel;
				}
				SetShortNoteDurationFromTable(index) => {
					// Set short note duration from table
					self.noteDuration = *seqPlayer.shortNoteDurationTable.add(index as usize);
				}

				/*_ => {
					eprintln!("Error: undefined note command: {cmd:02X}");
				}*/
			}
		};

		let mut sameSound = true;
		match cmd {
			ContinueCmd::Delay => {
				self.delay = state.read_compressed_u16() as i16;
				self.flags |= F::StopSomething;
			}
			ContinueCmd::Note(note_cmd, mut semi) => {
				self.flags -= F::StopSomething;

				let delay;
				use NoteCmd::*;
				if seqChannel.flags.contains(SequenceChannelFlags::LargeNotes) {
					let vel;
					match note_cmd {
						Note0 => {
							// Note0 (play percentage, velocity, duration)
							delay = state.read_compressed_u16();
							vel = state.read_u8() as f32;
							self.noteDuration = state.read_u8();
							self.playProcentage = delay as i16;
						}
						Note1 => {
							// Note1 (play percentage, velocity)
							delay = state.read_compressed_u16();
							vel = state.read_u8() as f32;
							self.noteDuration = 0;
							self.playProcentage = delay as i16;
						}
						Note2 => {
							// Note2 (velocity, duration, uses last play percentage)
							delay = self.playProcentage as u16;
							vel = state.read_u8() as f32;
							self.noteDuration = state.read_u8();
						}
					}

					self.velocitySquare = vel * vel;
				} else {
					match note_cmd {
						Note0 => {
							// Note0 (play percentage)
							delay = state.read_compressed_u16();
							self.playProcentage = delay as i16;
						}
						Note1 => {
							// Note1 (uses default play percentage)
							delay = self.shortNoteDefaultPlayPercentage as u16;
						}
						Note2 => {
							// Note2 (uses last play percentage)
							delay = self.playProcentage as u16;
						}
					}
				}

				self.delay = delay as i16;
				self.duration = ((self.noteDuration as u32 * delay as u32) >> 8) as i16;
				if (
					seqPlayer.flags.contains(SequencePlayerFlags::Muted) && seqChannel.muteBehavior.contains(MuteBehaviorFlags::StopNotes)
				) || seqChannel.flags.contains(SequenceChannelFlags::StopSomething2) {
					self.flags |= F::StopSomething;
				} else {
					let mut temp = self.instrOrWave as i32;
					if temp == 0xff {
						temp = seqChannel.instrOrWave as i32;
					}
					let temp = temp;
					if temp == 0 {
						// Drum
						semi = (
							semi as i16 +
							seqChannel.transposition +
							self.transposition) as u8;

						let drum = get_drum(seqChannel.bankId as i32, semi as i32);
						if drum.is_null() {
							self.flags |= F::StopSomething;
						} else {
							let drum = unsafe { &*drum };
							self.adsr.envelope = drum.envelope;
							self.adsr.releaseRate = drum.releaseRate;
							if !self.flags.contains(F::IgnoreDrumPan) {
								self.pan = drum.pan;
							}
							self.sound = &drum.sound;
							self.freqScale = drum.sound.tuning;
						}
					} else {
						// Instrument
						semi = (
							semi as i16 +
							seqPlayer.transposition +
							seqChannel.transposition +
							self.transposition
						) as u8;
						if semi >= 0x80 {
							self.flags |= F::StopSomething;
						} else {
							let instrument = if self.instrOrWave == 0xff {
								seqChannel.instrument
							} else {
								self.instrument
							};

							if self.portamento.mode != 0 {
								let max_semi = semi.max(self.portamentoTargetNote);

								let tuning = if !instrument.is_null() {
									let sound = (*instrument).get_audio_bank_sound(max_semi as i32);
									sameSound = self.sound == sound;
									self.sound = sound;
									sound.tuning
								} else {
									self.sound = ptr::null();
									1.0
								};

								let freqNote = gNoteFrequencies[semi as usize] * tuning;
								let freqPortaTarget = gNoteFrequencies[self.portamentoTargetNote as usize] * tuning;
								let (porta_special, porta_mode) = self.portamento.mode_split();
								let freqScale = match porta_mode {
									PORTAMENTO_MODE_ONE_SHOT_TARGET | PORTAMENTO_MODE_HOLD_TARGET | PORTAMENTO_MODE_HOLD_TARGET_SINGLE => {
										freqPortaTarget
									}
									PORTAMENTO_MODE_ONE_SHOT_NOTE | PORTAMENTO_MODE_HOLD_NOTE | _ => {
										freqNote
									}
								};
								// This is either 0.0 for freqScale=freqNote
								// or freqNote / freqPortaTarget - 1.0
								self.portamento.extent = freqNote / freqScale - 1.0;
								self.portamento.speed = if porta_special {
									32512.0 * seqPlayer.tempo as f32 /
									(self.delay as f32 * gTempoInternalToExternal as f32 * self.portamentoTime as f32)
								} else {
									127.0 / self.portamentoTime as f32
								};
								self.portamento.cur = 0.0;
								self.freqScale = freqScale;
								if porta_mode == PORTAMENTO_MODE_HOLD_TARGET_SINGLE {
									self.portamentoTargetNote = semi;
								}
							} else if let Some(instrument) = instrument.as_ref() {
								let sound = instrument.get_audio_bank_sound(semi as i32);
								sameSound = self.sound == sound;
								self.sound = sound;
								self.freqScale = gNoteFrequencies[semi as usize] * sound.tuning;
							} else {
								self.sound = ptr::null();
								self.freqScale = gNoteFrequencies[semi as usize];
							}
						}
					}
				}
			}
		}

		if self.flags.contains(F::StopSomething) {
			if !self.note.is_null() || self.flags.contains(F::ContinuousNotes) {
				self.note_decay();
			}
			return;
		}

		let allocNote =
			!self.flags.contains(F::ContinuousNotes) ||
			self.note.is_null() || self.status == SoundLoadStatus::NotLoaded
			|| if !sameSound {
				self.note_decay();
				true
			} else if (*self.note).playbackState.parentLayer != self {
				true
			} else {
				if self.sound.is_null() {
					println!("Debug: init synthetic wave");
					init_synthetic_wave(&mut *self.note, self);
				}
				false
			};

		if allocNote {
			self.note = alloc_note(self);
		}

		if let Some(note) = self.note.as_mut() {
			if note.playbackState.parentLayer == self {
				note.vibrato_init();
			}
		}
	}
}

pub enum M64ChannelCommandStandalone {
	End,
	Delay1,
	Delay,
	Hang,
	Call,
	LoopStart,
	LoopEnd,
	LoopBreak,
	Jump(JumpCond),
	JumpRelative(JumpCond),
	ReserveNotes,
	UnReserveNotes,
	SetDynTable,
	DynSetDynTable,
	SetBankAndInstrument,
	SetInstrument,
	LargetNotesOff,
	LargetNotesOn,
	SetVolume,
	SetVolumeScale,
	SetFrequencyScale,
	PitchBend,
	SetFreqScaleFromLutSh,
	SetPan,
	SetPanMix,
	Transpose,
	SetEnvelope,
	SetDecayRelease,
	SetVibratoExtent,
	SetVibratoRate,
	SetVibratoExtentLinear,
	SetVibratoRateLinear,
	SetVibratoDelay,
	SetUpdatesPerFrameUnused,
	SetReverb,
	SetBank,
	WriteSeq,
	Subtract,
	BitAnd,
	LoadValue,
	DisableChannel,
	SetMuteBehavior,
	ReadSeq,
	SetDynPtr,
	StoreSeqDyn,
	StereoHeadsetEffects,
	SetNoteAllocPolicy,
	SetSustain,
	SetReverbIndex,
	DynamicCall,
	BookOffset,
	InitFromSeqData,
	InitInline,
	ResetVibratoFreqScale,
	SetNotePriority,
	SetNotePrioritySh,

	SetSynthesisVolume,
	UnknownShI2U1,
	SetFilter,
	DisableFilter,
	SetDynPtrSeq,
	FillFilter,
	SetDynTableDynPtr,
	LoadDynPtrFromDynTable,
	IndexDynAcc,
}

pub enum M64ChannelCommand {
	Standalone(M64ChannelCommandStandalone),
	TestLayerFinished(u8),
	IoWrite(u8),
	IoRead(u8),
	IoReadSubtract(u8),
	DelayShort(u8),
	SetLayer(u8),
	FreeLayer(u8),
	SetLayerDyn(u8),
	DynSetLayer(u8),
	StartChannel(u8),
	DisableChannelShort(u8),
	IoWriteChannel(u8),
	IoReadChannel(u8),
	SetNotePriorityShort(u8),
	LoadBankIoSh(u8),
}

#[cfg(any())]
version_opcode_def! {
	decode_eu_channel_op M64ChannelCommandStandalone:

	0xFF = End;
	0xFE = Delay1;
	0xFD = Delay;
	0xEA = Hang;
	0xFC = Call;
	0xF8 = LoopStart;
	0xF7 = LoopEnd;
	0xF6 = LoopBreak;
	0xFB = Jump(JumpCond::Uncond);
	0xFA = Jump(JumpCond::IfZero);
	0xF9 = Jump(JumpCond::IfNegative);
	0xF5 = Jump(JumpCond::IfNonNegative);
	0xF4 = JumpRelative(JumpCond::Uncond);
	0xF3 = JumpRelative(JumpCond::IfZero);
	0xF2 = JumpRelative(JumpCond::IfNegative);
	0xF1 = ReserveNotes;
	0xF0 = UnReserveNotes;
	0xC2 = SetDynTable;
	0xC5 = DynSetDynTable;
	0xEB = SetBankAndInstrument;
	0xC1 = SetInstrument;
	0xC3 = LargetNotesOff;
	0xC4 = LargetNotesOn;
	0xDF = SetVolume;
	0xE0 = SetVolumeScale;
	0xDE = SetFrequencyScale;
	0xD3 = PitchBend;
	0xDD = SetPan;
	0xDC = SetPanMix;
	0xDB = Transpose;
	0xDA = SetEnvelope;
	0xD9 = SetDecayRelease;
	0xD8 = SetVibratoExtent;
	0xD7 = SetVibratoRate;
	0xE2 = SetVibratoExtentLinear;
	0xE1 = SetVibratoRateLinear;
	0xE3 = SetVibratoDelay;
	0xD4 = SetReverb;
	0xC6 = SetBank;
	0xC7 = WriteSeq;
	0xC8 = Subtract;
	0xC9 = BitAnd;
	0xCC = LoadValue;
	0xCA = SetMuteBehavior;
	0xCB = ReadSeq;
	0xD0 = StereoHeadsetEffects;
	0xD1 = NoteAllocPolicy;
	0xD2 = SetSustain;
	0xE5 = SetReverbIndex;
	0xE4 = DynamicCall;
	0xE6 = BookOffset;
	0xE7 = InitFromSeqData;
	0xE8 = InitInline;
	0xEC = ResetVibratoFreqScale;
	0xE9 = SetNotePriority;

	Packed(
		M64ChannelCommand
		mask 0xF0
		test(> 0xC0)
	) {
		0x00 = TestLayerFinished;
		0x70 = IoWrite;
		0x80 = IoRead;
		0x50 = IoReadSubtract;
		0x60 = DelayShort;
		0x90 = SetLayer;
		0xA0 = FreeLayer;
		0xB0 = DynSetLayer;
		0x10 = StartChannel;
		0x20 = DisableChannel;
		0x30 = IoWriteChannel;
		0x40 = IoReadChannel;
	}
}

//version_opcode_def_ext!
m64_opcode_def_ext! {
	M64ChannelCommand:
	Versions {
		jp: decode_jp_channel_op,
		us: decode_us_channel_op,
		eu: decode_eu_channel_op,
		sh: decode_sh_channel_op,
	}

	@(jp, us, eu) {
		Test(> 0xC0)
	} @(sh) {
		Test(>= 0xB0)
	} Standalone(M64ChannelCommandStandalone) {
		@(jp, us) {
			0xF3 = Hang;
		}
		@(eu, sh) {
			0xEA = Hang;
		}
		0xFF = End;
		0xFE = Delay1;
		0xFD = Delay;

		0xFC = Call;
		0xF8 = LoopStart;
		0xF7 = LoopEnd;
		0xF6 = LoopBreak;
		0xFB = Jump(JumpCond::Uncond);
		0xFA = Jump(JumpCond::IfZero);
		0xF9 = Jump(JumpCond::IfNegative);
		0xF5 = Jump(JumpCond::IfNonNegative);
		@(eu, sh) {
			0xF4 = JumpRelative(JumpCond::Uncond);
			0xF3 = JumpRelative(JumpCond::IfZero);
			0xF2 = JumpRelative(JumpCond::IfNegative);

			0xF1 = ReserveNotes;
			0xF0 = UnReserveNotes;
		}
		@(jp, us) {
			0xF2 = ReserveNotes;
			0xF1 = UnReserveNotes;
		}

		0xC2 = SetDynTable;
		0xC5 = DynSetDynTable;
		@(eu, sh) {
			0xEB = SetBankAndInstrument;
		}
		0xC1 = SetInstrument;
		0xC3 = LargetNotesOff;
		0xC4 = LargetNotesOn;

		// These set update flags on EU/SH
		0xDF = SetVolume;
		0xE0 = SetVolumeScale;
		0xDE = SetFrequencyScale;
		// The LUT is structured slightly differently on EU/SH, but values have the same meaning
		0xD3 = PitchBend;

		@(sh) {
			0xEE = SetFreqScaleFromLutSh;
		}

		// Similarly, sets update flags on EU/SH, but pan is stored differently
		0xDD = SetPan;
		0xDC = SetPanMix;

		0xDB = Transpose;
		0xDA = SetEnvelope;
		0xD9 = SetDecayRelease;
		0xD8 = SetVibratoExtent;
		0xD7 = SetVibratoRate;
		0xE2 = SetVibratoExtentLinear;
		0xE1 = SetVibratoRateLinear;
		0xE3 = SetVibratoDelay;

		@(jp, us) {
			0xD6 = SetUpdatesPerFrameUnused;
		}

		0xD4 = SetReverb;
		0xC6 = SetBank;
		0xC7 = WriteSeq;
		0xC8 = Subtract;
		0xC9 = BitAnd;
		0xCC = LoadValue;

		@(sh) {
			0xCD = DisableChannel;
		}

		0xCA = SetMuteBehavior;
		0xCB = ReadSeq;
		@(sh) {
			0xCE = SetDynPtr;
			0xCF = StoreSeqDyn;
		}

		0xD0 = StereoHeadsetEffects;
		0xD1 = SetNoteAllocPolicy;
		0xD2 = SetSustain;
		@(eu, sh) {
			0xE5 = SetReverbIndex;
		}
		0xE4 = DynamicCall;
		@(eu, sh) {
			0xE6 = BookOffset;
			0xE7 = InitFromSeqData;
			0xE8 = InitInline;
			0xEC = ResetVibratoFreqScale;
		}
		@(eu) {
			0xE9 = SetNotePriority;
		}
		@(sh) {
			// Contains additional higher-bit logic
			0xE9 = SetNotePrioritySh;

			0xED = SetSynthesisVolume;
			0xEF = UnknownShI2U1;
			0xB0 = SetFilter;
			0xB1 = DisableFilter;
			0xB2 = SetDynPtrSeq;
			0xB3 = FillFilter;
			0xB4 = SetDynTableDynPtr;
			0xB5 = LoadDynPtrFromDynTable;
			0xB6 = IndexDynAcc;
		}
	}

	@(sh) {
		Test(>= 0x80) Packed(mask 0xF8) {
			0x80 = TestLayerFinished;
			0x88 = SetLayer;
			0x90 = FreeLayer;
			0x98 = SetLayerDyn;
		}
	}

	Packed(mask 0xF0) {
		@(sh) {
			0x00 = DelayShort;
			0x10 = LoadBankIoSh;
		}
		@(jp, us, eu) {
			// On EU/SH, sets -1 if layer is not set
			0x00 = TestLayerFinished;
		}

		0x70 = IoWrite;
		@(sh) { 0x60 = IoRead; }
		@(jp, us, eu) { 0x80 = IoRead; }
		0x50 = IoReadSubtract;

		@(eu) {
			0x60 = DelayShort;
		}
		@(jp, us, eu) {
			0x90 = SetLayer;
			0xA0 = FreeLayer;
			0xB0 = DynSetLayer;
		}

		@(jp, us) {
			0x60 = SetNotePriorityShort;
		}

		@(jp, us, eu) {
			0x10 = StartChannel;
			0x20 = DisableChannelShort;
		}
		@(sh) {
			0x20 = StartChannel;
		}

		0x30 = IoWriteChannel;
		0x40 = IoReadChannel;
	}
}

impl SequenceChannel {
	pub(super) unsafe fn process_script(&mut self) {
		use SequenceChannelFlags as F;
		if !self.flags.contains(F::Enabled) {
			return;
		}

		if self.flags.contains(F::StopScript) {
			for layer in self.layers {
				if let Some(layer) = layer.as_mut() {
					layer.process_script();
				}
			}
			return;
		}

		let seqPlayer = unsafe { &mut *self.seqPlayer };
		if seqPlayer.flags.contains(SequencePlayerFlags::Muted) && self.muteBehavior.contains(MuteBehaviorFlags::StopScript) {
			return;
		}

		if self.delay != 0 {
			self.delay -= 1;
		}

		let state = &mut *ptr::addr_of_mut!(self.scriptState);
		let mut accumulator = 0i8;
		if self.delay == 0 {
			loop {
				let cmd = state.read_u8();
				let cmd = M64_GAME_VERSION.decode_channel_op(cmd);
				use M64ChannelCommand::{*, Standalone as S};
				use M64ChannelCommandStandalone::*;

				match cmd {
					S(End) => {
						// End
						if state.depth == 0 {
							self.disable();
							break;
						} else {
							state.pc = state.stack[{ state.depth -= 1 ; state.depth as usize }];
						}
					}
					S(Delay1) => {
						// Delay1
						break;
					}
					S(Delay) => {
						// Delay
						self.delay = state.read_compressed_u16();
						break;
					}
					S(Hang) => {
						// Stop
						self.flags |= F::StopScript;
						break;
					}
					S(Call) => {
						// Call
						debug_assert!(state.depth < 4, "Stack overflow!");
						let offset = state.read_u16();
						state.stack[state.depth as usize] = state.pc;
						state.depth += 1;
						state.pc = seqPlayer.seqData.add(offset as usize);
					}
					S(LoopStart) => {
						// Loop start, N iterations (256 if N = 0)
						debug_assert!(state.depth < 4, "Stack overflow!");
						state.remLoopIters[state.depth as usize] = state.read_u8();
						state.stack[state.depth as usize] = state.pc;
						state.depth += 1;
					}
					S(LoopEnd) => {
						// Loop end
						if { let p = &mut state.remLoopIters[state.depth as usize - 1]; *p -= 1; *p } != 0 {
							state.pc = state.stack[state.depth as usize - 1];
						} else {
							state.depth -= 1;
						}
					}
					S(LoopBreak) => {
						// Break loop, if combined with jump
						state.depth -= 1;
					}
					S(Jump(jmp)) => {
						use std::cmp::Ordering;
						let offset = state.read_u16();
						let ord = accumulator.cmp(&0);
						let cond = match jmp {
							JumpCond::IfZero => matches!(ord, Ordering::Equal),
							JumpCond::IfNegative => matches!(ord, Ordering::Less),
							JumpCond::IfNonNegative => matches!(ord, Ordering::Equal | Ordering::Greater),
							JumpCond::Uncond => true,
							//_ => unreachable!(),
						};
						if cond {
							state.pc = seqPlayer.seqData.add(offset as usize);
						}
					}
					S(JumpRelative(jmp)) => {
						use std::cmp::Ordering;
						let offset = state.read_i8();
						let ord = accumulator.cmp(&0);
						let cond = match jmp {
							JumpCond::IfZero => matches!(ord, Ordering::Equal),
							JumpCond::IfNegative => matches!(ord, Ordering::Less),
							JumpCond::Uncond => true,
							_ => unreachable!(),
						};
						if cond {
							state.pc = state.pc.offset(offset as isize);
						}
					}

					S(ReserveNotes) => {
						// Reserve notes
						self.notePool.clear();
						self.notePool.fill(state.read_u8() as u32);
					}
					S(UnReserveNotes) => {
						// UnReserve notes
						self.notePool.clear();
					}

					S(SetDynTable) => {
						// Set dyntable
						let offset = state.read_u16();
						self.dynTable = seqPlayer.seqData.add(offset as usize).cast();
					}
					S(DynSetDynTable) => {
						// Dyn set dyntable
						if accumulator != -1 {
							let data = self.dynTable.offset(accumulator as isize).read();
							let data = u16::from_be_bytes(data);
							self.dynTable = seqPlayer.seqData.add(data as usize).cast();
						}
					}

					S(SetBankAndInstrument) => {
						// Set bank and instrument
						let bank = state.read_u8();
						// Switch to the (0-indexed) bank in this sequence's
						// bank set. Note that in the binary format (not in the JSON!)
						// the banks are listed backwards, so we count from the back.
						// (gAlBankSets[offset] is number of banks).
						let set = gAlBankSets.cast::<u16>().add(seqPlayer.seqId as usize).read() as usize;
						let off = gAlBankSets.add(set).read() as usize;
						let id = gAlBankSets.add(off+set - bank as usize).read();
						if !get_bank_or_seq(&mut gBankLoadedPool, 2, id as i32).is_null() {
							self.bankId = id;
						} else {
							eprintln!("Error: bank {id} not cached");
						}

						// Simulating fallthrough for 1 call
						self.set_instrument(state.read_u8());
					}
					S(SetInstrument) => {
						// Set instrument/program
						self.set_instrument(state.read_u8());
					}
					S(LargetNotesOff) => {
						// Large notes off
						self.flags -= F::LargeNotes;
					}
					S(LargetNotesOn) => {
						// Large notes on
						self.flags |= F::LargeNotes;
					}
					S(SetVolume) => {
						// Set volume
						self.set_volume(state.read_u8());
						self.changes |= SequenceChannelChanges::Volume;
					}
					S(SetVolumeScale) => {
						// Set volume scale
						self.volumeScale = state.read_u8() as f32 / 128.0;
						self.changes |= SequenceChannelChanges::Volume;
					}
					S(SetFrequencyScale) => {
						// Frequency scale
						// Pitch bend using raw frequency multiplier N/2**15 (N is u16)
						let scale = state.read_u16();
						self.freqScale = scale as f32 / 32768.0;
						self.changes |= SequenceChannelChanges::FreqScale;
					}
					S(PitchBend) => {
						// Pitch bend by ..1 octave in either direction (-127..=127)
						self.freqScale = gPitchBendFrequencyScale[(state.read_i8() as isize + 127) as usize];
						self.changes |= SequenceChannelChanges::FreqScale;
					}
					S(SetPan) => {
						// Set pan
						self.newPan = state.read_u8();
						self.changes |= SequenceChannelChanges::Pan;
					}
					S(SetPanMix) => {
						// Set pan mix, i.e. proportion of pan to come from channel (0..128)
						self.panChannelWeight = state.read_u8();
						self.changes |= SequenceChannelChanges::Pan;
					}
					S(Transpose) => {
						// Transpose
						self.transposition = state.read_i8() as i16;
					}
					S(SetEnvelope) => {
						// Set envelope
						let offset = state.read_u16() as usize;
						self.adsr.envelope = seqPlayer.seqData.add(offset).cast();
					}
					S(SetDecayRelease) => {
						// Set decay/release
						self.adsr.releaseRate = state.read_u8();
					}
					S(SetVibratoExtent) => {
						// Set vibrato extent
						self.vibratoExtentTarget = state.read_u8() as u16 * 8;
						self.vibratoExtentStart = 0;
						self.vibratoExtentChangeDelay = 0;
					}
					S(SetVibratoRate) => {
						// Set vibrato rate
						self.vibratoRateTarget = state.read_u8() as u16 * 32;
						self.vibratoRateStart = self.vibratoRateTarget;
					}
					S(SetVibratoExtentLinear) => {
						// Set vibrato extent linear
						self.vibratoExtentStart = state.read_u8() as u16 * 8;
						self.vibratoExtentTarget = state.read_u8() as u16 * 8;
						self.vibratoExtentChangeDelay = state.read_u8() as u16 * 16;
					}
					S(SetVibratoRateLinear) => {
						// Set vibrato rate linear
						self.vibratoRateStart = state.read_u8() as u16 * 32;
						self.vibratoRateTarget = state.read_u8() as u16 * 32;
						self.vibratoRateChangeDelay = state.read_u8() as u16 * 16;
					}
					S(SetVibratoDelay) => {
						// Set vibrato delay
						self.vibratoDelay = state.read_u8() as u16 * 16;
					}
					S(SetReverb) => {
						// Set reverb
						self.reverbVol = state.read_u8();
					}

					S(SetBank) => {
						// Set bank, change bank within set
						// This is exactly the same as "Set bank and instrument" (0xeb),
						// except that instrument is not also set.
						let bank = state.read_u8();
						let set = gAlBankSets.cast::<u16>().add(seqPlayer.seqId as usize).read() as usize;
						let off = gAlBankSets.add(set).read() as usize;
						let id = gAlBankSets.add(off+set - bank as usize).read();
						if !get_bank_or_seq(&mut gBankLoadedPool, 2, id as i32).is_null() {
							self.bankId = id;
						} else {
							eprintln!("Error: bank {id} not cached");
						}
					}

					S(WriteSeq) => {
						// Write to sequence data
						let diff = state.read_u8();
						let addr = state.read_u16() as usize;
						seqPlayer.seqData.add(addr).write((accumulator as u8).wrapping_add(diff));
					}

					| S(Subtract)
					| S(BitAnd)
					| S(LoadValue)
					=> {
						let op = state.read_i8();
						accumulator = match cmd {
							S(Subtract) => accumulator.wrapping_sub(op),
							S(LoadValue) => op,
							// S(BitAnd)
							_ => accumulator & op,
						};
					}

					S(SetMuteBehavior) => {
						// Set mute behavior
						self.muteBehavior = MuteBehaviorFlags::from_bits_retain(state.read_u8());
					}

					S(ReadSeq) => {
						// Read sequence data
						let addr = state.read_u16() as isize + accumulator as isize;
						accumulator = seqPlayer.seqData.offset(addr).read() as i8;
					}

					S(StereoHeadsetEffects) => {
						// Stereo headset effects
						self.flags.set(F::StereoHeadsetEffects, state.read_u8() != 0);
					}
					S(SetNoteAllocPolicy) => {
						// Set note allocation policy
						self.noteAllocPolicy = NoteAllocPolicy::from_bits_retain(state.read_u8());
					}
					S(SetSustain) => {
						// Set sustain
						self.adsr.sustain = state.read_u8();
					}
					S(SetReverbIndex) => {
						// Set reverb index
						self.reverbIndex = state.read_u8();
					}

					S(DynamicCall) => {
						// Dynamic call!
						if accumulator != -1 {
							debug_assert!(state.depth < 4, "Stack overflow!");
							let data = self.dynTable.offset(accumulator as isize).read();
							let data = u16::from_be_bytes(data);
							state.stack[state.depth as usize] = state.pc;
							state.depth += 1;
							state.pc = seqPlayer.seqData.add(data as usize);

							debug_assert!((data as u32) < (*(*gSeqFileHeader).seqArray.as_ptr().add(seqPlayer.seqId as usize)).len,
								"Error: sub {self:p}, address {pc:p}: undefined SubTrack function {data:04x}", pc= state.pc);
						}
					}

					S(BookOffset) => {
						// Set book offset
						self.bookOffset = state.read_u8();
					}

					// Initialization of some sort?
					S(InitFromSeqData) => {
						// Initialize from address
						let mut data: *const u8 = seqPlayer.seqData.add(state.read_u16() as usize);
						macro_rules! read {
							($($type:ty)*) => {{
								let v = *data $(as $type)*;
								data = data.add(1);
								v
							}};
						}
						self.muteBehavior = MuteBehaviorFlags::from_bits_retain(read!());
						self.noteAllocPolicy = NoteAllocPolicy::from_bits_retain(read!());
						self.notePriority = NotePriority::from_u8(read!());
						self.transposition = read!(i8 i16);
						self.newPan = read!();
						self.panChannelWeight = read!();
						self.reverbVol = read!();
						self.reverbIndex = read!();
						let _ = data;
						self.changes |= SequenceChannelChanges::Pan;
					}

					S(InitInline) => {
						// Initialize from bytecode stream
						macro_rules! read {
							($($type:ty)*) => {
								state.read_u8() $(as $type)*
							};
						}
						self.muteBehavior = MuteBehaviorFlags::from_bits_retain(read!());
						self.noteAllocPolicy = NoteAllocPolicy::from_bits_retain(read!());
						self.notePriority = NotePriority::from_u8(read!());
						self.transposition = read!(i8 i16);
						self.newPan = read!();
						self.panChannelWeight = read!();
						self.reverbVol = read!();
						self.reverbIndex = read!();
						self.changes |= SequenceChannelChanges::Pan;
					}

					S(ResetVibratoFreqScale) => {
						// Reset vibrato/freqScale (except for vibrato delay)
						self.vibratoExtentTarget = 0;
						self.vibratoExtentStart = 0;
						self.vibratoExtentChangeDelay = 0;
						self.vibratoRateTarget = 0;
						self.vibratoRateStart = 0;
						self.vibratoRateChangeDelay = 0;
						self.freqScale = 1.0;
					}

					S(SetNotePriority) => {
						// Set note priority
						self.notePriority = NotePriority::from_u8(state.read_u8());
					}

					TestLayerFinished(layer) => {
						// Test layer finished
						accumulator = if let Some(layer) = self.layers[layer as usize].as_ref() {
							layer.flags.contains(SequenceChannelLayerFlags::Finished) as i8
						} else {
							-1
						};
					}
					IoWrite(port) => {
						// IO write value
						// Write data back to audio lib
						self.soundScriptIO[port as usize] = accumulator;
					}
					IoRead(port) => {
						// IO read value
						// Read data from audio lib
						accumulator = self.soundScriptIO[port as usize];
						if port < 4 {
							self.soundScriptIO[port as usize] = -1;
						}
					}
					IoReadSubtract(port) => {
						// IO read value subtract
						accumulator -= self.soundScriptIO[port as usize];
					}

					DelayShort(delay) => {
						// Delay short
						self.delay = delay as u16;
						break;
					}

					SetLayer(layer) => {
						// Set layer
						let offset = state.read_u16() as usize;
						if self.set_layer(layer as usize).is_ok() {
							(*self.layers[layer as usize]).scriptState.pc = seqPlayer.seqData.add(offset);
						}
					}
					FreeLayer(layer) => {
						// Free layer
						self.free_layer(layer as usize);
					}
					DynSetLayer(layer) => {
						// Dynamic set layer
						if accumulator != -1 && self.set_layer(layer as usize).is_ok() {
							let data = self.dynTable.offset(accumulator as isize).read();
							let data = u16::from_be_bytes(data);
							(*self.layers[layer as usize]).scriptState.pc = seqPlayer.seqData.add(data as usize);
						}
					}

					StartChannel(channel) => {
						// Start channel
						let offset = state.read_u16() as usize;
						seqPlayer.enable_channel(channel as usize, seqPlayer.seqData.add(offset).cast());
					}
					DisableChannelShort(channel) => {
						// Disable channel
						(*seqPlayer.channels[channel as usize]).disable();
					}

					IoWriteChannel(channel) => {
						// IO write value on channel
						let port = state.read_u8() as usize;
						(*seqPlayer.channels[channel as usize]).soundScriptIO[port] = accumulator;
					}

					IoReadChannel(channel) => {
						// IO read value from channel
						let port = state.read_u8() as usize;
						accumulator = (*seqPlayer.channels[channel as usize]).soundScriptIO[port];
					}

					SetNotePriorityShort(pri) => {
						// Set note priority (JP/US)
						// According to decomp comments, must be >= 2
						/*self.notePriority = match pri {
							2..=7 => NotePriority::Min,
							8..=15 => NotePriority::Default,
							_ => panic!("Short priority isn't in range ({pri})"),
						}*/
						assert!(pri >= 2, "Note priority must not be a special value");
						self.notePriority = NotePriority::from_u8(pri);
					}

					S(SetUpdatesPerFrameUnused) => panic!("Legacy unused command"),

					S(
						| SetFreqScaleFromLutSh | DisableChannel | SetDynPtr
						| StoreSeqDyn | SetNotePrioritySh | SetSynthesisVolume
						| UnknownShI2U1 | SetFilter | DisableFilter | SetDynPtrSeq
						| FillFilter | SetDynTableDynPtr | LoadDynPtrFromDynTable
						| IndexDynAcc
					)
					| SetLayerDyn(_)
					| LoadBankIoSh(_)
					=> panic!("This version doesn't support Shindou commands"),
				}
			}
		}

		for layer in self.layers {
			if let Some(layer) = layer.as_mut() {
				layer.process_script();
			}
		}
	}
}

pub enum M64PlayerCommandStandalone {
	End,
	Delay1,
	Delay,
	Call,
	Jump(JumpCond),
	JumpRelative(JumpCond),
	LoopStart,
	LoopEnd,
	ReserveNotes,
	UnReserveNotes,
	Transpose,
	TransposeRelative,
	SetTempo,
	AddTempo,
	SetVolume,
	//SetVolumeEU,
	ChangeVolume,
	ChangeStateEU,
	SetFadeVolumeScale,
	InitChannels,
	DisableChannels,
	SetMuteVolumeScale,
	Mute,
	SetMuteBehavior,
	SetShortNoteVelocityTable,
	SetShortNoteDurationTable,
	SetNoteAllocPolicy,
	LoadValue,
	BitAnd,
	Subtract,
	StoreSeqAddSh,
	SetUnknownSh,
}

pub enum M64PlayerCommand {
	Standalone(M64PlayerCommandStandalone),
	TestChannelDisabled(u8),
	Nop(u8),
	SubtractVariation(u8),
	SetVariation(u8),
	GetVariation(u8),
	StartChannel(u8),
}

m64_opcode_def_ext! {
	M64PlayerCommand:
	Versions {
		jp: decode_jp_player_op,
		us: decode_us_player_op,
		eu: decode_eu_player_op,
		sh: decode_sh_player_op,
	}

	Test(>= 0xC0) Standalone(M64PlayerCommandStandalone) {
		0xFF = End;
		0xFD = Delay;
		0xFE = Delay1;
		0xFC = Call;
		0xF8 = LoopStart;
		0xF7 = LoopEnd;

		0xFB = Jump(JumpCond::Uncond);
		0xFA = Jump(JumpCond::IfZero);
		0xF9 = Jump(JumpCond::IfNegative);
		0xF5 = Jump(JumpCond::IfNonNegative);

		@(eu, sh) {
			0xF4 = JumpRelative(JumpCond::Uncond);
			0xF3 = JumpRelative(JumpCond::IfZero);
			0xF2 = JumpRelative(JumpCond::IfNegative);

			0xF1 = ReserveNotes;
			0xF0 = UnReserveNotes;
		}
		@(jp, us) {
			0xF2 = ReserveNotes;
			0xF1 = UnReserveNotes;
		}

		0xDF = Transpose;
		0xDE = TransposeRelative;

		0xDD = SetTempo;
		// SH handles the logic differently
		0xDC = AddTempo;

		@(jp, us) {
			0xDA = ChangeVolume;
			0xDB = SetVolume;
		}
		@(eu, sh) {
			0xDA = ChangeStateEU;
			0xDB = SetVolume;

			0xD9 = SetFadeVolumeScale;
		}

		0xD7 = InitChannels;
		0xD6 = DisableChannels;
		0xD5 = SetMuteVolumeScale;
		0xD4 = Mute;
		0xD3 = SetMuteBehavior;
		0xD2 = SetShortNoteVelocityTable;
		0xD1 = SetShortNoteDurationTable;
		0xD0 = SetNoteAllocPolicy;
		0xCC = LoadValue;
		0xC9 = BitAnd;
		0xC8 = Subtract;

		@(sh) {
			0xC7 = StoreSeqAddSh;
			0xC6 = SetUnknownSh;
		}
	}
	Packed(mask 0xF0) {
		// JP/US perform the test only if channel is valid (should be an assertion)
		0x00 = TestChannelDisabled;
		0x10 = Nop;
		0x20 = Nop;
		// --
		0x40 = Nop;
		0x50 = SubtractVariation;
		0x60 = Nop;
		0x70 = SetVariation;
		0x80 = GetVariation;
		0x90 = StartChannel;
		0xA0 = Nop;
		// JP/US have 2 additional impossible encodings for nop (lower bits set)
	}
}

impl SequencePlayer {
	pub(super) unsafe fn process_sequence(&mut self) {
		use SequencePlayerFlags as F;
		if !self.flags.contains(F::Enabled) {
			return;
		}

		if self.flags.contains(F::BankDmaInProgress) {
			if ultra::osRecvMesg(self.bankDmaMesgQueue.assume_init_ref(), ptr::null_mut(), ultra::Blocking::NoBlock).is_err() {
				return;
			}
			if self.bankDmaRemaining == 0 {
				self.flags -= F::BankDmaInProgress;
				let loadingBankId = self.loadingBankId as usize;
				patch_audio_bank(
					&mut *(*gCtlEntries.add(loadingBankId)).instruments.offset(-1).cast_thin(),
					(*(*gAlTbl).seqArray.as_ptr().add(loadingBankId)).offset.cast(),
					(*gCtlEntries.add(loadingBankId)).numInstruments as u32,
					(*gCtlEntries.add(loadingBankId)).numDrums as u32,
				);
				(*gCtlEntries.add(loadingBankId)).drums =
					(*((*gCtlEntries.add(loadingBankId)).instruments.offset(-1) as *const AudioBank)).drums;
				gBankLoadStatus[loadingBankId] = SoundLoadStatus::Complete;
			} else {
				audio_dma_partial_copy_async(
					&mut self.bankDmaCurrDevAddr,
					&mut self.bankDmaCurrMemAddr,
					&mut self.bankDmaRemaining,
					self.bankDmaMesgQueue.assume_init_ref(),
					&mut self.bankDmaIoMesg,
				);
			}
			return;
		}

		if self.flags.contains(F::SeqDmaInProgress) {
			if ultra::osRecvMesg(self.seqDmaMesgQueue.assume_init_ref(), ptr::null_mut(), ultra::Blocking::NoBlock).is_err() {
				return;
			}

			self.flags -= F::SeqDmaInProgress;
			gSeqLoadStatus[self.seqId as usize] = SoundLoadStatus::Complete;
		}

		// If discarded, bail out.
		if !gSeqLoadStatus[self.seqId as usize].is_load_complete()
			|| !gBankLoadStatus[self.defaultBank as usize].is_load_complete()
		{
			eprintln!("Disappeared sequence on bank {}", self.seqId);
			self.disable();
			return;
		}

		// Remove possible SoundLoadStatus::Discardable marks.
		gSeqLoadStatus[self.seqId as usize] = SoundLoadStatus::Complete;
		gBankLoadStatus[self.defaultBank as usize] = SoundLoadStatus::Complete;

		if self.flags.contains(F::Muted) && self.muteBehavior.contains(MuteBehaviorFlags::StopScript) {
			return;
		}

		// Check if we surpass the number of ticks needed for a tatum, else stop.
		self.tempoAcc += self.tempo;
		if self.tempoAcc < gTempoInternalToExternal as u16 {
			return;
		}
		self.tempoAcc -= gTempoInternalToExternal as u16;

		let state = &mut *ptr::addr_of_mut!(self.scriptState);
		let mut accumulator = 0i32;
		if self.delay > 1 {
			self.delay -= 1;
		} else {
			self.flags |= F::RecalculateVolume;
			loop {
				let cmd = state.read_u8();
				let cmd = M64_GAME_VERSION.decode_player_op(cmd);
				use M64PlayerCommand::{*, Standalone as S};
				use M64PlayerCommandStandalone::*;

				match cmd {
					S(End) => {
						// End
						if state.depth == 0 {
							self.disable();
							break;
						}
						state.pc = state.stack[{ state.depth -= 1; state.depth as usize }];
					}
					S(Delay) => {
						// Delay
						self.delay = state.read_compressed_u16();
						break;
					}
					S(Delay1) => {
						// Delay = 1
						self.delay = 1;
						break;
					}

					S(Call) => {
						// Call
						debug_assert!(state.depth < 4, "Stack overflow!");
						let offset = state.read_u16();
						state.stack[state.depth as usize] = state.pc;
						state.depth += 1;
						state.pc = self.seqData.add(offset as usize);
					}
					S(LoopStart) => {
						// Loop start, N iterations (256 if N = 0)
						debug_assert!(state.depth < 4, "Stack overflow!");
						state.remLoopIters[state.depth as usize] = state.read_u8();
						state.stack[state.depth as usize] = state.pc;
						state.depth += 1;
					}
					S(LoopEnd) => {
						// Loop end
						if { let p = &mut state.remLoopIters[state.depth as usize - 1]; *p -= 1; *p } != 0 {
							state.pc = state.stack[state.depth as usize - 1];
						} else {
							state.depth -= 1;
						}
					}

					S(Jump(jmp)) => {
						use std::cmp::Ordering;
						let offset = state.read_u16();
						let ord = accumulator.cmp(&0);
						let cond = match jmp {
							JumpCond::IfZero => matches!(ord, Ordering::Equal),
							JumpCond::IfNegative => matches!(ord, Ordering::Less),
							JumpCond::IfNonNegative => matches!(ord, Ordering::Equal | Ordering::Greater),
							JumpCond::Uncond => true,
							//_ => unreachable!(),
						};
						if cond {
							state.pc = self.seqData.add(offset as usize);
						}
					}
					S(JumpRelative(jmp)) => {
						use std::cmp::Ordering;
						let offset = state.read_i8();
						let ord = accumulator.cmp(&0);
						let cond = match jmp {
							JumpCond::IfZero => matches!(ord, Ordering::Equal),
							JumpCond::IfNegative => matches!(ord, Ordering::Less),
							JumpCond::Uncond => true,
							_ => unreachable!(),
						};
						if cond {
							state.pc = state.pc.offset(offset as isize);
						}
					}

					S(ReserveNotes) => {
						// Reserve notes
						self.notePool.clear();
						self.notePool.fill(state.read_u8() as u32);
					}
					S(UnReserveNotes) => {
						// UnReserve notes
						self.notePool.clear();
					}

					S(Transpose) => {
						// Transpose
						// (originally fallthrough to next case)
						self.transposition = state.read_u8() as i8 as i16;
					}
					S(TransposeRelative) => {
						// Transpose relative
						self.transposition += state.read_u8() as i8 as i16;
					}

					| S(SetTempo)
					| S(AddTempo) => {
						let tempo = state.read_u8() as u8 as i16 * TEMPO_SCALE;
						if let S(SetTempo) = cmd {
							self.tempo = tempo as u16;
						} else {
							self.tempo = (self.tempo as i16 + tempo) as u16;
						}

						// These two operations interpret tempo with different signedness.
						// First one uses u16->i32 coercion (unsigned).
						// Second one explicitly casts u16 to i16.
						self.tempo = (self.tempo as i32).min(gTempoInternalToExternal as i32) as u16;
						self.tempo = (self.tempo as i16).max(1) as u16;
					}

					S(ChangeStateEU) => {
						// Change state (?)
						let newState = SequencePlayerStateEU::from_u8(state.read_u8());
						let frames = state.read_u16();
						match newState {
							| SequencePlayerStateEU::FadingNormal
							| SequencePlayerStateEU::FadingIn => {
								if self.state != SequencePlayerStateEU::FadeOut {
									self.fadeTimerUnkEu = frames;
									self.state = newState;
								}
							}
							SequencePlayerStateEU::FadeOut => {
								self.fadeRemainingFrames = frames;
								self.state = newState;
								self.fadeVelocity = (- self.fadeVolume) / frames as f32;
							}
						}
					}
					S(SetVolume) => {
						// Fade with volume (?)
						let targetVolume = state.read_u8() as f32 / 127.0;
						match self.state {
							| SequencePlayerStateEU::FadingNormal
							| SequencePlayerStateEU::FadingIn => {
								if self.state == SequencePlayerStateEU::FadingIn {
									self.state = SequencePlayerStateEU::FadingNormal;
									self.fadeVolume = 0.0;
								}
								self.fadeRemainingFrames = self.fadeTimerUnkEu;
								if self.fadeRemainingFrames != 0 {
									self.fadeVelocity = (targetVolume - self.fadeVolume) / self.fadeRemainingFrames as f32;
								} else {
									self.fadeVolume = targetVolume;
								}
							}
							SequencePlayerStateEU::FadeOut => {}
						}
					}

					//S(SetVolume | ChangeVolume) => panic!("JP/US versions of set/change volume are not supported"),
					// Compat commands to support US sequences
					S(ChangeVolume) => {
						self.fadeVolume += state.read_i8() as f32 / 127.0;
					}
					/*S(SetVolume) => {
						// Fade with volume (?)
						let targetVolume = state.read_u8() as f32 / 127.0;
						match self.state as u8 {
							0 | 2 => {
								if self.state as u8 == 2 && self.fadeRemainingFrames != 0 {
									self.fadeVelocity = (targetVolume - self.fadeVolume) / self.fadeRemainingFrames as f32;
								} else {
									self.fadeVolume = targetVolume;
								}
							}
							1 | 4 => {
								self.volume = targetVolume;
							}
							_ => {}
						}
					}*/

					S(SetFadeVolumeScale) => {
						// Set fade volume scale
						self.fadeVolumeScale = state.read_i8() as f32 / 127.0;
					}

					S(InitChannels) => {
						// Init channels
						self.init_channels(state.read_u16());
					}
					S(DisableChannels) => {
						// Disable channels
						self.disable_channels(state.read_u16());
					}

					S(SetMuteVolumeScale) => {
						// Set mute scale
						self.muteVolumeScale = state.read_i8() as f32 / 127.0;
					}
					S(Mute) => {
						// Mute
						self.flags |= F::Muted;
					}
					S(SetMuteBehavior) => {
						// Set mute behavior
						self.muteBehavior = MuteBehaviorFlags::from_bits_retain(state.read_u8());
					}

					| S(SetShortNoteVelocityTable)
					| S(SetShortNoteDurationTable)
					=> {
						let offset = state.read_u16() as usize;
						let data = self.seqData.add(offset);
						if let S(SetShortNoteVelocityTable) = cmd {
							self.shortNoteVelocityTable = data;
						} else {
							self.shortNoteDurationTable = data;
						}
					}
					S(SetNoteAllocPolicy) => {
						// Set note allocation policy
						self.noteAllocPolicy = NoteAllocPolicy::from_bits_retain(state.read_u8());
					}

					S(LoadValue) => {
						// Set accumulator
						accumulator = state.read_u8() as i32;
					}
					S(BitAnd) => {
						// BitAnd
						accumulator &= state.read_u8() as i32;
					}
					S(Subtract) => {
						// Subtract
						accumulator -= state.read_u8() as i32;
					}

					TestChannelDisabled(channel) => {
						// Test channel disabled
						let channel = self.channels[channel as usize];
						assert!(channel != ptr::addr_of_mut!(gSequenceChannelNone), "Testing non-existing channel");
						accumulator = (*channel).flags.contains(SequenceChannelFlags::Finished) as i32;
					}
					Nop(_) => {}
					SubtractVariation(_) => {
						// Subtract variation
						accumulator -= self.seqVariationEu as i32;
					}
					SetVariation(_) => {
						// Set variation
						self.seqVariationEu = accumulator as i8;
					}
					GetVariation(_) => {
						// Get variation
						accumulator = self.seqVariationEu as i32;
					}

					StartChannel(channel) => {
						// Start channel
						let offset = state.read_u16() as usize;
						self.enable_channel(channel as usize, self.seqData.add(offset).cast());
					}

					S(StoreSeqAddSh | SetUnknownSh) => panic!("This version doesn't support Shindou commands"),
				}
			}
		}

		for chan in self.channels {
			if chan != ptr::addr_of_mut!(gSequenceChannelNone) {
				(*chan).process_script();
			}
		}
	}
}
