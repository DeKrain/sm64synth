use super::*;

impl SequenceChannelLayer {
	pub(super) unsafe fn process_script(&mut self) {
		use SequenceChannelLayerFlags as F;
		if !self.flags.contains(F::Enabled) {
			return;
		}

		if self.delay > 1 {
			self.delay -= 1;
			if !self.flags.contains(F::StopSomething) && self.delay <= self.duration {
				self.note_decay();
				self.flags |= F::StopSomething;
			}
			return;
		}

		if !self.flags.contains(F::ContinuousNotes) {
			self.note_decay();
		}

		if matches!(self.portamento.mode_split().1, PORTAMENTO_MODE_ONE_SHOT_TARGET | PORTAMENTO_MODE_ONE_SHOT_NOTE) {
			self.portamento.mode = 0;
		}

		let seqChannel = unsafe { &mut *self.seqChannel };
		let seqPlayer = unsafe { &mut *seqChannel.seqPlayer };
		self.flags |= F::NotePropertiesNeedInit;

		let state = &mut *ptr::addr_of_mut!(self.scriptState);
		let cmd = loop {
			let cmd = state.read_u8();

			match cmd {
				..=0xc0 => break cmd,
				0xff => {
					// Layer end
					// Return or end of script
					if state.depth == 0 { 
						// N.B. this function call is *not* inlined even though it's
						// within the same file, unlike in the rest of this function.
						self.disable();
						return;
					}
					state.depth -= 1;
					state.pc = state.stack[state.depth as usize];
				}
				0xfc => {
					// Call
					debug_assert!(state.depth < 4, "Stack overflow!");
					let offset = state.read_u16();
					state.stack[state.depth as usize] = state.pc;
					state.depth += 1;
					state.pc = seqPlayer.seqData.add(offset as usize);
				}
				0xf8 => {
					// Loop start, N iterations (256 if N = 0)
					debug_assert!(state.depth < 4, "Stack overflow!");
					state.remLoopIters[state.depth as usize] = state.read_u8();
					state.stack[state.depth as usize] = state.pc;
					state.depth += 1;
				}
				0xf7 => {
					// Loop end
					if { let p = &mut state.remLoopIters[state.depth as usize - 1]; *p -= 1; *p } != 0 {
						state.pc = state.stack[state.depth as usize - 1];
					} else {
						state.depth -= 1;
					}
				}
				0xfb => {
					// Jump
					let offset = state.read_u16();
					state.pc = seqPlayer.seqData.add(offset as usize);
				}
				0xf4 => {
					// Relative jump
					state.pc = state.pc.offset(state.read_i8() as isize);
				}
				0xc1 => {
					// Set short note velocity
					let vel = state.read_u8() as f32;
					self.velocitySquare = vel * vel;
				}
				0xca => {
					// Set pan
					self.pan = state.read_u8();
				}
				0xc2 => {
					// Transpose
					self.transposition = state.read_u8() as i16;
				}
				0xc9 => {
					// Set short note duration
					self.noteDuration = state.read_u8();
				}
				0xc4 => {
					// Continuous notes ON
					self.flags |= F::ContinuousNotes;
					self.note_decay();
				}
				0xc5 => {
					// Continuous notes OFF
					self.flags -= F::ContinuousNotes;
					self.note_decay();
				}
				0xc3 => {
					// Set short note default play percentage
					self.shortNoteDefaultPlayPercentage = state.read_compressed_u16() as i16;
				}
				0xc6 => {
					// Set instrument
					let cmd = state.read_u8();
					if cmd >= 0x7F {
						if cmd == 0x7F {
							self.instrOrWave = 0;
						} else {
							self.instrOrWave = cmd;
							self.instrument = ptr::null_mut();
						}

						if cmd == 0xFF {
							self.adsr.releaseRate = 0;
						}
					} else {
						self.instrOrWave = seqChannel.get_instrument(cmd, &mut self.instrument, &mut self.adsr);
						if self.instrOrWave == 0 {
							eprintln!("Warning: cannot change instrument: {cmd}");
							self.instrOrWave = 0xFF;
						}
					}
				}
				0xc7 => {
					// Portamento
					self.portamento.mode = state.read_u8();

					let mut target = (state.read_u8() as i16
						+ seqChannel.transposition
						+ self.transposition
						+ seqPlayer.transposition) as u8;

					if target >= 0x80 {
						target = 0;
					}

					self.portamentoTargetNote = target;

					if self.portamento.mode_split().0 {
						self.portamentoTime = state.read_u8() as u16;
					} else {
						self.portamentoTime = state.read_compressed_u16();
					}
				}
				0xc8 => {
					// Disable portamento
					self.portamento.mode = 0;
				}
				0xcb => {
					// Set envelope
					let address = state.read_u16() as usize;
					self.adsr.envelope = seqPlayer.seqData.add(address).cast();
					self.adsr.releaseRate = state.read_u8();
				}
				0xcc => {
					// Ignore drum pan
					self.flags |= F::IgnoreDrumPan;
				}

				cmd @ 0xd0..=0xdf => {
					// Set short note velocity from table
					let vel = *seqPlayer.shortNoteVelocityTable.add(cmd as usize & 0xf) as f32;
					self.velocitySquare = vel * vel;
				}
				cmd @ 0xe0..=0xef => {
					// Set short note duration from table
					self.noteDuration = *seqPlayer.shortNoteDurationTable.add(cmd as usize & 0xf);
				}

				_ => {
					eprintln!("Error: undefined note command: {cmd:02X}");
				}
			}
		};

		let mut sameSound = true;
		if cmd == 0xc0 {
			// Delay
			self.delay = state.read_compressed_u16() as i16;
			self.flags |= F::StopSomething;
		} else {
			self.flags -= F::StopSomething;

			let delay;
			enum NoteCmd {
				Note0,
				Note1,
				Note2,
			}
			use NoteCmd::*;
			let note_cmd = match cmd & 0xc0 {
				0x00 => Note0,
				0x40 => Note1,
				0x80 => Note2,
				// 0xc0 was handled above and cmd <= 0xc0
				_ => unreachable!(),
			};
			if seqChannel.flags.contains(SequenceChannelFlags::LargeNotes) {
				let vel;
				match note_cmd {
					Note0 => {
						// Note0 (play percentage, velocity, duration)
						delay = state.read_compressed_u16();
						vel = state.read_u8() as f32;
						self.noteDuration = state.read_u8();
						self.playProcentage = delay as i16;
					}
					Note1 => {
						// Note1 (play percentage, velocity)
						delay = state.read_compressed_u16();
						vel = state.read_u8() as f32;
						self.noteDuration = 0;
						self.playProcentage = delay as i16;
					}
					Note2 => {
						// Note2 (velocity, duration, uses last play percentage)
						delay = self.playProcentage as u16;
						vel = state.read_u8() as f32;
						self.noteDuration = state.read_u8();
					}
				}

				self.velocitySquare = vel * vel;
			} else {
				match note_cmd {
					Note0 => {
						// Note0 (play percentage)
						delay = state.read_compressed_u16();
						self.playProcentage = delay as i16;
					}
					Note1 => {
						// Note1 (uses default play percentage)
						delay = self.shortNoteDefaultPlayPercentage as u16;
					}
					Note2 => {
						// Note2 (uses last play percentage)
						delay = self.playProcentage as u16;
					}
				}
			}
			// the remaining bits are used for the semitone
			let mut semi = cmd & 0x3f;

			self.delay = delay as i16;
			self.duration = ((self.noteDuration as u32 * delay as u32) >> 8) as i16;
			if (
				seqPlayer.flags.contains(SequencePlayerFlags::Muted) && seqChannel.muteBehavior.contains(MuteBehaviorFlags::StopNotes)
			) || seqChannel.flags.contains(SequenceChannelFlags::StopSomething2) {
				self.flags |= F::StopSomething;
			} else {
				let mut temp = self.instrOrWave as i32;
				if temp == 0xff {
					temp = seqChannel.instrOrWave as i32;
				}
				let temp = temp;
				if temp == 0 {
					// Drum
					semi = (
						semi as i16 +
						seqChannel.transposition +
						self.transposition) as u8;

					let drum = get_drum(seqChannel.bankId as i32, semi as i32);
					if drum.is_null() {
						self.flags |= F::StopSomething;
					} else {
						let drum = unsafe { &*drum };
						self.adsr.envelope = drum.envelope;
						self.adsr.releaseRate = drum.releaseRate;
						if !self.flags.contains(F::IgnoreDrumPan) {
							self.pan = drum.pan;
						}
						self.sound = &drum.sound;
						self.freqScale = drum.sound.tuning;
					}
				} else {
					// Instrument
					semi = (
						semi as i16 +
						seqPlayer.transposition +
						seqChannel.transposition +
						self.transposition
					) as u8;
					if semi >= 0x80 {
						self.flags |= F::StopSomething;
					} else {
						let instrument = if self.instrOrWave == 0xff {
							seqChannel.instrument
						} else {
							self.instrument
						};

						if self.portamento.mode != 0 {
							let max_semi = semi.max(self.portamentoTargetNote);

							let tuning = if !instrument.is_null() {
								let sound = (*instrument).get_audio_bank_sound(max_semi as i32);
								sameSound = self.sound == sound;
								self.sound = sound;
								sound.tuning
							} else {
								self.sound = ptr::null();
								1.0
							};

							let temp_f2 = gNoteFrequencies[semi as usize] * tuning;
							let temp_f12 = gNoteFrequencies[self.portamentoTargetNote as usize] * tuning;
							let (porta_special, porta_mode) = self.portamento.mode_split();
							let freqScale = match porta_mode {
								PORTAMENTO_MODE_ONE_SHOT_TARGET | PORTAMENTO_MODE_HOLD_TARGET | PORTAMENTO_MODE_HOLD_TARGET_SINGLE => {
									temp_f12
								}
								PORTAMENTO_MODE_ONE_SHOT_NOTE | PORTAMENTO_MODE_HOLD_NOTE | _ => {
									temp_f2
								}
							};
							self.portamento.extent = temp_f2 / freqScale - 1.0;
							self.portamento.speed = if porta_special {
								32512.0 * seqPlayer.tempo as f32 /
								(self.delay as f32 * gTempoInternalToExternal as f32 * self.portamentoTime as f32)
							} else {
								127.0 / self.portamentoTime as f32
							};
							self.portamento.cur = 0.0;
							self.freqScale = freqScale;
							if porta_mode == PORTAMENTO_MODE_HOLD_TARGET_SINGLE {
								self.portamentoTargetNote = semi;
							}
						} else if let Some(instrument) = instrument.as_ref() {
							let sound = instrument.get_audio_bank_sound(semi as i32);
							sameSound = self.sound == sound;
							self.sound = sound;
							self.freqScale = gNoteFrequencies[semi as usize] * sound.tuning;
						} else {
							self.sound = ptr::null();
							self.freqScale = gNoteFrequencies[semi as usize];
						}
					}
				}
			}
		}

		if self.flags.contains(F::StopSomething) {
			if !self.note.is_null() || self.flags.contains(F::ContinuousNotes) {
				self.note_decay();
			}
			return;
		}

		let allocNote =
			!self.flags.contains(F::ContinuousNotes) ||
			self.note.is_null() || self.status == SoundLoadStatus::NotLoaded
			|| if !sameSound {
				self.note_decay();
				true
			} else if (*self.note).playbackState.parentLayer != self {
				true
			} else {
				if self.sound.is_null() {
					init_synthetic_wave(&mut *self.note, self);
				}
				false
			};

		if allocNote {
			self.note = alloc_note(self);
		}

		if let Some(note) = self.note.as_mut() {
			if note.playbackState.parentLayer == self {
				note.vibrato_init();
			}
		}
	}
}

const M64_CHANNEL_END: u8 = 0xFF;
const M64_CHANNEL_DELAY_1: u8 = 0xFE;
const M64_CHANNEL_DELAY: u8 = 0xFD;
const M64_CHANNEL_HANG: u8 = 0xEA;
const M64_CHANNEL_CALL: u8 = 0xFC;
const M64_CHANNEL_LOOP_START: u8 = 0xF8;
const M64_CHANNEL_LOOP_END: u8 = 0xF7;
const M64_CHANNEL_LOOP_BREAK: u8 = 0xF6;
const M64_CHANNEL_JUMP: u8 = 0xFB;
const M64_CHANNEL_JUMP_IF_ZERO: u8 = 0xFA;
const M64_CHANNEL_JUMP_IF_NEGATIVE: u8 = 0xF9;
const M64_CHANNEL_JUMP_IF_NON_NEGATIVE: u8 = 0xF5;
const M64_CHANNEL_JUMP_RELATIVE: u8 = 0xF4;
const M64_CHANNEL_JUMP_RELATIVE_IF_ZERO: u8 = 0xF3;
const M64_CHANNEL_JUMP_RELATIVE_IF_NON_ZERO: u8 = 0xF2;
const M64_CHANNEL_RESERVE_NOTES: u8 = 0xF1;
const M64_CHANNEL_UNRESERVE_NOTES: u8 = 0xF0;
const M64_CHANNEL_SET_DYN_TABLE: u8 = 0xC2;
const M64_CHANNEL_DYN_SET_DYN_TABLE: u8 = 0xC5;
const M64_CHANNEL_SET_BANK_AND_INSTRUMENT: u8 = 0xEB;
const M64_CHANNEL_INSTRUMENT: u8 = 0xC1;
const M64_CHANNEL_LARGE_NOTES_OFF: u8 = 0xC3;
const M64_CHANNEL_LARGE_NOTES_ON: u8 = 0xC4;
const M64_CHANNEL_SET_VOLUME: u8 = 0xDF;
const M64_CHANNEL_SET_VOLUME_SCALE: u8 = 0xE0;
const M64_CHANNEL_SET_FREQUENCY_SCALE: u8 = 0xDE;
const M64_CHANNEL_PITCH_BEND: u8 = 0xD3;
const M64_CHANNEL_SET_PAN: u8 = 0xDD;
const M64_CHANNEL_SET_PAN_MIX: u8 = 0xDC;
const M64_CHANNEL_TRANSPOSE: u8 = 0xDB;
const M64_CHANNEL_SET_ENVELOPE: u8 = 0xDA;
const M64_CHANNEL_SET_DECAY_RELEASE: u8 = 0xD9;
const M64_CHANNEL_SET_VIBRATO_EXTENT: u8 = 0xD8;
const M64_CHANNEL_SET_VIBRATO_RATE: u8 = 0xD7;
const M64_CHANNEL_SET_VIBRATO_EXTENT_LINEAR: u8 = 0xE2;
const M64_CHANNEL_SET_VIBRATO_RATE_LINEAR: u8 = 0xE1;
const M64_CHANNEL_SET_VIBRATO_DELAY: u8 = 0xE3;
const M64_CHANNEL_SET_REVERB: u8 = 0xD4;
const M64_CHANNEL_SET_BANK: u8 = 0xC6;
const M64_CHANNEL_WRITE_SEQ: u8 = 0xC7;
const M64_CHANNEL_SUBTRACT: u8 = 0xC8;
const M64_CHANNEL_BIT_AND: u8 = 0xC9;
const M64_CHANNEL_LOAD_VALUE: u8 = 0xCC;
const M64_CHANNEL_SET_MUTE_BEHAVIOR: u8 = 0xCA;
const M64_CHANNEL_READ_SEQ: u8 = 0xCB;
const M64_CHANNEL_STEREO_HEADSET_EFFECTS: u8 = 0xD0;
const M64_CHANNEL_NOTE_ALLOC_POLICY: u8 = 0xD1;
const M64_CHANNEL_SET_SUSTAIN: u8 = 0xD2;
const M64_CHANNEL_SET_REVERB_INDEX: u8 = 0xE5;
const M64_CHANNEL_DYNAMIC_CALL: u8 = 0xE4;
const M64_CHANNEL_BOOK_OFFSET: u8 = 0xE6;
const M64_CHANNEL_INIT_SEQDATA: u8 = 0xE7;
const M64_CHANNEL_INIT_INLINE: u8 = 0xE8;
const M64_CHANNEL_RESET_VIBRATO_FREQ_SCALE: u8 = 0xEC;
const M64_CHANNEL_SET_NOTE_PRIORITY: u8 = 0xE9;

const M64_CHANNEL_TEST_LAYER_FINISHED_BASE: u8 = 0x00;
const M64_CHANNEL_IO_WRITE_BASE: u8 = 0x70;
const M64_CHANNEL_IO_READ_BASE: u8 = 0x80;
const M64_CHANNEL_IO_READ_SUBTRACT_BASE: u8 = 0x50;
const M64_CHANNEL_DELAY_SHORT_BASE: u8 = 0x60;
const M64_CHANNEL_SET_LAYER_BASE: u8 = 0x90;
const M64_CHANNEL_FREE_LAYER_BASE: u8 = 0xA0;
const M64_CHANNEL_DYN_SET_LAYER_BASE: u8 = 0xB0;
const M64_CHANNEL_START_CHANNEL_BASE: u8 = 0x10;
const M64_CHANNEL_DISABLE_CHANNEL_BASE: u8 = 0x20;
const M64_CHANNEL_IO_WRITE_CHANNEL_BASE: u8 = 0x30;
const M64_CHANNEL_IO_READ_CHANNEL_BASE: u8 = 0x40;

impl SequenceChannel {
	pub(super) unsafe fn process_script(&mut self) {
		use SequenceChannelFlags as F;
		if !self.flags.contains(F::Enabled) {
			return;
		}

		if self.flags.contains(F::StopScript) {
			for layer in self.layers {
				if let Some(layer) = layer.as_mut() {
					layer.process_script();
				}
			}
			return;
		}

		let seqPlayer = unsafe { &mut *self.seqPlayer };
		if seqPlayer.flags.contains(SequencePlayerFlags::Muted) && self.muteBehavior.contains(MuteBehaviorFlags::StopScript) {
			return;
		}

		if self.delay != 0 {
			self.delay -= 1;
		}

		let state = &mut *ptr::addr_of_mut!(self.scriptState);
		let mut accumulator = 0i8;
		if self.delay == 0 {
			loop {
				let cmd = state.read_u8();

				if cmd > 0xc0 {
					match cmd {
						M64_CHANNEL_END => {
							// End
							if state.depth == 0 {
								self.disable();
								break;
							} else {
								state.pc = state.stack[{ state.depth -= 1 ; state.depth as usize }];
							}
						}
						M64_CHANNEL_DELAY_1 => {
							// Delay1
							break;
						}
						M64_CHANNEL_DELAY => {
							// Delay
							self.delay = state.read_compressed_u16();
							break;
						}
						M64_CHANNEL_HANG => {
							// Stop
							self.flags |= F::StopScript;
							break;
						}
						M64_CHANNEL_CALL => {
							// Call
							debug_assert!(state.depth < 4, "Stack overflow!");
							let offset = state.read_u16();
							state.stack[state.depth as usize] = state.pc;
							state.depth += 1;
							state.pc = seqPlayer.seqData.add(offset as usize);
						}
						M64_CHANNEL_LOOP_START => {
							// Loop start, N iterations (256 if N = 0)
							debug_assert!(state.depth < 4, "Stack overflow!");
							state.remLoopIters[state.depth as usize] = state.read_u8();
							state.stack[state.depth as usize] = state.pc;
							state.depth += 1;
						}
						M64_CHANNEL_LOOP_END => {
							// Loop end
							if { let p = &mut state.remLoopIters[state.depth as usize - 1]; *p -= 1; *p } != 0 {
								state.pc = state.stack[state.depth as usize - 1];
							} else {
								state.depth -= 1;
							}
						}
						M64_CHANNEL_LOOP_BREAK => {
							// Break loop, if combined with jump
							state.depth -= 1;
						}
						| M64_CHANNEL_JUMP // Jump
						| M64_CHANNEL_JUMP_IF_ZERO // Jump if zero
						| M64_CHANNEL_JUMP_IF_NEGATIVE // Jump if negative
						| M64_CHANNEL_JUMP_IF_NON_NEGATIVE // Jump if non-negative
						=> {
							use std::cmp::Ordering;
							let offset = state.read_u16();
							match (cmd, accumulator.cmp(&0)) {
								| (M64_CHANNEL_JUMP_IF_ZERO, Ordering::Equal)
								| (M64_CHANNEL_JUMP_IF_NEGATIVE, Ordering::Less)
								| (M64_CHANNEL_JUMP_IF_NON_NEGATIVE, Ordering::Equal | Ordering::Greater)
								| (M64_CHANNEL_JUMP, _) => {
									state.pc = seqPlayer.seqData.add(offset as usize);
								}
								_ => {}
							}
						}
						| M64_CHANNEL_JUMP_RELATIVE // Jump relative
						| M64_CHANNEL_JUMP_RELATIVE_IF_ZERO // Jump relative if zero
						| M64_CHANNEL_JUMP_RELATIVE_IF_NON_ZERO // Jump relative if negative
						=> {
							use std::cmp::Ordering;
							let offset = state.read_i8();
							match (cmd, accumulator.cmp(&0)) {
								| (M64_CHANNEL_JUMP_RELATIVE_IF_ZERO, Ordering::Equal)
								| (M64_CHANNEL_JUMP_RELATIVE_IF_NON_ZERO, Ordering::Less)
								| (M64_CHANNEL_JUMP_RELATIVE, _) => {
									state.pc = state.pc.offset(offset as isize);
								}
								_ => {}
							}
						}

						M64_CHANNEL_RESERVE_NOTES => {
							// Reserve notes
							self.notePool.clear();
							self.notePool.fill(state.read_u8() as u32);
						}
						M64_CHANNEL_UNRESERVE_NOTES => {
							// UnReserve notes
							self.notePool.clear();
						}

						M64_CHANNEL_SET_DYN_TABLE => {
							// Set dyntable
							let offset = state.read_u16();
							self.dynTable = seqPlayer.seqData.add(offset as usize).cast();
						}
						M64_CHANNEL_DYN_SET_DYN_TABLE => {
							// Dyn set dyntable
							if accumulator != -1 {
								let data = self.dynTable.offset(accumulator as isize).read();
								let data = u16::from_be_bytes(data);
								self.dynTable = seqPlayer.seqData.add(data as usize).cast();
							}
						}

						M64_CHANNEL_SET_BANK_AND_INSTRUMENT => {
							// Set bank and instrument
							let bank = state.read_u8();
							// Switch to the (0-indexed) bank in this sequence's
							// bank set. Note that in the binary format (not in the JSON!)
							// the banks are listed backwards, so we counts from the back.
							// (gAlBankSets[offset] is number of banks).
							let set = gAlBankSets.cast::<u16>().add(seqPlayer.seqId as usize).read() as usize;
							let off = gAlBankSets.add(set).read() as usize;
							let id = gAlBankSets.add(off+set - bank as usize).read();
							if !get_bank_or_seq(&mut gBankLoadedPool, 2, id as i32).is_null() {
								self.bankId = id;
							} else {
								eprintln!("Error: bank {id} not cached");
							}

							// Simulating fallthrough for 1 call
							self.set_instrument(state.read_u8());
						}
						M64_CHANNEL_INSTRUMENT => {
							// Set instrument/program
							self.set_instrument(state.read_u8());
						}
						M64_CHANNEL_LARGE_NOTES_OFF => {
							// Large notes off
							self.flags -= F::LargeNotes;
						}
						M64_CHANNEL_LARGE_NOTES_ON => {
							// Large notes on
							self.flags |= F::LargeNotes;
						}
						M64_CHANNEL_SET_VOLUME => {
							// Set volume
							self.set_volume(state.read_u8());
							self.changes |= SequenceChannelChanges::Volume;
						}
						M64_CHANNEL_SET_VOLUME_SCALE => {
							// Set volume scale
							self.volumeScale = state.read_u8() as f32 / 128.0;
							self.changes |= SequenceChannelChanges::Volume;
						}
						M64_CHANNEL_SET_FREQUENCY_SCALE => {
							// Frequency scale
							// Pitch bend using raw frequency multiplier N/2**15 (N is u16)
							let scale = state.read_u16();
							self.freqScale = scale as f32 / 32768.0;
							self.changes |= SequenceChannelChanges::FreqScale;
						}
						M64_CHANNEL_PITCH_BEND => {
							// Pitch bend by ..1 octave in either direction (-127..=127)
							self.freqScale = gPitchBendFrequencyScale[(state.read_i8() as isize + 127) as usize];
							self.changes |= SequenceChannelChanges::FreqScale;
						}
						M64_CHANNEL_SET_PAN => {
							// Set pan
							self.newPan = state.read_u8();
							self.changes |= SequenceChannelChanges::Pan;
						}
						M64_CHANNEL_SET_PAN_MIX => {
							// Set pan mix, i.e. proportion of pan to come from channel (0..128)
							self.panChannelWeight = state.read_u8();
							self.changes |= SequenceChannelChanges::Pan;
						}
						M64_CHANNEL_TRANSPOSE => {
							// Transpose
							self.transposition = state.read_i8() as i16;
						}
						M64_CHANNEL_SET_ENVELOPE => {
							// Set envelope
							let offset = state.read_u16() as usize;
							self.adsr.envelope = seqPlayer.seqData.add(offset).cast();
						}
						M64_CHANNEL_SET_DECAY_RELEASE => {
							// Set decay/release
							self.adsr.releaseRate = state.read_u8();
						}
						M64_CHANNEL_SET_VIBRATO_EXTENT => {
							// Set vibrato extent
							self.vibratoExtentTarget = state.read_u8() as u16 * 8;
							self.vibratoExtentStart = 0;
							self.vibratoExtentChangeDelay = 0;
						}
						M64_CHANNEL_SET_VIBRATO_RATE => {
							// Set vibrato rate
							self.vibratoRateTarget = state.read_u8() as u16 * 32;
							self.vibratoRateStart = self.vibratoRateTarget;
						}
						M64_CHANNEL_SET_VIBRATO_EXTENT_LINEAR => {
							// Set vibrato extent linear
							self.vibratoExtentStart = state.read_u8() as u16 * 8;
							self.vibratoExtentTarget = state.read_u8() as u16 * 8;
							self.vibratoExtentChangeDelay = state.read_u8() as u16 * 16;
						}
						M64_CHANNEL_SET_VIBRATO_RATE_LINEAR => {
							// Set vibrato rate linear
							self.vibratoRateStart = state.read_u8() as u16 * 32;
							self.vibratoRateTarget = state.read_u8() as u16 * 32;
							self.vibratoRateChangeDelay = state.read_u8() as u16 * 16;
						}
						M64_CHANNEL_SET_VIBRATO_DELAY => {
							// Set vibrato delay
							self.vibratoDelay = state.read_u8() as u16 * 16;
						}
						M64_CHANNEL_SET_REVERB => {
							// Set reverb
							self.reverbVol = state.read_u8();
						}

						M64_CHANNEL_SET_BANK => {
							// Set bank, change bank within set
							// This is exactly the same as "Set bank and instrument" (0xeb),
							// except that instrument is not also set.
							let bank = state.read_u8();
							let set = gAlBankSets.cast::<u16>().add(seqPlayer.seqId as usize).read() as usize;
							let off = gAlBankSets.add(set).read() as usize;
							let id = gAlBankSets.add(off+set - bank as usize).read();
							if !get_bank_or_seq(&mut gBankLoadedPool, 2, id as i32).is_null() {
								self.bankId = id;
							} else {
								eprintln!("Error: bank {id} not cached");
							}
						}

						M64_CHANNEL_WRITE_SEQ => {
							// Write to sequence data
							let diff = state.read_u8();
							let addr = state.read_u16() as usize;
							seqPlayer.seqData.add(addr).write((accumulator as u8).wrapping_add(diff));
						}

						| M64_CHANNEL_SUBTRACT // Subtract
						| M64_CHANNEL_BIT_AND // BitAnd
						| M64_CHANNEL_LOAD_VALUE // Set value
						=> {
							let op = state.read_i8();
							accumulator = match cmd {
								M64_CHANNEL_SUBTRACT => accumulator.wrapping_sub(op),
								M64_CHANNEL_LOAD_VALUE => op,
								// M64_CHANNEL_BIT_AND
								_ => accumulator & op,
							};
						}

						M64_CHANNEL_SET_MUTE_BEHAVIOR => {
							// Set mute behavior
							self.muteBehavior = MuteBehaviorFlags::from_bits_retain(state.read_u8());
						}

						M64_CHANNEL_READ_SEQ => {
							// Read sequence data
							let addr = state.read_u16() as isize + accumulator as isize;
							accumulator = seqPlayer.seqData.offset(addr).read() as i8;
						}

						M64_CHANNEL_STEREO_HEADSET_EFFECTS => {
							// Stereo headset effects
							self.flags.set(F::StereoHeadsetEffects, state.read_u8() != 0);
						}
						M64_CHANNEL_NOTE_ALLOC_POLICY => {
							// Set note allocation policy
							self.noteAllocPolicy = NoteAllocPolicy::from_bits_retain(state.read_u8());
						}
						M64_CHANNEL_SET_SUSTAIN => {
							// Set sustain
							self.adsr.sustain = state.read_u8();
						}
						M64_CHANNEL_SET_REVERB_INDEX => {
							// Set reverb index
							self.reverbIndex = state.read_u8();
						}

						M64_CHANNEL_DYNAMIC_CALL => {
							// Dynamic call!
							if accumulator != -1 {
								debug_assert!(state.depth < 4, "Stack overflow!");
								let data = self.dynTable.offset(accumulator as isize).read();
								let data = u16::from_be_bytes(data);
								state.stack[state.depth as usize] = state.pc;
								state.depth += 1;
								state.pc = seqPlayer.seqData.add(data as usize);

								debug_assert!((data as i32) < (*(*gSeqFileHeader).seqArray.as_ptr().add(seqPlayer.seqId as usize)).len,
									"Error: sub {self:p}, address {pc:p}: undefined SubTrack function {data:04x}", pc= state.pc);
							}
						}

						M64_CHANNEL_BOOK_OFFSET => {
							// Set book offset
							self.bookOffset = state.read_u8();
						}

						// Initialization of some sort?
						M64_CHANNEL_INIT_SEQDATA => {
							// Initialize from address
							let mut data: *const u8 = seqPlayer.seqData.add(state.read_u16() as usize);
							macro_rules! read {
								($($type:ty)*) => {{
									let v = *data $(as $type)*;
									data = data.add(1);
									v
								}};
							}
							self.muteBehavior = MuteBehaviorFlags::from_bits_retain(read!());
							self.noteAllocPolicy = NoteAllocPolicy::from_bits_retain(read!());
							self.notePriority = NotePriority::from_u8(read!());
							self.transposition = read!(i8 i16);
							self.newPan = read!();
							self.panChannelWeight = read!();
							self.reverbVol = read!();
							self.reverbIndex = read!();
							let _ = data;
							self.changes |= SequenceChannelChanges::Pan;
						}

						M64_CHANNEL_INIT_INLINE => {
							// Initialize from bitcode stream
							macro_rules! read {
								($($type:ty)*) => {
									state.read_u8() $(as $type)*
								};
							}
							self.muteBehavior = MuteBehaviorFlags::from_bits_retain(read!());
							self.noteAllocPolicy = NoteAllocPolicy::from_bits_retain(read!());
							self.notePriority = NotePriority::from_u8(read!());
							self.transposition = read!(i8 i16);
							self.newPan = read!();
							self.panChannelWeight = read!();
							self.reverbVol = read!();
							self.reverbIndex = read!();
							self.changes |= SequenceChannelChanges::Pan;
						}

						M64_CHANNEL_RESET_VIBRATO_FREQ_SCALE => {
							// Reset vibrato/freqScale (except for vibrato delay)
							self.vibratoExtentTarget = 0;
							self.vibratoExtentStart = 0;
							self.vibratoExtentChangeDelay = 0;
							self.vibratoRateTarget = 0;
							self.vibratoRateStart = 0;
							self.vibratoRateChangeDelay = 0;
							self.freqScale = 1.0;
						}

						M64_CHANNEL_SET_NOTE_PRIORITY => {
							// Set note priority
							self.notePriority = NotePriority::from_u8(state.read_u8());
						}

						_ => unreachable!("Bad cmd"),
					}
				} else {
					let loBits = cmd as usize & 0xf;
					match cmd & 0xf0 {
						M64_CHANNEL_TEST_LAYER_FINISHED_BASE => {
							// Test layer finished
							accumulator = if let Some(layer) = self.layers[loBits].as_ref() {
								layer.flags.contains(SequenceChannelLayerFlags::Finished) as i8
							} else {
								-1
							};
						}
						M64_CHANNEL_IO_WRITE_BASE => {
							// IO write value
							// Write data back to audio lib
							self.soundScriptIO[loBits] = accumulator;
						}
						M64_CHANNEL_IO_READ_BASE => {
							// IO read value
							// Read data from audio lib
							accumulator = self.soundScriptIO[loBits];
							if loBits < 4 {
								self.soundScriptIO[loBits] = -1;
							}
						}
						M64_CHANNEL_IO_READ_SUBTRACT_BASE => {
							// IO read value subtract
							accumulator -= self.soundScriptIO[loBits];
						}

						M64_CHANNEL_DELAY_SHORT_BASE => {
							// Delay short
							self.delay = loBits as u16;
							break;
						}

						M64_CHANNEL_SET_LAYER_BASE => {
							// Set layer
							let offset = state.read_u16() as usize;
							if self.set_layer(loBits).is_ok() {
								(*self.layers[loBits]).scriptState.pc = seqPlayer.seqData.add(offset);
							}
						}
						M64_CHANNEL_FREE_LAYER_BASE => {
							// Free layer
							self.free_layer(loBits);
						}
						M64_CHANNEL_DYN_SET_LAYER_BASE => {
							// Dynamic set layer
							if accumulator != -1 && self.set_layer(loBits).is_ok() {
								let data = self.dynTable.offset(accumulator as isize).read();
								let data = u16::from_be_bytes(data);
								(*self.layers[loBits]).scriptState.pc = seqPlayer.seqData.add(data as usize);
							}
						}

						M64_CHANNEL_START_CHANNEL_BASE => {
							// Start channel
							let offset = state.read_u16() as usize;
							seqPlayer.enable_channel(loBits, seqPlayer.seqData.add(offset).cast());
						}
						M64_CHANNEL_DISABLE_CHANNEL_BASE => {
							// Disable channel
							(*seqPlayer.channels[loBits]).disable();
						}

						M64_CHANNEL_IO_WRITE_CHANNEL_BASE => {
							// IO write value on channel
							let port = state.read_u8() as usize;
							(*seqPlayer.channels[loBits]).soundScriptIO[port] = accumulator;
						}

						M64_CHANNEL_IO_READ_CHANNEL_BASE => {
							// IO read value from channel
							let port = state.read_u8() as usize;
							accumulator = (*seqPlayer.channels[loBits]).soundScriptIO[port];
						}

						_ => unreachable!("Bad cmd")
					}
				}
			}
		}

		for layer in self.layers {
			if let Some(layer) = layer.as_mut() {
				layer.process_script();
			}
		}
	}
}

const M64_PLAYER_END: u8 = 0xFF;
const M64_PLAYER_DELAY_1: u8 = 0xFE;
const M64_PLAYER_DELAY: u8 = 0xFD;
const M64_PLAYER_CALL: u8 = 0xFC;
const M64_PLAYER_JUMP: u8 = 0xFB;
const M64_PLAYER_JUMP_IF_ZERO: u8 = 0xFA;
const M64_PLAYER_JUMP_IF_NEGATIVE: u8 = 0xF9;
const M64_PLAYER_LOOP_START: u8 = 0xF8;
const M64_PLAYER_LOOP_END: u8 = 0xF7;
const M64_PLAYER_JUMP_IF_NON_NEGATIVE: u8 = 0xF5;
const M64_PLAYER_JUMP_RELATIVE: u8 = 0xF4;
const M64_PLAYER_JUMP_RELATIVE_IF_ZERO: u8 = 0xF3;
const M64_PLAYER_JUMP_RELATIVE_IF_NEGATIVE: u8 = 0xF2;
const M64_PLAYER_RESERVE_NOTES: u8 = 0xF1;
const M64_PLAYER_UNRESERVE_NOTES: u8 = 0xF0;
const M64_PLAYER_TRANSPOSE: u8 = 0xDF;
const M64_PLAYER_TRANSPOSE_RELATIVE: u8 = 0xDE;
const M64_PLAYER_SET_TEMPO: u8 = 0xDD;
const M64_PLAYER_ADD_TEMPO: u8 = 0xDC;
const M64_PLAYER_FADE: u8 = 0xDB;
const M64_PLAYER_CHANGE_STATE: u8 = 0xDA;
const M64_PLAYER_FADE_VOLUME_SCALE: u8 = 0xD9;
const M64_PLAYER_INIT_CHANNELS: u8 = 0xD7;
const M64_PLAYER_DISABLE_CHANNELS: u8 = 0xD6;
const M64_PLAYER_MUTE_VOLUME_SCALE: u8 = 0xD5;
const M64_PLAYER_MUTE: u8 = 0xD4;
const M64_PLAYER_MUTE_BEHAVIOR: u8 = 0xD3;
const M64_PLAYER_SHORT_NOTE_VEL_TAB: u8 = 0xD2;
const M64_PLAYER_SHORT_NOTE_DUR_TAB: u8 = 0xD1;
const M64_PLAYER_NOTE_ALLOC_POLICY: u8 = 0xD0;
const M64_PLAYER_LOAD_ACC: u8 = 0xCC;
const M64_PLAYER_BIT_AND: u8 = 0xC9;
const M64_PLAYER_SUBTRACT: u8 = 0xC8;

const M64_PLAYER_IS_CHANNEL_DISABLED_BASE: u8 = 0x00;
const M64_PLAYER_NOP_10_BASE: u8 = 0x10;
const M64_PLAYER_NOP_20_BASE: u8 = 0x20;
const M64_PLAYER_NOP_40_BASE: u8 = 0x40;
const M64_PLAYER_SUBTRACT_VARIATION_BASE: u8 = 0x50;
const M64_PLAYER_NOP_60_BASE: u8 = 0x60;
const M64_PLAYER_SET_VARIATION_BASE: u8 = 0x70;
const M64_PLAYER_GET_VARIATION_BASE: u8 = 0x80;
const M64_PLAYER_START_CHANNEL_BASE: u8 = 0x90;
const M64_PLAYER_NOP_A0_BASE: u8 = 0xA0;

impl SequencePlayer {
	pub(super) unsafe fn process_sequence(&mut self) {
		use SequencePlayerFlags as F;
		if !self.flags.contains(F::Enabled) {
			return;
		}

		if self.flags.contains(F::BankDmaInProgress) {
			if ultra::osRecvMesg(self.bankDmaMesgQueue.assume_init_ref(), ptr::null_mut(), ultra::Blocking::NoBlock).is_err() {
				return;
			}
			if self.bankDmaRemaining == 0 {
				self.flags -= F::BankDmaInProgress;
				let loadingBankId = self.loadingBankId as usize;
				patch_audio_bank(
					&mut *(*gCtlEntries.add(loadingBankId)).instruments.offset(-1).cast_thin(),
					(*(*gAlTbl).seqArray.as_ptr().add(loadingBankId)).offset.cast(),
					(*gCtlEntries.add(loadingBankId)).numInstruments as u32,
					(*gCtlEntries.add(loadingBankId)).numDrums as u32,
				);
				(*gCtlEntries.add(loadingBankId)).drums =
					(*((*gCtlEntries.add(loadingBankId)).instruments.offset(-1) as *const AudioBank)).drums;
				gBankLoadStatus[loadingBankId] = SoundLoadStatus::Complete;
			} else {
				audio_dma_partial_copy_async(
					&mut self.bankDmaCurrDevAddr,
					&mut self.bankDmaCurrMemAddr,
					&mut self.bankDmaRemaining,
					self.bankDmaMesgQueue.assume_init_ref(),
					&mut self.bankDmaIoMesg,
				);
			}
			return;
		}

		if self.flags.contains(F::SeqDmaInProgress) {
			if ultra::osRecvMesg(self.seqDmaMesgQueue.assume_init_ref(), ptr::null_mut(), ultra::Blocking::NoBlock).is_err() {
				return;
			}

			self.flags -= F::SeqDmaInProgress;
			gSeqLoadStatus[self.seqId as usize] = SoundLoadStatus::Complete;
		}

		// If discarded, bail out.
		if !gSeqLoadStatus[self.seqId as usize].is_load_complete()
			|| !gBankLoadStatus[self.defaultBank as usize].is_load_complete()
		{
			eprintln!("Disappeared sequence on bank {}", self.seqId);
			self.disable();
			return;
		}

		// Remove possible SoundLoadStatus::Discardable marks.
		gSeqLoadStatus[self.seqId as usize] = SoundLoadStatus::Complete;
		gBankLoadStatus[self.defaultBank as usize] = SoundLoadStatus::Complete;

		if self.flags.contains(F::Muted) && self.muteBehavior.contains(MuteBehaviorFlags::StopScript) {
			return;
		}

		// Check if we surpass the number of ticks needed for a tatum, else stop.
		self.tempoAcc += self.tempo;
		if self.tempoAcc < gTempoInternalToExternal as u16 {
			return;
		}
		self.tempoAcc -= gTempoInternalToExternal as u16;

		let state = &mut *ptr::addr_of_mut!(self.scriptState);
		let mut accumulator = 0i32;
		if self.delay > 1 {
			self.delay -= 1;
		} else {
			self.flags |= F::RecalculateVolume;
			loop {
				let cmd = state.read_u8();
				if cmd == M64_PLAYER_END {
					// End
					if state.depth == 0 {
						self.disable();
						break;
					}
					state.pc = state.stack[{ state.depth -= 1; state.depth as usize }];
				}

				if cmd == M64_PLAYER_DELAY {
					// Delay
					self.delay = state.read_compressed_u16();
					break;
				}

				if cmd == M64_PLAYER_DELAY_1 {
					// Delay = 1
					self.delay = 1;
					break;
				}

				if cmd >= 0xc0 {
					match cmd {
						// End
						M64_PLAYER_END => break,

						M64_PLAYER_CALL => {
							// Call
							debug_assert!(state.depth < 4, "Stack overflow!");
							let offset = state.read_u16();
							state.stack[state.depth as usize] = state.pc;
							state.depth += 1;
							state.pc = self.seqData.add(offset as usize);
						}
						M64_PLAYER_LOOP_START => {
							// Loop start, N iterations (256 if N = 0)
							debug_assert!(state.depth < 4, "Stack overflow!");
							state.remLoopIters[state.depth as usize] = state.read_u8();
							state.stack[state.depth as usize] = state.pc;
							state.depth += 1;
						}
						M64_PLAYER_LOOP_END => {
							// Loop end
							if { let p = &mut state.remLoopIters[state.depth as usize - 1]; *p -= 1; *p } != 0 {
								state.pc = state.stack[state.depth as usize - 1];
							} else {
								state.depth -= 1;
							}
						}

						| M64_PLAYER_JUMP // Jump
						| M64_PLAYER_JUMP_IF_ZERO // Jump if zero
						| M64_PLAYER_JUMP_IF_NEGATIVE // Jump if negative
						| M64_PLAYER_JUMP_IF_NON_NEGATIVE // Jump if non-negative
						=> {
							use std::cmp::Ordering;
							let offset = state.read_u16();
							match (cmd, accumulator.cmp(&0)) {
								| (M64_PLAYER_JUMP_IF_ZERO, Ordering::Equal)
								| (M64_PLAYER_JUMP_IF_NEGATIVE, Ordering::Less)
								| (M64_PLAYER_JUMP_IF_NON_NEGATIVE, Ordering::Equal | Ordering::Greater)
								| (M64_PLAYER_JUMP, _) => {
									state.pc = self.seqData.add(offset as usize);
								}
								_ => {}
							}
						}
						| M64_PLAYER_JUMP_RELATIVE // Jump relative
						| M64_PLAYER_JUMP_RELATIVE_IF_ZERO // Jump relative if zero
						| M64_PLAYER_JUMP_RELATIVE_IF_NEGATIVE // Jump relative if negative
						=> {
							use std::cmp::Ordering;
							let offset = state.read_i8();
							match (cmd, accumulator.cmp(&0)) {
								| (M64_PLAYER_JUMP_RELATIVE_IF_ZERO, Ordering::Equal)
								| (M64_PLAYER_JUMP_RELATIVE_IF_NEGATIVE, Ordering::Less)
								| (M64_PLAYER_JUMP_RELATIVE, _) => {
									state.pc = state.pc.offset(offset as isize);
								}
								_ => {}
							}
						}

						M64_PLAYER_RESERVE_NOTES => {
							// Reserve notes
							self.notePool.clear();
							self.notePool.fill(state.read_u8() as u32);
						}
						M64_PLAYER_UNRESERVE_NOTES => {
							// UnReserve notes
							self.notePool.clear();
						}

						M64_PLAYER_TRANSPOSE => {
							// Transpose
							// (originally fallthrough to next case)
							self.transposition = state.read_u8() as i8 as i16;
						}
						M64_PLAYER_TRANSPOSE_RELATIVE => {
							// Transpose relative
							self.transposition += state.read_u8() as i8 as i16;
						}

						| M64_PLAYER_SET_TEMPO // Set tempo
						| M64_PLAYER_ADD_TEMPO // Add tempo
						 => {
							let tempo = state.read_u8() as u8 as i16 * TEMPO_SCALE;
							if cmd == M64_PLAYER_SET_TEMPO {
								self.tempo = tempo as u16;
							} else {
								self.tempo = (self.tempo as i16 + tempo) as u16;
							}

							// These two operations interpret tempo with different signedness.
							// First one uses u16->i32 coercion (unsigned).
							// Second one explicitly casts u16 to i16.
							self.tempo = (self.tempo as i32).min(gTempoInternalToExternal as i32) as u16;
							self.tempo = (self.tempo as i16).max(1) as u16;
						}

						M64_PLAYER_CHANGE_STATE => {
							// Change state (?)
							let newState = SequencePlayerStateEU::from_u8(state.read_u8());
							let frames = state.read_u16();
							match newState {
								| SequencePlayerStateEU::FadingNormal
								| SequencePlayerStateEU::FadingIn => {
									if self.state != SequencePlayerStateEU::FadeOut {
										self.fadeTimerUnkEu = frames;
										self.state = newState;
									}
								}
								SequencePlayerStateEU::FadeOut => {
									self.fadeRemainingFrames = frames;
									self.state = newState;
									self.fadeVelocity = (- self.fadeVolume) / frames as f32;
								}
							}
						}
						M64_PLAYER_FADE => {
							// Fade with volume (?)
							let targetVolume = state.read_u8() as f32;
							match self.state {
								| SequencePlayerStateEU::FadingNormal
								| SequencePlayerStateEU::FadingIn => {
									if self.state == SequencePlayerStateEU::FadingIn {
										self.state = SequencePlayerStateEU::FadingNormal;
										self.fadeVolume = 0.0;
									}
									self.fadeRemainingFrames = self.fadeTimerUnkEu;
									if self.fadeTimerUnkEu != 0 {
										self.fadeVelocity = (targetVolume / 127.0 - self.fadeVolume) / self.fadeRemainingFrames as f32;
									} else {
										self.fadeVolume = targetVolume / 127.0;
									}
								}
								SequencePlayerStateEU::FadeOut => {}
							}
						}

						M64_PLAYER_FADE_VOLUME_SCALE => {
							// Set fade volume scale
							self.fadeVolumeScale = state.read_i8() as f32 / 127.0;
						}

						M64_PLAYER_INIT_CHANNELS => {
							// Init channels
							self.init_channels(state.read_u16());
						}
						M64_PLAYER_DISABLE_CHANNELS => {
							// Disable channels
							self.disable_channels(state.read_u16());
						}

						M64_PLAYER_MUTE_VOLUME_SCALE => {
							// Set mute scale
							self.muteVolumeScale = state.read_i8() as f32 / 127.0;
						}
						M64_PLAYER_MUTE => {
							// Mute
							self.flags |= F::Muted;
						}
						M64_PLAYER_MUTE_BEHAVIOR => {
							// Set mute behavior
							self.muteBehavior = MuteBehaviorFlags::from_bits_retain(state.read_u8());
						}

						| M64_PLAYER_SHORT_NOTE_VEL_TAB // Set short note velocitiy table
						| M64_PLAYER_SHORT_NOTE_DUR_TAB // Set short note duration table
						=> {
							let offset = state.read_u16() as usize;
							let data = self.seqData.add(offset);
							if cmd == M64_PLAYER_SHORT_NOTE_VEL_TAB {
								self.shortNoteVelocityTable = data;
							} else {
								self.shortNoteDurationTable = data;
							}
						}
						M64_PLAYER_NOTE_ALLOC_POLICY => {
							// Set note allocation policy
							self.noteAllocPolicy = NoteAllocPolicy::from_bits_retain(state.read_u8());
						}

						M64_PLAYER_LOAD_ACC => {
							// Set accumulator
							accumulator = state.read_u8() as i32;
						}
						M64_PLAYER_BIT_AND => {
							// BitAnd
							accumulator &= state.read_u8() as i32;
						}
						M64_PLAYER_SUBTRACT => {
							// Subtract
							accumulator -= state.read_u8() as i32;
						}

						_ => {
							eprintln!("Error: undefined upper 0xC0 group command {cmd:02X}");
						}
					}
				} else {
					let loBits = cmd as usize & 0xf;
					match cmd & 0xf0 {
						M64_PLAYER_IS_CHANNEL_DISABLED_BASE => {
							// Test channel disabled
							accumulator = (*self.channels[loBits]).flags.contains(SequenceChannelFlags::Finished) as i32;
						}
						M64_PLAYER_NOP_10_BASE | M64_PLAYER_NOP_20_BASE | M64_PLAYER_NOP_40_BASE => {}
						M64_PLAYER_SUBTRACT_VARIATION_BASE => {
							// Subtract variation
							accumulator -= self.seqVariationEu as i32;
						}
						M64_PLAYER_NOP_60_BASE => {}
						M64_PLAYER_SET_VARIATION_BASE => {
							// Set variation
							self.seqVariationEu = accumulator as i8;
						}
						M64_PLAYER_GET_VARIATION_BASE => {
							// Get variation
							accumulator = self.seqVariationEu as i32;
						}

						M64_PLAYER_START_CHANNEL_BASE => {
							// Start channel
							let offset = state.read_u16() as usize;
							self.enable_channel(loBits, self.seqData.add(offset).cast());
						}

						M64_PLAYER_NOP_A0_BASE => {}
						_ => {
							eprintln!("Error: undefined group command {cmd:02X}");
						}
					}
				}
			}
		}

		for chan in self.channels {
			if chan != ptr::addr_of_mut!(gSequenceChannelNone) {
				(*chan).process_script();
			}
		}
	}
}
