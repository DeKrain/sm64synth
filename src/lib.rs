#![allow(non_upper_case_globals, non_camel_case_types, non_snake_case)]

#![cfg_attr(feature = "nightly", feature(
	extern_types, ptr_metadata,
))]

#[macro_use]
pub(crate) mod support;

pub(crate) mod heap {
	mod common;
	//mod malloc;
	mod pool;

	//#[cfg(system_alloc)]
	//pub(crate) use malloc::*;
	#[cfg(not(system_alloc))]
	pub(crate) use pool::*;

	pub(crate) use common::*;
}
pub(crate) mod ultra;
pub(crate) mod ultra64_abi;
pub(crate) mod data;
pub(crate) mod file;
pub(crate) mod structs;
pub(crate) mod load;
pub(crate) mod synthesis;
pub(crate) mod playback;
pub(crate) mod sequencer;
pub(crate) mod effects;

mod sound_data;

pub mod external;
