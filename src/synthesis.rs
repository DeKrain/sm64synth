use crate::{
	structs::*,
	ultra64_abi::*,
	load::*,
	heap::*,
	data::*,
	sequencer::*,
};

use std::ptr::NonNull;

pub const DEFAULT_LEN_1CH: usize = 0x140;
pub const DEFAULT_LEN_2CH: usize = 0x280;

pub const MAX_UPDATES_PER_FRAME: usize = 5;

pub struct ReverbRingBufferItem {
	// never read
	pub numSamplesAfterDownsampling: u16,
	pub chunkLen: u16,

	// Data pointed to by left and right are adjacent in memory.
	pub toDownsampleLeft: *mut i16,
	pub toDownsampleRight: *mut i16,
	/// Start position in ring buffer
	pub startPos: u32,
	/// First length in ring buffer (from startPos, at most until end).
	pub lengthA: u16,
	/// Second length in ring buffer (starting at pos 0).
	pub lengthB: u16,
}

pub struct SynthesisReverb {
	pub resampleFlags: AudioFlags,
	pub useReverb: bool,
	pub framesLeftToIgnore: u8,
	pub curFrame: u8,
	pub downsampleRate: u8,
	pub windowSize: u16,
	pub reverbGain: u16,
	pub resampleRate: u16,
	pub nextRingBufferPos: i32,
	pub bufSizePerChannel: u32,
	pub ringBufferLeft: *mut i16,
	pub ringBufferRight: *mut i16,
	pub resampleStateLeft: *mut RESAMPLE_STATE,
	pub resampleStateRight: *mut RESAMPLE_STATE,
	pub items: [[ReverbRingBufferItem; MAX_UPDATES_PER_FRAME]; 2],
}

/*impl SynthesisReverb {
	pub const INIT: Self = Self { resampleFlags: 0, useReverb: 0, framesLeftToIgnore: 0, curFrame: 0, downsampleRate: 0, windowSize: 0, reverbGain: 0, resampleRate: 0, nextRingBufferPos: 0, bufSizePerChannel: 0, ringBufferLeft: std::ptr::null_mut(), resampleStateRight: std::ptr::null_mut(), ringBufferRight: std::ptr::null_mut(), resampleStateLeft: std::ptr::null_mut(), items: std::array::from_fn(|_| ReverbRingBufferItem::INIT) };
}*/

zeroed! {
	#[no_mangle] pub static mut gSynthesisReverbs: [SynthesisReverb; 4];
}

#[no_mangle] pub static mut gVolume: i16 = 0;
#[no_mangle] pub static mut gUseReverb: bool = false;
#[no_mangle] pub static mut gNumSynthesisReverbs: u8 = 0;
#[no_mangle] pub static mut gNoteSubsEu: *mut NoteSubEu = std::ptr::null_mut();

#[no_mangle] pub static mut gLeftVolRampings: [[f32; 0x400]; 3] = [[0.0; 0x400]; 3];
#[no_mangle] pub static mut gRightVolRampings: [[f32; 0x400]; 3] = [[0.0; 0x400]; 3];
/// Points to any of the three left buffers above
#[no_mangle] pub static mut gCurrentLeftVolRamping: *const [f32; 0x400] = std::ptr::null();
/// Points to any of the three right buffers above
#[no_mangle] pub static mut gCurrentRightVolRamping: *const [f32; 0x400] = std::ptr::null();


const DMEM_ADDR_TEMP: u16 = 0x0;
const DMEM_ADDR_RESAMPLED: u16 = 0x20;
const DMEM_ADDR_RESAMPLED2: u16 = 0x160;
const DMEM_ADDR_UNCOMPRESSED_NOTE: u16 = 0x180;
const DMEM_ADDR_NOTE_PAN_TEMP: u16 = 0x200;
const DMEM_ADDR_STEREO_STRONG_TEMP_DRY: u16 = 0x200;
const DMEM_ADDR_STEREO_STRONG_TEMP_WET: u16 = 0x340;
const DMEM_ADDR_COMPRESSED_ADPCM_DATA: u16 = 0x3f0;
const DMEM_ADDR_LEFT_CH: u16 = 0x4c0;
const DMEM_ADDR_RIGHT_CH: u16 = 0x600;
const DMEM_ADDR_WET_LEFT_CH: u16 = 0x740;
const DMEM_ADDR_WET_RIGHT_CH: u16 = 0x880;

struct VolumeChange {
	sourceLeft: u16,
	sourceRight: u16,
	targetLeft: u16,
	targetRight: u16,
}

macro_rules! align {
	($val:expr, $amnt:expr) => {
		($val + (1 << $amnt) - 1) & !((1 << $amnt) - 1)
	};
}

macro_rules! shr_round_up {
	($val:expr, $exp:expr) => {
		($val + (1 << $exp) - 1) >> $exp
	};
}

unsafe fn prepare_reverb_ring_buffer(chunkLen: i32, updateIndex: usize, reverbIndex: usize) {
	let reverb = &mut gSynthesisReverbs[reverbIndex];
	let item = &mut reverb.items[reverb.curFrame as usize][updateIndex];
	if reverb.downsampleRate != 1 && reverb.framesLeftToIgnore == 0 {
		// Now that the RSP has finished, downsample the samples produced two frames ago
		// by skipping samples.

		// osInvalDCache(item.toDownsampleLeft, DEFAULT_LEN_2CH);

		let mut srcPos = 0;
		for dstPos in 0..item.lengthA as usize / 2 {
			*reverb.ringBufferLeft.add(item.startPos as usize + dstPos) = *item.toDownsampleLeft.add(srcPos);
			*reverb.ringBufferRight.add(item.startPos as usize + dstPos) = *item.toDownsampleRight.add(srcPos);
			srcPos += reverb.downsampleRate as usize;
		}
		for dstPos in 0..item.lengthB as usize / 2 {
			*reverb.ringBufferLeft.add(dstPos) = *item.toDownsampleLeft.add(srcPos);
			*reverb.ringBufferRight.add(dstPos) = *item.toDownsampleRight.add(srcPos);
			srcPos += reverb.downsampleRate as usize;
		}
	}

	let nSamples = chunkLen / reverb.downsampleRate as i32;
	let excessiveSamples = nSamples + reverb.nextRingBufferPos - reverb.bufSizePerChannel as i32;
	if excessiveSamples < 0 {
		// There is space in the ring buffer before it wraps around
		item.lengthA = nSamples as u16 * 2;
		item.lengthB = 0;
		item.startPos = reverb.nextRingBufferPos as u32;
		reverb.nextRingBufferPos += nSamples;
	} else {
		// Ring buffer wrapped around
		item.lengthA = (nSamples as u16 - excessiveSamples as u16) * 2;
		item.lengthB = excessiveSamples as u16 * 2;
		item.startPos = reverb.nextRingBufferPos as u32;
		reverb.nextRingBufferPos = excessiveSamples;
	}
	item.numSamplesAfterDownsampling = nSamples as u16;
	item.chunkLen = chunkLen as u16;
}

unsafe fn synthesis_load_reverb_ring_buffer(cmd: &mut AudioCmdPtr, addr: u16, srcOffset: u16, len: u16, reverbIndex: usize) {
	cmd.aSetBuffer(0, addr, 0, len);
	cmd.aLoadBuffer(gSynthesisReverbs[reverbIndex].ringBufferLeft.add(srcOffset as usize).cast());
	
	cmd.aSetBuffer(0, addr + DEFAULT_LEN_1CH as u16, 0, len);
	cmd.aLoadBuffer(gSynthesisReverbs[reverbIndex].ringBufferRight.add(srcOffset as usize).cast());
}

unsafe fn synthesis_save_reverb_ring_buffer(cmd: &mut AudioCmdPtr, addr: u16, destOffset: u16, len: u16, reverbIndex: usize) {
	cmd.aSetBuffer(0, 0, addr, len);
	cmd.aSaveBuffer(gSynthesisReverbs[reverbIndex].ringBufferLeft.add(destOffset as usize).cast());
	
	cmd.aSetBuffer(0, 0, addr + DEFAULT_LEN_1CH as u16, len);
	cmd.aSaveBuffer(gSynthesisReverbs[reverbIndex].ringBufferRight.add(destOffset as usize).cast());
}

unsafe fn synthesis_load_note_subs_eu(updateIndex: usize) {
	for i in 0..gMaxSimultaneousNotes as usize {
		let src = &mut (*gNotes.add(i)).noteSubEu;
		let dest = &mut *gNoteSubsEu.add(gMaxSimultaneousNotes as usize * updateIndex + i);
		if src.flags.contains(NoteSubEuFlags::Enabled) {
			*dest = *src;
			src.flags -= NoteSubEuFlags::NeedsInit;
		} else {
			dest.flags -= NoteSubEuFlags::Enabled;
		}
	}
}

pub unsafe fn synthesis_execute(
	cmdBuf: NonNull<Acmd>,
	writtenCmds: &mut usize,
	aiBuf: *mut i16,
	mut bufLen: usize,
) -> NonNull<Acmd> {
	for i in (0..gAudioBufferParameters.updatesPerFrame as usize).rev() {
		// I think these are done in bulk for instruction cache efficiency.
		process_sequences(i as u32);
		synthesis_load_note_subs_eu(gAudioBufferParameters.updatesPerFrame as usize - i - 1);
	}

	let mut cmd = AudioCmdPtr::new(cmdBuf);
	cmd.aSegment(0, 0);
	let mut aiBuf = aiBuf.cast::<u32>();
	let mut nextVolRampTable = !0i32;
	for i in (1..=gAudioBufferParameters.updatesPerFrame as usize).rev() {
		let (chunkLen, leftVolRamp, rightVolRamp);
		if i == 1 {
			chunkLen = bufLen;
			// nextVolRampTable = nextVolRampTable;
			leftVolRamp = &gLeftVolRampings[nextVolRampTable as usize];
			rightVolRamp = &gRightVolRampings[nextVolRampTable as usize];
		} else if bufLen / i >= gAudioBufferParameters.samplesPerUpdateMax as usize {
			chunkLen = gAudioBufferParameters.samplesPerUpdateMax as usize;
			nextVolRampTable = 2;
			leftVolRamp = &gLeftVolRampings[2];
			rightVolRamp = &gRightVolRampings[2];
		} else if bufLen / i <= gAudioBufferParameters.samplesPerUpdateMin as usize {
			chunkLen = gAudioBufferParameters.samplesPerUpdateMin as usize;
			nextVolRampTable = 0;
			leftVolRamp = &gLeftVolRampings[0];
			rightVolRamp = &gRightVolRampings[0];
		} else {
			chunkLen = gAudioBufferParameters.samplesPerUpdate as usize;
			nextVolRampTable = 1;
			leftVolRamp = &gLeftVolRampings[1];
			rightVolRamp = &gRightVolRampings[1];
		}

		gCurrentLeftVolRamping = leftVolRamp;
		gCurrentRightVolRamping = rightVolRamp;
		for idx in 0..gNumSynthesisReverbs as usize {
			if gSynthesisReverbs[idx].useReverb {
				prepare_reverb_ring_buffer(chunkLen as i32, gAudioBufferParameters.updatesPerFrame as usize - i, idx);
			}
		}
		synthesis_do_one_audio_update(aiBuf.cast(), chunkLen, &mut cmd, gAudioBufferParameters.updatesPerFrame as usize - i);
		bufLen -= chunkLen;
		aiBuf = aiBuf.add(chunkLen);
	}

	for reverb in &mut gSynthesisReverbs[..gNumSynthesisReverbs as usize] {
		if reverb.framesLeftToIgnore != 0 {
			reverb.framesLeftToIgnore -= 1;
		}
		reverb.curFrame ^= 1;
	}
	*writtenCmds = cmd.get().as_ptr().offset_from(cmdBuf.as_ptr()) as usize;
	cmd.get()
}

unsafe fn synthesis_resample_and_mix_reverb(cmd: &mut AudioCmdPtr, bufLen: usize, reverbIndex: usize, updateIndex: usize) {
	let reverb = &gSynthesisReverbs[reverbIndex];
	let item = &reverb.items[reverb.curFrame as usize][updateIndex];
	cmd.aClearBuffer(DMEM_ADDR_WET_LEFT_CH as u32, DEFAULT_LEN_2CH);
	if reverb.downsampleRate == 1 {
		synthesis_load_reverb_ring_buffer(cmd, DMEM_ADDR_WET_LEFT_CH, item.startPos as u16, item.lengthA, reverbIndex);
		if item.lengthB != 0 {
			synthesis_load_reverb_ring_buffer(cmd, DMEM_ADDR_WET_LEFT_CH + item.lengthA, 0, item.lengthB, reverbIndex)
		}
		cmd.aSetBuffer(0, 0, 0, DEFAULT_LEN_2CH as u16);
		cmd.aMix(0, 0x7fff, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_LEFT_CH);
		cmd.aMix(0, 0x8000 + reverb.reverbGain, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_WET_LEFT_CH);
	} else {
		let startPad = (item.startPos as u16 & 7) * 2;
		let paddedLengthA = align!(startPad + item.lengthA, 4);

		synthesis_load_reverb_ring_buffer(cmd, DMEM_ADDR_RESAMPLED, item.startPos as u16 - startPad / 2, DEFAULT_LEN_1CH as u16, reverbIndex);
		if item.lengthB != 0 {
			synthesis_load_reverb_ring_buffer(cmd, DMEM_ADDR_RESAMPLED + paddedLengthA, 0, DEFAULT_LEN_1CH as u16 - paddedLengthA, reverbIndex);
		}

		cmd.aSetBuffer(0, DMEM_ADDR_RESAMPLED + startPad, DMEM_ADDR_WET_LEFT_CH, bufLen as u16 * 2);
		cmd.aResample(reverb.resampleFlags, reverb.resampleRate, reverb.resampleStateLeft);

		cmd.aSetBuffer(0, DMEM_ADDR_RESAMPLED2 + startPad, DMEM_ADDR_WET_RIGHT_CH, bufLen as u16 * 2);
		cmd.aResample(reverb.resampleFlags, reverb.resampleRate, reverb.resampleStateRight);

		cmd.aSetBuffer(0, 0, 0, DEFAULT_LEN_2CH as u16);
		cmd.aMix(0, 0x7fff, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_LEFT_CH);
		cmd.aMix(0, 0x8000 + reverb.reverbGain, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_WET_LEFT_CH);
	}
}

unsafe fn synthesis_save_reverb_samples(cmd: &mut AudioCmdPtr, reverbIndex: usize, updateIndex: usize) {
	let reverb = &mut gSynthesisReverbs[reverbIndex];
	let item = &reverb.items[reverb.curFrame as usize][updateIndex];
	if reverb.useReverb {
		if reverb.downsampleRate == 1 {
			// Put the oldest samples in the ring buffer into the wet channels
			synthesis_save_reverb_ring_buffer(cmd, DMEM_ADDR_WET_LEFT_CH, item.startPos as u16, item.lengthA, reverbIndex);
			if item.lengthB != 0 {
				// Ring buffer wrapped
				synthesis_save_reverb_ring_buffer(cmd, DMEM_ADDR_WET_LEFT_CH + item.lengthA, 0, item.lengthB, reverbIndex);
			}
		} else {
			// Downsampling is done later by CPU when RSP is done, therefore we need to have double
			// buffering. Left and right buffers are adjacent in memory.
			cmd.aSetBuffer(0, 0, DMEM_ADDR_WET_LEFT_CH, DEFAULT_LEN_2CH as u16);
			cmd.aSaveBuffer(reverb.items[reverb.curFrame as usize][updateIndex].toDownsampleLeft.cast());
			reverb.resampleFlags = 0;
		}
	}
}

unsafe fn synthesis_do_one_audio_update(aiBuf: *mut i16, bufLen: usize, cmd: &mut AudioCmdPtr, updateIndex: usize) {
	let mut noteIndices = [0u8; 56];
	let mut notePos = 0usize;
	if gNumSynthesisReverbs == 0 {
		for i in 0..gMaxSimultaneousNotes as usize {
			let sub = &*gNoteSubsEu.add(gMaxSimultaneousNotes as usize * updateIndex + i);
			if sub.flags.contains(NoteSubEuFlags::Enabled) {
				noteIndices[notePos] = i as u8;
				notePos += 1;
			}
		}
	} else {
		for j in 0..gNumSynthesisReverbs {
			for i in 0..gMaxSimultaneousNotes as usize {
				let sub = &*gNoteSubsEu.add(gMaxSimultaneousNotes as usize * updateIndex + i);
				if sub.flags.contains(NoteSubEuFlags::Enabled) && j == sub.reverb_index() {
					noteIndices[notePos] = i as u8;
					notePos += 1;
				}
			}
		}

		for i in 0..gMaxSimultaneousNotes as usize {
			let sub = &*gNoteSubsEu.add(gMaxSimultaneousNotes as usize * updateIndex + i);
			if sub.flags.contains(NoteSubEuFlags::Enabled) && sub.reverb_index() >= gNumSynthesisReverbs {
				noteIndices[notePos] = i as u8;
				notePos += 1;
			}
		}
	}
	cmd.aClearBuffer(DMEM_ADDR_LEFT_CH as u32, DEFAULT_LEN_2CH);
	let mut i = 0;
	for reverbIndex in 0..gNumSynthesisReverbs as usize {
		gUseReverb = gSynthesisReverbs[reverbIndex].useReverb;
		if gUseReverb {
			synthesis_resample_and_mix_reverb(cmd, bufLen, reverbIndex, updateIndex);
		}
		while i < notePos {
			let temp = updateIndex * gMaxSimultaneousNotes as usize;
			if reverbIndex == (*gNoteSubsEu.add(temp + noteIndices[i] as usize)).reverb_index() as usize {
				synthesis_process_note(
					&mut *gNotes.add(noteIndices[i] as usize),
					&mut *gNoteSubsEu.add(temp + noteIndices[i] as usize),
					&mut (*gNotes.add(noteIndices[i] as usize)).synthesisState,
					aiBuf, bufLen as u16, cmd);
				i += 1;
			} else {
				break;
			}
		}
		if gSynthesisReverbs[reverbIndex].useReverb {
			synthesis_save_reverb_samples(cmd, reverbIndex, updateIndex);
		}
	}
	while i < notePos {
		let temp = updateIndex * gMaxSimultaneousNotes as usize;
		if gBankLoadStatus[(*gNoteSubsEu.add(temp + noteIndices[i] as usize)).bankId as usize].is_load_complete() {
			synthesis_process_note(
				&mut *gNotes.add(noteIndices[i] as usize),
				&mut *gNoteSubsEu.add(temp + noteIndices[i] as usize),
				&mut (*gNotes.add(noteIndices[i] as usize)).synthesisState,
				aiBuf, bufLen as u16, cmd);
		} else {
			gAudioErrorFlags = ((*gNoteSubsEu.add(temp + noteIndices[i] as usize)).bankId as i32 + ((i as i32) << 8)) + 0x10000000;
		}
		i += 1;
	}

	let temp = bufLen * 2;
	cmd.aSetBuffer(0, 0, DMEM_ADDR_TEMP, temp as u16);
	cmd.aInterleave(DMEM_ADDR_LEFT_CH, DMEM_ADDR_RIGHT_CH);
	cmd.aSetBuffer(0, 0, DMEM_ADDR_TEMP, temp as u16 * 2);
	cmd.aSaveBuffer(aiBuf.cast());
}

/// Process just one note.
unsafe fn synthesis_process_note(
	note: &mut Note,
	noteSubEu: &mut NoteSubEu,
	synthesisState: &mut NoteSynthesisState,
	_aiBuf: *mut i16,
	bufLen: u16,
	cmd: &mut AudioCmdPtr,
) {
	use NoteSubEuFlags as SF;
	if !note.noteSubEu.flags.contains(SF::Enabled) {
		return;
	}

	/*
		@DeKrain:
		Synthesizing a note consists of a few steps:
		* The number of desired samples is calculated
		* The raw decompressed samples are loaded into DMEM_ADDR_UNCOMPRESSED_NOTE
			* This come from either from the synthesis wave tables stored directly in the ROM,
			* or they can be decompressed from ADPCM data with a codebook
				* ADPCM samples are segregated into 16 sample blocks (32 bytes), each compressed into a 9-byte frame
				* Codebooks are auxillary tables of vectors that help decode the sample streams
				* Each codebook has up to 8 predictors, composed of 2 8-sample vectors, where each frame selects one predictor for decompression, plus a shift value to scale the coefficients
				* ADPCM instruments also allow looping segments of the ADPCM streams, to create sustained instruments
		* After that, resampling is done to get the correct pitch
		* Finally, envelopes & optional headset effects are applied
	*/

	let mut flags: AudioFlags = 0;
	let tempBufLen = bufLen;
	if noteSubEu.flags.contains(SF::NeedsInit) {
		flags = A_INIT;
		synthesisState.restart = false;
		synthesisState.samplePosInt = 0;
		synthesisState.samplePosFrac = 0;
		synthesisState.curVolLeft = 1;
		synthesisState.curVolRight = 1;
		synthesisState.prevHeadsetPanRight = 0;
		synthesisState.prevHeadsetPanLeft = 0;
	}

	#[derive(Clone, Copy)]
	enum NumParts {
		One,
		Two,
	}

	#[derive(Clone, Copy)]
	enum PartIndex {
		First,
		Second,
	}

	impl IntoIterator for NumParts {
		type Item = PartIndex;
		type IntoIter = std::iter::Copied<std::slice::Iter<'static, PartIndex>>;
		#[inline(always)]
		fn into_iter(self) -> Self::IntoIter {
			[PartIndex::First, PartIndex::Second][..match self {
				NumParts::One => 1,
				NumParts::Two => 2,
			}].into_iter().copied()
		}
	}

	let resamplingRateFixedPoint = noteSubEu.resamplingRateFixedPoint;
	let nParts = if noteSubEu.flags.contains(SF::HasTwoAdpcmParts) { NumParts::Two } else { NumParts::One };
	/*match nParts {
		NumParts::One => println!("One part"),
		NumParts::Two => println!("Two parts"),
	}*/
	let samplesLenFixedPoint = (resamplingRateFixedPoint as u32 * tempBufLen as u32 * 2) + synthesisState.samplePosFrac as u32;
	synthesisState.samplePosFrac = samplesLenFixedPoint as u16;
	// Will be overriden.
	let mut noteSamplesDmemAddrBeforeResampling: u16 = 0;
	if noteSubEu.flags.contains(SF::IsSyntheticWave) {
		// Load the samples into DMEM_ADDR_UNCOMPRESSED_NOTE
		load_wave_samples(cmd, noteSubEu, synthesisState, (samplesLenFixedPoint >> 0x10) as u16);
		noteSamplesDmemAddrBeforeResampling = DMEM_ADDR_UNCOMPRESSED_NOTE + 2 * synthesisState.samplePosInt as u16;
		synthesisState.samplePosInt += samplesLenFixedPoint >> 0x10;
	} else {
		// ADPCM
		let audioBookSample = &*(*noteSubEu.sound.audioBankSound).sample;
		let loopInfo = &*audioBookSample.r#loop;
		let sampleAddr = audioBookSample.sampleAddr;
		let mut curLoadedBook = std::ptr::null();
		// Will be overriden.
		let mut resampledTempLen = 0;
		let mut readySamplesOffset: u16 = 0;
		for curPart in nParts {
			let mut nSamplesProcessed = 0;
			let mut decompressedPos = 0;

			let mut samplesLenAdjusted = samplesLenFixedPoint >> 0x10;
			if matches!(nParts, NumParts::Two) && samplesLenAdjusted & 1 != 0 {
				samplesLenAdjusted = (samplesLenAdjusted & !1) + match curPart { PartIndex::First => 0, PartIndex::Second => 2 };
			}

			// @DeKrain: We should just be able to load this once, before the loop.
			if curLoadedBook != (*audioBookSample.book).book.as_ptr() {
				curLoadedBook = (*audioBookSample.book).book.as_ptr();
				let nEntries = 16 * (*audioBookSample.book).order * (*audioBookSample.book).npredictors;
				cmd.aLoadADPCM(nEntries, curLoadedBook.add(noteSubEu.book_offset() as usize));
			}

			if noteSubEu.book_offset() != 0 {
				// This seems to be a testing/placeholder book, but it's never actually used.
				curLoadedBook = euUnknownData_80301950.as_ptr();
			}

			while nSamplesProcessed != samplesLenAdjusted {
				let nSamplesToProcess = samplesLenAdjusted - nSamplesProcessed;
				let samplesRemaining = loopInfo.end - synthesisState.samplePosInt;
				let mut otherBlocksSamples: u32;
				let mut blockOffset = synthesisState.samplePosInt & 0xf;
				if blockOffset == 0 && !synthesisState.restart {
					blockOffset = 16;
				}
				let mut samplesLeftInBlock = 16 - blockOffset;

				enum LoopState {
					Running,
					Restart,
					NoteFinished,
				}
				let blocksRemaining: u32;
				let lastBlockOverrun: u32;
				let loopState;
				if nSamplesToProcess < samplesRemaining {
					// @DeKrain: Strictness of comparison doesn't matter here
					if nSamplesToProcess <= samplesLeftInBlock {
						blocksRemaining = 0;
						otherBlocksSamples = 0;
						//samplesLeftInBlock = nSamplesToProcess;
					} else {
						blocksRemaining = shr_round_up!(nSamplesToProcess - samplesLeftInBlock, 4);
						otherBlocksSamples = blocksRemaining << 4;
					}
					lastBlockOverrun = samplesLeftInBlock + otherBlocksSamples - nSamplesToProcess;
					loopState = LoopState::Running;
				} else {
					lastBlockOverrun = 0;
					// @DeKrain: Strictness of comparison doesn't matter here
					if samplesRemaining <= samplesLeftInBlock {
						otherBlocksSamples = 0;
						samplesLeftInBlock = samplesRemaining;
					} else {
						otherBlocksSamples = samplesRemaining - samplesLeftInBlock;
					}
					blocksRemaining = shr_round_up!(otherBlocksSamples, 4);
					loopState = if loopInfo.count != 0 {
						// Loop around and restart
						LoopState::Restart
					} else {
						LoopState::NoteFinished
					};
				}

				let bookMemOffset: u16;
				if blocksRemaining != 0 {
					let nextBlockIdx = ((synthesisState.samplePosInt + 0x10 - blockOffset) >> 4) as usize;
					debug_assert!(audioBookSample.loaded & AudioBankSample::LOAD_STATE_LOADED_BIT != 0);
					let bookAddrRaw = sampleAddr.add(nextBlockIdx * 9);
					let bookAddr = if audioBookSample.loaded & AudioBankSample::LOAD_STATE_ALLOC_BIT != 0 {
						//println!("Loaded samples");
						bookAddrRaw
					} else {
						//println!("DMA samples");
						dma_sample_data(bookAddrRaw, blocksRemaining * 9, flags, &mut synthesisState.sampleDmaIndex)
					};

					bookMemOffset = (bookAddr as usize & 0xf) as u16;
					cmd.aSetBuffer(0, DMEM_ADDR_COMPRESSED_ADPCM_DATA, 0, blocksRemaining as u16 * 9 + bookMemOffset);
					cmd.aLoadBuffer(bookAddr.sub(bookMemOffset as usize).cast());
				} else {
					otherBlocksSamples = 0;
					bookMemOffset = 0;
				}

				if synthesisState.restart {
					cmd.aSetLoop(&loopInfo.state);
					flags = A_LOOP;
					synthesisState.restart = false;
				}

				let nSamplesInThisIteration = samplesLeftInBlock + otherBlocksSamples - lastBlockOverrun;
				if nSamplesProcessed == 0 {
					//println!("First samples");
					cmd.aSetBuffer(0, DMEM_ADDR_COMPRESSED_ADPCM_DATA + bookMemOffset, DMEM_ADDR_UNCOMPRESSED_NOTE, otherBlocksSamples as u16 * 2);
					cmd.aADPCMdec(flags, &mut (*synthesisState.synthesisBuffers).adpcmDecState);
					readySamplesOffset = blockOffset as u16 * 2;
				} else {
					//println!("Samples continued");
					let alignedPos = align!(decompressedPos, 5);
					cmd.aSetBuffer(0, DMEM_ADDR_COMPRESSED_ADPCM_DATA + bookMemOffset, DMEM_ADDR_UNCOMPRESSED_NOTE + alignedPos as u16, otherBlocksSamples as u16 * 2);
					cmd.aADPCMdec(flags, &mut (*synthesisState.synthesisBuffers).adpcmDecState);
					cmd.aDMEMMove(DMEM_ADDR_UNCOMPRESSED_NOTE as u32 + alignedPos + blockOffset * 2, DMEM_ADDR_UNCOMPRESSED_NOTE + decompressedPos as u16, nSamplesInThisIteration as u16 * 2);
				}

				nSamplesProcessed += nSamplesInThisIteration;
				match flags {
					A_INIT => {
						readySamplesOffset = 0;
						decompressedPos += otherBlocksSamples * 2;
					}
					A_LOOP => decompressedPos += nSamplesInThisIteration * 2,
					_ => if decompressedPos != 0 {
						decompressedPos += nSamplesInThisIteration * 2;
					} else {
						decompressedPos = (blockOffset + nSamplesInThisIteration) * 2;
					}
				}
				flags = 0;

				match loopState {
					LoopState::NoteFinished => {
						// Zero out the rest of the buffer
						cmd.aClearBuffer(DMEM_ADDR_UNCOMPRESSED_NOTE as u32 + decompressedPos, (samplesLenAdjusted as usize - nSamplesProcessed as usize) * 2);
						noteSubEu.flags |= SF::Finished;
						note.noteSubEu.flags |= SF::Finished;
						note.noteSubEu.flags -= SF::Enabled;
						break;
					}
					LoopState::Restart => {
						synthesisState.restart = true;
						synthesisState.samplePosInt = loopInfo.start;
					}
					LoopState::Running => {
						synthesisState.samplePosInt += nSamplesToProcess;
					}
				}
			}

			// 0xFF60 = 0b1111 1111 0110 0000
			// = UQ1.15 1. 111 1111 011
			// = 2. - 0b0. 000 0000 101
			// = 2.0 - 5 * 2**-10 = 1.9951171875
			match nParts {
				NumParts::One => noteSamplesDmemAddrBeforeResampling = DMEM_ADDR_UNCOMPRESSED_NOTE + readySamplesOffset,
				NumParts::Two => match curPart {
					PartIndex::First => {
						cmd.aSetBuffer(0, DMEM_ADDR_UNCOMPRESSED_NOTE + readySamplesOffset, DMEM_ADDR_RESAMPLED, samplesLenAdjusted as u16 + 4);
						cmd.aResample(A_INIT, 0xff60, &mut (*synthesisState.synthesisBuffers).dummyResampleState);
						resampledTempLen = samplesLenAdjusted + 4;
						noteSamplesDmemAddrBeforeResampling = DMEM_ADDR_RESAMPLED + 4;
						if noteSubEu.flags.contains(SF::Finished) {
							cmd.aClearBuffer(DMEM_ADDR_RESAMPLED as u32 + resampledTempLen as u32, samplesLenAdjusted as usize + 0x10);
						}
					}
					PartIndex::Second => {
						cmd.aSetBuffer(0, DMEM_ADDR_UNCOMPRESSED_NOTE + readySamplesOffset, DMEM_ADDR_RESAMPLED2, samplesLenAdjusted as u16 + 8);
						cmd.aResample(A_INIT, 0xff60, &mut (*synthesisState.synthesisBuffers).dummyResampleState);
						cmd.aDMEMMove(DMEM_ADDR_RESAMPLED2 as u32 + 4, DMEM_ADDR_RESAMPLED + resampledTempLen as u16, samplesLenAdjusted as u16 + 4);
					}
				}
			}

			if noteSubEu.flags.contains(SF::Finished) {
				break;
			}
		}
	}

	flags = 0;
	if noteSubEu.flags.contains(SF::NeedsInit) {
		flags = A_INIT;
		noteSubEu.flags -= SF::NeedsInit;
	}

	final_resample(cmd, synthesisState, bufLen * 2, resamplingRateFixedPoint, noteSamplesDmemAddrBeforeResampling, flags);

	let leftRight = if noteSubEu.headsetPanRight != 0 || synthesisState.prevHeadsetPanRight != 0 {
		HeadsetPan::Right
	} else if noteSubEu.headsetPanLeft != 0 || synthesisState.prevHeadsetPanLeft != 0 {
		HeadsetPan::Left
	} else {
		HeadsetPan::Default
	};

	process_envelope(cmd, noteSubEu, synthesisState, bufLen, 0, leftRight, flags);

	if noteSubEu.flags.contains(SF::UsesHeadsetPanEffects) {
		note_apply_headset_pan_effects(cmd, noteSubEu, synthesisState, bufLen * 2, flags, leftRight);
	}
}

unsafe fn load_wave_samples(cmd: &mut AudioCmdPtr, noteSubEu: &NoteSubEu, synthesisState: &mut NoteSynthesisState, nSamplesToLoad: u16) {
	cmd.aSetBuffer(0, DMEM_ADDR_UNCOMPRESSED_NOTE, 0, 2 * WAVE_CYCLE_SAMPLES);
	cmd.aLoadBuffer(noteSubEu.sound.samples.cast());
	synthesisState.samplePosInt &= WAVE_CYCLE_SAMPLES_MASK as u32;
	let remainingCycleSamples = WAVE_CYCLE_SAMPLES - synthesisState.samplePosInt as u16;
	if remainingCycleSamples < nSamplesToLoad {
		let repeats = shr_round_up!(nSamplesToLoad - remainingCycleSamples, WAVE_CYCLE_SAMPLES_SHIFT);
		for idx in 1..repeats + 1 {
			cmd.aDMEMMove(DMEM_ADDR_UNCOMPRESSED_NOTE as u32, DMEM_ADDR_UNCOMPRESSED_NOTE + (2 * WAVE_CYCLE_SAMPLES * idx), 2 * WAVE_CYCLE_SAMPLES);
		}
	}
}

unsafe fn final_resample(cmd: &mut AudioCmdPtr, synthesisState: &NoteSynthesisState, count: u16, pitch: u16, dmemIn: u16, flags: AudioFlags) {
	cmd.aSetBuffer(0, dmemIn, DMEM_ADDR_TEMP, count);
	cmd.aResample(flags, pitch, &mut (*synthesisState.synthesisBuffers).finalResampleState);
}

#[derive(Clone, Copy)]
enum HeadsetPan {
	Default = 0,
	Right = 1,
	Left = 2,
}

unsafe fn process_envelope(
	cmd: &mut AudioCmdPtr,
	note: &NoteSubEu,
	synthesisState: &mut NoteSynthesisState,
	_nSamples: u16,
	inBuf: u16,
	headsetPanSettings: HeadsetPan,
	_flags: AudioFlags,
) {
	use NoteSubEuFlags as SF;

	let (sourceLeft, sourceRight) = (synthesisState.curVolLeft, synthesisState.curVolRight);
	let (targetLeft, targetRight) = (note.targetVolLeft << 5, note.targetVolRight << 5);
	let targetLeft = targetLeft.max(1);
	let targetRight = targetRight.max(1);
	synthesisState.curVolLeft = targetLeft;
	synthesisState.curVolRight = targetRight;

	// For aEnvMixer, five buffers and count are set using aSetBuffer.
	// in, dry left, count without A_AUX flag.
	// dry right, wet left, wet right with A_AUX flag.

	let nSamples2 = _nSamples * 2;
	if note.flags.contains(SF::UsesHeadsetPanEffects) {
		cmd.aClearBuffer(DMEM_ADDR_NOTE_PAN_TEMP as u32, DEFAULT_LEN_1CH);
		match headsetPanSettings {
			HeadsetPan::Right => {
				cmd.aSetBuffer(0, inBuf, DMEM_ADDR_NOTE_PAN_TEMP, nSamples2);
				cmd.aSetBuffer(A_AUX, DMEM_ADDR_RIGHT_CH, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_WET_RIGHT_CH);
			}
			HeadsetPan::Left => {
				cmd.aSetBuffer(0, inBuf, DMEM_ADDR_LEFT_CH, nSamples2);
				cmd.aSetBuffer(A_AUX, DMEM_ADDR_NOTE_PAN_TEMP, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_WET_RIGHT_CH);
			}
			HeadsetPan::Default => {
				cmd.aSetBuffer(0, inBuf, DMEM_ADDR_LEFT_CH, nSamples2);
				cmd.aSetBuffer(A_AUX, DMEM_ADDR_RIGHT_CH, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_WET_RIGHT_CH);
			}
		}
	} else {
		// It's a bit unclear what the "stereo strong" concept does.
		// Instead of mixing the opposite channel to the normal buffers, the sound is first
		// mixed into a temporary buffer and then subtracted from the normal buffer.
		if note.flags.contains(SF::StereoStrongRight) {
			cmd.aClearBuffer(DMEM_ADDR_STEREO_STRONG_TEMP_DRY as u32, DEFAULT_LEN_2CH);
			cmd.aSetBuffer(0, inBuf, DMEM_ADDR_STEREO_STRONG_TEMP_DRY, nSamples2);
			cmd.aSetBuffer(A_AUX, DMEM_ADDR_RIGHT_CH, DMEM_ADDR_STEREO_STRONG_TEMP_WET, DMEM_ADDR_WET_RIGHT_CH);
		} else if note.flags.contains(SF::StereoStrongLeft) {
			cmd.aClearBuffer(DMEM_ADDR_STEREO_STRONG_TEMP_DRY as u32, DEFAULT_LEN_2CH);
			cmd.aSetBuffer(0, inBuf, DMEM_ADDR_LEFT_CH, nSamples2);
			cmd.aSetBuffer(A_AUX, DMEM_ADDR_STEREO_STRONG_TEMP_DRY, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_STEREO_STRONG_TEMP_WET);
		} else {
			cmd.aSetBuffer(0, inBuf, DMEM_ADDR_LEFT_CH, nSamples2);
			cmd.aSetBuffer(A_AUX, DMEM_ADDR_RIGHT_CH, DMEM_ADDR_WET_LEFT_CH, DMEM_ADDR_WET_RIGHT_CH);
		}
	}

	let mixerFlags;
	if targetLeft == sourceLeft && targetRight == sourceRight && !note.flags.contains(SF::EnvMixerNeedsInit) {
		mixerFlags = A_CONTINUE;
	} else {
		mixerFlags = A_INIT;

		let rampLeft: i32 = ((*gCurrentLeftVolRamping)[targetLeft as usize >> 5] * (*gCurrentRightVolRamping)[sourceLeft as usize >> 5]) as i32;
		let rampRight: i32 = ((*gCurrentLeftVolRamping)[targetRight as usize >> 5] * (*gCurrentRightVolRamping)[sourceRight as usize >> 5]) as i32;

		cmd.aSetVolume(A_VOL | A_LEFT, sourceLeft, 0, 0);
		cmd.aSetVolume(A_VOL | A_RIGHT, sourceRight, 0, 0);
		cmd.aSetVolume32(A_RATE | A_LEFT, targetLeft, rampLeft as u32);
		cmd.aSetVolume32(A_RATE | A_RIGHT, targetRight, rampRight as u32);
		cmd.aSetVolume(A_AUX, gVolume as u16, 0, (note.reverbVol as u16) << 8);
	}

	if gUseReverb && note.reverbVol != 0 {
		cmd.aEnvMixer(mixerFlags | A_AUX, &mut (*synthesisState.synthesisBuffers).mixEnvelopeState);
		if note.flags.contains(SF::StereoStrongRight) {
			cmd.aSetBuffer(0, 0, 0, nSamples2);
			cmd.aMix(0, 0x8000, DMEM_ADDR_STEREO_STRONG_TEMP_DRY, DMEM_ADDR_LEFT_CH);
			cmd.aMix(0, 0x8000, DMEM_ADDR_STEREO_STRONG_TEMP_WET, DMEM_ADDR_WET_LEFT_CH);
		} else if note.flags.contains(SF::StereoStrongLeft) {
			cmd.aSetBuffer(0, 0, 0, nSamples2);
			cmd.aMix(0, 0x8000, DMEM_ADDR_STEREO_STRONG_TEMP_DRY, DMEM_ADDR_RIGHT_CH);
			cmd.aMix(0, 0x8000, DMEM_ADDR_STEREO_STRONG_TEMP_WET, DMEM_ADDR_WET_RIGHT_CH);
		}
	} else {
		cmd.aEnvMixer(mixerFlags, &mut (*synthesisState.synthesisBuffers).mixEnvelopeState);
		if note.flags.contains(SF::StereoStrongRight) {
			cmd.aSetBuffer(0, 0, 0, nSamples2);
			cmd.aMix(0, 0x8000, DMEM_ADDR_STEREO_STRONG_TEMP_DRY, DMEM_ADDR_LEFT_CH);
		} else if note.flags.contains(SF::StereoStrongLeft) {
			cmd.aSetBuffer(0, 0, 0, nSamples2);
			cmd.aMix(0, 0x8000, DMEM_ADDR_STEREO_STRONG_TEMP_DRY, DMEM_ADDR_RIGHT_CH);
		}
	}
}

unsafe fn note_apply_headset_pan_effects(
	cmd: &mut AudioCmdPtr,
	noteSubEu: &NoteSubEu,
	note: &mut NoteSynthesisState,
	bufLen: u16,
	flags: AudioFlags,
	leftRight: HeadsetPan,
) {
	let (dest, panShift, prevPanShift);
	match leftRight {
		HeadsetPan::Right => {
			dest = DMEM_ADDR_LEFT_CH;
			panShift = noteSubEu.headsetPanRight;
			note.prevHeadsetPanLeft = 0;
			prevPanShift = note.prevHeadsetPanRight;
			note.prevHeadsetPanRight = panShift;
		}
		HeadsetPan::Left => {
			dest = DMEM_ADDR_RIGHT_CH;
			panShift = noteSubEu.headsetPanLeft;
			note.prevHeadsetPanRight = 0;
			prevPanShift = note.prevHeadsetPanLeft;
			note.prevHeadsetPanLeft = panShift;
		}
		HeadsetPan::Default => return,
	}

	if flags != A_INIT {
		let pitch: u16;
		// Slightly adjust the sample rate in order to fit a change in pan shift
		if prevPanShift == 0 {
			// Kind of a hack that moves the first samples into the resample state
			cmd.aDMEMMove(DMEM_ADDR_NOTE_PAN_TEMP as u32, DMEM_ADDR_TEMP, 8);
			// Set pitch accumulator to 0 in the resample state
			cmd.aClearBuffer(8, 8);
			// (org notes:) No idea, result seems to be overwritten later
			cmd.aDMEMMove(DMEM_ADDR_NOTE_PAN_TEMP as u32, DMEM_ADDR_TEMP + 0x10, 0x10);

			cmd.aSetBuffer(0, 0, DMEM_ADDR_TEMP, 0x20);
			cmd.aSaveBuffer((*note.synthesisBuffers).panResampleState.as_mut_ptr().cast());

			pitch = (((bufLen as i32) << 0xf) / (bufLen as i32 + panShift as i32 - prevPanShift as i32 + 8)) as u16;
			cmd.aSetBuffer(0, DMEM_ADDR_NOTE_PAN_TEMP + 8, DMEM_ADDR_TEMP, panShift as u16 + bufLen as u16 - prevPanShift as u16);
			cmd.aResample(0, pitch, &mut (*note.synthesisBuffers).panResampleState);
		} else {
			if panShift == 0 {
				pitch = (((bufLen as i32) << 0xf) / (bufLen as i32 - prevPanShift as i32 - 4)) as u16;
			} else {
				pitch = (((bufLen as i32) << 0xf) / (bufLen as i32 + panShift as i32 - prevPanShift as i32)) as u16;
			}
			cmd.aSetBuffer(0, DMEM_ADDR_NOTE_PAN_TEMP, DMEM_ADDR_TEMP, panShift as u16 + bufLen as u16 - prevPanShift as u16);
			cmd.aResample(0, pitch, &mut (*note.synthesisBuffers).panResampleState);
		}

		if prevPanShift != 0 {
			cmd.aSetBuffer(0, DMEM_ADDR_NOTE_PAN_TEMP, 0, prevPanShift as u16);
			cmd.aLoadBuffer((*note.synthesisBuffers).panSamplesBuffer.as_ptr().cast());
			cmd.aDMEMMove(DMEM_ADDR_TEMP as u32, DMEM_ADDR_NOTE_PAN_TEMP + prevPanShift as u16, panShift as u16 + bufLen as u16 - prevPanShift as u16);
		} else {
			cmd.aDMEMMove(DMEM_ADDR_TEMP as u32, DMEM_ADDR_NOTE_PAN_TEMP, panShift as u16 + bufLen as u16 - prevPanShift as u16);
		}
	} else {
		// Just shift right
		cmd.aDMEMMove(DMEM_ADDR_NOTE_PAN_TEMP as u32, DMEM_ADDR_TEMP, bufLen);
		cmd.aDMEMMove(DMEM_ADDR_TEMP as u32, DMEM_ADDR_NOTE_PAN_TEMP + panShift as u16, bufLen);
		cmd.aClearBuffer(DMEM_ADDR_NOTE_PAN_TEMP as u32, panShift as usize);
	}

	if panShift != 0 {
		// Save excessive samples for next iteration
		cmd.aSetBuffer(0, 0, DMEM_ADDR_NOTE_PAN_TEMP + bufLen, panShift as u16);
		cmd.aSaveBuffer((*note.synthesisBuffers).panSamplesBuffer.as_mut_ptr().cast());
	}

	cmd.aSetBuffer(0, 0, 0, bufLen);
	cmd.aMix(0, 0x7fff, DMEM_ADDR_NOTE_PAN_TEMP, dest);
}
