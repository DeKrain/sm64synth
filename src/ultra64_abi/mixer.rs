//! Software implementation of the Ultra64 Audio Binary Interface

use super::*;
use cfg_if::cfg_if;

macro_rules! unword {
	($word:expr ; $shift:expr ; $width:expr) => {
		(($word >> $shift) as u32 & ((1u32 << $width) - 1)) as _
	};
}

pub unsafe fn process_audio_cmd(commands: &[Acmd]) {
	for cmd in commands {
		let op = cmd.0 >> 24;
		if false {
			let str = match op {
				A_SPNOOP => "A_SPNOOP",
				A_ADPCM => "A_ADPCM",
				A_CLEARBUFF => "A_CLEARBUFF",
				A_ENVMIXER => "A_ENVMIXER",
				A_LOADBUFF => "A_LOADBUFF",
				A_RESAMPLE => "A_RESAMPLE",
				A_SAVEBUFF => "A_SAVEBUFF",
				A_SEGMENT => "A_SEGMENT",
				A_SETBUFF => "A_SETBUFF",
				A_SETVOL => "A_SETVOL",
				A_DMEMMOVE => "A_DMEMMOVE",
				A_LOADADPCM => "A_LOADADPCM",
				A_MIXER => "A_MIXER",
				A_INTERLEAVE => "A_INTERLEAVE",
				A_POLEF => "A_POLEF",
				A_SETLOOP => "A_SETLOOP",
				_ => "",
			};
			if !str.is_empty() {
				println!("Exec cmd: {str}");
			}
		}
		match op {
			A_SPNOOP => {}
			A_ADPCM => aADPCMdec(unword!(cmd.0 ; 16 ; 8), cmd.1 as *mut ADPCM_STATE),
			A_CLEARBUFF => aClearBuffer(unword!(cmd.0 ; 0 ; 24), cmd.1 as u32),
			A_ENVMIXER => aEnvMixer(unword!(cmd.0 ; 16 ; 8), cmd.1 as *mut ENVMIX_STATE),
			A_LOADBUFF => aLoadBuffer(cmd.1 as *const ()),
			A_RESAMPLE => aResample(unword!(cmd.0 ; 16 ; 8), unword!(cmd.0 ; 0 ; 16), cmd.1 as *mut RESAMPLE_STATE),
			A_SAVEBUFF => aSaveBuffer(cmd.1 as *mut ()),
			A_SEGMENT => {}
			A_SETBUFF => aSetBuffer(
				unword!(cmd.0 ; 16 ; 8), unword!(cmd.0 ; 0 ; 16),
				unword!(cmd.1 ; 16 ; 16), unword!(cmd.1 ; 0 ; 16),
			),
			A_SETVOL => aSetVolume(
				unword!(cmd.0 ; 16 ; 16), unword!(cmd.0 ; 0 ; 16),
				unword!(cmd.1 ; 16 ; 16), unword!(cmd.1 ; 0 ; 16),
			),
			A_DMEMMOVE => aDMEMMove(unword!(cmd.0 ; 0 ; 24), unword!(cmd.1 ; 16 ; 16), unword!(cmd.1 ; 0 ; 16)),
			A_LOADADPCM => aLoadADPCM(unword!(cmd.0 ; 0 ; 24), cmd.1 as *const i16),
			A_MIXER => aMix(
				unword!(cmd.0 ; 16 ; 8), unword!(cmd.0 ; 0 ; 16),
				unword!(cmd.1 ; 16 ; 16), unword!(cmd.1 ; 0 ; 16),
			),
			A_INTERLEAVE => aInterleave(unword!(cmd.1 ; 16 ; 16), unword!(cmd.1 ; 0 ; 16)),
			A_POLEF => unimplemented!("Pole filter is unimplemented"),
			A_SETLOOP => aSetLoop(cmd.1 as *const ADPCM_STATE),
			_ => panic!("Unknown synthesis op 0x{op:02X}"),
		}
	}
}


use std::ptr::{copy_nonoverlapping as memcpy, copy as memmove};

macro_rules! align {
	($val:expr, $amnt:expr) => {
		($val + ($amnt - 1)) & !($amnt - 1)
	};
}

const BUF_SIZE: usize = 2512;

#[repr(C)]
union RspaBuffer {
	as_u8: [u8; BUF_SIZE],
	as_i16: [i16; BUF_SIZE / 2],
}

static mut rspa_buf: RspaBuffer = RspaBuffer { as_u8: [0; BUF_SIZE] };

impl RspaBuffer {
	#[inline]
	unsafe fn get_u8(&self, addr: u16) -> *const u8 {
		debug_assert!(addr < BUF_SIZE as u16);
		self.as_u8.as_ptr().add(addr as usize)
	}

	#[inline]
	unsafe fn get_u8_mut(&mut self, addr: u16) -> *mut u8 {
		debug_assert!(addr < BUF_SIZE as u16);
		self.as_u8.as_mut_ptr().add(addr as usize)
	}

	#[inline]
	unsafe fn get_i16(&self, addr: u16) -> *const i16 {
		debug_assert!(addr & 1 == 0 && addr < BUF_SIZE as u16);
		self.as_i16.as_ptr().add((addr >> 1) as usize)
	}

	#[inline]
	unsafe fn get_i16_mut(&mut self, addr: u16) -> *mut i16 {
		debug_assert!(addr & 1 == 0 && addr < BUF_SIZE as u16);
		self.as_i16.as_mut_ptr().add((addr >> 1) as usize)
	}
}

struct BufferConf {
	inp: u16,
	out: u16,
	count: u16,

	dry_right: u16,
	wet_left: u16,
	wet_right: u16,
}

static mut rspa_conf: BufferConf = BufferConf { inp: 0, out: 0, count: 0, dry_right: 0, wet_left: 0, wet_right: 0 };

static mut rspa_vol: [i16; 2] = [0; 2];
static mut rspa_vol_dry: i16 = 0;
static mut rspa_vol_wet: i16 = 0;

static mut rspa_target: [i16; 2] = [0; 2];
static mut rspa_rate: [i32; 2] = [0; 2];

static mut adpcm_loop_state: *const ADPCM_STATE = std::ptr::null();
static mut adpcm_table: [[[i16; 8]; 2]; 8] = [[[0; 8]; 2]; 8];

static resample_table: [[i16; 4]; 64] = include_data_lebe!("resample_table");

unsafe fn aClearBuffer(addr: u16, mut count: u32) {
	count = align!(count, 16);
	debug_assert!(addr as u32 <= BUF_SIZE as u32 - count);
	rspa_buf.get_u8_mut(addr).write_bytes(0, count as usize);
}

unsafe fn aLoadBuffer(mem_addr: *const ()) {
	memcpy(mem_addr.cast::<i16>(), rspa_buf.get_i16_mut(rspa_conf.inp), align!(rspa_conf.count as usize, 8) >> 1);
}

unsafe fn aSaveBuffer(buffer: *mut ()) {
	memcpy(rspa_buf.get_i16(rspa_conf.inp), buffer.cast::<i16>(), align!(rspa_conf.count as usize, 8) >> 1);
}

unsafe fn aSetBuffer(flags: AudioFlags, inp: u16, out: u16, byte_count: u16) {
	if flags & A_AUX != 0 {
		rspa_conf.dry_right = inp;
		rspa_conf.wet_left = out;
		rspa_conf.wet_right = byte_count;
	} else {
		rspa_conf.inp = inp;
		rspa_conf.out = out;
		rspa_conf.count = byte_count;

		debug_assert!(inp <= BUF_SIZE as u16);
		debug_assert!(out <= BUF_SIZE as u16);
		debug_assert!(inp + byte_count <= BUF_SIZE as u16);
		debug_assert!(out + byte_count <= BUF_SIZE as u16);
	}
}

unsafe fn aSetVolume(flags: AudioFlags, v: i16, t: i16, r: i16) {
	if flags & A_AUX != 0 {
		rspa_vol_dry = v;
		rspa_vol_wet = r;
	} else if flags & A_VOL != 0 {
		if flags & A_LEFT != 0 {
			rspa_vol[0] = v;
		} else {
			rspa_vol[1] = v;
		}
	} else {
		let idx = if flags & A_LEFT != 0 { 0 } else { 1 };
		rspa_target[idx] = v;
		rspa_rate[idx] = (((t as u16 as u32) << 16) | (r as u16 as u32)) as i32;
	}
}

unsafe fn aDMEMMove(source: u16, destination: u16, mut byte_count: u16) {
	byte_count = align!(byte_count, 16);
	//rspa_buf.as_u8.copy_within(source as usize .. (source + byte_count) as usize, destination as usize);
	memmove(rspa_buf.get_u8(source), rspa_buf.get_u8_mut(destination), byte_count as usize);
}

unsafe fn aLoadADPCM(count16: u32, book_addr: *const i16) {
	memcpy(book_addr.cast::<u8>(), adpcm_table.as_mut_ptr().cast::<u8>(), count16 as usize);
}

unsafe fn aInterleave(left: u16, right: u16) {
	let count = align!(rspa_conf.count, 16) as usize / 16;
	let (mut l, mut r, mut d) = (
		rspa_buf.get_i16_mut(left),
		rspa_buf.get_i16_mut(right),
		rspa_buf.get_i16_mut(rspa_conf.out),
	);
	for _ in 0..count {
		for idx in 0..8 {
			*d.add(2*idx + 0) = *l.add(idx);
			*d.add(2*idx + 1) = *r.add(idx);
		}
		d = d.add(2*8);
		l = l.add(8);
		r = r.add(8);
	}
}

unsafe fn aSetLoop(loop_state: *const ADPCM_STATE) {
	adpcm_loop_state = loop_state;
}


//// These functions can be SIMD-accelerated.

cfg_if! {
	// x86 platforms
	if #[cfg(all(
		//any(),
		any(target_arch = "x86_64", target_arch = "x86"),
		target_feature = "sse2",
	))] {
		#[cfg(target_arch = "x86_64")]
		use std::arch::x86_64::*;

		#[cfg(target_arch = "x86")]
		use std::arch::x86::*;

		//use std::arch::x86_64::{__m128i, _mm_setr_epi8, _mm_set_epi8};

		unsafe fn aADPCMdec(flags: AudioFlags, state: *mut ADPCM_STATE) {
			let tblrev: __m128i = _mm_setr_epi8(12, 13, 10, 11, 8, 9, 6, 7, 4, 5, 2, 3, 0, 1, -1, -1);
			let pos0: __m128i = _mm_set_epi8(3, -1, 3, -1, 2, -1, 2, -1, 1, -1, 1, -1, 0, -1, 0, -1);
			let pos1: __m128i = _mm_set_epi8(7, -1, 7, -1, 6, -1, 6, -1, 5, -1, 5, -1, 4, -1, 4, -1);
			let mult: __m128i = _mm_set_epi16(0x10, 0x01, 0x10, 0x01, 0x10, 0x01, 0x10, 0x01);
			let mask: __m128i = _mm_set1_epi16(0xf000u16 as i16);

			let mut inp = rspa_buf.get_u8(rspa_conf.inp);
			let mut out = rspa_buf.get_i16_mut(rspa_conf.out);
			let mut nbytes = align!(rspa_conf.count, 32);

			if false {
				print!("ADPCM dec flags:");
				if flags & A_INIT != 0 {
					print!(" init");
				}
				if flags & A_LOOP != 0 {
					print!(" loop");
				}
				if flags & !(A_INIT | A_LOOP) != 0 {
					print!(" invalid");
				}
				println!();
			}

			if flags & A_INIT != 0 {
				out.write_bytes(0, 16);
			} else if flags & A_LOOP != 0 {
				debug_assert!(!adpcm_loop_state.is_null(), "ADPCM loop state must be non-null");
				memcpy(adpcm_loop_state.cast::<i16>(), out, 16);
			} else {
				memcpy(state.cast::<i16>(), out, 16);
			}
			out = out.add(16);

			let mut prev_interleaved = _mm_set1_epi32(*out.offset(-2) as u16 as i32 | ((*out.offset(-1) as u16 as u32) << 16) as i32);

			//println!("Iter begin");
			while nbytes > 0 {
				//println!("iter cont...");
				let shift = *inp >> 4; // should be in 0..12
				let table_index = *inp & 0xf; // should be in 0..7
				debug_assert!(shift <= 12, "Shift should be in range 0..=12, is {shift}");
				debug_assert!(table_index < 8, "Table index should be in range 0..8, is {table_index}");
				inp = inp.add(1);

				let tbl = &adpcm_table[table_index as usize];

				let inv = _mm_loadu_si64(inp);
				let mut invec = [_mm_shuffle_epi8(inv, pos0), _mm_shuffle_epi8(inv, pos1)];
				let tblvec0 = _mm_loadu_si128(tbl[0].as_ptr().cast());
				let tblvec1 = _mm_loadu_si128(tbl[1].as_ptr().cast());
				let tbllo = _mm_unpacklo_epi16(tblvec0, tblvec1);
				let tblhi = _mm_unpackhi_epi16(tblvec0, tblvec1);
				let shiftcount = _mm_set_epi64x(0, 12 - shift as i64); // _mm_cvtsi64_si128 does not exist on 32-bit x86

				let mut tblvec1_rev: [__m128i; 8] = std::mem::zeroed();
				tblvec1_rev[0] = _mm_insert_epi16(_mm_shuffle_epi8(tblvec1, tblrev), 1 << 11, 7);
				tblvec1_rev[1] = _mm_bsrli_si128(tblvec1_rev[0], 2);
				tblvec1_rev[2] = _mm_bsrli_si128(tblvec1_rev[0], 4);
				tblvec1_rev[3] = _mm_bsrli_si128(tblvec1_rev[0], 6);
				tblvec1_rev[4] = _mm_bsrli_si128(tblvec1_rev[0], 8);
				tblvec1_rev[5] = _mm_bsrli_si128(tblvec1_rev[0], 10);
				tblvec1_rev[6] = _mm_bsrli_si128(tblvec1_rev[0], 12);
				tblvec1_rev[7] = _mm_bsrli_si128(tblvec1_rev[0], 14);
				inp = inp.add(8);

				for i in [0, 1] {
					let mut acc0 = _mm_madd_epi16(prev_interleaved, tbllo);
					let mut acc1 = _mm_madd_epi16(prev_interleaved, tblhi);
					let mut muls: [__m128i; 8] = std::mem::zeroed();
					let result: __m128i;
					invec[i] = _mm_sra_epi16(_mm_and_si128(_mm_mullo_epi16(invec[i], mult), mask), shiftcount);

					muls[7] = _mm_madd_epi16(tblvec1_rev[0], invec[i]);
					muls[6] = _mm_madd_epi16(tblvec1_rev[1], invec[i]);
					muls[5] = _mm_madd_epi16(tblvec1_rev[2], invec[i]);
					muls[4] = _mm_madd_epi16(tblvec1_rev[3], invec[i]);
					muls[3] = _mm_madd_epi16(tblvec1_rev[4], invec[i]);
					muls[2] = _mm_madd_epi16(tblvec1_rev[5], invec[i]);
					muls[1] = _mm_madd_epi16(tblvec1_rev[6], invec[i]);
					muls[0] = _mm_madd_epi16(tblvec1_rev[7], invec[i]);

					acc0 = _mm_add_epi32(acc0, _mm_hadd_epi32(_mm_hadd_epi32(muls[0], muls[1]), _mm_hadd_epi32(muls[2], muls[3])));
					acc1 = _mm_add_epi32(acc1, _mm_hadd_epi32(_mm_hadd_epi32(muls[4], muls[5]), _mm_hadd_epi32(muls[6], muls[7])));

					acc0 = _mm_srai_epi32(acc0, 11);
					acc1 = _mm_srai_epi32(acc1, 11);

					result = _mm_packs_epi32(acc0, acc1);
					_mm_storeu_si128(out.cast(), result);

					out = out.add(8);

					prev_interleaved = _mm_shuffle_epi32(result, _MM_SHUFFLE(3, 3, 3, 3));

					// Copied from `core_arch::x86::sse` because that version is unstable for some reason:
					/// A utility function for creating masks to use with Intel shuffle and
					/// permute intrinsics.
					#[inline]
					#[allow(non_snake_case)]
					const fn _MM_SHUFFLE(z: u32, y: u32, x: u32, w: u32) -> i32 {
						((z << 6) | (y << 4) | (x << 2) | w) as i32
					}
				}


				nbytes -= 16 * std::mem::size_of::<i16>() as u16;
			}

			memcpy(out.offset(-16), state.cast::<i16>(), 16);
		}

		unsafe fn aEnvMixer(flags: AudioFlags, state: *mut ENVMIX_STATE) {
			let mut inp = rspa_buf.get_i16(rspa_conf.inp);
			let mut dry = [rspa_buf.get_i16_mut(rspa_conf.out), rspa_buf.get_i16_mut(rspa_conf.dry_right)];
			let mut wet = [rspa_buf.get_i16_mut(rspa_conf.wet_left), rspa_buf.get_i16_mut(rspa_conf.wet_right)];
			let mut nbytes = align!(rspa_conf.count, 16);

			let mut vols: [[__m128; 2]; 2] = std::mem::zeroed();
			let dry_factor: __m128i;
			let wet_factor: __m128i;
			let mut target: [__m128; 2] = std::mem::zeroed();
			let mut rate: [__m128; 2] = std::mem::zeroed();
			let mut in_loaded: __m128i;
			let mut vol_s16: __m128i;
			let mut increasing: [bool; 2] = [false; 2];

			if flags & A_INIT != 0 {
				let vol_init = rspa_vol.map(|x| x as f32);
				let rate_float = rspa_rate.map(|x| x as f32 * (1.0 / 65536.0));
				let step_diff = [0, 1].map(|idx| vol_init[idx] * (rate_float[idx] - 1.0));

				for c in [0, 1] {
					vols[c][0] = _mm_add_ps(
						_mm_set_ps1(vol_init[c]),
						_mm_mul_ps(_mm_set1_ps(step_diff[c]), _mm_setr_ps(1.0 / 8.0, 2.0 / 8.0, 3.0 / 8.0, 4.0 / 8.0)));
					vols[c][1] = _mm_add_ps(
						_mm_set_ps1(vol_init[c]),
						_mm_mul_ps(_mm_set1_ps(step_diff[c]), _mm_setr_ps(5.0 / 8.0, 6.0 / 8.0, 7.0 / 8.0, 8.0 / 8.0)));

					increasing[c] = rate_float[c] >= 1.0;
					target[c] = _mm_set1_ps(rspa_target[c] as f32);
					rate[c] = _mm_set1_ps(rate_float[c]);
				}

				dry_factor = _mm_set1_epi16(rspa_vol_dry);
				wet_factor = _mm_set1_epi16(rspa_vol_wet);

				memcpy(rate_float.as_ptr(), state.cast::<i16>().add(32).cast::<f32>(), 2);
				memcpy(rspa_target.as_ptr(), state.cast::<i16>().add(36), 2);
				(*state)[38] = rspa_vol_dry;
				(*state)[39] = rspa_vol_wet;
			} else {
				let mut floats: [f32; 2] = [0.0; 2];
				vols[0][0] = _mm_loadu_ps(state.cast());
				vols[0][1] = _mm_loadu_ps((state.cast::<i16>().add(8)).cast());
				vols[1][0] = _mm_loadu_ps((state.cast::<i16>().add(16)).cast());
				vols[1][1] = _mm_loadu_ps((state.cast::<i16>().add(24)).cast());
				memcpy(state.cast::<i16>().add(32).cast(), floats.as_mut_ptr(), 2);
				rate[0] = _mm_set1_ps(floats[0]);
				rate[1] = _mm_set1_ps(floats[1]);
				increasing[0] = floats[0] >= 1.0;
				increasing[1] = floats[1] >= 1.0;
				target[0] = _mm_set1_ps((*state)[36] as f32);
				target[1] = _mm_set1_ps((*state)[37] as f32);
				dry_factor = _mm_set1_epi16((*state)[38]);
				wet_factor = _mm_set1_epi16((*state)[39]);
			}

			loop {
				in_loaded = _mm_loadu_si128(inp.cast());
				inp = inp.add(8);
				for c in [0, 1] {
					if increasing[c] {
						vols[c][0] = _mm_min_ps(vols[c][0], target[c]);
						vols[c][1] = _mm_min_ps(vols[c][1], target[c]);
					} else {
						vols[c][0] = _mm_max_ps(vols[c][0], target[c]);
						vols[c][1] = _mm_max_ps(vols[c][1], target[c]);
					}

					vol_s16 = _mm_packs_epi32(_mm_cvtps_epi32(vols[c][0]), _mm_cvtps_epi32(vols[c][1]));
					_mm_storeu_si128(dry[c].cast(),
						_mm_adds_epi16(
							_mm_loadu_si128(dry[c].cast()),
							_mm_mulhrs_epi16(in_loaded, _mm_mulhrs_epi16(vol_s16, dry_factor))));
					dry[c] = dry[c].add(8);

					if flags & A_AUX != 0 {
						_mm_storeu_si128(wet[c].cast(),
							_mm_adds_epi16(
								_mm_loadu_si128(wet[c].cast()),
								_mm_mulhrs_epi16(in_loaded, _mm_mulhrs_epi16(vol_s16, wet_factor))));
						wet[c] = wet[c].add(8);
					}

					vols[c][0] = _mm_mul_ps(vols[c][0], rate[c]);
					vols[c][1] = _mm_mul_ps(vols[c][1], rate[c]);
				}

				nbytes -= 8 * std::mem::size_of::<i16>() as u16;
				if nbytes <= 0 {
					break;
				}
			}

			_mm_storeu_ps(state.cast(), vols[0][0]);
			_mm_storeu_ps((state.cast::<i16>().add(8)).cast(), vols[0][1]);
			_mm_storeu_ps((state.cast::<i16>().add(16)).cast(), vols[1][0]);
			_mm_storeu_ps((state.cast::<i16>().add(24)).cast(), vols[1][1]);
		}

		unsafe fn aResample(flags: AudioFlags, pitch: u16, state: *mut RESAMPLE_STATE) {
			let inp_org = rspa_buf.get_i16(rspa_conf.inp);
			let mut inp = inp_org.cast_mut();
			let mut out = rspa_buf.get_i16_mut(rspa_conf.out);
			let mut nbytes = align!(rspa_conf.count, 16);
			let mut pitch_accumulator: u32;

			{
				use std::mem::MaybeUninit;
				let mut tmp: MaybeUninit<[i16; 16]> = MaybeUninit::uninit();
				if flags & A_INIT != 0 {
					tmp.as_mut_ptr().cast::<i16>().write_bytes(0, 5);
				} else {
					tmp.as_mut_ptr().copy_from(state, 1);
				}
				if flags & A_OUT != 0 {
					memcpy(tmp.as_ptr().cast::<i16>().add(8), inp.offset(-8), 8);
					inp = inp.offset(tmp.assume_init_ref()[5] as isize / std::mem::size_of::<i16>() as isize);
				}
				inp = inp.offset(-4);
				pitch_accumulator = tmp.assume_init_ref()[4] as u16 as u32;
				memcpy(tmp.as_ptr().cast::<i16>(), inp, 4);
			}

			let multiples = _mm_setr_epi16(0, 2, 4, 6, 8, 10, 12, 14);
			let pitchvec = _mm_set1_epi16(pitch as i16);
			let pitchvec_8_steps = _mm_set1_epi32(((pitch as i32) << 1) * 8);
			let pitchacclo_vec = _mm_set1_epi32(pitch_accumulator as i32 & 0xFFFF);
			let pl = _mm_mullo_epi16(multiples, pitchvec);
			let ph = _mm_mulhi_epu16(multiples, pitchvec);
			let mut acc_a = _mm_add_epi32(_mm_unpacklo_epi16(pl, ph), pitchacclo_vec);
			let mut acc_b = _mm_add_epi32(_mm_unpackhi_epi16(pl, ph), pitchacclo_vec);

			loop {
				let tbl_positions = _mm_srli_epi16(_mm_packus_epi32(
					_mm_and_si128(acc_a, _mm_set1_epi32(0xffff)),
					_mm_and_si128(acc_b, _mm_set1_epi32(0xffff))), 10);

				let in_positions = _mm_packus_epi32(_mm_srli_epi32(acc_a, 16), _mm_srli_epi32(acc_b, 16));
				let mut tbl_entries: [__m128i; 4] = std::mem::zeroed();
				let mut samples: [__m128i; 4] = std::mem::zeroed();

				macro_rules! LOADLH {
					($l:expr, $h:expr) => (_mm_castpd_si128(_mm_loadh_pd(_mm_load_sd($l.cast::<f64>()), $h.cast::<f64>())));
				}

				tbl_entries[0] = LOADLH!(resample_table[_mm_extract_epi16(tbl_positions, 0) as usize].as_ptr(), resample_table[_mm_extract_epi16(tbl_positions, 1) as usize].as_ptr());
				tbl_entries[1] = LOADLH!(resample_table[_mm_extract_epi16(tbl_positions, 2) as usize].as_ptr(), resample_table[_mm_extract_epi16(tbl_positions, 3) as usize].as_ptr());
				tbl_entries[2] = LOADLH!(resample_table[_mm_extract_epi16(tbl_positions, 4) as usize].as_ptr(), resample_table[_mm_extract_epi16(tbl_positions, 5) as usize].as_ptr());
				tbl_entries[3] = LOADLH!(resample_table[_mm_extract_epi16(tbl_positions, 6) as usize].as_ptr(), resample_table[_mm_extract_epi16(tbl_positions, 7) as usize].as_ptr());
				samples[0] = LOADLH!(inp.offset(_mm_extract_epi16(in_positions, 0) as isize), inp.offset(_mm_extract_epi16(in_positions, 1) as isize));
				samples[1] = LOADLH!(inp.offset(_mm_extract_epi16(in_positions, 2) as isize), inp.offset(_mm_extract_epi16(in_positions, 3) as isize));
				samples[2] = LOADLH!(inp.offset(_mm_extract_epi16(in_positions, 4) as isize), inp.offset(_mm_extract_epi16(in_positions, 5) as isize));
				samples[3] = LOADLH!(inp.offset(_mm_extract_epi16(in_positions, 6) as isize), inp.offset(_mm_extract_epi16(in_positions, 7) as isize));
				samples[0] = _mm_mulhrs_epi16(samples[0], tbl_entries[0]);
				samples[1] = _mm_mulhrs_epi16(samples[1], tbl_entries[1]);
				samples[2] = _mm_mulhrs_epi16(samples[2], tbl_entries[2]);
				samples[3] = _mm_mulhrs_epi16(samples[3], tbl_entries[3]);

				_mm_storeu_si128(out.cast(), _mm_hadds_epi16(_mm_hadds_epi16(samples[0], samples[1]), _mm_hadds_epi16(samples[2], samples[3])));

				acc_a = _mm_add_epi32(acc_a, pitchvec_8_steps);
				acc_b = _mm_add_epi32(acc_b, pitchvec_8_steps);

				out = out.add(8);
				nbytes -= 8 * std::mem::size_of::<i16>() as u16;
				if nbytes <= 0 {
					break;
				}
			}

			inp = inp.add(_mm_extract_epi16(acc_a, 1) as u16 as usize);
			pitch_accumulator = _mm_extract_epi16(acc_a, 0) as u32;

			(*state)[4] = pitch_accumulator as i16;
			memcpy(inp, state.cast::<i16>(), 4);
			let mut tmp = (inp.offset_from(inp_org) + 4) & 7;
			inp = inp.offset(-tmp);
			if tmp != 0 {
				tmp = -8 - tmp;
			}
			(*state)[5] = tmp as i16;
			memcpy(inp, state.cast::<i16>().add(8), 8);
		}

		unsafe fn aMix(_flags: AudioFlags, gain: i16, inp: u16, out: u16) {
			let mut nbytes = align!(rspa_conf.count, 32);
			let mut inp = rspa_buf.get_i16(inp);
			let mut out = rspa_buf.get_i16_mut(out);
			let gain_vec = _mm_set1_epi16(gain);

			if gain == -0x8000 {
				while nbytes > 0 {
					let (mut out1, mut out2, in1, in2);
					out1 = _mm_loadu_si128(out.cast());
					out2 = _mm_loadu_si128(out.add(8).cast());
					in1 = _mm_loadu_si128(inp.cast());
					in2 = _mm_loadu_si128(inp.add(8).cast());

					out1 = _mm_subs_epi16(out1, in1);
					out2 = _mm_subs_epi16(out2, in2);

					_mm_storeu_si128(out.cast(), out1);
					_mm_storeu_si128(out.add(8).cast(), out2);

					out = out.add(16);
					inp = inp.add(16);

					nbytes -= 16 * std::mem::size_of::<i16>() as u16;
				}
			}

			while nbytes > 0 {
				let (mut out1, mut out2, in1, in2);
				out1 = _mm_loadu_si128(out.cast());
				out2 = _mm_loadu_si128(out.add(8).cast());
				in1 = _mm_loadu_si128(inp.cast());
				in2 = _mm_loadu_si128(inp.add(8).cast());

				out1 = _mm_adds_epi16(out1, _mm_mulhrs_epi16(in1, gain_vec));
				out2 = _mm_adds_epi16(out2, _mm_mulhrs_epi16(in2, gain_vec));

				_mm_storeu_si128(out.cast(), out1);
				_mm_storeu_si128(out.add(8).cast(), out2);

				out = out.add(16);
				inp = inp.add(16);

				nbytes -= 16 * std::mem::size_of::<i16>() as u16;
			}
		}
	}
	// Fallback implementation
	else {
		unsafe fn aADPCMdec(flags: AudioFlags, state: *mut ADPCM_STATE) {
			let mut inp = rspa_buf.get_u8(rspa_conf.inp);
			let mut out = rspa_buf.get_i16_mut(rspa_conf.out);
			let mut nbytes = align!(rspa_conf.count, 32);

			if false {
				print!("ADPCM dec flags:");
				if flags & A_INIT != 0 {
					print!(" init");
				}
				if flags & A_LOOP != 0 {
					print!(" loop");
				}
				if flags & !(A_INIT | A_LOOP) != 0 {
					print!(" invalid");
				}
				println!();
			}

			if flags & A_INIT != 0 {
				out.write_bytes(0, 16);
			} else if flags & A_LOOP != 0 {
				debug_assert!(!adpcm_loop_state.is_null(), "ADPCM loop state must be non-null");
				memcpy(adpcm_loop_state.cast::<i16>(), out, 16);
			} else {
				memcpy(state.cast::<i16>(), out, 16);
			}
			out = out.add(16);

			//println!("Iter begin");
			while nbytes > 0 {
				//println!("iter cont...");
				let shift = *inp >> 4; // should be in 0..12
				let table_index = *inp & 0xf; // should be in 0..7
				debug_assert!(shift <= 12, "Shift should be in range 0..=12, is {shift}");
				debug_assert!(table_index < 8, "Table index should be in range 0..8, is {table_index}");
				inp = inp.add(1);

				let tbl = &adpcm_table[table_index as usize];

				for _ in 0..2 {
					let mut ins = [0i16; 8];
					let prev1 = *out.offset(-1);
					let prev2 = *out.offset(-2);

					for j in 0..4 {
						ins[j*2 + 0] = ((*inp >> 4) as i16) << 12 >> 12 << shift;
						ins[j*2 + 1] = ((*inp & 0xF) as i16) << 12 >> 12 << shift;
						inp = inp.add(1);
					}
					for j in 0..8 {
						let mut acc = tbl[0][j] as i32 * prev2 as i32 + tbl[1][j] as i32 * prev1 as i32 + ((ins[j] as i32) << 11);
						for k in 0..j {
							acc += tbl[1][j - k - 1] as i32 * ins[k] as i32;
						}
						acc >>= 11;
						*out = acc.clamp(i16::MIN as i32, i16::MAX as i32) as i16;
						out = out.add(1);
					}
				}


				nbytes -= 16 * std::mem::size_of::<i16>() as u16;
			}

			memcpy(out.offset(-16), state.cast::<i16>(), 16);
		}

		unsafe fn aEnvMixer(flags: AudioFlags, state: *mut ENVMIX_STATE) {
			let mut inp = rspa_buf.get_i16(rspa_conf.inp);
			let mut dry = [rspa_buf.get_i16_mut(rspa_conf.out), rspa_buf.get_i16_mut(rspa_conf.dry_right)];
			let mut wet = [rspa_buf.get_i16_mut(rspa_conf.wet_left), rspa_buf.get_i16_mut(rspa_conf.wet_right)];
			let mut nbytes = align!(rspa_conf.count, 16);

			let target: [i16; 2];
			let rate: [i32; 2];
			let vol_dry: i16;
			let vol_wet: i16;

			let step_diff: [i32; 2];
			let mut vols: [[i32; 8]; 2];

			if flags & A_INIT != 0 {
				target = rspa_target;
				rate = rspa_rate;
				vol_dry = rspa_vol_dry;
				vol_wet = rspa_vol_wet;
				step_diff = rate.map(|r| rspa_vol[0] as i32 * (r - 0x1_0000) / 8);
				vols = [0, 1].map(|chan|
						std::array::from_fn(
							|i| ((rspa_vol[chan] as i32) << 16).saturating_add(step_diff[chan].saturating_mul(i as i32 + 1))
				));
			} else {
				vols = std::mem::transmute_copy(&*state);
				target = [ (*state)[32], (*state)[35] ];
				rate = [
					(((*state)[33] as i32) << 16) | (*state)[34] as i32,
					(((*state)[36] as i32) << 16) | (*state)[37] as i32,
				];
				vol_dry = (*state)[38];
				vol_wet = (*state)[39];
			}

			loop {
				for c in [0, 1] {
					for i in 0..8 {
						let vol = &mut vols[c][i];
						let need_adjust = if (rate[c] >> 16) > 0 {
							// Increasing volume
							((*vol >> 16) as i16) > target[c]
						} else {
							// Decreasing volume
							((*vol >> 16) as i16) < target[c]
						};
						if need_adjust {
							*vol = (target[c] as i32) << 16;
						}
						let dry = dry[c].add(i);
						*dry = ((*dry as i32 * 0x7fff + *inp.add(i) as i32 * (((*vol >> 16) * vol_dry as i32 + 0x4000) >> 15) + 0x4000) >> 15).clamp(i16::MIN as i32, i16::MAX as i32) as i16;
						if flags & A_AUX != 0 {
							let wet = wet[c].add(i);
							*wet = ((*wet as i32 * 0x7fff + *inp.add(i) as i32 * (((*vol >> 16) * vol_wet as i32 + 0x4000) >> 15) + 0x4000) >> 15).clamp(i16::MIN as i32, i16::MAX as i32) as i16;
						}
						*vol = ((*vol as i64 * rate[c] as i64) >> 16).clamp(i32::MIN as i64, i32::MAX as i64) as i32;
					}

					dry[c] = dry[c].add(8);
					if flags & A_AUX != 0 {
						wet[c] = wet[c].add(8);
					}
				}

				inp = inp.add(8);
				nbytes -= 8 * std::mem::size_of::<i16>() as u16;
				if nbytes <= 0 {
					break;
				}
			}

			memcpy(&vols, state.cast(), 1);
			(*state)[32] = target[0];
			(*state)[35] = target[1];
			(*state)[33] = (rate[0] >> 16) as i16;
			(*state)[34] = rate[0] as i16;
			(*state)[36] = (rate[1] >> 16) as i16;
			(*state)[37] = rate[1] as i16;
			(*state)[38] = vol_dry;
			(*state)[39] = vol_wet;
		}

		unsafe fn aResample(flags: AudioFlags, pitch: u16, state: *mut RESAMPLE_STATE) {
			let inp_org = rspa_buf.get_i16(rspa_conf.inp);
			let mut inp = inp_org.cast_mut();
			let mut out = rspa_buf.get_i16_mut(rspa_conf.out);
			let mut nbytes = align!(rspa_conf.count, 16);
			let mut pitch_accumulator: u32;

			{
				use std::mem::MaybeUninit;
				let mut tmp: MaybeUninit<[i16; 16]> = MaybeUninit::uninit();
				if flags & A_INIT != 0 {
					tmp.as_mut_ptr().cast::<i16>().write_bytes(0, 5);
				} else {
					tmp.as_mut_ptr().copy_from(state, 1);
				}
				if flags & A_OUT != 0 {
					memcpy(tmp.as_ptr().cast::<i16>().add(8), inp.offset(-8), 8);
					inp = inp.offset(tmp.assume_init_ref()[5] as isize / std::mem::size_of::<i16>() as isize);
				}
				inp = inp.offset(-4);
				pitch_accumulator = tmp.assume_init_ref()[4] as u16 as u32;
				memcpy(tmp.as_ptr().cast::<i16>(), inp, 4);
			}

			loop {
				for _ in 0..8 {
					let tbl = resample_table[((pitch_accumulator as usize) << 6) >> 16];
					let sample: i32 = (0..3).map(
						|i| (*inp.add(i) as i32 * tbl[i] as i32 + 0x4000) >> 15
					).sum();
					*out = sample.clamp(i16::MIN as i32, i16::MAX as i32) as i16;
					out = out.add(1);

					pitch_accumulator += (pitch as u32) << 1;
					inp = inp.add(pitch_accumulator as usize >> 16);
					pitch_accumulator &= 0xffff;
				}

				nbytes -= 8 * std::mem::size_of::<i16>() as u16;
				if nbytes <= 0 {
					break;
				}
			}

			(*state)[4] = pitch_accumulator as i16;
			memcpy(inp, state.cast::<i16>(), 4);
			let mut tmp = (inp.offset_from(inp_org) + 4) & 7;
			inp = inp.offset(-tmp);
			if tmp != 0 {
				tmp = -8 - tmp;
			}
			(*state)[5] = tmp as i16;
			memcpy(inp, state.cast::<i16>().add(8), 8);
		}

		unsafe fn aMix(_flags: AudioFlags, gain: i16, inp: u16, out: u16) {
			let mut nbytes = align!(rspa_conf.count, 32);
			let mut inp = rspa_buf.get_i16(inp);
			let mut out = rspa_buf.get_i16_mut(out);
			

			if gain == -0x8000 {
				while nbytes > 0 {
					for i in 0..16 {
						*out.add(i) = (*out.add(i)).saturating_sub(*inp.add(i));
					}

					out = out.add(16);
					inp = inp.add(16);

					nbytes -= 16 * std::mem::size_of::<i16>() as u16;
				}
			}

			while nbytes > 0 {
				for i in 0..16 {
					let sample = ((*out.add(i) as i32 * 0x7fff + *inp.add(i) as i32 * gain as i32) + 0x4000) >> 15;
					*out.add(i) = sample.clamp(i16::MIN as i32, i16::MAX as i32) as i16;
				}

				out = out.add(16);
				inp = inp.add(16);

				nbytes -= 16 * std::mem::size_of::<i16>() as u16;
			}
		}
	}
}
