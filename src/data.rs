use crate::structs::*;
use crate::ultra64_abi::Acmd;
use crate::ultra::SPTask;

use std::sync::atomic::{Ordering, AtomicU32};

pub const NUMAIBUFFERS: usize = 3;
pub const AIBUFFER_LEN: usize = 0xA00;

pub struct AudioLock {
	lock: AtomicU32,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct AudioLockState(u32);

impl AudioLock {
	const VAL_UNINITIALIZED: u32 = 0;
	const VAL_NOT_LOADING: u32 = 0x76557364;
	const VAL_LOADING: u32 = 0x19710515;

	pub const UNINITIALIZED: AudioLockState = AudioLockState(Self::VAL_UNINITIALIZED);
	pub const NOT_LOADING: AudioLockState = AudioLockState(Self::VAL_NOT_LOADING);
	pub const LOADING: AudioLockState = AudioLockState(Self::VAL_LOADING);

	pub const INIT: Self = AudioLock { lock: AtomicU32::new(Self::VAL_UNINITIALIZED) };
	pub fn set(&self, state: AudioLockState) {
		self.lock.store(state.0, Ordering::SeqCst);
	}
}

macro_rules! fill_struct_array {
	[
		$name:ident $members:tt
		$($body:tt),* $(,)?
	] => {
		[ $(
			fill_struct! {
				$name $members
				$body
			}
		),* ]
	};
}

macro_rules! fill_struct {
	{
		$name:ident { $($member: ident)+ }
		{
			$($val: expr),* $(,)?
		}
	} => {
		$name { $($member: $val),+ }
	};
}

#[no_mangle] pub static gTatumsPerBeat: i16 = TATUMS_PER_BEAT;
#[no_mangle] pub static gAudioLoadLock: AudioLock = AudioLock::INIT;

//// .bss


zeroed! {
	#[no_mangle] pub static mut gAudioFrameCount: u32;
	#[no_mangle] pub static mut gCurrAudioFrameDmaCount: usize;
	#[no_mangle] pub static mut gAudioTaskIndex: i32;
	#[no_mangle] pub static mut gCurrAiBufferIndex: i32;

	#[no_mangle] pub static mut gAudioCmdBuffers: [*mut Acmd; 2];
	#[no_mangle] pub static mut gAudioCmd: *mut u64;
}

#[no_mangle] pub static mut gAudioTask: *mut SPTask = std::ptr::null_mut();
#[no_mangle] pub static mut gAudioTasks: [SPTask; 2] = [SPTask::INIT; 2];

#[no_mangle] pub static mut gAproxRefreshInterval: f32 = 0.0;
zeroed! {
	#[no_mangle] pub static mut gRefreshRate: u32;

	#[no_mangle] pub static mut gAiBuffers: [*mut i16; NUMAIBUFFERS];
	#[no_mangle] pub static mut gAiBufferLengths: [i16; NUMAIBUFFERS];

	#[no_mangle] pub static mut gAudioRandom: u32;
	#[no_mangle] pub static mut gAudioErrorFlags: i32;
}

// EU

static sReverbSettings: [ReverbSettingsEU; 6] = fill_struct_array![
	ReverbSettingsEU { downsampleRate windowSize gain }
	{ 0x04, 0x0c, 0x2fff },
	{ 0x04, 0x0a, 0x47ff },
	{ 0x04, 0x10, 0x2fff },
	{ 0x04, 0x0e, 0x3fff },
	{ 0x04, 0x0c, 0x4fff },
	{ 0x04, 0x0a, 0x37ff },
];

use std::slice::from_ref as slice_one;

pub const AI_SAMPLE_RATE: u32 = 0x00007d00;

#[no_mangle]
pub static gAudioSessionPresets: [AudioSessionSettingsEU; 8] = fill_struct_array![
	AudioSessionSettingsEU {frequency unk1 maxSimultaneousNotes unk2 reverbSettings volume unk3 persistentSeqMem persistentBankMem temporarySeqMem temporaryBankMem }
	{ AI_SAMPLE_RATE, 0x01, 0x10, 0x00, slice_one(&sReverbSettings[0]), 0x7fff, 0x0000, 0x00003a40, 0x00006d00,
	0x00004400, 0x00002a00 },
	{ AI_SAMPLE_RATE, 0x01, 0x10, 0x00, slice_one(&sReverbSettings[1]), 0x7fff, 0x0000, 0x00003a40, 0x00006d00,
	0x00004400, 0x00002a00 },
	{ AI_SAMPLE_RATE, 0x01, 0x10, 0x00, slice_one(&sReverbSettings[2]), 0x7fff, 0x0000, 0x00003a40, 0x00006d00,
	0x00004400, 0x00002a00 },
	{ AI_SAMPLE_RATE, 0x01, 0x10, 0x00, slice_one(&sReverbSettings[3]), 0x7fff, 0x0000, 0x00003a40, 0x00006d00,
	0x00004400, 0x00002a00 },
	{ AI_SAMPLE_RATE, 0x01, 0x10, 0x00, slice_one(&sReverbSettings[4]), 0x7fff, 0x0000, 0x00003a40, 0x00006d00,
	0x00004400, 0x00002a00 },
	{ AI_SAMPLE_RATE, 0x01, 0x10, 0x00, slice_one(&sReverbSettings[0]), 0x7fff, 0x0000, 0x00004000, 0x00006e00,
	0x00003f00, 0x00002a00 },
	{ AI_SAMPLE_RATE, 0x01, 0x10, 0x00, slice_one(&sReverbSettings[1]), 0x7fff, 0x0000, 0x00004100, 0x00006e00,
	0x00004400, 0x00002a80 },
	{ AI_SAMPLE_RATE, 0x01, 0x14, 0x00, slice_one(&sReverbSettings[5]), 0x7fff, 0x0000, 0x00003500, 0x00006280,
	0x00004000, 0x00001b00 }
];

/* US/JP

#[no_mangle]
pub static gAudioSessionPresets: [AudioSessionSettings; 18] = fill_struct_array![
	AudioSessionSettings { frequency maxSimultaneousNotes reverbDownsampleRate reverbWindowSize reverbGain volume persistentSeqMem persistentBankMem temporarySeqMem temporaryBankMem }
	{ AI_SAMPLE_RATE, 16, 1, 0x0C00, 0x2FFF, 0x7FFF, 0x3A00, 0x6D00, 0x4400, 0x2A00 },
	{ AI_SAMPLE_RATE, 16, 1, 0x0A00, 0x47FF, 0x7FFF, 0x3A00, 0x6D00, 0x4400, 0x2A00 },
	{ AI_SAMPLE_RATE, 16, 1, 0x1000, 0x2FFF, 0x7FFF, 0x3A00, 0x6D00, 0x4400, 0x2A00 },
	{ AI_SAMPLE_RATE, 16, 1, 0x0E00, 0x3FFF, 0x7FFF, 0x3A00, 0x6D00, 0x4400, 0x2A00 },
	{ AI_SAMPLE_RATE, 16, 1, 0x0C00, 0x4FFF, 0x7FFF, 0x3A00, 0x6D00, 0x4400, 0x2A00 },
	{ AI_SAMPLE_RATE, 16, 1, 0x0C00, 0x2FFF, 0x7FFF, 0x4000, 0x6E00, 0x3F00, 0x2A00 },
	{ AI_SAMPLE_RATE, 16, 1, 0x0A00, 0x47FF, 0x7FFF, 0x4100, 0x6E00, 0x4400, 0x2A80 },
	{ AI_SAMPLE_RATE, 20, 1, 0x0800, 0x37FF, 0x7FFF, 0x34C0, 0x6280, 0x4000, 0x1B00 },
	// The following 5 entries have a different sampling rate, but presets
	// with index above 7 don't seem to be used anywhere in the game.
	{ 27000, 16, 1, 0x0800, 0x2FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ 27000, 16, 1, 0x0800, 0x3FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ 27000, 16, 1, 0x1000, 0x2FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ 27000, 16, 1, 0x1000, 0x3FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ 27000, 16, 1, 0x0C00, 0x4FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ AI_SAMPLE_RATE, 14, 1, 0x0800, 0x2FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ AI_SAMPLE_RATE, 12, 1, 0x0800, 0x2FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ AI_SAMPLE_RATE, 10, 1, 0x0800, 0x2FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ AI_SAMPLE_RATE, 8, 1, 0x0800, 0x2FFF, 0x7FFF, 0x2500, 0x5500, 0x7400, 0x2400 },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
];
*/

/// Transforms a pitch scale factor in -127..=127 into a frequency scale factor
/// between -1 and +1 octave.
/// `gPitchBendFrequencyScale[k] = 0.5 * 2**(k/127)`
#[no_mangle] pub static gPitchBendFrequencyScale: [f32; 0x100] = include_data_lebe!("pitch_bend_frequency_scale");

/// Frequencies for notes using the standard twelve-tone equal temperament scale.
/// For indices 0..117, gNoteFrequencies[k] = 2**((k-39) / 12).
/// For indices 117..128, gNoteFrequencies[k] = 0.5 * 2**((k-39) / 12).
/// The 39 in the formula refers to piano key 40 (middle C, at 256 Hz) being
/// the reference frequency, which is assigned value 1.
#[no_mangle] pub static gNoteFrequencies: [f32; 0x80] = include_data_lebe!("note_frequencies");

/// Goes up by ~12 at each step for the first 4 values (starting from 0), then by ~6.
#[no_mangle] pub static gDefaultShortNoteVelocityTable: [u8; 0x10] = [
	12, 25, 38, 51, 57, 64, 71, 76, 83, 89, 96, 102, 109, 115, 121, 127,
];

/// Goes down by 26 at each step for the first 4 values (starting from 255), then by ~12.
#[no_mangle] pub static gDefaultShortNoteDurationTable: [u8; 0x10] = [
	229, 203, 177, 151, 139, 126, 113, 100, 87, 74, 61, 48, 36, 23, 10, 0,
];

#[no_mangle] pub static gDefaultEnvelope: [AdsrEnvelope; 3] = [
	// Go from 0 to 32000 over the course of 16ms;
	AdsrEnvelope::new(4i16, 32000i16),
	// Stay there for 4.16 seconds;
	AdsrEnvelope::new(1000i16, 32000i16),
	// Then continue staying there.
	AdsrEnvelope::new(AdsrEnvelope::DELAY_HANG, 0),
];

pub const gZeroNoteSub: NoteSubEu = NoteSubEu {
	flags: NoteSubEuFlags::empty(),
	bankId: 0,
	headsetPanLeft: 0,
	headsetPanRight: 0,
	reverbVol: 0,
	targetVolLeft: 0,
	targetVolRight: 0,
	resamplingRateFixedPoint: 0,
	sound: NoteSubEuSound {
		samples: std::ptr::null(),
	},
};

pub const gDefaultNoteSub: NoteSubEu = NoteSubEu {
	flags: NoteSubEuFlags::Enabled.union(NoteSubEuFlags::NeedsInit),
	bankId: 0,
	headsetPanLeft: 0,
	headsetPanRight: 0,
	reverbVol: 0,
	targetVolLeft: 0,
	targetVolRight: 0,
	resamplingRateFixedPoint: 0,
	sound: NoteSubEuSound {
		samples: std::ptr::null(),
	},
};

// Each wave sample contains 4 sections for 4 octaves
pub const WAVE_CYCLE_SAMPLES: u16 = 0x40;
pub const WAVE_CYCLE_SAMPLES_SHIFT: u8 = 6;
pub const WAVE_CYCLE_SAMPLES_MASK: u16 = WAVE_CYCLE_SAMPLES - 1;

#[no_mangle] static sSawtoothWaves: [i16; 0x100] = include_data_lebe!("sawtooth_waves");
#[no_mangle] static sTriangleWaves: [i16; 0x100] = include_data_lebe!("triangle_waves");
#[no_mangle] static sSineWaves: [i16; 0x100] = include_data_lebe!("sine_waves");
#[no_mangle] static sSquareWaves: [i16; 0x100] = include_data_lebe!("square_waves");
#[no_mangle] static sEqUnknown6Waves: [i16; 0x100] = include_data_lebe!("eu_unknown6_waves");
#[no_mangle] static sEqUnknown7Waves: [i16; 0x100] = include_data_lebe!("eu_unknown7_waves");

#[no_mangle] pub static gWaveSamples: [&'static [i16; 0x100]; 6] = [
	&sSawtoothWaves,
	&sTriangleWaves,
	&sSineWaves,
	&sSquareWaves,
	&sEqUnknown6Waves,
	&sEqUnknown7Waves,
];

#[allow(dead_code)] pub const WAVE_SAWTOOTH: usize = 0;
#[allow(dead_code)] pub const WAVE_TRIANGLE: usize = 1;
#[allow(dead_code)] pub const WAVE_SINE: usize = 2;
#[allow(dead_code)] pub const WAVE_SQUARE: usize = 3;
#[allow(dead_code)] pub const WAVE_EU_UNK_6: usize = 4;
#[allow(dead_code)] pub const WAVE_EU_UNK_7: usize = 5;

// This is just 1 << (6 - OctaveIndex)
#[no_mangle] pub static gOctaveScalar: [u8; 4] = [ 0x40, 0x20, 0x10, 0x08 ];

#[no_mangle] pub static gHeadsetPanQuantization: [u16; 0x10] = [
	0x40, 0x40, 0x30, 0x30, 0x20, 0x20, 0x10, 0x00, 0x00, 0x00,
	0, 0, 0, 0, 0, 0,
];

#[no_mangle] pub static euUnknownData_80301950: [i16; 0x40] = [
	0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0,
	0, 0, 0, 500, 0, 0, 0, 0, 0, 0, 0, 500, 0, 0, 0, 0, 0, 0, 0, 500, 0, 0, 0, 0, 0, 0, 0, 500, 0, 0, 0, 0,
];

/// Linearly interpolated between
/// - `f(0/2 * 127)` = `1`
/// - `f(1/2 * 127)` = `1/sqrt(2)`
/// - `f(2/2 * 127)` = `0`
#[no_mangle] pub static gHeadsetPanVolume: [f32; 0x80] = include_data_lebe!("pan_volume/headset");

/// Linearly interpolated between
/// - `f(0/4 * 127)` = `1/sqrt(2)`
/// - `f(1/4 * 127)` = `1`
/// - `f(2/4 * 127)` = `1/sqrt(2)`
/// - `f(3/4 * 127)` = `0`
/// - `f(4/4 * 127)` = `1/sqrt(8)`
#[no_mangle] pub static gStereoPanVolume: [f32; 0x80] = include_data_lebe!("pan_volume/stereo");

/// `cos(π/2 * k / 127)`
#[no_mangle] pub static gDefaultPanVolume: [f32; 0x80] = include_data_lebe!("pan_volume/default");
