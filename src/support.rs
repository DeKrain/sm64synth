//! Additional types and traits to aid with development.

#![allow(dead_code, unused_macros)]

macro_rules! group {
	($($items:item)*) => {
		$($items)*
	};
}

#[cfg(feature = "nightly")]
group! {
	extern {
		/// Marker for Dynamically Sized Types that __do not__ require fat
		/// pointer metadata.
		pub type DynamicSizeMarker;
	}

	pub use std::ptr::Thin as DynThin;
}

#[cfg(not(feature = "nightly"))]
group! {
	// Have to resort to hacks without thin DSTs
	#[cfg(not(feature = "nightly"))]
	enum _DSMInner {}
	
	#[cfg(not(feature = "nightly"))]
	/// Marker for Dynamically Sized Types that __do not__ require fat
	/// pointer metadata.
	pub struct DynamicSizeMarker(_DSMInner);

	pub trait DynThin: Sized {}
	impl<T: Sized> DynThin for T {}
}

macro_rules! static_assert {
	($cond:expr $(, $message:literal)? $(,)?) => {
		const _: () = assert!($cond $(, $message)?);
	};
}
pub(crate) use static_assert;

/// Pointer that points to device/physical memory, used to communicate with
/// the RSP on N64.
pub type DevPtr<T> = *mut T;
pub type DevPtrRO<T> = *const T;

/// Dynamic sized array member.
/// Must be put as the last member of a repr `C`/`sequential` struct.
///
/// It provides indexing & pointer offset operations for cleaner programming.
#[repr(C)]
pub struct DSArray<T: Sized> {
	_array: [T; 0],
	_marker: DynamicSizeMarker,
}

unsafe impl<T: Sync> Sync for DSArray<T> {}

use std::ptr::NonNull;

impl<T> DSArray<T> {
	/// # Safety
	/// `ptr` must be a valid, non-null pointer to an array.
	pub unsafe fn new<'l>(ptr: *const T) -> &'l Self {
		debug_assert!(!ptr.is_null());
		unsafe { &*(ptr as *const Self) }
	}

	/// # Safety
	/// `ptr` must be a valid, non-null pointer to an array.
	pub unsafe fn new_mut<'l>(ptr: *mut T) -> &'l mut Self {
		debug_assert!(!ptr.is_null());
		unsafe { &mut *(ptr as *mut Self) }
	}

	/// # Safety
	/// `ptr` must be valid.
	pub const unsafe fn new_non_null<'l>(ptr: NonNull<T>) -> &'l Self {
		unsafe { &*(ptr.as_ptr() as *const Self) }
	}

	/// # Safety
	/// `ptr` must be valid.
	pub unsafe fn new_non_null_mut<'l>(ptr: NonNull<T>) -> &'l mut Self {
		unsafe { &mut *(ptr.as_ptr() as *mut Self) }
	}

	/// Gets a pointer to array elements.
	#[inline(always)]
	pub const fn as_ptr(&self) -> *const T {
		self._array.as_ptr()
	}

	/// Gets a mutable pointer to array elements.
	#[inline(always)]
	pub fn as_mut_ptr(&mut self) -> *mut T {
		self._array.as_mut_ptr()
	}

	/// Calculates a pointer to the corresponding array element.
	#[inline]
	pub unsafe fn offset(&self, index: usize) -> *const T {
		self.as_ptr().add(index)
	}

	/// Calculates a mutable pointer to the corresponding array element.
	#[inline]
	pub unsafe fn offset_mut(&mut self, index: usize) -> *mut T {
		self.as_mut_ptr().add(index)
	}

	/// Obtains a reference to the corresponding array element.
	/// `index` **must** be within array bounds.
	#[inline]
	pub unsafe fn index(&self, index: usize) -> &T {
		&*self.offset(index)
	}

	/// Obtains a mutable reference to the corresponding array element.
	/// `index` **must** be within array bounds.
	#[inline]
	pub unsafe fn index_mut(&mut self, index: usize) -> &mut T {
		&mut *self.offset_mut(index)
	}

	/// Obtains a slice to the array's contents, assuming it contains
	/// at least `length` elements.
	#[inline]
	pub unsafe fn as_slice(&self, length: usize) -> &[T] {
		std::slice::from_raw_parts(self.as_ptr(), length)
	}

	/// Obtains a mutable slice to the array's contents, assuming it contains
	/// at least `length` elements.
	#[inline]
	pub unsafe fn as_mut_slice(&mut self, length: usize) -> &mut [T] {
		std::slice::from_raw_parts_mut(self.as_mut_ptr(), length)
	}

	/// Reads (copies) an array element at the corresponding index.
	///
	/// # Safety
	/// Since the array is of unknown size, `index` is not bound checked.
	/// It is the caller's responsibility that `index` is in allocated array range.
	pub unsafe fn get(&self, index: usize) -> T
	where T: Copy
	{
		self.offset(index).read()
	}
}

impl<T> DSArray<T> {
	/// Gets a pointer to array elements without dereferencing a pointer.
	///
	/// This function can be used where obtaining a mutable reference and back
	/// with [`as_ptr`] would be unsafe, such as when dealing with unitialized memory.
	///
	/// [`as_ptr`]: DSArray::as_ptr
	pub fn ptr_to_inner(ptr: *const Self) -> *const T {
		ptr.cast()
	}

	/// Gets a mutable pointer to array elements without dereferencing a pointer.
	///
	/// This function can be used where obtaining a mutable reference and back
	/// with [`as_mut_ptr`] would be unsafe, such as when dealing with unitialized memory.
	///
	/// [`as_mut_ptr`]: DSArray::as_ptr
	pub fn ptr_to_inner_mut(ptr: *mut Self) -> *mut T {
		ptr.cast()
	}
}

/// Helper function for [`zeroed`].
pub const fn __zeroed<T: Sized, const Sz: usize>() -> T {
	unsafe {
		union Zeroed<T, const Sz: usize> { val: std::mem::ManuallyDrop<T>, bytes: [u8; Sz] }
		let uninit = Zeroed { bytes: [0; Sz] };
		std::mem::ManuallyDrop::into_inner(uninit.val)
	}
}
/*pub const fn __zeroed<T: Sized>() -> T {
	let arr = [0; std::mem::size_of::<T>()];
	unsafe { std::mem::transmute_copy(&arr) }
}*/

/// Declares a section of global variables that are zeroed on startup.
/// It's used until _const_ version of [`core::mem::zeroed`] gets stabilized.
macro_rules! zeroed {
	($($(#[$meta:meta])* $vis:vis static mut $name:ident : $type:ty;)*) => {
		$($(#[$meta])* $vis static mut $name: $type = crate::support::__zeroed::<$type, {std::mem::size_of::<$type>()}>();)*
		//$($(#[$meta])* $vis static mut $name: $type = crate::support::__zeroed();)*
	};
}

pub trait PtrExt {
	type Ptr<U: ?Sized>;

	/// Extension method, because `pointer::cast` doesn't support `?Sized + Thin`
	/// types *sigh*.
	fn cast_thin<U: ?Sized + DynThin>(self) -> Self::Ptr<U>;
}

#[cfg(feature = "nightly")]
macro_rules! impl_ptr_ext {
	($mut:ident $from_raw_parts:ident) => {
		impl<T: ?Sized + DynThin> PtrExt for *$mut T {
			type Ptr<U: ?Sized> = *$mut U;
		
			#[inline(always)]
			fn cast_thin<U: ?Sized + DynThin>(self) -> *$mut U {
				std::ptr::$from_raw_parts(self.cast(), ())
			}
		}
	};
}

#[cfg(not(feature = "nightly"))]
macro_rules! impl_ptr_ext {
	($mut:ident $from_raw_parts:ident) => {
		impl<T> PtrExt for *$mut T {
			type Ptr<U: ?Sized> = *$mut U;
		
			#[inline(always)]
			fn cast_thin<U>(self) -> *$mut U {
				self.cast()
			}
		}
	};
}

impl_ptr_ext!(const from_raw_parts);
impl_ptr_ext!(mut from_raw_parts_mut);

#[cfg(target_endian = "little")]
macro_rules! data_endian_suffix {
	() => (".le.bin")
}

#[cfg(target_endian = "big")]
macro_rules! data_endian_suffix {
	() => (".be.bin")
}

macro_rules! data_path_prefix {
	() => (concat!(env!("CARGO_MANIFEST_DIR"), "/data/"));
}

macro_rules! include_data_lebe {
	($name: expr) => {
		unsafe { ::std::mem::transmute(*include_bytes!(concat!(data_path_prefix!(), $name, data_endian_suffix!()))) }
	};
	(@byte_size $name: expr) => {
		include_bytes!(concat!(data_path_prefix!(), $name, data_endian_suffix!())).len()
	};
}


macro_rules! include_data_agn {
	($name: expr) => {
		unsafe { ::std::mem::transmute(*include_bytes!(concat!(data_path_prefix!(), $name, ".bin"))) }
	};
	(@byte_size $name: expr) => {
		unsafe { include_bytes!(concat!(data_path_prefix!(), $name)).len() }
	};
	(@raw $name: expr) => {
		unsafe { *include_bytes!(concat!(data_path_prefix!(), $name)) }
	};
}
