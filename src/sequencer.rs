use crate::{
	support::PtrExt,
	structs::*,
	load::*,
	heap::*,
	data::*,
	playback::*,
	ultra,
};

use std::ptr;

const PORTAMENTO_MODE_ONE_SHOT_TARGET: u8 = 1;
const PORTAMENTO_MODE_ONE_SHOT_NOTE: u8 = 2;
const PORTAMENTO_MODE_HOLD_TARGET: u8 = 3;
const PORTAMENTO_MODE_HOLD_NOTE: u8 = 4;
const PORTAMENTO_MODE_HOLD_TARGET_SINGLE: u8 = 5;

impl SequenceChannelLayer {
	pub unsafe fn disable(&mut self) {
		self.note_decay();
		self.flags -= SequenceChannelLayerFlags::Enabled;
		self.flags |= SequenceChannelLayerFlags::Finished;
	}
}

impl SequenceChannel {
	pub unsafe fn disable(&mut self) {
		for i in 0..LAYERS_MAX {
			self.free_layer(i);
		}
		self.notePool.clear();
		self.flags -= SequenceChannelFlags::Enabled;
		self.flags |= SequenceChannelFlags::Finished;
	}
}

impl SequencePlayer {
	pub unsafe fn disable(&mut self) {
		self.disable_channels(!0);
		self.notePool.clear();
		self.flags |= SequencePlayerFlags::Finished;
		self.flags -= SequencePlayerFlags::Enabled;
		if gSeqLoadStatus[self.seqId as usize].is_load_complete() {
			gSeqLoadStatus[self.seqId as usize] = SoundLoadStatus::Discardable;
		}
		if gBankLoadStatus[self.defaultBank as usize].is_load_complete() {
			gBankLoadStatus[self.defaultBank as usize] = SoundLoadStatus::Discardable;
		}
		// (Note that if this is called from alloc_bank_or_seq, the side will get swapped
		// later in that function. Thus, we signal that we want to load into the slot
		// of the bank that we no longer need.)
		if self.defaultBank as i32 == gBankLoadedPool.temporary.entries[0].id {
			gBankLoadedPool.temporary.nextSide = 1;
		} else if self.defaultBank as i32 == gBankLoadedPool.temporary.entries[1].id {
			gBankLoadedPool.temporary.nextSide = 0;
		}
	}
}

impl AudioListItem {
	pub unsafe fn push_back(&mut self, item: &mut Self) {
		if !item.prev.is_null() {
			panic!("Error: item is already in a list");
		}
		unsafe { &mut *self.prev}.next = item;
		item.prev = self.prev;
		item.next = self;
		self.prev = item;
		self.u.count += 1;
		item.pool = self.pool;
	}

	pub unsafe fn pop_back_raw(&mut self) -> *mut () {
		if ptr::eq(self, self.prev) {
			return ptr::null_mut();
		}
		let item = unsafe { &mut *self.prev };
		unsafe { &mut *item.prev }.next = self;
		self.prev = item.prev;
		item.prev = ptr::null_mut();
		self.u.count -= 1;
		item.u.value
	}

	#[inline]
	pub unsafe fn pop_back<T>(&mut self) -> *mut T {
		self.pop_back_raw().cast()
	}
}

/// This runs 240 times per second.
pub unsafe fn process_sequences(iterationsRemaining: u32) {
	let _ = iterationsRemaining;
	for player in &mut gSequencePlayers {
		if player.flags.contains(SequencePlayerFlags::Enabled) {
			player.process_sequence();
			player.process_sound();
		}
	}
	process_notes();
}

#[inline]
pub unsafe fn init_sequence_player(player: usize) {
	gSequencePlayers[player].init();
}

impl SequencePlayer {
	unsafe fn init(&mut self) {
		self.disable();
		self.flags -= SequencePlayerFlags::Muted;
		self.delay = 0;
		self.state = SequencePlayerStateEU::FadingIn;
		self.fadeRemainingFrames = 0;
		self.fadeTimerUnkEu = 0;
		self.tempoAcc = 0;
		self.tempo = 120 * TEMPO_SCALE as u16;
		self.transposition = 0;
		self.muteBehavior = MuteBehaviorFlags::StopScript | MuteBehaviorFlags::StopNotes | MuteBehaviorFlags::Soften;
		self.noteAllocPolicy = NoteAllocPolicy::empty();
		self.shortNoteVelocityTable = gDefaultShortNoteVelocityTable.as_ptr();
		self.shortNoteDurationTable = gDefaultShortNoteDurationTable.as_ptr();
		self.fadeVolume = 1.0;
		self.fadeVolumeScale = 1.0;
		self.fadeVelocity = 0.0;
		self.volume = 0.0;
		self.muteVolumeScale = 0.5;
	}
}

/// Initialization function, called from audio_init.
pub unsafe fn init_sequence_players() {
	for chan in &mut gSequenceChannels {
		chan.seqPlayer = ptr::null_mut();
		chan.flags -= SequenceChannelFlags::Enabled;

		// @bug The original source had a bug.
		// Size of wrong array. Zeroes out second half of gSequenceChannels[0],
		// all of gSequenceChannels[1..31], and part of gSequenceLayers[0].
		// However, this is only called at startup, so it's harmless.
		chan.layers.fill(ptr::null_mut());
	}

	init_layer_freelist();

	for layer in &mut gSequenceLayers {
		layer.seqChannel = ptr::null_mut();
		layer.flags -= SequenceChannelLayerFlags::Enabled;
	}

	for player in &mut gSequencePlayers {
		player.channels.fill(ptr::addr_of_mut!(gSequenceChannelNone));
		player.seqVariationEu = -1;
		player.flags -= SequencePlayerFlags::BankDmaInProgress | SequencePlayerFlags::SeqDmaInProgress;
		player.notePool.init_note_lists();
		//init_sequence_player(index);
		player.init();
	}
}


impl SequenceChannel {
	unsafe fn init(&mut self) {
		self.flags = SequenceChannelFlags::empty();
		self.transposition = 0;
		self.bookOffset = 0;
		self.changes = SequenceChannelChanges::from_bits_retain(0xff);
		self.scriptState.depth = 0;
		self.newPan = 0;
		self.panChannelWeight = 0x80;
		self.reverbIndex = 0;
		self.reverbVol = 0;
		self.notePriority = NotePriority::Default;
		self.delay = 0;
		self.adsr.envelope = gDefaultEnvelope.as_ptr();
		self.adsr.releaseRate = 0x20;
		self.adsr.sustain = 0;
		self.vibratoRateTarget = 0x800;
		self.vibratoRateStart = 0x800;
		self.vibratoExtentTarget = 0;
		self.vibratoExtentStart = 0;
		self.vibratoRateChangeDelay = 0;
		self.vibratoExtentChangeDelay = 0;
		self.vibratoDelay = 0;
		self.volume = 1.0;
		self.volumeScale = 1.0;
		self.freqScale = 1.0;
		self.soundScriptIO.fill(-1);
		self.notePool.init_note_lists();
	}

	unsafe fn set_layer(&mut self, idx: usize) -> Result<(), ()> {
		if self.layers[idx].is_null() {
			let layer = gLayerFreeList.pop_back();
			self.layers[idx] = layer;
			if layer.is_null() {
				return Err(());
			}
		} else {
			(*self.layers[idx]).note_decay();
		}

		let layer = unsafe { &mut *self.layers[idx] };
		layer.seqChannel = self;
		layer.adsr = self.adsr;
		layer.adsr.releaseRate = 0;
		layer.flags |= SequenceChannelLayerFlags::Enabled;
		layer.flags -= SequenceChannelLayerFlags::StopSomething | SequenceChannelLayerFlags::ContinuousNotes | SequenceChannelLayerFlags::Finished | SequenceChannelLayerFlags::IgnoreDrumPan;
		layer.portamento.mode = 0;
		layer.scriptState.depth = 0;
		layer.status = SoundLoadStatus::NotLoaded;
		layer.noteDuration = 0x80;
		layer.pan = 0x40;
		layer.transposition = 0;
		layer.delay = 0;
		layer.duration = 0;
		layer.note = ptr::null_mut();
		layer.instrument = ptr::null_mut();
		layer.freqScale = 1.0;
		layer.velocitySquare = 0.0;
		layer.instrOrWave = 0xff;
		Ok(())
	}

	unsafe fn free_layer(&mut self, idx: usize) {
		if let Some(layer) = self.layers[idx].as_mut() {
			gLayerFreeList.push_back(&mut layer.listItem);
			layer.disable();
			self.layers[idx] = ptr::null_mut();
		}
	}
}

unsafe fn allocate_sequence_channel() -> *mut SequenceChannel {
	for channel in &mut gSequenceChannels {
		if channel.seqPlayer.is_null() {
			return channel;
		}
	}
	ptr::addr_of_mut!(gSequenceChannelNone)
}

impl SequencePlayer {
	unsafe fn init_channels(&mut self, channelBits: u16) {
		for i in 0..CHANNELS_MAX {
			if channelBits & (1 << i) == 0 {
				continue;
			}
			let seqChannel = self.channels[i];
			if seqChannel != ptr::addr_of_mut!(gSequenceChannelNone) && (*seqChannel).seqPlayer == self {
				(*seqChannel).disable();
				(*seqChannel).seqPlayer = ptr::null_mut();
			}
			let seqChannel = allocate_sequence_channel();
			if seqChannel == ptr::addr_of_mut!(gSequenceChannelNone) {
				eprintln!("Warning: no free notetracks");
				// i is in range 0..16, so it fits in low 4 bits
				gAudioErrorFlags = i as i32 | 0x10000_i32;
				self.channels[i] = seqChannel;
			} else {
				let seqChannel = unsafe { &mut *seqChannel };
				seqChannel.init();
				self.channels[i] = seqChannel;
				seqChannel.seqPlayer = self;
				seqChannel.bankId = self.defaultBank;
				seqChannel.muteBehavior = self.muteBehavior;
				seqChannel.noteAllocPolicy = self.noteAllocPolicy;
			}
		}
	}

	unsafe fn disable_channels(&mut self, channelBits: u16) {
		for i in 0..CHANNELS_MAX {
			if channelBits & (1 << i) == 0 {
				continue;
			}
			let seqChannel = self.channels[i];
			if seqChannel == ptr::addr_of_mut!(gSequenceChannelNone) {
				continue;
			}
			let seqChannel = unsafe { &mut *seqChannel };
			if seqChannel.seqPlayer == self {
				seqChannel.disable();
				seqChannel.seqPlayer = ptr::null_mut();
			} else {
				eprintln!("Warning: subtrack parent changed");
			}
			self.channels[i] = ptr::addr_of_mut!(gSequenceChannelNone);
		}
	}

	unsafe fn enable_channel(&mut self, index: usize, script: *const ()) {
		let channel = self.channels[index];
		if channel == ptr::addr_of_mut!(gSequenceChannelNone) {
			let [ref mut bgMusic, ref mut miscMusic, ..] = gSequencePlayers;

			if ptr::eq(self, bgMusic) {
				eprintln!("Group 0");
			} else if ptr::eq(self, miscMusic) {
				eprintln!("Group 1");
			} else {
				eprintln!("SeqId:{} BankId:{}", self.seqId, self.defaultBank);
			}
			eprintln!("Error: subtrack {} not allocated", index);
		} else {
			let channel = unsafe { &mut *channel };
			channel.flags |= SequenceChannelFlags::Enabled;
			channel.flags -= SequenceChannelFlags::Finished;
			channel.scriptState.depth = 0;
			channel.scriptState.pc = script.cast();
			channel.delay = 0;
			for i in 0..LAYERS_MAX {
				if !channel.layers[i].is_null() {
					channel.free_layer(i);
				}
			}
		}
	}
}

unsafe fn init_layer_freelist() {
	gLayerFreeList.prev = &mut gLayerFreeList;
	gLayerFreeList.next = &mut gLayerFreeList;
	gLayerFreeList.u.count = 0;
	gLayerFreeList.pool = ptr::null_mut();

	for layer in &mut gSequenceLayers {
		layer.listItem.u.value_layer = layer;
		layer.listItem.prev = ptr::null_mut();
		gLayerFreeList.push_back(&mut layer.listItem);
	}
}

impl M64ScriptState {
	#[inline(always)]
	unsafe fn read_u8(&mut self) -> u8 {
		let v = *self.pc;
		self.pc = self.pc.offset(1);
		v
	}

	#[inline(always)]
	unsafe fn read_i8(&mut self) -> i8 {
		self.read_u8() as i8
	}

	#[inline]
	unsafe fn read_u16(&mut self) -> u16 {
		let mut ret = (*self.pc as u16) << 8;
		ret |= *self.pc.offset(1) as u16;
		self.pc = self.pc.offset(2);
		ret
	}

	#[inline(always)]
	#[allow(dead_code)]
	unsafe fn read_i16(&mut self) -> i16 {
		self.read_u16() as i16
	}

	unsafe fn read_compressed_u16(&mut self) -> u16 {
		let mut ret = *self.pc as u16;
		if ret & 0x80 != 0 {
			ret = (ret << 8) & 0x7F00;
			ret |= *self.pc.offset(1) as u16;
			self.pc = self.pc.offset(2);
		} else {
			self.pc = self.pc.offset(1);
		}
		ret
	}
}

// mod m64_legacy;
// pub use m64_legacy::*;

mod m64_portable;
pub use m64_portable::*;

impl SequenceChannel {
	unsafe fn get_instrument(&mut self, instId: u8, instOut: &mut *mut Instrument, adsr: &mut AdsrSettings) -> u8 {
		let instrument = get_instrument_inner(self.bankId as i32, instId as i32);
		let Some(instrument) = instrument.as_mut() else {
			*instOut = ptr::null_mut();
			return 0;
		};
		adsr.envelope = instrument.envelope;
		adsr.releaseRate = instrument.releaseRate;
		*instOut = instrument;
		instId + 1
	}

	unsafe fn set_instrument(&mut self, instrId: u8) {
		if instrId >= 0x80 {
			self.instrOrWave = instrId as i16;
			self.instrument = ptr::null_mut();
		} else if instrId == 0x7f {
			self.instrOrWave = 0;
			self.instrument = 1 as *mut _;
		} else {
			let instOut = &mut *ptr::addr_of_mut!(self.instrument);
			let adsr = &mut *ptr::addr_of_mut!(self.adsr);
			self.instrOrWave = self.get_instrument(instrId, instOut, adsr) as i16;
			if self.instrOrWave == 0 {
				self.flags -= SequenceChannelFlags::HasInstrument;
				return;
			}
		}
		self.flags |= SequenceChannelFlags::HasInstrument;
	}

	fn set_volume(&mut self, volume: u8) {
		self.volume = volume as f32 / 127.0;
	}
}
