use crate::{
	support::{DSArray, PtrExt, DevPtrRO},
	data::*,
	heap::*,
	file::*,
	structs::*,
	sequencer::*,
	ultra::*,
	external::port_eu_init,
};

use std::mem::size_of;
use std::ptr::{
	null_mut,
	addr_of_mut,
	copy_nonoverlapping,
};

#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SoundMode {
	Stereo = 0,
	Headset = 1,
	// ---
	Mono = 3,
}

impl SoundMode {
	pub unsafe fn from_u8(val: u8) -> Self {
		match val {
			0 | 1 | 3 => std::mem::transmute(val),
			_ => panic!("Out of bounds value"),
		}
	}
}

bitflags::bitflags! {
	pub struct PreloadMask: u8 {
		const Banks = 2;
		const Sequence = 1;
	}
}

extern "C" {
	static gSoundDataDefinition: DSArray<u8>; // sound_data.ctl
	static gSoundDataSamples: DSArray<u8>; // sound_data.tbl
	static gMusicData: DSArray<u8>; // sequences.s
	static gBankSetsData: DSArray<u8>; // bank_sets.s
}

#[no_mangle] pub static mut gNotes: *mut Note = null_mut();

zeroed! {
	#[no_mangle] pub static mut gSequencePlayers: [SequencePlayer; SEQUENCE_PLAYERS];
	#[no_mangle] pub static mut gSequenceChannels: [SequenceChannel; SEQUENCE_CHANNELS];
	#[no_mangle] pub static mut gSequenceLayers: [SequenceChannelLayer; SEQUENCE_LAYERS];

	#[no_mangle] pub static mut gSequenceChannelNone: SequenceChannel;
	#[no_mangle] pub static mut gLayerFreeList: AudioListItem;
	#[no_mangle] pub static mut gNoteFreeLists: NotePool;
}

#[repr(C)]
struct SharedDma {
	/// target, points to pre-allocated buffer
	buffer: *mut u8,
	/// device address
	source: DevPtrRO<u8>,
	/// set to bufSize, never read
	_size: u16,
	/// size of buffer
	bufSize: u16,
	/// set to 0, never read
	_unused: u8,
	/// position in sSampleDmaReuseQueue1/2, if ttl == 0
	reuseIndex: u8,
	/// duration after which the DMA can be discarded
	ttl: u8,
}

macro_rules! align16 {
	($val: expr) => {
		($val + 0xF) & !0xF
	};
}

zeroed! {
	static mut sSampleDmas: [SharedDma; 0x60];
}
#[no_mangle] pub static mut gSampleDmaNumListItems: u32 = 0;
static mut sSampleDmaListSize1: u32 = 0;

pub const AUDIO_FRAME_DMA_QUEUE_SIZE: usize = 0x40;

zeroed! {
	#[no_mangle] static mut gCurrAudioFrameDmaQueue: OSMesgQueue;
	#[no_mangle] static mut gCurrAudioFrameDmaMesgBufs: [OSMesg; AUDIO_FRAME_DMA_QUEUE_SIZE];
	#[no_mangle] static mut gCurrAudioFrameDmaIoMesgBufs: [OSIoMesg; AUDIO_FRAME_DMA_QUEUE_SIZE];

	#[no_mangle] static mut gAudioDmaMesgQueue: OSMesgQueue;
	#[no_mangle] static mut gAudioDmaMesg: OSMesg;
	#[no_mangle] static mut gAudioDmaIoMesg: OSIoMesg;
}

// Circular buffer of DMAs with ttl = 0. tail <= head, wrapping around mod 256.
static mut sSampleDmaReuseQueue1: [u8; 0x100] = [0; 0x100];
static mut sSampleDmaReuseQueue2: [u8; 0x100] = [0; 0x100];
static mut sSampleDmaReuseQueueTail1: u8 = 0;
static mut sSampleDmaReuseQueueTail2: u8 = 0;
static mut sSampleDmaReuseQueueHead1: u8 = 0;
static mut sSampleDmaReuseQueueHead2: u8 = 0;

#[no_mangle] pub static mut gSeqFileHeader: *mut ALSeqFile = null_mut();
#[no_mangle] pub static mut gAlCtlHeader: *mut ALSeqFile = null_mut();
#[no_mangle] pub static mut gAlTbl: *mut ALSeqFile = null_mut();
#[no_mangle] pub static mut gAlBankSets: *mut u8 = null_mut();
#[no_mangle] pub static mut gSequenceCount: u16 = 0;

#[no_mangle] pub static mut gCtlEntries: *mut CtlEntry = null_mut();
zeroed! {
	#[no_mangle] pub static mut gAudioBufferParameters: AudioBufferParametersEU;
}
// #[no_mangle] pub static mut gAiFrequency: i32 = 0;

#[no_mangle] pub static mut gMaxAudioCmds: i32 = 0;
#[no_mangle] pub static mut gMaxSimultaneousNotes: i32 = 0;

// #[no_mangle] pub static mut gSamplesPerFrameTarget: i32 = 0;
// #[no_mangle] pub static mut gMinAiBufferLength: i32 = 0;

#[no_mangle] pub static mut gTempoInternalToExternal: i16 = 0;

#[no_mangle] pub static mut gSoundMode: SoundMode = SoundMode::Stereo;
#[no_mangle] pub static mut gAudioUpdatesPerFrame: i8 = 0;


/// Performs an immediate DMA copy.
unsafe fn audio_dma_copy_immediate(devAddr: DevPtrRO<()>, vAddr: *mut (), nBytes: usize) {
	/*osInvalDCache(vAddr, nBytes);
	osPiStartDma(&mut gAudioDmaIoMesg, MesgPriority::High, TransferDirection::Read, devAddr, vAddr.cast(), nBytes, &mut gAudioDmaMesgQueue);
	osRecvMesg(&mut gAudioDmaMesgQueue, null_mut(), Blocking::Block);*/
	copy_nonoverlapping(devAddr.cast::<u8>(), vAddr.cast::<u8>(), nBytes);
}

/// Performs an asynchronus (normal priority) DMA copy.
unsafe fn audio_dma_copy_async(devAddr: DevPtrRO<()>, vAddr: *mut (), nbytes: usize, queue: &OSMesgQueue, mesg: &mut OSIoMesg) {
	osInvalDCache(vAddr, nbytes);
	osPiStartDma(mesg, MesgPriority::Normal, TransferDirection::Read, devAddr.cast_mut().cast(), vAddr.cast(), nbytes, queue);
}

/// Performs a partial asynchronous (normal priority) DMA copy. This is limited
/// to `0x1000` bytes transfer at once.
pub unsafe fn audio_dma_partial_copy_async(
	devAddr: &mut DevPtrRO<u8>,
	vAddr: &mut *mut u8,
	remaining: &mut isize,
	queue: &OSMesgQueue,
	mesg: &mut OSIoMesg,
) {
	let transfer = (*remaining).min(0x1000);
	*remaining -= transfer;
	osInvalDCache((*vAddr).cast(), transfer as usize);
	osPiStartDma(mesg, MesgPriority::Normal, TransferDirection::Read, (*devAddr).cast_mut(), *vAddr, transfer as usize, queue);
	*devAddr = (*devAddr).offset(transfer);
	*vAddr = vAddr.offset(transfer);
}

pub unsafe fn decrease_sample_dma_ttls() {
	for idx in 0..sSampleDmaListSize1 {
		let temp = &mut sSampleDmas[idx as usize];
		if temp.ttl != 0 {
			temp.ttl -= 1;
			if temp.ttl == 0 {
				temp.reuseIndex = sSampleDmaReuseQueueHead1;
				sSampleDmaReuseQueue1[sSampleDmaReuseQueueHead1 as usize] = idx as u8;
				sSampleDmaReuseQueueHead1 = sSampleDmaReuseQueueHead1.wrapping_add(1);
			}
		}
	}

	for idx in sSampleDmaListSize1..gSampleDmaNumListItems {
		let temp = &mut sSampleDmas[idx as usize];
		if temp.ttl != 0 {
			temp.ttl -= 1;
			if temp.ttl == 0 {
				temp.reuseIndex = sSampleDmaReuseQueueHead2;
				sSampleDmaReuseQueue2[sSampleDmaReuseQueueHead2 as usize] = idx as u8;
				sSampleDmaReuseQueueHead2 = sSampleDmaReuseQueueHead2.wrapping_add(1);
			}
		}
	}
}

pub unsafe fn dma_sample_data(devAddr: DevPtrRO<u8>, size: u32, flags: crate::ultra64_abi::AudioFlags, dmaIndexRef: &mut u8) -> *mut u8 {
	let mut dma = None;
	let mut dmaIndex = 0;
	if flags != 0 || *dmaIndexRef as u32 >= sSampleDmaListSize1 {
		for idx in sSampleDmaListSize1..gSampleDmaNumListItems {
			let dma = &mut sSampleDmas[idx as usize];
			if dma.source <= devAddr && devAddr.offset_from(dma.source) as usize <= dma.bufSize as usize - size as usize {
				// We already have a DMA request for this memory range.
				if dma.ttl == 0 && sSampleDmaReuseQueueTail2 != sSampleDmaReuseQueueHead2 {
					// Move the DMA out of the reuse queue, by swapping it with the
					// tail, and then incrementing the tail.
					if dma.reuseIndex != sSampleDmaReuseQueueTail2 {
						sSampleDmaReuseQueue2[dma.reuseIndex as usize] = sSampleDmaReuseQueue2[sSampleDmaReuseQueueTail2 as usize];
						sSampleDmas[sSampleDmaReuseQueue2[sSampleDmaReuseQueueTail2 as usize] as usize].reuseIndex = dma.reuseIndex;
					}
					sSampleDmaReuseQueueTail2 = sSampleDmaReuseQueueTail2.wrapping_add(1);
				}
				dma.ttl = 60;
				*dmaIndexRef = idx as u8;
				return dma.buffer.add(devAddr.offset_from(dma.source) as usize);
			}
		}

		if sSampleDmaReuseQueueTail2 != sSampleDmaReuseQueueHead2 && flags != 0 {
			// Allocate a DMA from reuse queue 2. This queue can be empty, since TTL 60 is pretty large.
			dmaIndex = sSampleDmaReuseQueue2[sSampleDmaReuseQueueTail2 as usize];
			sSampleDmaReuseQueueTail2 = sSampleDmaReuseQueueTail2.wrapping_add(1);
			dma = Some(&mut sSampleDmas[dmaIndex as usize]);
		}
	} else {
		let dma = &mut sSampleDmas[*dmaIndexRef as usize];
		if dma.source <= devAddr && devAddr.offset_from(dma.source) as usize <= dma.bufSize as usize - size as usize {
			// We already have DMA for this memory range.
			if dma.ttl == 0 {
				// Move the DMA out of the reuse queue, by swapping it with the
				// tail, and then incrementing the tail.
				if dma.reuseIndex != sSampleDmaReuseQueueTail1 {
					sSampleDmaReuseQueue1[dma.reuseIndex as usize] = sSampleDmaReuseQueue1[sSampleDmaReuseQueueTail1 as usize];
					sSampleDmas[sSampleDmaReuseQueue1[sSampleDmaReuseQueueTail1 as usize] as usize].reuseIndex = dma.reuseIndex;
				}
				sSampleDmaReuseQueueTail1 = sSampleDmaReuseQueueTail1.wrapping_add(1);
			}
			dma.ttl = 2;
			return dma.buffer.offset(devAddr.offset_from(dma.source));
		}
	}

	let dma = dma.unwrap_or_else(|| {
		// Allocate a DMA from reuse queue 1. This queue will hopefully never
		// be empty, since TTL 2 is so small.
		dmaIndex = sSampleDmaReuseQueue1[sSampleDmaReuseQueueTail1 as usize];
		sSampleDmaReuseQueueTail1 = sSampleDmaReuseQueueTail1.wrapping_add(1);
		&mut sSampleDmas[dmaIndex as usize]
	});

	let dmaDevAddr = (devAddr as usize & !0xf) as DevPtrRO<u8>;
	dma.ttl = 2;
	dma.source = dmaDevAddr;
	dma._size = dma.bufSize;
	// On US:
	//osInvalDCache(dma.buffer.cast(), dma.bufSize as usize);
	osPiStartDma(&mut gCurrAudioFrameDmaIoMesgBufs[gCurrAudioFrameDmaCount], MesgPriority::Normal,
		TransferDirection::Read, dmaDevAddr.cast_mut(), dma.buffer, dma.bufSize as usize, &mut gCurrAudioFrameDmaQueue);
	gCurrAudioFrameDmaCount += 1;
	*dmaIndexRef = dmaIndex;
	dma.buffer.offset(devAddr.offset_from(dmaDevAddr))
}

pub unsafe fn init_sample_dma_buffers(_maxNotes: i32) {
	let mut sDmaBufSize = 0x400;
	for _idx in 0..3 * gMaxSimultaneousNotes * gAudioBufferParameters.presetUnk4 as i32 {
		let dma = &mut sSampleDmas[gSampleDmaNumListItems as usize];
		dma.buffer = gNotesAndBuffersPool.alloc(sDmaBufSize as u32);
		if dma.buffer.is_null() {
			eprintln!("Warning: couldn't allocate #{_idx} large DMA buffer");
			break;
		}
		dma.bufSize = sDmaBufSize;
		dma.source = std::ptr::null_mut();
		dma._size = 0;
		dma._unused = 0;
		dma.ttl = 0;
		// reuse_index is assigned later
		gSampleDmaNumListItems += 1;
	}

	for idx in 0..gSampleDmaNumListItems {
		sSampleDmaReuseQueue1[idx as usize]  = idx as u8;
		sSampleDmas[idx as usize].reuseIndex = idx as u8;
	}

	sSampleDmaReuseQueue1[gSampleDmaNumListItems as usize..].fill(0);
	sSampleDmaReuseQueueTail1 = 0;
	sSampleDmaReuseQueueHead1 = gSampleDmaNumListItems as u8;
	sSampleDmaListSize1 = gSampleDmaNumListItems;

	sDmaBufSize = 0x200;
	for _idx in 0..gMaxSimultaneousNotes {
		let dma = &mut sSampleDmas[gSampleDmaNumListItems as usize];
		dma.buffer = gNotesAndBuffersPool.alloc(sDmaBufSize as u32);
		if dma.buffer.is_null() {
			eprintln!("Warning: couldn't allocate #{_idx} small DMA buffer");
			break;
		}
		dma.bufSize = sDmaBufSize;
		dma.source = std::ptr::null_mut();
		dma._size = 0;
		dma._unused = 0;
		dma.ttl = 0;
		// reuse_index is assigned later
		gSampleDmaNumListItems += 1;
	}

	for idx in sSampleDmaListSize1..gSampleDmaNumListItems {
		sSampleDmaReuseQueue2[(idx - sSampleDmaListSize1) as usize] = idx as u8;
		sSampleDmas[idx as usize].reuseIndex = (idx - sSampleDmaListSize1) as u8;
	}

	// This probably meant to touch the range size1..size2 as well... but it
	// doesn't matter, since these values are never read anyway.
	sSampleDmaReuseQueue2[gSampleDmaNumListItems as usize..].fill(sSampleDmaListSize1 as u8);

	sSampleDmaReuseQueueTail2 = 0;
	sSampleDmaReuseQueueHead2 = (gSampleDmaNumListItems - sSampleDmaListSize1) as u8;
}

unsafe fn patch_sound(sound: &mut AudioBankSound, bankAddr: *const AudioBank, sampleBank: *const ()) {
	if sound.sample.is_null() {
		return;
	}

	macro_rules! patch {
		($var: expr, $base: expr) => {
			$var = ($var as usize + $base as usize) as *mut _
		};
	}

	patch!(sound.sample, bankAddr);
	let sample = &mut *sound.sample;
	if sample.loaded == 0 {
		patch!(sample.sampleAddr, sampleBank);
		patch!(sample.r#loop, bankAddr);
		patch!(sample.book, bankAddr);
		sample.loaded = AudioBankSample::LOAD_STATE_LOADED_BIT;
	}
	// EU specific
	else if sample.loaded == AudioBankSample::LOAD_STATE_ALLOC_BIT {
		let samples = (sample.sampleAddr as usize + sampleBank as usize) as *mut u8;
		let mem = gNotesAndBuffersPool.alloc::<u8>(sample.sampleSize);
		if mem.is_null() {
			sample.sampleAddr = samples;
			sample.loaded = AudioBankSample::LOAD_STATE_LOADED_BIT;
		} else {
			audio_dma_copy_immediate(samples.cast(), mem.cast(), sample.sampleSize as usize);
			sample.loaded = AudioBankSample::LOAD_STATE_LOADED_BIT | AudioBankSample::LOAD_STATE_ALLOC_BIT;
			sample.sampleAddr = mem;
		}
		patch!(sample.r#loop, bankAddr);
		patch!(sample.book, bankAddr);
	}
}

pub unsafe fn patch_audio_bank(bank: &mut AudioBank, sampleBank: *const (), numInstruments: u32, numDrums: u32) {
	macro_rules! patch {
		($ptr: expr, $base: expr) => {
			($ptr as usize + $base as *const _ as usize) as *mut _
		};
	}
	if !bank.drums.is_null() && numDrums != 0 {
		bank.drums = patch!(bank.drums, bank);
		for pdrum in std::slice::from_raw_parts_mut(bank.drums, numDrums as usize) {
			if pdrum.is_null() {
				continue;
			}

			let drum = &mut *patch!(*pdrum, bank);
			*pdrum = drum;
			if !drum.loaded {
				patch_sound(&mut drum.sound, bank, sampleBank);
				drum.envelope = patch!(drum.envelope, bank);
				drum.loaded = true;
			}
		}
	}

	if numInstruments != 0 {
		#[cfg(any())] {
		let mut itInstrs = bank.instruments.as_mut_ptr();
		let end = itInstrs.add(numInstruments as usize);
		while itInstrs != end {
			if !(*itInstrs).is_null() {
				*itInstrs = patch!(*itInstrs, bank);
				let instrument = &mut **itInstrs;
				if !instrument.loaded {
					patch_sound(&mut instrument.lowNotesSound, bank, sampleBank);
					patch_sound(&mut instrument.normalNotesSound, bank, sampleBank);
					patch_sound(&mut instrument.highNotesSound, bank, sampleBank);
					instrument.envelope = patch!(instrument.envelope, bank);
					instrument.loaded = true;
				}
			}
			itInstrs = itInstrs.add(1);
		}
		}

		let bankAddr: *const AudioBank = bank;

		for instr in bank.instruments.as_mut_slice(numInstruments as usize) {
			if !(*instr).is_null() {
				*instr = patch!(*instr, bankAddr);
				let instrument = &mut **instr;
				if !instrument.loaded {
					patch_sound(&mut instrument.lowNotesSound, bankAddr, sampleBank);
					patch_sound(&mut instrument.normalNotesSound, bankAddr, sampleBank);
					patch_sound(&mut instrument.highNotesSound, bankAddr, sampleBank);
					instrument.envelope = patch!(instrument.envelope, bankAddr);
					instrument.loaded = true;
				}
			}
		}
	}
}

unsafe fn bank_load_immediate(bankId: usize, mode: AllocBankSeqMode) -> *mut AudioBank {
	let alloc = align16!((*gAlCtlHeader).seqArray.index(bankId).len);
	let ctlData = (*gAlCtlHeader).seqArray.index(bankId).offset;
	let bank: *mut AudioBank = alloc_bank_or_seq(&mut gBankLoadedPool, 1, alloc, mode, bankId as i32).cast_thin();
	if bank.is_null() {
		return null_mut();
	}

	let mut buf = [0u32; 4];
	audio_dma_copy_immediate(ctlData.cast(), buf.as_mut_ptr().cast(), 0x10);
	let [numInstruments, numDrums, _, _] = buf;
	// -- Until this part, both functions are the same. --
	audio_dma_copy_immediate(ctlData.add(0x10).cast(), bank.cast(), alloc as usize);
	let bank = &mut *bank;
	patch_audio_bank(bank, (*gAlTbl).seqArray.index(bankId).offset.cast(), numInstruments, numDrums);
	let entry = &mut *gCtlEntries.add(bankId);
	entry.numInstruments = numInstruments as u8;
	entry.numDrums = numDrums as u8;
	entry.instruments = bank.instruments.as_mut_ptr();
	entry.drums = bank.drums;
	gBankLoadStatus[bankId] = SoundLoadStatus::Complete;
	bank
}

unsafe fn bank_load_async(bankId: usize, mode: AllocBankSeqMode, seqPlayer: &mut SequencePlayer) -> *mut AudioBank {
	let alloc = align16!((*gAlCtlHeader).seqArray.index(bankId).len);
	let ctlData = (*gAlCtlHeader).seqArray.index(bankId).offset;
	let bank: *mut AudioBank = alloc_bank_or_seq(&mut gBankLoadedPool, 1, alloc, mode, bankId as i32).cast_thin();
	if bank.is_null() {
		return null_mut();
	}

	let mut buf = [0u32; 4];
	audio_dma_copy_immediate(ctlData.cast(), buf.as_mut_ptr().cast(), 0x10);
	let [numInstruments, numDrums, _, _] = buf;
	// -- Until this part, both functions are the same. --
	seqPlayer.loadingBankId = bankId as u8;
	let entry = &mut *gCtlEntries.add(bankId);
	entry.numInstruments = numInstruments as u8;
	entry.numDrums = numDrums as u8;
	entry.instruments = DSArray::ptr_to_inner_mut(addr_of_mut!((*bank).instruments));
	entry.drums = null_mut();
	seqPlayer.bankDmaCurrMemAddr = bank.cast();
	seqPlayer.bankDmaCurrDevAddr = ctlData.add(0x10);
	seqPlayer.bankDmaRemaining = alloc as isize;
	let mesgQueue = seqPlayer.bankDmaMesgQueue.as_mut_ptr();
	osCreateMesgQueue(mesgQueue, std::slice::from_mut(&mut seqPlayer.bankDmaMesg));
	seqPlayer.flags |= SequencePlayerFlags::BankDmaInProgress;
	audio_dma_partial_copy_async(
		&mut seqPlayer.bankDmaCurrDevAddr,
		&mut seqPlayer.bankDmaCurrMemAddr,
		&mut seqPlayer.bankDmaRemaining,
		&*mesgQueue,
		&mut seqPlayer.bankDmaIoMesg,
	);
	gBankLoadStatus[bankId] = SoundLoadStatus::InProgress;
	bank
}

unsafe fn sequence_dma_immediate(seqId: usize, mode: AllocBankSeqMode) -> *mut () {
	let seqLength = align16!((*gSeqFileHeader).seqArray.index(seqId).len);
	let seqData = (*gSeqFileHeader).seqArray.index(seqId).offset;
	let ptr = alloc_bank_or_seq(&mut gSeqLoadedPool, 1, seqLength, mode, seqId as i32);
	if !ptr.is_null() {
		audio_dma_copy_immediate(seqData.cast(), ptr, seqLength as usize);
		gSeqLoadStatus[seqId] = SoundLoadStatus::Complete;
	}
	ptr
}

unsafe fn sequence_dma_async(seqId: usize, mode: AllocBankSeqMode, seqPlayer: &mut SequencePlayer) -> *mut () {
	let seqLength = align16!((*gSeqFileHeader).seqArray.index(seqId).len);
	let seqData = (*gSeqFileHeader).seqArray.index(seqId).offset;
	let ptr = alloc_bank_or_seq(&mut gSeqLoadedPool, 1, seqLength, mode, seqId as i32);
	if !ptr.is_null() {
		if seqLength <= 0x40 {
			// Immediately load short sequenece
			audio_dma_copy_immediate(seqData.cast(), ptr, seqLength as usize);
			gSeqLoadStatus[seqId] = SoundLoadStatus::Complete;
		} else {
			audio_dma_copy_immediate(seqData.cast(), ptr, 0x40);
			let mesgQueue = seqPlayer.seqDmaMesgQueue.as_mut_ptr();
			osCreateMesgQueue(mesgQueue, std::slice::from_mut(&mut seqPlayer.seqDmaMesg));
			seqPlayer.flags |= SequencePlayerFlags::SeqDmaInProgress;
			audio_dma_copy_async(seqData.add(0x40).cast(), ptr.cast::<u8>().add(0x40).cast(), (seqLength - 0x40) as usize, &*mesgQueue, &mut seqPlayer.seqDmaIoMesg);
			gSeqLoadStatus[seqId] = SoundLoadStatus::InProgress;
		}
	}
	ptr
}

unsafe fn get_missing_bank(seqId: usize, nonNullCount: &mut i32, nullCount: &mut i32) -> u8 {
	*nullCount = 0;
	*nonNullCount = 0;

	let mut offset = gAlBankSets.cast::<u16>().add(seqId).read() as usize;
	let count = gAlBankSets.add(offset).read();
	let mut ret = 0;
	offset += 1;
	for _ in 0..count {
		let bankId = gAlBankSets.add(offset).read();
		offset += 1;

		let temp = if gBankLoadStatus[bankId as usize].is_load_complete() {
			get_bank_or_seq(&mut gBankLoadedPool, 2, bankId as i32)
		} else {
			null_mut()
		};

		if temp.is_null() {
			*nullCount += 1;
			ret = bankId;
		} else {
			*nonNullCount += 1;
		}
	}

	ret
}

unsafe fn load_banks_immediate(seqId: usize, outDefaultBank: &mut u8) -> *mut AudioBank {
	let mut offset = gAlBankSets.cast::<u16>().add(seqId).read() as usize;
	let count = gAlBankSets.add(offset).read();
	offset += 1;
	let mut bankId = 0;
	let mut ret = null_mut();
	for _ in 0..count {
		bankId = gAlBankSets.add(offset).read();
		offset += 1;
		ret = if gBankLoadStatus[bankId as usize].is_load_complete() {
			get_bank_or_seq(&mut gBankLoadedPool, 2, bankId as i32).cast_thin::<AudioBank>()
		} else {
			null_mut()
		};

		if ret.is_null() {
			ret = bank_load_immediate(bankId as usize, AllocBankSeqMode::UsePersistent_FallbackTemporary);
		}
	}
	*outDefaultBank = bankId;
	ret
}

pub unsafe fn preload_sequence(seqId: u8, preloadMask: PreloadMask) {
	if seqId as u16 >= gSequenceCount {
		return;
	}

	let seqId = seqId as usize;

	gAudioLoadLock.set(AudioLock::LOADING);
	if preloadMask.contains(PreloadMask::Banks) {
		let mut temp = 0;
		load_banks_immediate(seqId, &mut temp);
	}

	if preloadMask.contains(PreloadMask::Sequence) {
		// Original had a @bug, it used gBankLoadStatus instead
		let sequenceData = if gSeqLoadStatus[seqId].is_load_complete() {
			get_bank_or_seq(&mut gSeqLoadedPool, 2, seqId as i32)
		} else {
			null_mut()
		};

		if sequenceData.is_null() && sequence_dma_immediate(seqId, AllocBankSeqMode::UsePersistent_FallbackTemporary).is_null() {
			gAudioLoadLock.set(AudioLock::NOT_LOADING);
			return;
		}
	}

	gAudioLoadLock.set(AudioLock::NOT_LOADING);
}

pub unsafe fn load_sequence(player: usize, seqId: u8, loadAsync: bool) {
	if !loadAsync {
		gAudioLoadLock.set(AudioLock::LOADING);
	}
	load_sequence_internal(player, seqId as usize, loadAsync);
	if !loadAsync {
		gAudioLoadLock.set(AudioLock::NOT_LOADING);
	}
}

/// Assumes lock is held if loading synchronously.
unsafe fn load_sequence_internal(player: usize, seqId: usize, loadAsync: bool) {
	if seqId as u16 >= gSequenceCount {
		return;
	}

	let seqPlayer = &mut gSequencePlayers[player];
	seqPlayer.disable();

	if loadAsync {
		let mut dummy = 0;
		let mut numMissingBanks = 0;
		let bankId = get_missing_bank(seqId, &mut dummy, &mut numMissingBanks);
		if numMissingBanks == 1 {
			println!("Info: one bank slow load start");
			if bank_load_async(bankId as usize, AllocBankSeqMode::UsePersistent_FallbackTemporary, seqPlayer).is_null() {
				return;
			}
			// @bug This should set the last bank (i.e. the first in the JSON)
			// as default, not the missing one. This code path never gets
			// taken, though -- all sequence loading is synchronous.
			seqPlayer.defaultBank = bankId;
		} else {
			// No 0?
			eprintln!("Warning: too many ({numMissingBanks}) banks are none. Fast loading.");
			if load_banks_immediate(seqId, &mut seqPlayer.defaultBank).is_null() {
				return;
			}
		}
	} else if load_banks_immediate(seqId, &mut seqPlayer.defaultBank).is_null() {
		return;
	}

	println!("Seq {seqId}: default load Id is: {bank}", bank = seqPlayer.defaultBank);
	seqPlayer.seqId = seqId as u8;
	let mut seqData = get_bank_or_seq(&mut gSeqLoadedPool, 2, seqId as i32);
	if seqData.is_null() {
		if seqPlayer.flags.contains(SequencePlayerFlags::SeqDmaInProgress) {
			eprintln!("Error: slow seq DMA in progress. Cancelling.");
			return;
		}
		seqData = if loadAsync {
			sequence_dma_async(seqId, AllocBankSeqMode::UsePersistent_FallbackTemporary, seqPlayer)
		} else {
			sequence_dma_immediate(seqId, AllocBankSeqMode::UsePersistent_FallbackTemporary)
		};
		if seqData.is_null() {
			return;
		}
	}

	init_sequence_player(player);
	seqPlayer.scriptState.depth = 0;
	seqPlayer.delay = 0;
	seqPlayer.flags |= SequencePlayerFlags::Enabled;
	let seqData = seqData.cast();
	seqPlayer.seqData = seqData;
	seqPlayer.scriptState.pc = seqData;
}

#[no_mangle]
pub unsafe extern "C" fn audio_init() {
	gAudioLoadLock.set(AudioLock::UNINITIALIZED);
	zero_heap();

	gAproxRefreshInterval = 20.03042;
	gRefreshRate = 50;
	//gAproxRefreshInterval = 16.713;
	//gRefreshRate = 60;
	port_eu_init();

	gAiBufferLengths.fill(0xA0);
	gAudioFrameCount = 0;
	gAudioTaskIndex = 0;
	gCurrAiBufferIndex = 0;
	gSoundMode = SoundMode::Stereo;
	gAudioTask = null_mut();
	// gAudioTasks[0].task.t.data_size = 0;
	// gAudioTasks[1].task.t.data_size = 0;
	osCreateMesgQueue(addr_of_mut!(gAudioDmaMesgQueue), std::slice::from_mut(&mut gAudioDmaMesg));
	osCreateMesgQueue(addr_of_mut!(gCurrAudioFrameDmaQueue), &mut gCurrAudioFrameDmaMesgBufs);
	gCurrAudioFrameDmaCount = 0;
	gSampleDmaNumListItems = 0;

	sound_init_main_pools(gAudioInitPoolSize);
	gAiBuffers.fill_with(||
		gAudioInitPool.alloc(AIBUFFER_LEN as u32)
	);

	gAudioResetPresetIdToLoad = 0;
	gAudioResetStatus.set(AudioResetStatus::StepResetSession);
	audio_shut_down_and_reset_step();
	//audio_reset_session(&gAudioSessionPresets[0]);

	// Load headers for sounds and sequences
	let data = gMusicData.as_ptr();
	gSequenceCount = (*(data as *mut ALSeqFile)).seqCount;
	let size = align16!(gSequenceCount as usize * size_of::<ALSeqData>() + 4);
	gSeqFileHeader = gAudioInitPool.alloc(size as u32);
	audio_dma_copy_immediate(data.cast(), gSeqFileHeader.cast(), size);
	alSeqFileNew(&mut *gSeqFileHeader, data.cast());

	// Load header for CTL (instrument metadata)
	let data = gSoundDataDefinition.as_ptr();
	let seqCount = (*(data as *mut ALSeqFile)).seqCount as usize;
	let size = align16!(seqCount * size_of::<ALSeqData>() + 4);
	gCtlEntries = gAudioInitPool.alloc((seqCount * size_of::<CtlEntry>()) as u32);
	gAlCtlHeader = gAudioInitPool.alloc(size as u32);
	audio_dma_copy_immediate(data.cast(), gAlCtlHeader.cast(), size);
	alSeqFileNew(&mut *gAlCtlHeader, data.cast());

	// Load header for TBL (raw sound data)
	if false {
		// The game uses values from previous section.
		gAlTbl = gAudioInitPool.alloc(size as u32);
		audio_dma_copy_immediate(gSoundDataSamples.as_ptr().cast(), gAlTbl.cast(), size);
		alSeqFileNew(&mut *gAlTbl, gSoundDataSamples.as_ptr().cast());
	} else {
		let data = gSoundDataSamples.as_ptr();
		let seqCount = (*(data as *mut ALSeqFile)).seqCount as usize;
		let size = align16!(seqCount * size_of::<ALSeqData>() + 4);
		gAlTbl = gAudioInitPool.alloc(size as u32);
		audio_dma_copy_immediate(data.cast(), gAlTbl.cast(), size);
		alSeqFileNew(&mut *gAlTbl, data.cast());
	}

	// Load bank sets for each sequence
	gAlBankSets = gAudioInitPool.alloc(0x100);
	audio_dma_copy_immediate(gBankSetsData.as_ptr().cast(), gAlBankSets.cast(), 0x100);

	init_sequence_players();
	gAudioLoadLock.set(AudioLock::NOT_LOADING);
}
