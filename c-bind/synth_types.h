#ifndef SM64_SYNTH_TYPES_H
#define SM64_SYNTH_TYPES_H

#include <stdint.h>

// See sounds.h for settings
typedef uint32_t SoundBits;

enum SoundMode
#ifdef __cplusplus
: uint8_t
#endif
{
	SOUND_MODE_STEREO = 0,
	SOUND_MODE_HEADSET = 1,
	// ---
	SOUND_MODE_MONO = 3,
};
#ifndef __cplusplus
typedef uint8_t SoundMode;
#endif

#endif /* SM64_SYNTH_TYPES_H */
