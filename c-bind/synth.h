#ifndef SM64_SYNTH_H
#define SM64_SYNTH_H

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include "synth_types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void audio_init(void);

void play_sound(SoundBits soundBits, const float *pos);

void audio_signal_game_loop_tick(void);

void seq_player_fade_out(uint8_t player, uint16_t fadeDuration);

void fade_volume_scale(size_t player, uint8_t targetScale, uint16_t fadeDuration);

void seq_player_lower_volume(size_t player, uint16_t fadeDuration, uint8_t percentage);

void seq_player_unlower_volume(size_t player, uint16_t fadeDuration);

void set_audio_muted(bool muted);

void sound_init(void);

void stop_sound(SoundBits soundBits, const float *pos);

void stop_sounds_from_source(const float *pos);

void stop_sounds_in_continuous_banks(void);

void sound_banks_disable(size_t player, uint16_t bankMask);

void sound_banks_enable(size_t player, uint16_t bankMask);

void set_sound_moving_speed(size_t bank, uint8_t speed);

void play_dialog_sound(size_t dialogId);

void play_music(size_t player, uint16_t seqArgs, uint16_t fadeTimer);

void stop_background_music(uint8_t seqId);

void fadeout_background_music(uint8_t seqId, uint16_t fadeDuration);

void drop_queued_background_music(void);

uint16_t get_current_background_music(void);

void play_secondary_music(uint8_t seqId,
                          uint8_t bgMusicVolume,
                          uint8_t volume,
                          uint16_t fadeDuration);

void fade_out_environment_music(uint16_t fadeDuration);

void stop_all_sounds(uint16_t fadeDuration);

void play_course_clear(void);

void play_peach_jingle(void);

void play_puzzle_jingle(void);

void play_star_fanfare(void);

void play_power_star_jingle(bool keep_music_playing);

void play_race_fanfare(void);

void play_toads_jingle(void);

void sound_reset(uint8_t presetId);

void audio_set_sound_mode(SoundMode soundMode);

void sound_effects_start(void);

struct SPTask *create_next_audio_frame_task(void);

void create_next_audio_buffer(int16_t (*samples)[2], size_t num_samples);

extern const float (*gMarioState_getPosition(void))[3];

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif /* SM64_SYNTH_H */
