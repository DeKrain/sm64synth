This directory stores sound & sequence data extracted from game ROM.

This repository currently doesn't include them because of copyright reasons.

A script used to copy files to this directory from a PC Port repository is provided at [`scripts/copy_sound_data.sh`](/scripts/copy_sound_data.sh).

A script will be soon provided to assemble the data in a more portable format.

Files inside this directory:
- `bank_sets.{endian}.bin` - Contains descriptions of bank sets
- `sequences.{endian}.bin` - Contains all of the programmed music data, commonly referred to as M64 format (this file doesn't necessarily need to be endianness-depended, and will be corrected soon)
- `sound_data.ctl.le.bin` - Contains instrument definitions
- `sound_data.tbl.le.bin` - Contains actual instrument samples
