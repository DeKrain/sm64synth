// Bridge between synth & the environment/player/game

use sm64_synth::external as synth;

use std::ptr::NonNull;
use std::mem::MaybeUninit;
use std::sync::{Condvar, Mutex};
use std::sync::mpsc::{channel, Sender, Receiver};

#[no_mangle] static gCurrAreaIndex: i16 = 0;

#[no_mangle] static gCurrLevelNum: i16 = 0;

#[no_mangle] static gMarioCurrentRoom: i16 = 0;

#[no_mangle] extern "C"
fn gMarioState_getPosition<'a>() -> &'a [f32; 3] {
	&[0.0; 3]
}

#[inline]
const fn slice_bytes<T>(slice: &[T]) -> &[u8] {
	let std::ops::Range { start, end } = slice.as_ptr_range();
	let start = start.cast();
	unsafe {
		std::slice::from_raw_parts(start, end.cast::<u8>().offset_from(start) as usize)
	}
}

#[inline]
const unsafe fn slice_cast<T, U>(slice: &[U]) -> &[T] {
	let std::ops::Range { start, end } = slice.as_ptr_range();
	let start = start.cast();
	unsafe {
		std::slice::from_raw_parts(start, end.cast::<T>().offset_from(start) as usize)
	}
}

#[inline]
unsafe fn slice_cast_mut<T, U>(slice: &mut [U]) -> &mut [T] {
	let std::ops::Range { start, end } = slice.as_mut_ptr_range();
	let start = start.cast();
	unsafe {
		std::slice::from_raw_parts_mut(start, end.cast::<T>().offset_from(start) as usize)
	}
}

#[inline]
unsafe fn slice_assume_init_ref<T>(slice: &[MaybeUninit<T>]) -> &[T] {
	unsafe { &*(slice as *const [MaybeUninit<T>] as *const [T]) }
}

#[must_use]
struct DropGuard<F: FnOnce()>(std::mem::ManuallyDrop<F>);

impl<F: FnOnce()> DropGuard<F> {
	#[inline(always)]
	pub const fn new(f: F) -> Self {
		Self(std::mem::ManuallyDrop::new(f))
	}
}

impl<F: FnOnce()> Drop for DropGuard<F> {
	#[inline(always)]
	fn drop(&mut self) {
		unsafe { std::mem::ManuallyDrop::take(&mut self.0)() }
	}
}

const SAMPLES_HIGH: usize = 656;
// Divides the frame rate into FRAME_RATE / REPEAT, to sync with game time
const TICKS_PER_FRAME: usize = 1;

fn synth_thread(signal_frame: Receiver<&mut [i16]>, finish_frame: Sender<()>, init_fence: &Condvar) {
	let mut audio_buffer: [MaybeUninit<[i16; 2]>; SAMPLES_HIGH * TICKS_PER_FRAME] = unsafe { MaybeUninit::uninit().assume_init() };
	let mut cursor = 0usize;
	unsafe {
		synth::audio_init();
		synth::sound_init();

		init_fence.notify_all();

		loop {
			let fill_buffer = signal_frame.recv().unwrap();
			assert!(fill_buffer.len() & 1 == 0, "Buffer must have a whole number of frames");
			let _finish_handle = DropGuard::new(|| finish_frame.send(()).unwrap());
			let mut fill_buffer: &mut [[i16; 2]] = slice_cast_mut(fill_buffer);
			let req_samples = fill_buffer.len();
			if cursor != 0 && req_samples <= audio_buffer.len() - cursor {
				fill_buffer.copy_from_slice(slice_assume_init_ref(&audio_buffer[cursor..cursor + req_samples]));
				cursor += req_samples;
				if cursor == audio_buffer.len() {
					cursor = 0;
				}
				continue;
			}

			if cursor != 0 {
				let front;
				(front, fill_buffer) = fill_buffer.split_at_mut(audio_buffer.len() - cursor);
				front.copy_from_slice(slice_assume_init_ref(&audio_buffer[cursor..]));
				cursor = 0;
			}

			while fill_buffer.len() > 0 {
				let num_samples = SAMPLES_HIGH;
				//if fill_buffer.len() <= audio_buffer.len() * 2;
				synth::audio_signal_game_loop_tick();
				for idx in 0..TICKS_PER_FRAME {
					//synth::create_next_audio_buffer(NonNull::new_unchecked(audio_buffer.as_mut_ptr()));
					let buffer = std::ptr::slice_from_raw_parts_mut(audio_buffer[idx * num_samples].as_mut_ptr(), num_samples);
					synth::create_next_audio_buffer(NonNull::new_unchecked(buffer));
				}

				let num_samples = num_samples * TICKS_PER_FRAME;
				let samples = std::slice::from_raw_parts(audio_buffer.as_ptr().cast::<[i16; 2]>(), num_samples);
				//let bytes: &[i16] = slice_cast(samples);
				//assert_eq!(fill_buffer.len(), bytes.len(), "Buffer size must match");
				//fill_buffer.copy_from_slice(bytes);

				if fill_buffer.len() >= num_samples {
					let front;
					(front, fill_buffer) = fill_buffer.split_at_mut(num_samples);
					front.copy_from_slice(samples);
				} else {
					cursor = fill_buffer.len();
					fill_buffer.copy_from_slice(&samples[..cursor]);
					fill_buffer = &mut fill_buffer[cursor..];
				}
			}
		}
	}
}

fn main() {
	use cpal::traits::{HostTrait, DeviceTrait, StreamTrait};

	let host = cpal::default_host();
	let device = host.default_output_device().expect("Fatal: couldn't open default audio device");
	//let config = device.default_output_config().expect("Fatal: couldn't pick default config");
	const DESIRED_SAMPLE_RATE: cpal::SampleRate = cpal::SampleRate(synth::AI_SAMPLE_RATE);
	let config = device.supported_output_configs()
		.expect("Fatal: couldn't pick a config")
		.find(|config| {
			(config.min_sample_rate()..=config.max_sample_rate()).contains(&DESIRED_SAMPLE_RATE)
			&& config.sample_format() == cpal::SampleFormat::I16
			&& config.channels() == 2
		}).expect("Fatal: couldn't pick a config")
		.with_sample_rate(DESIRED_SAMPLE_RATE);
	/*match *config.buffer_size() {
		cpal::SupportedBufferSize::Unknown => println!("Debug: supported buffer size is unknown"),
		cpal::SupportedBufferSize::Range { min, max } => println!("Debug: supported buffer size is {min}..={max}"),
	}*/
	let desired_buffer_size = (2 * SAMPLES_HIGH * TICKS_PER_FRAME) as u32;
	//println!("Debug: Desired buffer size is {desired_buffer_size}");
	let mut config = config.config();
	config.buffer_size = cpal::BufferSize::Fixed(desired_buffer_size);

	let (frame_start, frame_finish);
	let engine_inited = Condvar::new();
	let synth_thread = {
		let (fs, ff);
		(frame_start, fs) = channel();
		(ff, frame_finish) = channel();
		let engine_inited = unsafe { std::mem::transmute(&engine_inited) };
		std::thread::Builder::new()
			.name("sm64-synth-thread".into())
			.spawn(move || synth_thread(fs, ff, engine_inited))
			.expect("Fatal: failed to spawn audio thread")
	};
	{
		let mtx = Mutex::<()>::new(());
		drop(engine_inited.wait(mtx.lock().unwrap()).unwrap());
	}

	let stream = device.build_output_stream(&config,
		move |samples: &mut [i16], _info| {
			frame_start.send(unsafe { std::mem::transmute(samples) }).unwrap();
			frame_finish.recv().unwrap();
		},
		|err| {
			eprintln!("Playback error occured: {err}");
		},
		None).expect("Fatal: couldn't build output stream");

	stream.play().unwrap();

	unsafe {
		synth::audio_set_sound_mode(synth::SoundMode::Stereo);
		//synth::sound_reset(0);
		synth::sound_effects_start();

		macro_rules! play_sequence {
			($seq:ident) => {
				synth::play_music(synth::SEQ_PLAYER_LEVEL, synth::sequence_args(0, synth::$seq), 0);
				println!(concat!("[INFO] Playing music: ", stringify!($seq)));
			};
		}
		play_sequence!(SEQ_LEVEL_KOOPA_ROAD);
		synth::set_audio_muted(false);
		//synth::play_sound(synth::sounds::SOUND_MENU_COIN_ITS_A_ME_MARIO, synth::gGlobalSoundSource.as_ptr());
		//synth::play_sound(synth::sounds::SOUND_ACTION_BONK, synth::gGlobalSoundSource.as_ptr());
		//synth::play_sound(synth::sounds::SOUND_GENERAL2_PURPLE_SWITCH, synth::gGlobalSoundSource.as_ptr());
		//synth::play_sound(synth::sounds::SOUND_GENERAL_RED_COIN, synth::gGlobalSoundSource.as_ptr());
		//synth::play_sound(synth::sounds::SOUND_MENU_COLLECT_RED_COIN, synth::gGlobalSoundSource.as_ptr());
	}

	synth_thread.join().unwrap();
}
