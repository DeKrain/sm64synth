use std::fs::File;
use std::io::{self, BufWriter, Write, Seek, SeekFrom};
use std::mem::MaybeUninit;
use std::ptr::NonNull;
use std::sync::{Condvar, Mutex};

use sm64_synth::external as synth;

#[no_mangle] static gCurrAreaIndex: i16 = 0;

#[no_mangle] static gCurrLevelNum: i16 = 0;

#[no_mangle] static gMarioCurrentRoom: i16 = 0;

#[no_mangle] extern "C"
fn gMarioState_getPosition<'a>() -> &'a [f32; 3] {
	&[0.0; 3]
}

#[derive(Clone, Copy)]
struct WaveSampleFormat(u16);

impl WaveSampleFormat {
	pub const PCM: Self = Self(0x0001);
	pub const FLOAT: Self = Self(0x0003);
}

#[repr(C)]
struct WaveFormatHeader {
	wave_tag: [u8; 4],
	fmt_tag: [u8; 4],
	header_size: u32, // 0x10

	// Header fields
	format: u16,
	channel_count: u16,
	block_rate: u32,
	byte_rate: u32,
	block_size: u16,
	bit_depth: u16,
}

fn write_format_header(
	writer: &mut impl Write,
	block_rate: u32,
	format: WaveSampleFormat,
	channel_count: u16,
	bit_depth: u16,
) -> io::Result<()> {
	assert_eq!(bit_depth & 7, 0, "Bit depth must contain full bytes");
	let block_size = bit_depth * channel_count;
	let header = WaveFormatHeader {
		wave_tag: *b"WAVE",
		fmt_tag: *b"fmt ",
		header_size: 0x10_u32.to_le(),
		format: format.0.to_le(),
		channel_count: channel_count.to_le(),
		block_rate: block_rate.to_le(),
		byte_rate: (block_size as u32 * block_rate).to_le(),
		block_size: block_size.to_le(),
		bit_depth: bit_depth.to_le(),
	};

	writer.write_all(unsafe{ std::slice::from_raw_parts(&header as *const _ as *const u8, std::mem::size_of_val(&header)) })
}

const SAMPLES_HIGH: usize = 656;
// Divides the frame rate into FRAME_RATE / REPEAT, to sync with game time
const TICKS_PER_FRAME: usize = 1;

struct RenderBuffer {
	writer: BufWriter<File>,
	written_bytes: u32,
}

impl RenderBuffer {
	#[inline]
	fn write(&mut self, bytes: &[u8]) -> io::Result<()> {
		self.written_bytes = (self.written_bytes as usize + bytes.len()).try_into()
			.expect("Stream is too large");
		self.writer.write_all(bytes)
	}
}

fn synth_thread(mut render_buffer: RenderBuffer, init_fence: &Condvar) {
	let mut audio_buffer: [MaybeUninit<[i16; 2]>; SAMPLES_HIGH * TICKS_PER_FRAME] = unsafe { MaybeUninit::uninit().assume_init() };
	unsafe {
		synth::audio_init();
		synth::sound_init();

		init_fence.notify_all();

		const BYTES_TO_RENDER: usize = 0x200000;
		let mut playing = true;
		let mut rendered_bytes = 0usize;
		while playing {
			let num_samples = SAMPLES_HIGH;
			synth::audio_signal_game_loop_tick();
			for idx in 0..TICKS_PER_FRAME {
				//synth::create_next_audio_buffer(NonNull::new_unchecked(audio_buffer.as_mut_ptr()));
				let buffer = std::ptr::slice_from_raw_parts_mut(audio_buffer[idx * num_samples].as_mut_ptr(), num_samples);
				synth::create_next_audio_buffer(NonNull::new_unchecked(buffer));
			}

			let num_samples = num_samples * TICKS_PER_FRAME;
			let samples = std::slice::from_raw_parts(audio_buffer.as_ptr().cast::<[i16; 2]>(), num_samples);
			//let bytes: &[i16] = slice_cast(samples);
			let bytes: &[u8] = std::slice::from_raw_parts(samples.as_ptr().cast(), std::mem::size_of_val(samples));

			render_buffer.write(bytes).unwrap();
			rendered_bytes += bytes.len();

			playing = rendered_bytes < BYTES_TO_RENDER;
		}
	}

	let mut writer = render_buffer.writer.into_inner().unwrap();
	writer.seek(SeekFrom::Start(0x4)).unwrap();
	writer.write_all(&(render_buffer.written_bytes + 0x24).to_le_bytes()).unwrap();
	writer.seek(SeekFrom::Start(0x28)).unwrap();
	writer.write_all(&render_buffer.written_bytes.to_le_bytes()).unwrap();
	drop(writer);
}

fn main() -> io::Result<()> {
	let file = File::create("output.wav")?;
	let mut writer = BufWriter::new(file);
	// Header
	writer.write_all(b"RIFF")?;
	// Spot for file size @0x4
	writer.write_all(&[0u8; 4])?;
	write_format_header(&mut writer, synth::AI_SAMPLE_RATE, WaveSampleFormat::PCM, 2, 16)?;
	writer.write_all(b"data")?;
	// Spot for data size @0x28
	writer.write_all(&[0u8; 4])?;

	// Render loop
	let render_buffer = RenderBuffer {
		writer,
		written_bytes: 0,
	};
	let engine_inited = Condvar::new();
	let synth_thread = {
		let engine_inited = unsafe { std::mem::transmute(&engine_inited) };
		std::thread::Builder::new()
			.name("sm64-synth-thread".into())
			.spawn(move || synth_thread(render_buffer, engine_inited))
			.expect("Fatal: failed to spawn audio thread")
	};
	{
		let mtx = Mutex::<()>::new(());
		drop(engine_inited.wait(mtx.lock().unwrap()).unwrap());
	}

	unsafe {
		synth::audio_set_sound_mode(synth::SoundMode::Stereo);
		synth::sound_effects_start();

		macro_rules! play_sequence {
			($seq:ident) => {
				synth::play_music(synth::SEQ_PLAYER_LEVEL, synth::sequence_args(0, synth::$seq), 0);
				println!(concat!("[INFO] Playing music: ", stringify!($seq)));
			};
		}
		play_sequence!(SEQ_LEVEL_KOOPA_ROAD);
		synth::set_audio_muted(false);
	}

	synth_thread.join().unwrap();
	Ok(())
}
