#![feature(yeet_expr)]

extern crate proc_macro as pm;

use pm::TokenTree as TT;

macro_rules! expect_token_stream {
    (($stream:expr) pc $pc:literal) => {
        match $stream {
            TT::Punct(pc) if pc.as_char() == $pc => {}
            _ => do yeet concat!("Expected ", stringify!($pc)),
        }
    };
    (($stream:expr) id $ident:ident) => {
        match $stream {
            TT::Ident(ident) if ident.to_string() == stringify!($ident) => {}
            _ => do yeet concat!("Expected ", stringify!($ident)),
        }
    };
    (($stream:expr) tree) => {
        match $stream {
            TT::Group(group) if group.delimiter() == pm::Delimiter::Brace => group.stream(),
            _ => do yeet "Expected {} tree",
        }
    };
    (($stream:expr) group) => {
        match $stream {
            TT::Group(group) if group.delimiter() == pm::Delimiter::Parenthesis => group.stream(),
            _ => do yeet "Expected () group",
        }
    };
}

macro_rules! punct {
    ($ch:literal) => {
        TT::Punct(pm::Punct::new($ch, pm::Spacing::Alone))
    };
    (joint $ch:literal) => {
        TT::Punct(pm::Punct::new($ch, pm::Spacing::Joint))
    };
}

macro_rules! wrap_stream {
    ($stream:expr) => {
        TT::Group(pm::Group::new(pm::Delimiter::None, $stream))
    };
}

struct MissingToken;

struct Preprocessor<'ver> {
    iter: Vec<pm::token_stream::IntoIter>,
    version: &'ver str,
}

enum PreprocessorError {
    Str(&'static str),
}

impl From<&'static str> for PreprocessorError {
    fn from(str: &'static str) -> Self {
        Self::Str(str)
    }
}

impl From<MissingToken> for PreprocessorError {
    fn from(_: MissingToken) -> Self {
        Self::Str("Unexpected end of tokens")
    }
}

impl<'ver> Preprocessor<'ver> {
    fn new(iter: pm::token_stream::IntoIter, version: &'ver str) -> Self {
        Preprocessor { iter: vec![iter], version }
    }

    fn parse_conditional(&mut self) -> Result<(), PreprocessorError> {
        let iter = self.iter.last_mut().expect("Stack is empty");
        macro_rules! expect_token {
            ($($toks:tt)*) => (expect_token_stream!((iter.next().ok_or(MissingToken)?) $($toks)*));
        }

        let cond = expect_token!(group);
        let body = expect_token!(tree);
        let mut found = false;
        {
            let mut want_comma = false;
            for ver in cond {
                if want_comma {
                    expect_token_stream!((ver) pc ',');
                    want_comma = false;
                    continue;
                } else {
                    want_comma = true;
                }
                let TT::Ident(ver) = ver else {
                    do yeet "Version must be an identifier";
                };
                found |= ver.to_string() == self.version;
            }
        }

        if found {
            self.iter.push(body.into_iter());
        }

        Ok(())
    }

    fn take_out(&mut self) -> Preprocessor {
        Preprocessor {
            iter: std::mem::take(&mut self.iter),
            version: self.version,
        }
    }
}

impl Iterator for Preprocessor<'_> {
    type Item = Result<TT, PreprocessorError>;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(iter) = self.iter.last_mut() {
            let Some(tok) = iter.next() else {
                self.iter.pop();
                continue;
            };
            match tok {
                TT::Punct(pc) if pc.as_char() == '@' => {
                    match self.parse_conditional() {
                        Ok(()) => {}
                        Err(err) => return Some(Err(err)),
                    }
                }
                _ => return Some(Ok(tok)),
            }
        }
        None
    }
}

/// Transfers opcode description into functions.
///
/// # Syntax
/// ```no_run
/// m64_opcode_def_ext! {
///     ReturnCommandType:
///     Versions {
///         version: decode_version_function,
///     }
///
///     // Includes the section if any of the versions are targeted
///     @(version1, version2) {
///         Command(args) {
///             // body...
///         }
///     }
///
///     // Commands:
///
///     // Commands after the body are put in an `else` case
///     Test(> EXPRESSION) {
///     }
///     Standalone(StandaloneCommandType) {
///         OPCODE = Variant;
///     }
///     Packed(mask MASK) {
///         MASKED_OPCODE = Variant;
///     }
/// }
/// ```
#[proc_macro]
pub fn m64_opcode_def_ext(input: pm::TokenStream) -> pm::TokenStream {
    enum Error {
        Str(&'static str),
        UnknownCommand(pm::Ident),
    }

    impl From<MissingToken> for Error {
        fn from(_: MissingToken) -> Self {
            Error::Str("Unexpected end of tokens")
        }
    }

    impl From<&'static str> for Error {
        fn from(str: &'static str) -> Self {
            Error::Str(str)
        }
    }

    impl From<PreprocessorError> for Error {
        fn from(error: PreprocessorError) -> Self {
            match error {
                PreprocessorError::Str(str) => Error::Str(str),
            }
        }
    }

    fn inner(input: pm::TokenStream, output: &mut pm::TokenStream) -> Result<(), Error> {
        let mut iter = input.into_iter();
        macro_rules! expect_token {
            ($($toks:tt)*) => (expect_token_stream!((iter.next().ok_or(MissingToken)?) $($toks)*));
        }

        let ret = iter.next().ok_or(MissingToken)?;
        let TT::Ident(command_type) = ret else {
            //return Err(Error::Str(""));
            do yeet "Command type must currently be an identifier";
        };
        expect_token!(pc ':');
        expect_token!(id Versions);
        let mut versions_tree = expect_token!(tree).into_iter();

        let mut versions = vec![];
        {
            let mut want_comma = false;
            while let Some(ver) = versions_tree.next() {
                if want_comma {
                    expect_token_stream!((ver) pc ',');
                    want_comma = false;
                    continue;
                } else {
                    want_comma = true;
                }
                let TT::Ident(ver) = ver else {
                    do yeet "Version must be an identifier";
                };
                expect_token_stream!((versions_tree.next().ok_or(MissingToken)?) pc ':');
                let TT::Ident(func) = versions_tree.next().ok_or(MissingToken)? else {
                    do yeet "Expected function name";
                };

                versions.push((ver.to_string(), func));
            }
        }

        for (ref version, func) in versions {
            struct Context<'out> {
                command_type: &'out pm::Ident,
                op_var: pm::Ident,
                kw_if: pm::Ident,
                kw_else: pm::Ident,
                kw_use: pm::Ident,
                kw_match: pm::Ident,
                kw_let: pm::Ident,
                standalone_variant: pm::Ident,
                inval_tail: pm::TokenStream,
                version: &'out str,
            }
            enum ParsingContext {
                CommandList,
                Command,
            }
            impl Context<'_> {
                fn parse_fragments(&self, mut iter: Preprocessor) -> Result<pm::TokenStream, Error> {
                    let mut write_head = vec![];
                    while let Some(cmd) = iter.next() {
                        let TT::Ident(cmd) = cmd? else {
                            do yeet "Command must begin with an identifier";
                        };

                        self.parse_command(&mut iter, &mut write_head, cmd, ParsingContext::CommandList)?;
                    }
                    Ok(write_head.into_iter().collect())
                }

                fn parse_command(&self, iter: &mut Preprocessor, write_head: &mut Vec<TT>, cmd: pm::Ident, ctx: ParsingContext) -> Result<(), Error> {
                    macro_rules! expect_token {
                        ($($toks:tt)*) => (expect_token_stream!((iter.next().ok_or(MissingToken)??) $($toks)*));
                    }

                    match &cmd.to_string()[..] {
                        "Test" => {
                            let mut test = pm::TokenStream::new();
                            test.extend(std::iter::once(TT::Ident(self.op_var.clone())));
                            test.extend(expect_token!(group));
                            let body = iter.next().ok_or(MissingToken)??;
                            let body = match body {
                                TT::Ident(cmd) => {
                                    let mut body = vec![];
                                    self.parse_command(iter, &mut body, cmd, ParsingContext::Command)?;
                                    body.into_iter().collect()
                                }
                                TT::Group(tree) if tree.delimiter() == pm::Delimiter::Brace => {
                                    let body = tree.stream();
                                    self.parse_fragments(Preprocessor::new(body.into_iter(), self.version))?
                                }
                                _ => do yeet "Expected a command or a sub-tree",
                            };
                            write_head.extend([
                                TT::Ident(self.kw_if.clone()),
                                wrap_stream!(test),
                                TT::Group(pm::Group::new(pm::Delimiter::Brace, body)),
                            ]);
                            match ctx {
                                ParsingContext::CommandList => {
                                    let outer = self.parse_fragments(iter.take_out())?;
                                    write_head.extend([
                                        TT::Ident(self.kw_else.clone()),
                                        TT::Group(pm::Group::new(pm::Delimiter::Brace, outer)),
                                    ]);
                                }
                                ParsingContext::Command => {}
                            }
                        }
                        "Match" => {
                            let pattern = expect_token!(group);
                            let body = iter.next().ok_or(MissingToken)??;
                            let body = match body {
                                TT::Ident(cmd) => {
                                    let mut body = vec![];
                                    self.parse_command(iter, &mut body, cmd, ParsingContext::Command)?;
                                    body.into_iter().collect()
                                }
                                TT::Group(tree) if tree.delimiter() == pm::Delimiter::Brace => {
                                    let body = tree.stream();
                                    self.parse_fragments(Preprocessor::new(body.into_iter(), self.version))?
                                }
                                _ => do yeet "Expected a command or a sub-tree",
                            };
                            write_head.extend([
                                TT::Ident(self.kw_if.clone()),
                                TT::Ident(self.kw_let.clone()),
                                wrap_stream!(pattern),
                                punct!('='),
                                TT::Ident(self.op_var.clone()),
                                TT::Group(pm::Group::new(pm::Delimiter::Brace, body)),
                            ]);
                            match ctx {
                                ParsingContext::CommandList => {
                                    let outer = self.parse_fragments(iter.take_out())?;
                                    write_head.extend([
                                        TT::Ident(self.kw_else.clone()),
                                        TT::Group(pm::Group::new(pm::Delimiter::Brace, outer)),
                                    ]);
                                }
                                ParsingContext::Command => {}
                            }
                        }
                        "Standalone" => {
                            let standalone_type = expect_token!(group);
                            let body = expect_token!(tree);
                            self.matching(body, write_head,
                                wrap_stream!(standalone_type),
                                TT::Ident(self.op_var.clone()),
                                |variant| pm::TokenStream::from_iter([
                                    TT::Ident(self.command_type.clone()),
                                    punct!(joint ':'),
                                    punct!(':'),
                                    TT::Ident(self.standalone_variant.clone()),
                                    TT::Group(pm::Group::new(pm::Delimiter::Parenthesis, variant.into_iter().collect())),
                                ]))?;
                        }
                        "Packed" => {
                            let mut args = expect_token!(group).into_iter();
                            let body = expect_token!(tree);
                            expect_token_stream!((args.next().ok_or(MissingToken)?) id mask);
                            let mask = match args.next().ok_or(MissingToken)? {
                                TT::Literal(lit) => lit,
                                _ => do yeet "Mask must be a number",
                            };
                            let None = args.next() else {
                                do yeet "Unexpected tokens in Packed command arguments";
                            };
                            let id_arg = pm::Ident::new("arg", cmd.span());
                            write_head.extend([
                                TT::Ident(self.kw_let.clone()),
                                TT::Ident(id_arg.clone()),
                                punct!('='),
                                TT::Ident(self.op_var.clone()),
                                punct!('&'),
                                punct!('!'),
                                TT::Literal(mask.clone()),
                                punct!(';'),
                            ]);
                            self.matching(body, write_head,
                                TT::Ident(self.command_type.clone()),
                                wrap_stream!(pm::TokenStream::from_iter([
                                    TT::Ident(self.op_var.clone()),
                                    punct!('&'),
                                    TT::Literal(mask),
                                ])),
                                |mut variant| {
                                    variant.push(TT::Group(pm::Group::new(pm::Delimiter::Parenthesis, pm::TokenStream::from_iter([
                                        TT::Ident(id_arg.clone()),
                                    ]))));
                                    variant.into_iter().collect()
                                })?;
                        }
                        "Return" => {
                            let expr = expect_token!(group);
                            write_head.extend(expr);
                        }
                        _ => do yeet Error::UnknownCommand(cmd),
                    }

                    Ok(())
                }

                fn matching(
                    &self,
                    body: pm::TokenStream,
                    write_head: &mut Vec<TT>,
                    scope_type: TT,
                    match_expr: TT,
                    gen_variant: impl Fn(Vec<TT>) -> pm::TokenStream,
                ) -> Result<(), Error> {
                    let mut body = Preprocessor::new(body.into_iter(), self.version);
                    macro_rules! expect_token {
                        ($($toks:tt)*) => (expect_token_stream!((body.next().ok_or(MissingToken)??) $($toks)*));
                    }
                    let mut match_body = vec![];
                    while let Some(op) = body.next() {
                        let op = op?;
                        let op = match op {
                            TT::Literal(lit) => lit,
                            _ => do yeet "Opcode must be a literal",
                        };
                        expect_token!(pc '=');
                        let mut variant = vec![];
                        loop {
                            let tok = body.next().ok_or(MissingToken)??;
                            match tok {
                                TT::Punct(pc) if pc.as_char() == ';' => break,
                                _ => variant.push(tok),
                            }
                        }
                        match_body.extend([
                            TT::Literal(op),
                            punct!(joint '='),
                            punct!('>'),
                            wrap_stream!(gen_variant(variant)),
                            punct!(','),
                        ]);
                    }
                    match_body.extend(self.inval_tail.clone());
                    write_head.extend([
                        TT::Ident(self.kw_use.clone()),
                        scope_type,
                        punct!(joint ':'),
                        punct!(':'),
                        punct!('*'),
                        punct!(';'),

                        TT::Ident(self.kw_match.clone()),
                        match_expr,
                        TT::Group(pm::Group::new(pm::Delimiter::Brace, match_body.into_iter().collect())),
                    ]);
                    Ok(())
                }
            }
            let body = Context {
                command_type: &command_type,
                op_var: pm::Ident::new("op", pm::Span::mixed_site()),
                kw_if: pm::Ident::new("if", pm::Span::mixed_site()),
                kw_else: pm::Ident::new("else", pm::Span::mixed_site()),
                kw_use: pm::Ident::new("use", pm::Span::mixed_site()),
                kw_match: pm::Ident::new("match", pm::Span::mixed_site()),
                kw_let: pm::Ident::new("let", pm::Span::mixed_site()),
                standalone_variant: pm::Ident::new("Standalone", pm::Span::mixed_site()),
                inval_tail: pm::TokenStream::from_iter([
                    TT::Ident(pm::Ident::new("_", pm::Span::mixed_site())),
                    punct!(joint '='),
                    punct!('>'),
                    TT::Ident(pm::Ident::new("panic", pm::Span::mixed_site())),
                    punct!('!'),
                    TT::Group(pm::Group::new(pm::Delimiter::Parenthesis, pm::TokenStream::from_iter([
                        TT::Literal(pm::Literal::string("Unknown M64 opcode")),
                    ]))),
                    punct!(','),
                ]),
                version,
            }.parse_fragments(Preprocessor::new(iter.clone(), version))?;
            output.extend([
                TT::Ident(pm::Ident::new("fn", pm::Span::call_site())),
                TT::Ident(func),
                TT::Group(pm::Group::new(pm::Delimiter::Parenthesis, pm::TokenStream::from_iter([
                    TT::Ident(pm::Ident::new("op", pm::Span::mixed_site())),
                    punct!(':'),
                    TT::Ident(pm::Ident::new("u8", pm::Span::mixed_site())),
                ]))),
                punct!(joint '-'),
                punct!('>'),
                TT::Ident(command_type.clone()),
                TT::Group(pm::Group::new(pm::Delimiter::Brace, body)),
            ]);
        }

        Ok(())
    }

    let mut output = pm::TokenStream::new();

    match inner(input, &mut output) {
        Ok(()) => {}
        Err(Error::Str(str)) => {
            output.extend([
                TT::Ident(pm::Ident::new("compile_error", pm::Span::mixed_site())),
                punct!('!'),
                TT::Group(pm::Group::new(pm::Delimiter::Brace, TT::Literal(pm::Literal::string(str)).into())),
            ]);
        }
        Err(Error::UnknownCommand(cmd)) => {
            output.extend([
                TT::Ident(pm::Ident::new("compile_error", cmd.span())),
                punct!('!'),
                TT::Group({
                    let mut group = pm::Group::new(pm::Delimiter::Brace,
                            TT::Literal(pm::Literal::string(&format!("Unknown command: {cmd}"))).into());
                    group.set_span(cmd.span());
                    group
                }),
            ]);
        }
    }

    output
}
