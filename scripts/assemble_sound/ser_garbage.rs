use std::io::{self, Write};

pub struct GarbageSerializer<Writer: Write> {
	inner: GarbageSerializerInner,
	writer: Writer,
}

pub struct GarbageSerializerInner {
	garbage_bufs: Vec<Garbage>,
	buffer: Vec<u8>,
	garbage_pos: usize,
}

struct Garbage {
	garbage_pos: usize,
	position: usize,
	length: usize,
}

impl<Writer: Write> GarbageSerializer<Writer> {
	pub fn new(writer: Writer) -> Self {
		GarbageSerializer {
			inner: GarbageSerializerInner {
				garbage_bufs: vec![],
				buffer: vec![],
				garbage_pos: 0,
			},
			writer,
		}
	}
}

innner_deref!((Writer: std::io::Write) GarbageSerializer<Writer> => inner: GarbageSerializerInner);

impl GarbageSerializerInner {
	pub fn len(&self) -> usize {
		self.buffer.len()
	}

	pub fn reset_garbage_pos(&mut self) {
		self.garbage_pos = 0;
	}

	pub fn add(&mut self, part: &[u8]) {
		self.garbage_bufs.push(Garbage { garbage_pos: self.garbage_pos, position: self.buffer.len(), length: part.len() });
		self.buffer.extend(part);
		self.garbage_pos += part.len();
	}

	pub fn align(&mut self, alignment: usize) {
		let new_size = (self.len() + alignment - 1) & alignment.wrapping_neg();
		self.add(&vec![0; new_size - self.len()]);
	}

	/// Find the last write to position pos & 0xffff, assuming a cyclic
	/// buffer of size 0x10000 where the write position is reset to 0 on
	/// each call to reset_garbage_pos.
	fn garbage_at(&mut self, mut pos: usize) -> u8 {
		pos &= 0xFFFF;
		for gar in self.garbage_bufs.iter().rev() {
			let q = ((gar.garbage_pos as isize + gar.length as isize - 1 - pos as isize) & !0xFFFF) + pos as isize;
			if q >= gar.garbage_pos as isize {
				let buf = &self.buffer[gar.position .. gar.position + gar.length];
				return buf[q as usize - gar.garbage_pos];
			}
		}
		0
	}

	pub fn align_garbage(&mut self, alignment: usize) {
		while self.len() % alignment != 0 {
			let garbage = self.garbage_at(self.garbage_pos);
			self.add(&[garbage]);
		}
	}
}

impl<Writer: Write> Drop for GarbageSerializer<Writer> {
	fn drop(&mut self) {
		self.writer.write_all(&self.inner.buffer).unwrap();
	}
}

impl Write for GarbageSerializerInner {
	#[inline]
	fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
		self.add(buf);
		Ok(buf.len())
	}

	#[inline]
	fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
		self.add(buf);
		Ok(())
	}

	#[inline(always)]
	fn flush(&mut self) -> io::Result<()> {
		Ok(())
	}
}
