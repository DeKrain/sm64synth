use std::{
	path::PathBuf,
	cell::{Cell, OnceCell},
};
use crate::{
	FileBuffer,
	insert_once,
	slice_cast_aligned,
};

pub struct Aifc {
	pub name: String,
	pub path: PathBuf,
	pub data: FileBuffer,
	pub sample_rate: f64,
	pub book: Book,
	pub r#loop: Option<Loop>,
	pub used: Cell<bool>,
	pub offset: OnceCell<u32>,
}

pub struct Book {
	pub order: u16,
	pub npredictors: u16,
	pub table: Vec<i16>,
}

pub struct Loop {
	pub start: u32,
	pub end: u32,
	pub count: u32,
	pub state: [i16; 16],
}

impl Aifc {
	pub fn parse(data: FileBuffer, name: String, path: PathBuf) -> Box<Self> {
		assert!(data.as_ptr().is_aligned_to(16));
		assert_eq!(data[..4], *b"FORM", "Must start with FORM");
		assert_eq!(data[8..12], *b"AIFC", "Format must be AIFC");
		let mut idx = 12;
		let mut sections = vec![];
		while idx < data.len() {
			let r#type: [u8; 4] = data[idx .. idx + 4].try_into().unwrap();
			let len = u32::from_be_bytes(data[idx + 4 .. idx + 8].try_into().unwrap()) as usize;
			idx += 8;
			sections.push((r#type, &data[idx .. idx + len]));
			idx = align!(idx + len, 2);
		}

		let mut audio_data = None;
		let mut vadpcm_codes = None;
		let mut vadpcm_loops = None;
		let mut sample_rate = None;

		for (tp, data) in sections {
			match &tp {
				b"APPL" => {
					if data[..4] != *b"stoc" {
						continue;
					}
					let plen = data[4];
					let tp = &data[5 .. 5 + plen as usize];
					let data = &data[align!(5 + plen as usize, 2)..];
					match tp {
						b"VADPCMCODES" => insert_once(&mut vadpcm_codes, data),
						b"VADPCMLOOPS" => insert_once(&mut vadpcm_loops, data),
						_ => {}
					}
				}
				b"SSND" => insert_once(&mut audio_data, &data[8..]),
				b"COMM" => insert_once(&mut sample_rate, Self::parse_f80(data[8..18].try_into().unwrap())),
				_ => {}
			}
		}

		let sample_rate = sample_rate.expect("No COMM section");
		let audio_data = audio_data.expect("No SSND section");
		let vadpcm_codes = vadpcm_codes.expect("No VADPCM CODES table");

		let book = Self::parse_book(unsafe { slice_cast_aligned(vadpcm_codes) });
		let r#loop = vadpcm_loops.map(Self::parse_loop);

		let data_start = usize::try_from(unsafe { audio_data.as_ptr().offset_from(data.as_ptr()) }).unwrap();
		let data_end = data_start + audio_data.len();
		let mut data = data;
		data.truncate(data_end);
		data.drain(..data_start);
		Box::new(Aifc {
			name, path,
			data,
			sample_rate,
			book, r#loop,
			used: Cell::new(false),
			offset: OnceCell::new(),
		})
	}

	#[inline]
	fn parse_f80(data: &[u8; 10]) -> f64 {
		let [exp_bits @ .., _, _, _, _, _, _, _, _] = *data;
		let [_, _, mant_bits @ ..] = *data;
		//let exp_bits = data[0..2];
		//let mant_bits = data[2..10];

		let mut exp_bits = u16::from_be_bytes(exp_bits);
		let mant_bits = u64::from_be_bytes(mant_bits);
		let sign_bit = exp_bits & 0x8000 != 0;
		exp_bits &= 0x7FFF;
		if exp_bits == 0 && mant_bits == 0 {
			return if sign_bit { -0.0 } else { 0.0 };
		}
		assert!(exp_bits != 0, "Sample rate is denormal");
		assert!(exp_bits != 0x7FFF, "Sample rate is Infinity/NaN");
		/*f64::from_bits(
			((sign_bit as u64) << 63)
			| exp_bits
		)*/
		fn flt_exp2(pow: i16) -> f64 {
			let base = (1u64 << pow.unsigned_abs()) as f64;
			if pow < 0 { base.recip() } else { base }
		}
		const fn flt_exp2u(pow: u16) -> f64 {
			(1u64 << pow) as f64
		}
		const EXP_M63: f64 = 1.0 / flt_exp2u(63);
		let mant = mant_bits as f64 * EXP_M63;
		let num = mant * flt_exp2(exp_bits as i16 - 0x3FFF);
		if sign_bit { -num } else { num }
	}

	#[inline]
	fn parse_book(data: &[u16]) -> Book {
		let version = u16::from_be(data[0]);
		let order = u16::from_be(data[1]);
		let npredictors = u16::from_be(data[2]);

		assert_eq!(version, 1, "Codebook version doesn't match");
		let data = &data[3..];
		assert_eq!(data.len(), 8 * order as usize * npredictors as usize, "Predictor book chunk size doesn't match");
		let table = data.iter().map(|sam| u16::from_be(*sam) as i16).collect();
		Book { order, npredictors, table }
	}

	#[inline]
	fn parse_loop(data: &[u8]) -> Loop {
		#[repr(C)]
		struct Header {
			version: u16,
			nloops: u16,
			start: [u8; 4],
			end: [u8; 4],
			count: [u8; 4],
		}

		assert_eq!(data.len(), 48, "Loop chunk size should be 48");
		let header = data.as_ptr().cast::<Header>();
		assert!(header.is_aligned(), "Header must be aligned");
		let header = unsafe { &*header };
		assert_eq!(u16::from_be(header.version), 1, "Loop version doesn't match");
		assert_eq!(u16::from_be(header.nloops), 1, "Only one loop is supported");
		let state: &[i16] = unsafe { slice_cast_aligned(&data[16..48]) };
		let state: &[i16; 16] = state.try_into().unwrap();
		let state = state.map(i16::from_be);
		/*fn u32_from_u16x2(arr: [u16; 2]) -> u32 {
			unsafe { std::mem::transmute(arr) }
		}*/
		Loop {
			//start: u32_from_u16x2(header.start),
			//end: u32_from_u16x2(header.end),
			//count: u32_from_u16x2(header.count),
			start: u32::from_be_bytes(header.start),
			end: u32::from_be_bytes(header.end),
			count: u32::from_be_bytes(header.count),
			state,
		}
	}
}
