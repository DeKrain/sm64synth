use std::io::{self, Write};
use std::rc::Rc;
use std::cell::RefCell;

pub struct ReserveSerializer<Writer: Write> {
	inner: ReserveSerializerInner,
	writer: Writer,
}

pub struct ReserveSerializerInner {
	/*parts: Vec<&'part [u8]>,
	sizes: Vec<usize>,
	size: usize,*/
	buffer: Rc<ReserveBuffer>,
}

struct ReserveBuffer {
	buf: RefCell<Vec<u8>>,
	reserves: RefCell<Vec<Reservation>>,
}

struct Reservation {
	start: usize,
	length: usize,
	written: usize,
}

#[derive(Clone)]
pub struct ReserveHandle {
	buffer: Rc<ReserveBuffer>,
	reserve_idx: usize,
}

impl<Writer: Write> ReserveSerializer<Writer> {
	pub fn new(writer: Writer) -> Self {
		ReserveSerializer {
			inner: ReserveSerializerInner {
				/*parts: vec![],
				sizes: vec![],
				size: 0,*/
				buffer: Rc::new(ReserveBuffer {
					buf: RefCell::new(Vec::new()),
					reserves: RefCell::new(Vec::new()),
				}),
			},
			writer,
		}
	}
}

innner_deref!((Writer: std::io::Write) ReserveSerializer<Writer> => inner: ReserveSerializerInner);

impl ReserveSerializerInner {
	pub fn len(&self) -> usize {
		self.buffer.buf.borrow().len()
	}

	#[cfg(any())]
	pub fn add(&mut self, part: &[u8]) {
		self.parts.push(part);
		self.sizes.push(part.len());
		self.size += part.len();
	}

	pub fn add(&mut self, part: &[u8]) {
		self.buffer.buf.borrow_mut().extend(part);
	}

	#[cfg(any())]
	pub fn reserve(&mut self, space: usize) -> &'part mut [u8] {
		let li = Vec::with_capacity(space);
		self.parts.push(li);
		self.sizes.push(space);
		self.size += space;
		li
	}

	pub fn reserve(&mut self, space: usize) -> ReserveHandle {
		let start;
		{
			let mut buf = self.buffer.buf.borrow_mut();
			start = buf.len();
			buf.reserve(space);
			unsafe {
				buf.set_len(start + space);
			}
		}
		let idx;
		{
			let mut reserves = self.buffer.reserves.borrow_mut();
			idx = reserves.len();
			reserves.push(Reservation { start, length: space, written: 0 });
		}
		ReserveHandle {
			buffer: self.buffer.clone(),
			reserve_idx: idx,
		}
	}

	pub fn align(&mut self, alignment: usize) {
		let mut buf = self.buffer.buf.borrow_mut();
		let new_size = (buf.len() + alignment - 1) & alignment.wrapping_neg();
		//self.add(vec![0; new_size - self.size]);
		buf.resize(new_size, 0);
	}
}

impl<Writer: Write> Drop for ReserveSerializer<Writer> {
	#[cfg(any())]
	fn drop(&mut self) {
		for (&li, &si) in std::iter::zip(&self.parts, &self.sizes) {
			assert_eq!(li.len(), si, "Unfulfilled reservation of size {si}, only got {}", li.len());
			self.writer.write_all(li).unwrap();
		}
	}

	fn drop(&mut self) {
		for reserve in &*self.buffer.reserves.borrow() {
			if reserve.written != reserve.length {
				panic!("Unfulfilled reservation of size {} at {}, only got {}", reserve.length, reserve.start, reserve.written);
			}
		}
		self.writer.write_all(&*self.inner.buffer.buf.borrow()).unwrap();
	}
}

impl ReserveHandle {
	pub fn write(&self, bytes: &[u8]) {
		self.inner(|res| {
			if res.length - res.written < bytes.len() {
				panic!("Reservation data overflow");
			}
			let mut buf = self.buffer.buf.borrow_mut();
			let offset = res.start + res.written;
			buf[offset..offset + bytes.len()].copy_from_slice(bytes);
			res.written += bytes.len();
		});
	}

	fn inner(&self, f: impl FnOnce(&mut Reservation)) {
		let mut reserves = self.buffer.reserves.borrow_mut();
		f(&mut reserves[self.reserve_idx]);
	}
}

impl Write for ReserveSerializerInner {
	#[inline]
	fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
		self.add(buf);
		Ok(buf.len())
	}

	#[inline]
	fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
		self.add(buf);
		Ok(())
	}

	#[inline(always)]
	fn flush(&mut self) -> io::Result<()> {
		Ok(())
	}
}
