//! Adaptation of `sm64_decomp/tools/assemble_sound.py`.
//!
//! This script aims to produce files with machine-agnostic data layout.

#![feature(pointer_is_aligned, panic_backtrace_config)]

use std::cell::{OnceCell, Cell};
use std::io::Write;
use std::path::{Path, PathBuf};
use std::collections::{HashMap, HashSet};
use std::fs::{self, File};
use std::rc::Rc;

use serde_json::{self as json, json, Value as Json};

macro_rules! align {
	($val:expr, $al:expr) => {
		($val + ($al - 1)) & !($al - 1)
	};
}

mod aifc;
use aifc::Aifc;

#[inline]
fn insert_once<T>(option: &mut Option<T>, value: T) {
	assert!(option.is_none(), "Cannot insert more than once");
	*option = Some(value);
}

#[inline]
unsafe fn slice_cast_aligned<To, From>(slice: &[From]) -> &[To] {
	use std::mem::{size_of_val, size_of};
	assert_eq!(size_of_val(slice) % size_of::<To>(), 0, "Slice size is not aligned");
	let range = slice.as_ptr_range();
	let range = range.start.cast::<To>() .. range.end.cast::<To>();
	assert!(range.start.is_aligned(), "Slice is not aligned");
	std::slice::from_raw_parts(range.start, range.end.offset_from(range.start) as usize)
}

#[inline]
unsafe fn apply_with<T>(value: &mut T, f: impl FnOnce(T) -> T) {
	struct DropGuard;
	impl Drop for DropGuard {
		fn drop(&mut self) {
			//panic!("Panic inside unsafe function");
			std::process::abort();
		}
	}

	let ptr: *mut T = value;
	let old = std::ptr::read(ptr);
	let guard = DropGuard;
	let new = f(old);
	std::ptr::write(ptr, new);
	std::mem::forget(guard);
}

trait SortByKeyRef<T> {
	fn sort_by_key_ref<K, F>(&mut self, f: F)
	where
		F: FnMut(&T) -> &K,
		K: ?Sized + Ord;
}

impl<T> SortByKeyRef<T> for [T] {
	fn sort_by_key_ref<K, F>(&mut self, mut f: F)
	where
		F: FnMut(&T) -> &K,
		K: ?Sized + Ord,
	{
		//self.sort_by_key(f);
		self.sort_by(|a, b| f(a).cmp(f(b)));
	}
}

enum Endian {
	Big,
	Little,
	Native,
}

enum Bitwidth {
	P32,
	P64,
	Native
}

struct PackConfig {
	endian: Endian,
	bitwidth: Bitwidth,
}

macro_rules! pack_fns {
	($(
		$pack_fn:ident [$ty:ty ; $bytes:expr] ;
	)*) => {$(
		pub fn $pack_fn(&self, val: $ty) -> [u8; $bytes] {
			match self.endian {
				Endian::Big => val.to_be_bytes(),
				Endian::Little => val.to_le_bytes(),
				Endian::Native => val.to_ne_bytes(),
			}
		}
	)*};
}

#[allow(dead_code)]
impl PackConfig {
	pack_fns! {
		pack_u16 [u16; 2];
		pack_i16 [i16; 2];
		pack_u32 [u32; 4];
		pack_i32 [i32; 4];
		pack_u64 [u64; 8];
		pack_i64 [i64; 8];
		pack_f32 [f32; 4];
		pack_f64 [f64; 8];
	}
}

impl PackConfig {
	pub fn ptr_size(&self) -> usize {
		match self.bitwidth {
			Bitwidth::P32 => 4,
			Bitwidth::P64 => 8,
			Bitwidth::Native => std::mem::size_of::<usize>(),
		}
	}

	pub fn pack_ptr<'b>(&self, ptr: u32, buf: &'b mut [u8; 8]) -> &'b mut [u8] {
		match self.bitwidth {
			Bitwidth::P32 => {
				let buf = &mut buf[..4];
				buf.copy_from_slice(&self.pack_u32(ptr));
				buf
			}
			Bitwidth::P64 => {
				buf.copy_from_slice(&self.pack_u64(ptr as u64));
				buf
			}
			Bitwidth::Native => {
				#[cfg(target_pointer_width = "32")] {
					let buf = &mut buf[..4];
					buf.copy_from_slice(&self.pack_u32(ptr));
					buf
				}
				#[cfg(target_pointer_width = "64")] {
					buf.copy_from_slice(&self.pack_u64(ptr as u64));
					buf
				}
			}
		}
	}

	pub fn std_padding(&self) -> &[u8] {
		const ZERO: &[u8] = &[];
		const PAD: &[u8] = &[0u8; 4];
		match self.bitwidth {
			Bitwidth::P32 => ZERO,
			Bitwidth::P64 => PAD,
			Bitwidth::Native => {
				#[cfg(target_pointer_width = "32")] { ZERO }
				#[cfg(target_pointer_width = "64")] { PAD }
			}
		}
	}
}

#[path = ""]
pub(crate) mod ser {
	macro_rules! innner_deref {
		(($($generics:tt)*) $ty:ty => $member:ident: $inner:ty) => {
			impl<$($generics)*> std::ops::Deref for $ty {
				type Target = $inner;
				#[inline(always)]
				fn deref(&self) -> &Self::Target {
					&self.$member
				}
			}
			
			impl<$($generics)*> std::ops::DerefMut for $ty {
				#[inline(always)]
				fn deref_mut(&mut self) -> &mut Self::Target {
					&mut self.$member
				}
			}
		};
	}

	#[path = "ser_reserve.rs"]
	pub mod reserve;

	#[path = "ser_garbage.rs"]
	pub mod garbage;
}

use ser::{reserve::{ReserveSerializer, ReserveSerializerInner}, garbage::{GarbageSerializer, GarbageSerializerInner}};

type Magic = u16;
const TYPE_CTL: Magic = 1;
const TYPE_TBL: Magic = 2;
const TYPE_SEQ: Magic = 3;

struct SequencesOutFiles {
	seq: PathBuf,
	header: PathBuf,
	bank_sets: PathBuf,
	sound_bank_dir: PathBuf,
	sequence_json: PathBuf,
}

type FileBuffer = Vec<u8>;

struct Bank {
	name: Rc<str>,
	sample_bank: Rc<SampleBank>,
	json: Json,
}

struct SampleBank {
	name: Rc<str>,
	first_use: OnceCell<Rc<str>>,
	multi_uses: Cell<bool>,
	index: OnceCell<u8>,
	entries: Vec<Box<Aifc>>,
	name_to_entry: HashMap<String, usize>,
}

impl SampleBank {
	fn new(name: Rc<str>, entries: Vec<Box<Aifc>>) -> Self {
		let mut bank = SampleBank {
			name,
			first_use: OnceCell::new(),
			multi_uses: Cell::new(false),
			index: OnceCell::new(),
			entries,
			name_to_entry: HashMap::new(),
		};
		for (idx, ent) in bank.entries.iter().enumerate() {
			bank.name_to_entry.insert(ent.name.clone(), idx);
		}
		bank
	}
}

/*struct SoundBank {
	date: String,
	sample_bank: String,
	envelopes: Vec<Envelope>,
	instruments: serde_json::Map<String, InstrumentDef>,
	instrument_list: Vec<String>,
}*/

// This isn't necessarily compatible with C's or Rust's rules about comments,
// but it matches the tool.
fn strip_comments(str: &mut String) {
	let mut idx = 0;
	while let Some(pos) = str[idx..].find("/*") {
		match str[pos + 2..].find("*/") {
			Some(end) => {
				str.drain(pos .. end + 2);
				idx = pos;
			}
			None => panic!("Unmatched /* in file"),
		}
	}

	idx = 0;
	while let Some(pos) = str[idx..].find("//") {
		match str[pos + 2..].find('\n') {
			Some(end) => {
				str.drain(pos .. end + 1);
				idx = pos;
			}
			None => {
				str.drain(pos..);
				break;
			}
		}
	}
}

fn print_help(wanted_help: bool) -> ! {
	print!(
"Usage:
{exe} [<flags>] <samples dir> <sound bank dir> <out .ctl file> <out .ctl Shindou header file> <out .tbl file> <out .tbl Shindou header file>
{exe} [<flags>] --sequences <out sequence .bin> <out Shindou header .bin> <out bank sets .bin> <sound bank dir> <sequences.json> [<flags>] <inputs...>

Supported flags:
	--help | -h
	--cpp <preprocessor>
	-D <symbol> | -D<symbol>
	--endian big|little|native
	--bitwidth 32|64|native
	--stack-trace
	--dump-individual-bins
	--print-samples
",
		exe= match std::env::args().next() {
			None => "assemble_sound",
			Some(ref exe) => exe.as_str(),
		}
	);

	std::process::exit(if wanted_help { 0 } else { 1 });
}

fn apply_ifs(mut tree: Json, defines: &HashSet<String>) -> Json {
	loop {
		match tree {
			Json::Object(mut dict)
			if dict.contains_key("ifdef")
			&& dict.contains_key("then")
			&& dict.contains_key("else") => {
				let Json::Array(defs) = dict.remove("ifdef").unwrap() else {
					panic!("Expected an array");
				};
				let then = dict.remove("then").unwrap();
				let r#else = dict.remove("else").unwrap();
				tree = if defs.iter().any(|def| defines.contains(def.as_str().unwrap())) {
					then
				} else {
					r#else
				};
			}
			Json::Object(ref mut dict) => {
				for value in dict.values_mut() {
					unsafe {
						apply_with(value, |val| apply_ifs(val, defines));
					}
				}
				return tree;
			}
			Json::Array(ref mut list) => {
				for value in list {
					unsafe {
						apply_with(value, |val| apply_ifs(val, defines));
					}
				}
				return tree;
			}
			_ => return tree,
		}
	}
}

fn apply_version_diffs(bank: &mut Json, defines: &HashSet<String>) {
	// TODO: Apply EU version date change
	let instruments = unsafe { let p: *mut _ = bank["instruments"].as_object_mut().unwrap(); &mut *p };
	for item in instruments.values_mut() {
		if let Json::Object(item) = item {
			if let Some(defs) = item.get("ifdef") {
				if !defs.as_array().unwrap().iter().any(|def| defines.contains(def.as_str().unwrap())) {
					item.clear();
					item.insert("removed".into(), Json::Bool(true));
				}
			}
		}
	}
	bank["instrument_list"].as_array_mut().unwrap().retain(|item| {
		let Json::String(item) = item else {
			return true;
		};
		if let Json::Object(instr) = &instruments[item] {
			if let Some(Json::Bool(true)) = instr.get("removed") {
				return false;
			}
		}
		true
	});
	instruments.retain(|_, instr| {
		if let Json::Object(instr) = instr {
			if let Some(Json::Bool(true)) = instr.get("removed") {
				return false;
			}
		}
		true
	});
}

fn normalize_sound_json(bank: &mut Json) {
	for (_, inst) in bank["instruments"].as_object_mut().unwrap() {
		if let Json::Array(inst) = inst {
			for drum in inst {
				fixup(drum, "sound");
			}
		} else {
			fixup(inst, "sound_lo");
			fixup(inst, "sound");
			fixup(inst, "sound_hi");
		}
	}

	fn fixup(json: &mut Json, key: &str) {
		if let Json::Object(dict) = json {
			if let Some(entry) = dict.get_mut(key) {
				if let Json::String(str) = entry {
					*entry = json! {{
						"sample": str,
					}}
				}
			}
		}
	}
}

fn mark_sample_bank_uses(bank: &mut Bank) {
	let sample_bank = bank.sample_bank.clone();
	let mut multi = true;
	sample_bank.first_use.get_or_init(|| {
		multi = false;
		bank.name.clone()
	});
	if multi {
		sample_bank.multi_uses.set(true);
	}

	for inst in bank.json["instruments"].as_object().unwrap().values() {
		if let Json::Array(drums) = inst {
			for drum in drums {
				mark_used(&sample_bank, drum["sound"]["sample"].as_str().unwrap());
			}
		} else {
			if let Some(sound) = inst.get("sound_lo") {
				mark_used(&sample_bank, sound["sample"].as_str().unwrap());
			}
			mark_used(&sample_bank, inst["sound"]["sample"].as_str().unwrap());
			if let Some(sound) = inst.get("sound_hi") {
				mark_used(&sample_bank, sound["sample"].as_str().unwrap());
			}
		}
	}

	fn mark_used(sample_bank: &SampleBank, name: &str) {
		sample_bank.entries[sample_bank.name_to_entry[name]].used.set(true);
	}
}

type Meta = [u8; 4];
const NO_META: Meta = [0; 4];

fn serialize_ctl(bank: &Bank, pack: &PackConfig, base_ser: &mut GarbageSerializerInner, is_shindou: bool) -> Meta {
	let json = &bank.json;

	let mut drums = None;
	let mut instruments = vec![];
	for inst in json["instruments"].as_object().unwrap().values() {
		if let Some(list) = inst.as_array() {
			if drums.is_some() {
				panic!("Duplicate drum entries");
			}
			drums = Some(list);
		} else {
			instruments.push(inst);
		}
	}

	if !is_shindou {
		fn to_bcd(mut num: u32) -> u32 {
			let mut shift = 0;
			let mut ret = 0;
			while num != 0 {
				ret |= (num % 10) << shift;
				shift += 4;
				num /= 10;
			}
			ret
		}

		let (y, m, d): (u16, u8, u8) = json.get("date").map(|date| {
			let str = date.as_str().unwrap();
			let (y, str) = str.split_once('-').unwrap();
			let (m, d) = str.split_once('-').unwrap();
			(y.parse().unwrap(), m.parse().unwrap(), d.parse().unwrap())
		}).unwrap_or((0, 0, 0));

		let date = y as u32 * 10_000 + m as u32 * 100 + d as u32;
		base_ser.add(&pack.pack_u32(json["instrument_list"].as_array().unwrap().len().try_into().unwrap()));
		base_ser.add(&pack.pack_u32(drums.map_or(0, |drums| drums.len().try_into().unwrap())));
		base_ser.add(&pack.pack_u32(bank.sample_bank.multi_uses.get() as u32));
		base_ser.add(&pack.pack_u32(to_bcd(date)));
	}

	let mut ser = ReserveSerializer::new(base_ser);
	let mut pack_buf = [0u8; 8];
	let drum_pos_buf = if drums.is_some() {
		Some(ser.reserve(pack.ptr_size()))
	} else {
		ser.add(pack.pack_ptr(0, &mut pack_buf));
		None
	};

	let inst_pos_buf = ser.reserve(pack.ptr_size() * json["instrument_list"].as_array().unwrap().len());
	ser.align(16);

	let mut used_samples = vec![];
	for inst in json["instruments"].as_object().unwrap().values() {
		if let Some(list) = inst.as_array() {
			for drum in list {
				used_samples.push(drum["sound"]["sample"].as_str().unwrap());
			}
		} else {
			if let Some(sound_lo) = inst.get("sound_lo") {
				used_samples.push(sound_lo["sample"].as_str().unwrap());
			}
			used_samples.push(inst["sound"]["sample"].as_str().unwrap());
			if let Some(sound_hi) = inst.get("sound_hi") {
				used_samples.push(sound_hi["sample"].as_str().unwrap());
			}
		}
	}

	let mut sample_name_to_addr = HashMap::new();
	for &name in &used_samples {
		match sample_name_to_addr.entry(name) {
			std::collections::hash_map::Entry::Occupied(_) => continue,
			std::collections::hash_map::Entry::Vacant(vac) => { vac.insert(u32::try_from(ser.len()).unwrap()); }
		}

		let aifc_idx = bank.sample_bank.name_to_entry[name];
		let aifc = &*bank.sample_bank.entries[aifc_idx];
		let sample_len = u32::try_from(aifc.data.len()).unwrap();

		// Sample
		ser.add(&pack.pack_u32(if is_shindou { align!(sample_len, 2) } else { 0 }));
		ser.add(pack.std_padding());
		ser.add(pack.pack_ptr(*aifc.offset.get().unwrap(), &mut pack_buf));
		let loop_addr_buf = ser.reserve(pack.ptr_size());
		let book_addr_buf = ser.reserve(pack.ptr_size());
		if !is_shindou {
			ser.add(&pack.pack_u32(align!(sample_len, 2)));
		}
		ser.align(16);

		// Book
		book_addr_buf.write(pack.pack_ptr(ser.len().try_into().unwrap(), &mut pack_buf));
		ser.add(&pack.pack_u32(aifc.book.order.into()));
		ser.add(&pack.pack_u32(aifc.book.npredictors.into()));
		for &x in &aifc.book.table {
			ser.add(&pack.pack_i16(x));
		}
		ser.align(16);

		// Loop
		loop_addr_buf.write(pack.pack_ptr(ser.len().try_into().unwrap(), &mut pack_buf));
		if let Some(ref lp) = aifc.r#loop {
			ser.add(&pack.pack_u32(lp.start));
			ser.add(&pack.pack_u32(lp.end));
			ser.add(&pack.pack_u32(lp.count));
			ser.add(&[0; 4]);
			assert_ne!(lp.count, 0);
			for &x in &lp.state {
				ser.add(&pack.pack_i16(x));
			}
		} else {
			assert!(sample_len % 9 <= 1);
			let end = sample_len / 9 * 16 + (sample_len & 1) + (sample_len % 9);
			ser.add(&[0; 4]);
			ser.add(&pack.pack_u32(end));
			ser.add(&[0; 8]);
		}
		ser.align(16);
	}

	let env_name_to_addr: HashMap<&str, u32> = json["envelopes"].as_object().unwrap()
		.iter().map(|(name, env)| {
			let addr = u32::try_from(ser.len()).unwrap();
			for entry in env.as_array().unwrap() {
				let frag: [u16; 2] = match entry.as_str() {
					Some("stop") => [0, 0],
					Some("hang") => [!0, 0],
					Some("restart") => [!2, 0],
					_ => if let Some(array) = entry.as_array() {
						let [ref delay, ref target] = array[..] else {
							panic!("Expected two segments");
						};
						let target = u16::try_from(target.as_u64().unwrap()).unwrap();
						if let Some("goto") = delay.as_str() {
							[!1, target]
						} else {
							let delay = u16::try_from(delay.as_u64().unwrap()).unwrap();
							[delay, target]
						}
					} else {
						panic!("Expected an array");
					}
				};
				// Envelopes are always written as big endian, to match sequence files
				// which are byte blobs and can embed envelopes.
				ser.add(&unsafe {std::mem::transmute::<[[u8; 2]; 2], [u8; 4]>([
					frag[0].to_be_bytes(),
					frag[1].to_be_bytes(),
				])});
			}
			ser.align(16);
			(name.as_str(), addr)
		})
		.collect();

	let ser_sound = |sound: &Json, ser: &mut ReserveSerializerInner| {
		let sample_name = sound["sample"].as_str();
		let sample_addr = sample_name
			.map_or(0, |samp| sample_name_to_addr[samp]);
		let tuning = if let Some(tuning) = sound.get("tuning") {
			tuning.as_f64().unwrap()
		} else {
			let aifc_idx = bank.sample_bank.name_to_entry[sample_name.unwrap()];
			let aifc = &bank.sample_bank.entries[aifc_idx];
			aifc.sample_rate / 32_000.0
		};
		let mut pack_buf = [0u8; 8];
		ser.add(pack.pack_ptr(sample_addr, &mut pack_buf));
		ser.add(&pack.pack_f32(tuning as f32));
		ser.add(pack.std_padding());
	};

	let no_sound = json!{{
		"sample": null,
		"tuning": 0.0,
	}};

	let inst_name_to_pos: HashMap<&str, u32> = json["instruments"].as_object().unwrap()
		.iter().filter_map(|(name, inst)| {
			if inst.is_array() {
				return None;
			}

			let addr = u32::try_from(ser.len()).unwrap();
			let env_addr = env_name_to_addr[inst["envelope"].as_str().unwrap()];
			ser.add(&[
				0, // loaded
				inst.get("normal_range_lo").map_or(0, |r| u8::try_from(r.as_u64().unwrap()).unwrap()),
				inst.get("normal_range_hi").map_or(0x7f, |r| u8::try_from(r.as_u64().unwrap()).unwrap()),
				inst["release_rate"].as_u64().unwrap().try_into().unwrap(),
			]);
			ser.add(pack.std_padding());
			ser.add(pack.pack_ptr(env_addr, &mut pack_buf));
			ser_sound(inst.get("sound_lo").unwrap_or(&no_sound), &mut ser);
			ser_sound(&inst["sound"], &mut ser);
			ser_sound(inst.get("sound_hi").unwrap_or(&no_sound), &mut ser);
			Some((name.as_str(), addr))
		})
		.collect();
	ser.align(16);

	for name in json["instrument_list"].as_array().unwrap() {
		inst_pos_buf.write(pack.pack_ptr(
			name.as_str().map_or(0, |name| inst_name_to_pos[name]),
			&mut pack_buf));
	}

	if let Some(drums) = drums {
		let drum_addrs: Vec<u32> = drums.iter()
			.map(|drum| {
				let addr = u32::try_from(ser.len()).unwrap();
				ser.add(&[
					drum["release_rate"].as_u64().unwrap().try_into().unwrap(),
					drum["pan"].as_u64().unwrap().try_into().unwrap(),
					0, // loaded
					0,
				]);
				ser.add(pack.std_padding());
				ser_sound(&drum["sound"], &mut ser);
				let env_addr = env_name_to_addr[drum["envelope"].as_str().unwrap()];
				ser.add(pack.pack_ptr(env_addr, &mut pack_buf));
				addr
			})
			.collect();
		ser.align(16);

		drum_pos_buf.unwrap().write(pack.pack_ptr(ser.len().try_into().unwrap(), &mut pack_buf));
		for &addr in &drum_addrs {
			ser.add(pack.pack_ptr(addr, &mut pack_buf));
		}
		ser.align(16);
	}

	unsafe { std::mem::transmute::<[[u8; 2]; 2], [u8; 4]>([
		pack.pack_u16(((*bank.sample_bank.index.get().unwrap() as u16) << 8) | 0xFF),
		pack.pack_u16(((u8::try_from(json["instrument_list"].as_array().unwrap().len()).unwrap() as u16) << 8) | drums.map_or(0, |drums| u8::try_from(drums.len()).unwrap() as u16)),
	]) }
}

fn serialize_tbl(sample_bank: &Rc<SampleBank>, _pack: &PackConfig, ser: &mut GarbageSerializerInner, is_shindou: bool) -> Meta {
	let sample_bank = &**sample_bank;
	ser.reset_garbage_pos();
	let base_addr = ser.len();
	for aifc in &sample_bank.entries {
		if !aifc.used.get() { continue; }
		ser.align(16);
		aifc.offset.set(u32::try_from(ser.len() - base_addr).unwrap()).unwrap();
		ser.add(&aifc.data);
	}
	if is_shindou && !matches!(*sample_bank.index.get().unwrap(), 4 | 10) {
		ser.align(16);
	} else {
		ser.align(2);
		ser.align_garbage(16);
	}
	NO_META
}

fn serialize_seqfile<T, F, I>(
	pack: &PackConfig,
	out_filename: &Path,
	out_header_filename: &Path,
	entries: &[T],
	serialize_entry: F,
	entry_list: I,
	magic: Magic,
	is_shindou: bool,
	extra_padding: bool,
) where
	F: Fn(&T, &PackConfig, &mut GarbageSerializerInner, bool) -> Meta,
	I: Iterator<Item = usize> + ExactSizeIterator,
{
	let mut data: Vec<u8> = vec![];
	let mut data_ser = GarbageSerializer::new(&mut data);
	let mut entry_offsets = vec![];
	let mut entry_meta = vec![];
	let mut entry_lens = vec![];
	for entry in entries {
		let offset = u32::try_from(data_ser.len()).unwrap();
		entry_offsets.push(offset);
		entry_meta.push(serialize_entry(entry, pack, &mut data_ser, is_shindou));
		entry_lens.push(u32::try_from(data_ser.len()).unwrap() - offset);
	}
	drop(data_ser);

	let mut pack_buf = [0u8; 8];
	if is_shindou {
		let header = File::create(out_header_filename).unwrap();
		let mut ser = ReserveSerializer::new(header);
		ser.add(&pack.pack_u16(entries.len() as u16));
		ser.align(16);
		let medium = 2u8; // Cartrige
		let sh_magic = if magic == TYPE_TBL { 4u8 } else { 3u8 };

		// Ignore entry_list and loop over all entries instead. This makes a
		// difference for sample banks, where US/JP/EU doesn't use a normal
		// header for sample banks but instead has a mapping from sound bank to
		// sample bank offset/length. Shindou uses a normal header and makes the
		// mapping part of the sound bank header instead (part of entry_meta).
		for ((&offset, &lens), meta) in entry_offsets.iter().zip(&entry_lens).zip(&entry_meta) {
			ser.add(pack.pack_ptr(offset, &mut pack_buf));
			ser.add(&pack.pack_u32(lens));
			ser.add(&[medium, sh_magic]);
			ser.add(meta);
			ser.align(pack.ptr_size());
		}
		drop(ser);

		let mut out_file = File::create(out_filename).unwrap();
		out_file.write_all(&data).unwrap();
	} else {
		let file = File::create(out_filename).unwrap();
		let mut ser = ReserveSerializer::new(file);
		ser.add(&pack.pack_u16(magic));
		ser.add(&pack.pack_u16(entry_list.len().try_into().unwrap()));
		ser.add(pack.std_padding());
		let table = ser.reserve(entry_list.len() * 2 * pack.ptr_size());
		ser.align(16);
		let data_start = u32::try_from(ser.len()).unwrap();

		ser.add(&data);
		if extra_padding {
			ser.add(&[0]);
		}
		ser.align(64);

		for index in entry_list {
			table.write(pack.pack_ptr(entry_offsets[index] + data_start, &mut pack_buf));
			table.write(&pack.pack_u32(entry_lens[index]));
			table.write(pack.std_padding());
		}
	}
}

fn validate_and_normalize_sequence_json(json: &mut json::Map<String, Json>, bank_names: &[String], defines: &HashSet<String>) {
	//let json = json.as_object_mut().expect("Must have a top-level object");
	json.remove("comment");
	for (_, seq) in json {
		//if key == "comment" { continue; }

		match seq {
			Json::Object(obj) => {
				//validate_json_format!(seq, {"ifdef": list, "banks": list});
				let ifdef = obj["ifdef"].as_array().unwrap();
				assert!(ifdef.iter().all(|x| x.is_string()));
				if ifdef.iter().any(|d| defines.contains(d.as_str().unwrap())) {
					*seq = obj.remove("banks").unwrap();
				} else {
					*seq = Json::Null;
				}
			}
			Json::Array(list) => {
				for x in list {
					let str = x.as_str().expect("bank list must be an array of strings");
					assert!(bank_names.iter().find(|n| **n == *str).is_some(), "reference to non-existing sound bank {str}");
				}
			}
			Json::Null => {}
			_ => panic!("bad JSON type, expected null, array, or object"),
		}
	}
}

fn write_sequences(
	pack: &PackConfig,
	mut inputs: Vec<PathBuf>,
	files: SequencesOutFiles,
	defines: &HashSet<String>,
	is_shindou: bool,
) {
	let mut bank_names = files.sound_bank_dir.read_dir().unwrap()
		.filter_map(|f| {
			let f = f.unwrap();
			let Ok(mut fname) = f.file_name().into_string() else { return None };
			if !fname.ends_with(".json") {
				return None;
			}
			fname.truncate(fname.len() - 5);
			Some(fname)
		})
		.collect::<Vec<_>>();
	bank_names.sort();

	let mut seq_file = fs::read_to_string(&files.sequence_json).unwrap();
	strip_comments(&mut seq_file);
	let mut seq_json: json::Map<String, Json> = json::from_str(&seq_file).unwrap();
	validate_and_normalize_sequence_json(&mut seq_json, &bank_names, defines);
	inputs.sort_by_key_ref(|n| n.file_name().unwrap_or(n.as_os_str()));

	let mut name_to_fname = HashMap::new();
	for input in inputs {
		let name = input.file_stem().expect("File without extension").to_str().unwrap().to_owned();
		if !seq_json.contains_key(&name) {
			panic!("Sequence file {input:?} is not mentioned in sequences.json. Either assign it a list of sound banks, or set it to null to explicitly leave it out from the build.");
		}
		match name_to_fname.entry(name) {
			std::collections::hash_map::Entry::Occupied(ent) => panic!("Files {input:?} and {:?} conflict. Remove one of them", ent.get()),
			std::collections::hash_map::Entry::Vacant(vac) => { vac.insert(input); }
		}
	}

	for (key, seq) in &seq_json {
		if !seq.is_null() && !name_to_fname.contains_key(key) {
			panic!("sequences.json assigns sound banks to {key:?}, but there is no such sequence file. Either remove the entry (or set it to null), or create \"sound/sequences/{key}.m64\".");
		}
	}

	let mut ind_to_name = Vec::new();
	for key in seq_json.keys() {
		let key = key.as_str();
		let ind = u32::from_str_radix(key.split_once('_').map_or(key, |(ind, _)| ind), 16).unwrap();
		if ind_to_name.len() <= ind as usize {
			ind_to_name.resize(ind as usize + 1, None);
		} else if let Some(name) = ind_to_name[ind as usize] {
			panic!("Sequence files {key} and {name} have the same index. Renumber or delete one of them.");
		}
		ind_to_name[ind as usize] = Some(key);
	}

	loop {
		if let Some(last) = ind_to_name.last() {
			if last.is_some() {
				break;
			}
		} else {
			break;
		}
		ind_to_name.pop();
	}

	let serialize_file = |&name: &Option<&str>, _: &PackConfig, ser: &mut GarbageSerializerInner, is_shindou: bool| {
		let Some(name) = name else {
			return NO_META;
		};
		if !seq_json.contains_key(name) {
			return NO_META;
		}
		ser.reset_garbage_pos();
		let file = fs::read(&name_to_fname[name]).unwrap();
		ser.add(&file);
		if is_shindou && name.starts_with("17") {
			ser.align(16);
		} else {
			ser.align_garbage(16);
		}
		NO_META
	};

	serialize_seqfile(
		pack,
		&files.seq,
		&files.header,
		&ind_to_name,
		serialize_file,
		0..ind_to_name.len(),
		TYPE_SEQ,
		is_shindou,
		false,
	);

	let bank_sets = File::create(&files.bank_sets).unwrap();
	let mut ser = ReserveSerializer::new(bank_sets);
	let table = ser.reserve(ind_to_name.len() * 2);
	for &name in &ind_to_name {
		let bank_set = if let Some(name) = name {
			seq_json[name].as_array().unwrap().as_slice()
		} else {
			&[]
		};
		table.write(&pack.pack_u16(ser.len().try_into().unwrap()));
		ser.add(&[u8::try_from(bank_set.len()).unwrap()]);
		for bank in bank_set.iter().rev() {
			ser.add(&[bank_names.iter().position(|b| b == bank).unwrap() as u8]);
		}
	}
	ser.align(16);
}

fn main() {
	let mut need_help = false;
	let mut print_samples = false;
	let mut stack_traces = false;
	let mut dump_individual_bins = false;
	let mut cpp_command = None;
	let mut seq_out_files = None;
	let mut defines = vec![];
	let mut inputs: Vec<PathBuf> = vec![];
	let mut endian = Endian::Big;
	let mut bitwidth = Bitwidth::P32;

	let mut args = std::env::args().skip(1);
	macro_rules! arg {
		() => {
			args.next().expect("Expected argument")
		};
		($msg:expr) => {
			args.next().expect($msg)
		};
	}

	while let Some(arg) = args.next() {
		match &arg[..] {
			"--help" | "-h" => need_help = true,
			"--cpp" => cpp_command = Some(arg!()),
			"-D" => defines.push(arg!()),
			def if def.starts_with("-D") => defines.push(def[2..].to_owned()),
			"--endian" => endian = match &arg!()[..] {
				"big" => Endian::Big,
				"little" => Endian::Little,
				"native" => Endian::Native,
				_ => panic!("Endianness can be big, little, or native"),
			},
			"--bitwidth" => bitwidth = match &arg!()[..] {
				"32" => Bitwidth::P32,
				"64" => Bitwidth::P64,
				"native" => Bitwidth::Native,
				_ => panic!("Bitwidth can be 32, 64, or native"),
			},
			"--stack-trace" => stack_traces = true,
			"--dump-individual-bins" =>  dump_individual_bins = true,
			"--print-samples" => print_samples = true,
			"--sequences" => {
				seq_out_files = Some(SequencesOutFiles {
					seq: arg!().into(),
					header: arg!().into(),
					bank_sets: arg!().into(),
					sound_bank_dir: arg!().into(),
					sequence_json: arg!().into(),
				});
			}
			opt if opt.starts_with('-') => panic!("Unrecognized option: {opt}"),
			_ => inputs.push(arg.into()),
		}
	}

	let pack = PackConfig { endian, bitwidth, };

	std::panic::set_backtrace_style(match stack_traces {
		true => std::panic::BacktraceStyle::Short,
		false => std::panic::BacktraceStyle::Off,
	});

	if need_help {
		print_help(true);
	}

	let defines_set = defines.iter()
		.map(|directive| {
			if let Some(idx) = directive.find('=') {
				directive[..idx].to_owned()
			} else {
				directive.to_owned()
			}
		}).collect::<HashSet<_>>();

	for define in &mut defines {
		define.insert_str(0, "-D");
	}

	let is_shindou = defines_set.contains("VERSION_SH");

	if let Some(seq_out_files) = seq_out_files {
		write_sequences(&pack, inputs, seq_out_files, &defines_set, is_shindou);
		return;
	}

	let Ok([
		sample_bank_dir,
		sound_bank_dir,
		ctl_data,
		ctl_data_header,
		tbl_data,
		tbl_data_header,
	]) = <[_; 6]>::try_from(inputs) else {
		print_help(false);
	};

	let mut banks = vec![];
	let mut sample_banks = vec![];
	let mut name_to_sample_bank = HashMap::new();

	let mut sample_bank_names = sample_bank_dir.read_dir().unwrap()
		.filter_map(|ent| {
			let ent = ent.unwrap();
			if !ent.file_type().unwrap().is_dir() {
				return None;
			}
			let name = ent.file_name().into_string().expect("Failed to convert bank name to string");
			let path = ent.path();
			Some((name, path))
		})
		.collect::<Vec<_>>();
	sample_bank_names.sort_by_key_ref(|(n, _)| n);
	for (name, dir) in sample_bank_names {
		let mut entries = vec![];
		for f in dir.read_dir().unwrap() {
			let f = f.unwrap();
			let Ok(mut fname) = f.file_name().into_string() else { continue };
			if !fname.ends_with(".aifc") {
				continue;
			}
			fname.truncate(fname.len() - 5);
			let path = f.path();
			let f = fs::read(&path).unwrap();
			entries.push(Aifc::parse(f, fname, path));
		}
		if !entries.is_empty() {
			let name = Rc::<str>::from(name);
			entries.sort_by_key_ref(|aifc| &aifc.name);
			let sample_bank = Rc::new(SampleBank::new(name.clone(), entries));
			sample_banks.push(sample_bank.clone());
			name_to_sample_bank.insert(name, sample_bank);
		}
	}

	let mut bank_names = sound_bank_dir.read_dir().unwrap()
		.filter_map(|ent| {
			let ent = ent.unwrap();
			let mut name = ent.file_name().into_string().expect("Failed to convert bank name to string");
			if !name.ends_with(".json") {
				return None;
			}
			name.truncate(name.len() - 5);
			let path = ent.path();
			Some((name, path))
		})
		.collect::<Vec<_>>();
	bank_names.sort_by_key_ref(|(n, _)| n);
	for (name, path) in bank_names {
		let data = if let Some(ref cpp_command) = cpp_command {
			let cpp = std::process::Command::new(cpp_command)
				.arg(&path)
				.args(&defines)
				.stdout(std::process::Stdio::piped())
				.spawn().expect("Failed to execute preprocessor");
			let status = cpp.wait_with_output().unwrap();
			assert!(status.status.success(), "Preprocessor failed");
			String::from_utf8(status.stdout).unwrap()
		} else {
			let mut buf = fs::read_to_string(&path).unwrap();
			strip_comments(&mut buf);
			buf
		};
		let bank_json = json::from_str(&data).unwrap();
		let mut bank_json = apply_ifs(bank_json, &defines_set);
		// validate_bank_toplevel
		apply_version_diffs(&mut bank_json, &defines_set);
		normalize_sound_json(&mut bank_json);

		let sample_bank_name = bank_json["sample_bank"].as_str().unwrap();
		let sample_bank = name_to_sample_bank[sample_bank_name].clone();

		// validate_bank

		let mut bank = Bank {
			name: name.into(),
			sample_bank,
			json: bank_json,
		};
		mark_sample_bank_uses(&mut bank);
		banks.push(bank);
	}

	sample_banks.retain(|bank| bank.first_use.get().is_some());
	sample_banks.sort_by_key_ref(|bank| &**bank.first_use.get().unwrap());
	assert!(sample_banks.len() <= u8::MAX as usize, "Too many sample banks");
	for (sample_bank, idx) in sample_banks.iter().zip(0u8..) {
		sample_bank.index.set(idx).unwrap();
	}

	serialize_seqfile(
		&pack,
		&tbl_data,
		&tbl_data_header,
		&sample_banks,
		serialize_tbl,
		banks.iter().map(|bank| *bank.sample_bank.index.get().unwrap() as usize),
		TYPE_TBL,
		is_shindou,
		true,
	);

	if dump_individual_bins {
		// Debug logic, may simplify diffing
		let _ = fs::create_dir("ctl");
		for bank in &banks {
			let file = File::create(Path::new("ctl").join(format!("{}.seq64", bank.name))).unwrap();
			let mut ser = GarbageSerializer::new(file);
			serialize_ctl(bank, &pack, &mut ser, is_shindou);
		}
		println!("Wrote to ctl/");
	}

	serialize_seqfile(
		&pack,
		&ctl_data,
		&ctl_data_header,
		&banks,
		serialize_ctl,
		0..banks.len(),
		TYPE_CTL,
		is_shindou,
		true,
	);

	if print_samples {
		for sample_bank in &sample_banks {
			for entry in &sample_bank.entries {
				if entry.used.get() {
					println!("{:?}", entry.path);
				}
			}
		}
	}
}
