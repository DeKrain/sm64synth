#!/bin/sh

########################################################
# First, build the necessary assets with
#   make $build/sound/sound_data.ctl $build/sound/sequences.bin
# Invoke this script from this project's root directory.
#
# 1st argument must be target platform's endianness,
# either `le` or `be` for little/big endian.
#
# 2nd argument must be a path to PC Port's platform
# build directory. From the repo root, it will be something
# like `build/ver_pc`. PC version is required for correct
# pointer ABI.
########################################################

endian="$1"
build="$2"

cp "$build/sound/sound_data.ctl" "data/sound_data/sound_data.ctl.$endian.bin"
cp "$build/sound/sound_data.tbl" "data/sound_data/sound_data.tbl.$endian.bin"
cp "$build/sound/sequences.bin" "data/sound_data/sequences.$endian.bin"
cp "$build/sound/bank_sets" "data/sound_data/bank_sets.$endian.bin"
