//! Generates a binary file containing the table's contents.
//! For output template `path/to/file`, there will be two files generated:
//! - `path/to/file.le.bin` containing bytes in Little Endian order
//! - `path/to/file.be.bin` containing bytes in Big Endian order
//! Unless the format is only one byte (`u8`/`i8`), in which case only one file is generated:
//! - `path/to/file.bin`
//!
//! Supported formats are:
//! - `u8`/`i8`, `u16`/`i16`, `u32`/`i32`, `u64`/`i64`, `f32`, `f64`

use std::{
	io::{self, BufWriter, BufRead, Write},
	str::FromStr,
	fs,
	env,
	process::exit,
};

#[allow(non_camel_case_types)]
#[derive(Clone, Copy, PartialEq, Eq)]
enum Element {
	i8,
	i16,
	i32,
	i64,
	f32,
	f64,
}

enum Files {
	SingleByte(BufWriter<fs::File>),
	LeBe { format: Element, le: BufWriter<fs::File>, be: BufWriter<fs::File> },
}

fn main() -> io::Result<()> {
	let mut args: Vec<_> = env::args_os().collect();
	let [_, ref format, ref mut output] = args[..] else  {
		eprintln!("Usage: {gen_table} <format> <output-file>",
			gen_table = args.first().and_then(|s| s.to_str()).unwrap_or("gen_table"));
		exit(1);
	};
	let mut output = std::mem::take(output);
	let format = match format.to_str() {
		Some("u8" | "i8") => Element::i8,
		Some("u16" | "i16") => Element::i16,
		Some("u32" | "i32") => Element::i32,
		Some("u64" | "i64") => Element::i64,
		Some("f32") => Element::f32,
		Some("f64") => Element::f64,
		_ => {
			eprintln!("Invalid format {format:?}");
			exit(2);
		}
	};

	let mut files = if format == Element::i8 {
		Files::SingleByte(BufWriter::new(fs::File::create({ output.push(".bin"); output })?))
	} else {
		Files::LeBe {
			le: BufWriter::new(fs::File::create({ let mut fname = output.clone(); fname.push(".le.bin"); fname })?),
			be: BufWriter::new(fs::File::create({ output.push(".be.bin"); output })?),
			format,
		}
	};
	let mut stdin = io::stdin().lock();
	let mut buf = Vec::new();
	loop {
		if stdin.read_until(b',', &mut buf)? == 0 {
			break;
		}
		if buf.last() == Some(&b',') {
			buf.pop();
		}
		let Ok(s) = std::str::from_utf8(&buf) else {
			eprintln!("Invalid UTF-8 (really ASCII) on input");
			exit(3);
		};
		let mut s = s.trim();
		if s.is_empty() {
			break;
		}
		match format {
			Element::i8 | Element::i16 | Element::i32 | Element::i64 => {
				let is_neg = if s.as_bytes().first() == Some(&b'-') {
					s = &s[1..];
					true
				} else if s.as_bytes().first() == Some(&b'+') {
					s = &s[1..];
					false
				} else {
					false
				};
				let base: u32 = match s.get(..2) {
					Some("0x") => {
						s = &s[2..];
						16
					}
					Some("0b") => {
						s = &s[2..];
						2
					}
					Some("0o") => {
						s = &s[2..];
						8
					}
					_ => 10,
				};
				let Ok(val) = u64::from_str_radix(s, base) else {
					eprintln!("Invalid number literal");
					exit(3);
				};
				let is_overflow = match format {
					Element::i8 => val & !0xFF != 0,
					Element::i16 => val & !0xFFFF != 0,
					Element::i32 => val & !0xFFFF_FFFF != 0,
					// Element::i64 => val & !0xFFFF_FFFF_FFFF_FFFF != 0,
					_ => false,
				};
				if is_overflow {
					eprintln!("Warning: truncating value");
				}
				let val = if is_neg { -(val as i64) } else { val as i64 };
				match files {
					Files::SingleByte(ref mut file) => {
						file.write(&[val as u8])?;
					}
					Files::LeBe { format: Element::i16, ref mut le, ref mut be } => {
						le.write_all(&(val as i16).to_le_bytes())?;
						be.write_all(&(val as i16).to_be_bytes())?;
					}
					Files::LeBe { format: Element::i32, ref mut le, ref mut be } => {
						le.write_all(&(val as i32).to_le_bytes())?;
						be.write_all(&(val as i32).to_be_bytes())?;
					}
					Files::LeBe { format: Element::i64, ref mut le, ref mut be } => {
						le.write_all(&(val as i64).to_le_bytes())?;
						be.write_all(&(val as i64).to_be_bytes())?;
					}
					_ => unreachable!("Unexpected format"),
				}
			}
			Element::f32 => {
				let Ok(val) = f32::from_str(s) else {
					eprintln!("Invalid number literal");
					exit(3);
				};
				let Files::LeBe { format: Element::f32, ref mut le, ref mut be } = files else {
					unreachable!("Unexpected format");
				};
				le.write_all(&val.to_le_bytes())?;
				be.write_all(&val.to_be_bytes())?;
			}
			Element::f64 => {
				let Ok(val) = f64::from_str(s) else {
					eprintln!("Invalid number literal");
					exit(3);
				};
				let Files::LeBe { format: Element::f64, ref mut le, ref mut be } = files else {
					unreachable!("Unexpected format");
				};
				le.write_all(&val.to_le_bytes())?;
				be.write_all(&val.to_be_bytes())?;
			}
		}
		buf.clear();
	}
	Ok(())
}
