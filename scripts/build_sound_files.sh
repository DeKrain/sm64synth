#!/bin/bash

set -e
shopt -s nullglob

VERSION=us
C_DEFINES=(-D VERSION_US)
SOUND_SEQUENCE_DIRS=(sound/sequences sound/sequences/$VERSION)
SOUND_SEQUENCE_FILES=()

echo 'Sequence dirs:' "${SOUND_SEQUENCE_DIRS[@]}"

for dir in "${SOUND_SEQUENCE_DIRS[@]}"; do
	SOUND_SEQUENCE_FILES+=("$dir"/*.m64)
	for file in "$dir"/*.s; do
		SOUND_SEQUENCE_FILES+=("$BUILD_DIR/${file/.s/.m64}")
	done
done

echo 'Sequence files:' "${SOUND_SEQUENCE_FILES[@]}"

scripts="$(dirname "$0")"

# Instruments & samples
echo 'Building ctl & tbl files'

"$scripts"/assemble_sound.exe \
	"$BUILD_DIR/sound/samples/" \
	sound/sound_banks/ \
	"$SOUND_BIN_DIR/sound_data.ctl.$end.bin" \
	"$SOUND_BIN_DIR/ctl_header.$end.bin" \
	"$SOUND_BIN_DIR/sound_data.tbl.$end.bin" \
	"$SOUND_BIN_DIR/tbl_header.$end.bin" \
	"${C_DEFINES[@]}" \
	--endian $ENDIAN --bitwidth $BITWIDTH \
	"$@"

# Sequences & bank sets
echo 'Building seq & bank_sets files'

"$scripts"/assemble_sound.exe --sequences \
	"$SOUND_BIN_DIR/sequences.$end.bin" \
	"$SOUND_BIN_DIR/sequences_header.$end.bin" \
	"$SOUND_BIN_DIR/bank_sets.$end.bin" \
	sound/sound_banks/ \
	sound/sequences.json \
	"${SOUND_SEQUENCE_FILES[@]}" \
	"${C_DEFINES[@]}" \
	--endian $ENDIAN --bitwidth $BITWIDTH \
	"$@"
