## _Super Mario 64_ audio engine rewrite

This is a port/rewrite of classic **Super Mario 64** for _Nintendo 64_'s audio engine, written in Rust.
This version aims to stay closely to the original programming, with some readability enhancements, to
provide as a reference implementation for future work. It is currently based around the European (PAL)
version of the game.

All of this is possible due to @n64decomp team's effort for reconstructing the original game source.
See also [the license](LICENSE).

Currently, extracted sound assets are not privided; see [`data/sound_data`](data/sound_data/README.md).

## Preperation

Before building, sound assets from game ROM must be extracted.

The procedure is as follows:
First, grab a compatible game ROM (EU or JP/US are recommended, SH is not yet fully supported), then put it in **PC Port** repository.
Then, build & copy sound assets from the PC version of the game; see the [`scripts/copy_sound_data.sh` script](scripts/copy_sound_data.sh) for more details. You should also make sure the M64 decoding version is set correctly. It is controlled by `M64_GAME_VERSION` constant in [`m64_portable.rs`](src/sequencer/m64_portable.rs).
Finally, you can build the music player with `cargo build -p sm64-synth-player --bin play`, or the WAV renderer with `cargo build -p sm64-synth-player --bin render`. Omit the `--bin play/render` arguments to build both.

**Currently, only __nightly__ toolchain is supported**. Soon, I may fix the stable build.
Until then, the toolchain used with _rustup_ is set in the `rust-toolchain.toml` file, and you should pass in `-F nightly` option to `cargo build` commands.
Rust-analyser on VS Code is configured to use this feature in the repo.

For performance reasons, by default the development version of the synth core crate is built with `opt-level=2` with checks & debug info enabled. To change it, edit [`Cargo.toml`](Cargo.toml) under `profile.dev` section.
